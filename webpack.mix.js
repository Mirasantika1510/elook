let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
		'resources/assets/admin/global_assets/css/icons/icomoon/styles.css'
   	], 	'public/css/styles.css')
	.copyDirectory('resources/assets/admin/global_assets/css/icons/icomoon/fonts', 'public/css/icons/fonts')
	.copyDirectory('resources/assets/admin/global_assets/css/icons/summernote', 'public/css/icons/summernote')
	.styles([
	 'resources/assets/admin/css/bootstrap.min.css',
	 'resources/assets/admin/css/bootstrap_limitless.min.css',
	 'resources/assets/admin/css/layout.css',
	 'resources/assets/admin/css/components.css',
	 'resources/assets/admin/css/colors.min.css'
	], 'public/css/vendor.css')
    .styles('resources/assets/admin/css/ekko-lightbox.css', 'public/css/ekko-lightbox.min.css')
    .scripts('resources/assets/admin/js/ekko-lightbox.js', 'public/js/ekko-lightbox.min.js')
    .copy('resources/assets/admin/css/jquery-confirm-3.0.min.css', 'public/css/jquery-confirm.min.css')
	.scripts([
	 'resources/assets/admin/global_assets/js/main/jquery.min.js',
	 'resources/assets/admin/global_assets/js/main/bootstrap.bundle.min.js'
	], 'public/js/vendor.js')
	.js([
	 'resources/assets/admin/js/app.js'
	], 'public/js/admin_init.js')
	.copyDirectory('resources/assets/admin/global_assets/js/plugins/editors/summernote', 'public/js/summernote')
	.copyDirectory('resources/assets/images', 'public/images')
	.copyDirectory('resources/assets/admin/global_assets/js/plugins/forms/selects', 'public/js/selects')
	.copyDirectory('resources/assets/admin/global_assets/js/plugins/pickers', 'public/js/pickers')
	.scripts([
	 'resources/assets/admin/global_assets/js/plugins/uploaders/fileinput/plugins/piexif.min.js',
	 'resources/assets/admin/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js',
	 'resources/assets/admin/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js'
	], 'public/js/fileinput.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/tables/datatables/datatables.min.js', 'public/js/datatables.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/tables/datatables/extensions/select.min.js', 'public/js/datatable-extension-select.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js', 'public/js/datatable-extension-buttons.min.js')
	.copy('resources/assets/admin/js/handlebar.min.js', 'public/js/handlebar.min.js')
	.copy('resources/assets/admin/global_assets/js/plugins/loaders/blockui.min.js', 'public/js/blockui.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/forms/styling/uniform.min.js', 'public/js/uniform.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/forms/styling/switchery.min.js', 'public/js/switchery.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/forms/styling/switch.min.js', 'public/js/switch.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/pickers/anytime.min.js', 'public/js/anytime.min.js')
    .copyDirectory('resources/assets/admin/global_assets/js/plugins/ui/fullcalendar', 'public/js/fullcalendar')
    .copyDirectory('resources/assets/admin/global_assets/js/plugins/ui/moment', 'public/js/moment')
    .copyDirectory('resources/assets/admin/global_assets/js/plugins/notifications', 'public/js/notifications')
    .copyDirectory('resources/assets/admin/global_assets/js/plugins/forms', 'public/js/forms')
	.copyDirectory('resources/assets/admin/global_assets/js/plugins/tables/handsontable', 'public/js/handsontable')
    .copy('resources/assets/admin/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js', 'public/js/interactions.min.js')
    .copy('resources/assets/js/cleave.min.js', 'public/js/cleave.min.js')
    .copy('resources/assets/js/jquery-mask.min.js', 'public/js/jquery-mask.min.js')
    .copy('resources/assets/admin/js/jquery-confirm-3.0.min.js', 'public/js/jquery-confirm.min.js')
    .copy('resources/assets/admin/js/jquery.fancytree-all-deps.min.js', 'public/js/jquery.fancytree-all-deps.min.js')
    .copy('resources/assets/admin/js/packery.pkgd.min.js', 'public/js/packery.pkgd.min.js')
    .copy('resources/assets/admin/global_assets/js/plugins/forms/inputs/duallistbox/duallistbox.min.js', 'public/js/duallistbox.min.js')
    .copyDirectory('resources/assets/frontend', 'public/front')
	.version();
