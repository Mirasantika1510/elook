$('#search-open').on('click',function(){
    $('#search-form').show();
});

$('#search-close').on('click',function(){
    $('#search-form').hide();
});


var readURL = function(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.avatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


$(".file-upload").on('change', function(){
    readURL(this);
});