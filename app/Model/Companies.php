<?php

namespace App\Model;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Storage;

class Companies extends BaseModel
{
    protected $table = 'companies';

    protected $appends = [ 'size' ];

    public function getLogoAttribute($value)
    {
        $agent = new Agent();

        return Storage::url("images/companies/$value");
    }

    public function getPopUpImageAttribute($value)
    {
        $agent = new Agent();

        return Storage::url("images/companies/$value");
    }

    public function getConfirmationPaymentBannerImageAttribute($value)
    {
        $agent = new Agent();

        return Storage::url("images/banners/$value");
    }

    public function getTrackingOrderBannerImageAttribute($value)
    {
        $agent = new Agent();

        return Storage::url("images/banners/$value");
    }

    public function getContactBannerImageAttribute($value)
    {
        $agent = new Agent();

        return Storage::url("images/banners/$value");
    }

    public function getSizeAttribute()
    {
        $agent = new Agent();

        return Storage::url('images/companies/' . $this->attributes['logo']);
    }
}
