<?php

namespace App\Model;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Storage;

class Testimonial extends BaseModel
{
    protected $table = 'testimonials';

    protected $hidden = ['id'];

    public function getImageAttribute($value)
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url("images/testimonials/$value/$value-large");
        }else{
            return Storage::url("images/testimonials/$value/$value-large.jpg");
        }
    }
}
