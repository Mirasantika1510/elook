<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $table = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'id'
    ];

    public function allAddress()
    {
        return $this->hasMany('App\Model\CustomerAddress', 'customer_id');
    }

    public function address()
    {
        return $this->hasOne('App\Model\CustomerAddress', 'customer_id')->where('active', 1);
    }

    public function wishlist()
    {
        return $this->hasMany('App\Model\Wishlist', 'user_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Model\Order', 'customer_id');
    }

    public function getImageAttribute()
    {
        $agent = new Agent();
        if ($this->attributes['image']) {
            if ($agent->is('Chrome') || $agent->is('Opera')) {
                return Storage::url('images/customer/' . $this->attributes['id'] . '/' . $this->attributes['image'] . '/' . $this->attributes['image']);
            } else {
                return Storage::url('images/customer/' . $this->attributes['id'] . '/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '.jpg');
            }
        } else {
            return url(asset('front/img/avatar.png'));
        }
    }

    public function getImageThumbAttribute()
    {
        $agent = new Agent();

        if ($agent->is('Chrome') || $agent->is('Opera')) {
            return Storage::url('images/customer/' . $this->attributes['id'] . '/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-thumbnail');
        } else {
            return Storage::url('images/customer/' . $this->attributes['id'] . '/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-thumbnail.jpg');
        }
    }
}
