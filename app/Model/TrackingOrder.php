<?php

namespace App\Model;

class TrackingOrder extends BaseModel
{
    protected $table = 'tracking_orders';

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }
}
