<?php

namespace App\Model;

class Order extends BaseModel
{
    protected $table = 'orders';

    public function payment()
    {
        return $this->hasOne('App\Model\OrderPayments', 'order_id');
    }

    public function shipping()
    {
        return $this->hasOne('App\Model\OrderShippings', 'order_id');
    }

    public function products()
    {
        return $this->hasMany('App\Model\OrderProducts', 'order_id');
    }

    public function trackings()
    {
        return $this->hasMany('App\Model\TrackingOrder', 'order_id');
    }

    public function voucher()
    {
        return $this->hasOne('App\Model\OrderVoucher', 'order_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Model\Customer', 'customer_id');
    }
}
