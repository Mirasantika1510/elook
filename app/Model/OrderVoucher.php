<?php

namespace App\Model;

class OrderVoucher extends BaseModel
{
    protected $table = 'orders_voucher';

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }
}
