<?php

namespace App\Model;

use Illuminate\Support\Facades\Storage;

class OrderPayments extends BaseModel
{
    protected $table = 'payment_details';

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }

    public function getPaymentReceiptAttribute($value)
    {
        if(!$value) {
            return $value;
        }else {
            return Storage::url('images/receipt/' . $value );
        }
    }
}
