<?php

namespace App\Model;

class Wishlist extends BaseModel
{
    protected $table = 'wishlists';

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }
}
