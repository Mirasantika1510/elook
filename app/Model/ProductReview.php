<?php

namespace App\Model;

class ProductReview extends BaseModel
{
    protected $table = 'product_reviews';

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Model\Customer', 'customer_id');
    }
}
