<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class Banner extends Model
{
    protected $table = 'banner';
    protected $hidden = ['id', 'created_at', 'updated_at'];
    protected $appends = [ 'size' ];

    protected $fillable = [
        'page', 'title', 'description', 'image'
    ];

    public function getImageAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url('images/banner/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::url('images/banner/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }

    public function getSizeAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::size('images/banner/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::size('images/banner/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }
}
