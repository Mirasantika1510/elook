<?php

namespace App\Model;

use Jenssegers\Agent\Agent;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategories extends BaseModel
{
    use NodeTrait, SoftDeletes;

    protected $table = 'product_categories';

    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany('App\Model\Product', 'products_join_categories', 'category_id', 'product_id');
    }

    public function getImageAttribute($value)
    {
        $agent = new Agent();
        if($agent->is('Chrome') || $agent->is('Opera')) {
            return Storage::url("images/product_categories/$value");
        }else{
            return Storage::url("images/product_categories/$value.jpg");
        }
    }

    public function getBannerAttribute($value)
    {
        if($value){
            $agent = new Agent();

            if($agent->is('Chrome') || $agent->is('Opera')){
                return Storage::url("images/banners/$value");
            }else{
                return Storage::url("images/banners/$value.jpg");
            }
        }else{
            return $value;
        }
    }

}
