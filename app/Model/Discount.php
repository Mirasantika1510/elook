<?php

namespace App\Model;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Storage;

class Discount extends BaseModel
{
    protected $table = 'discounts';

    protected $appends = [ 'size' ];

    public function products()
    {
    	return $this->hasMany('App\Model\Product', 'discount_id');
    }

    public function getImageAttribute($value)
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url("images/discounts/$value/$value-large");
        }else{
            return Storage::url("images/discounts/$value/$value-large.jpg");
        }
    }

    public function getBannerAttribute($value)
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url("images/banners/$value");
        }else{
            return Storage::url("images/banners/$value.jpg");
        }
    }

    public function getSizeAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::size('images/discounts/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::size('images/discounts/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }
}
