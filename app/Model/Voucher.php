<?php

namespace App\Model;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Storage;

class Voucher extends BaseModel
{
    protected $table = 'vouchers';

    protected $appends = [ 'size' ];

    public function products()
    {
    	return $this->belongsToMany('App\Model\Product', 'voucher_products', 'voucher_id', 'product_id');
    }

    public function getImageAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url('images/vouchers/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::url('images/vouchers/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }

    public function getSizeAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::size('images/vouchers/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::size('images/vouchers/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }
}
