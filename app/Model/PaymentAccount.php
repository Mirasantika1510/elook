<?php

namespace App\Model;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Storage;

class PaymentAccount extends BaseModel
{
    protected $table = 'payment_accounts';
}
