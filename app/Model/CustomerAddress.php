<?php

namespace App\Model;

class CustomerAddress extends BaseModel
{
    protected $table = 'customers_address';


    public function getprovince()
    {
        return $this->hasOne('App\Model\Province', 'province_id', 'province');
    }


    public function getcity()
    {
        return $this->hasOne('App\Model\City', 'city_id', 'city');
    }


    public function getsubdistrict()
    {
        return $this->hasOne('App\Model\Subdistrict', 'subdistrict_id', 'subdistrict');
    }

}
