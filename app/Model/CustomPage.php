<?php

namespace App\Model;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Storage;

class CustomPage extends BaseModel
{
    protected $table = 'custom_pages';

    protected $appends = [ 'size' ];

    public function getBannerAttribute($value)
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url("images/custom_pages/$value-large");
        }else{
            return Storage::url("images/custom_pages/$value-large.jpg");
        }
    }

    public function getSizeAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::size('images/custom_pages/' . $this->attributes['banner'] . '-large');
        }else{
            return Storage::size('images/custom_pages/' . $this->attributes['banner'] . "-large.jpg");
        }
    }
}
