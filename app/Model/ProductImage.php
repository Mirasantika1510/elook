<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class ProductImage extends Model
{
    use SoftDeletes;

    protected $table = 'product_images';

    protected $hidden = ['id'];

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    protected $appends = ['size', 'imageUrl', 'imageMediumUrl', 'imageLargeUrl', 'downloadUrl', 'filetype', 'url'];

    protected $visible = ['cover', 'imageUrl', 'imageMediumUrl', 'imageLargeUrl'];

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function getSizeAttribute()
    {
        return Storage::size('images/product_images/' . $this->product->id . '/' . $this->attributes['filename'] . '/' . $this->attributes['filename'] . '-thumbnail');
    }

    public function getImageUrlAttribute()
    {
        $agent = new Agent();

        if ($agent->is('Chrome') || $agent->is('Opera')) {
            return Storage::url('images/product_images/' . $this->product->id . '/' . $this->attributes['filename'] . '/' . $this->attributes['filename'] . '-thumbnail');
        } else {
            return Storage::url('images/product_images/' . $this->product->id . '/' . $this->attributes['filename'] . '/' . $this->attributes['filename'] . '-thumbnail.jpg');
        }
    }

    public function getImageMediumUrlAttribute()
    {
        $agent = new Agent();

        if ($agent->is('Chrome') || $agent->is('Opera')) {
            return Storage::url('images/product_images/' . $this->product->id . '/' . $this->attributes['filename'] . '/' . $this->attributes['filename'] . '-medium');
        } else {
            return Storage::url('images/product_images/' . $this->product->id . '/' . $this->attributes['filename'] . '/' . $this->attributes['filename'] . '-medium.jpg');
        }
    }

    public function getImageLargeUrlAttribute()
    {
        $agent = new Agent();

        if ($agent->is('Chrome') || $agent->is('Opera')) {
            return Storage::url('images/product_images/' . $this->product->id . '/' . $this->attributes['filename'] . '/' . $this->attributes['filename'] . '-large');
        } else {
            return Storage::url('images/product_images/' . $this->product->id . '/' . $this->attributes['filename'] . '/' . $this->attributes['filename'] . '-large.jpg');
        }
    }

    public function getDownloadUrlAttribute()
    {
        return url(config('elook.admin_url') . '/product-download-image') . '/' . $this->attributes['id'];
    }

    public function getUrlAttribute()
    {
        return url(config('elook.admin_url') . '/product-delete-image') . '/' . $this->attributes['id'];
    }

    public function getFileTypeAttribute()
    {
        $agent = new Agent();

        if ($agent->is('Chrome') || $agent->is('Opera')) {
            return 'image/webp';
        } else {
            return 'image/jpeg';
        }
    }
}
