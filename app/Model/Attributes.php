<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attributes extends Model
{
    use SoftDeletes;

    protected $table = 'attributes';

    protected $hidden = ['id', 'attribute_id', 'created_at', 'deleted_at', 'updated_at'];

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    protected $appends = ['key'];

    public function products()
    {
        return $this->belongsToMany('App\Model\Product', 'App/Model/ProductJoinAttributes', 'attribute_id', 'product_id');
    }
}
