<?php

namespace App\Model;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class Lookbook extends BaseModel
{
    protected $table = 'lookbooks';

    protected $appends = [ 'size' ];

    protected $casts = [
        'products' => 'array'
    ];

    public function getImageAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url('images/lookbooks/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::url('images/lookbooks/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }

    public function getBannerAttribute($value)
    {
        if($value){
            $agent = new Agent();

            if($agent->is('Chrome') || $agent->is('Opera')){
                return Storage::url("images/banners/$value");
            }else{
                return Storage::url("images/banners/$value.jpg");
            }
        }else{
            return $value;
        }
    }

    public function getSizeAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::size('images/lookbooks/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::size('images/lookbooks/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }
}
