<?php

namespace App\Model;

use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class OrderProducts extends BaseModel
{
    protected $table = 'orders_products';

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function getProductImageAttribute($value)
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url("images/product_images/{$this->attributes['product_id']}/$value/$value-thumbnail");
        }else{
            return Storage::url("images/product_images/{$this->attributes['product_id']}/$value/$value-thumbnail.jpg");
        }

    }
}
