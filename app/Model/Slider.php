<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class Slider extends Model
{
    protected $table = 'sliders';

    protected $hidden = ['id'];

    protected $guarded = [];

    protected $appends = [ 'size' ];

    public function getHasButtonAttribute($value)
    {
        return (boolean) $value;
    }

    public function getImageAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::url('images/sliders/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::url('images/sliders/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }

    public function getSizeAttribute()
    {
        $agent = new Agent();

        if($agent->is('Chrome') || $agent->is('Opera')){
            return Storage::size('images/sliders/' . $this->attributes['image'] . '/' . $this->attributes['image'] . '-large');
        }else{
            return Storage::size('images/sliders/' . $this->attributes['image'] . '/' . $this->attributes['image'] . "-large.jpg");
        }
    }
}
