<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $dates = ['deleted_at', 'start_date', 'end_date'];

    protected $guarded = [];

    protected $casts = [
        'related' => 'array'
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Model\ProductCategories', 'products_join_categories', 'product_id', 'category_id');
    }

    public function discount()
    {
        return $this->belongsTo('App\Model\Discount', 'discount_id');
    }

    public function publishedDiscount()
    {
        return $this->belongsTo('App\Model\Discount', 'discount_id')->where('published', true);
    }

    public function tags()
    {
        return $this->belongsToMany('App\Model\ProductTag', 'products_join_tags', 'product_id', 'tag_id');
    }

    public function getFeaturedAttribute($value)
    {
        return (boolean) $value;
    }

    public function images()
    {
        return $this->hasMany('App\Model\ProductImage', 'product_id')->orderBy('order');
    }

    public function reviews()
    {
        return $this->hasMany('App\Model\ProductReview', 'product_id');
    }

    public function ordered()
    {
        return $this->hasMany('App\Model\OrderProducts', 'product_id');
    }

    public function scopeFetchWithFilter($query, Request $request)
    {
        if ($request->has('minrange') and $request->has('maxrange')) {
            $query = $query->whereRaw(DB::raw('min_value BETWEEN (CASE WHEN stock_status = "attributes" THEN (CASE WHEN ' . $request->minrange . ' > max_value THEN ' . $request->minrange . ' ELSE 0 END) ELSE ' . $request->minrange . ' END) AND (CASE WHEN stock_status = "attributes" THEN max_value ELSE ' . $request->maxrange . ' END)'))
                        ->whereRaw(DB::raw('max_value BETWEEN (CASE WHEN stock_status = "attributes" THEN min_value ELSE ' . $request->minrange . ' END) AND (CASE WHEN stock_status = "attributes" THEN (CASE WHEN ' . $request->maxrange . ' < min_value THEN ' . $request->maxrange . ' ELSE 1000000000 END) ELSE ' . $request->maxrange . ' END)'));
        }

        if ($request->has('sortby')) {
            if ($request->sortby === 'default') {
                $query = $query->orderBy('title', 'desc');
            } elseif ($request->sortby === 'lowtohigh') {
                $query = $query->orderBy('min_value', 'asc');
            } elseif ($request->sortby === 'hightolow') {
                $query = $query->orderBy('max_value', 'desc');
            } else {
                $query = $query->orderBy('title', 'desc');
            }
        }

        return $query;
    }
}
