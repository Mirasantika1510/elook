<?php

namespace App\Model;

class OrderShippings extends BaseModel
{
    protected $table = 'shipping_details';

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }
}
