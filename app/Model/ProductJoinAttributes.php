<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductJoinAttributes extends Pivot
{
    protected $table = 'products_join_attributes';

    protected $hidden = [ 'attribute_id', 'product_id' ];

    protected $guarded = [];
}
