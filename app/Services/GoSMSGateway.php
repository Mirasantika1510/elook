<?php

namespace App\Services;

use App\Model\Order;
use GuzzleHttp\Client;

class GoSMSGateway
{
    protected $goSMSGatewayHost;
    protected $goSMSGatewayUsername;
    protected $goSMSGatewayPassword;

    public function __construct()
    {
        $this->goSMSGatewayHost = config('smsgateway.host');
        $this->goSMSGatewayUsername = config('smsgateway.username');
        $this->goSMSGatewayPassword = config('smsgateway.password');
    }

    public function sendMessage($order, $phone)
    {
        $CURLOPT_URL = $this->goSMSGatewayHost. "/Send.php";

        $client = new Client(['timeout'  => 30]);

        $message = "Halo! Terimakasih sudah order di E.Look :)\nOrderId : {$order->invoice_number}.\nTotal payment: Rp. " . number_format($order->total_price, 0, ',', '.') . "\nTransfer : {$order->payment->account_bank} {$order->payment->account_number} a.n {$order->payment->account_name}.\nBatas Transfer: 1x24 jam";

        try {
            $response = $client->request('GET', $CURLOPT_URL, [
                'query' => [
                    'username'  => $this->goSMSGatewayUsername,
                    'mobile'    => $phone,
                    'message'   => $message,
                    'password'  => $this->goSMSGatewayPassword,
                ],
                'verify' => false
            ]);
        } catch (\Exception $e) {

            return false;
        }

        $body = $response->getBody();

        $content = $body->getContents();

        if((int) $body->getContents() !== 1701) {
            return false;
        }

        return json_decode($content);
    }
}
