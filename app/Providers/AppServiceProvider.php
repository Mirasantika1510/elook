<?php

namespace App\Providers;

use App\Model\Discount;
use App\Model\Companies;
use App\Model\CustomPage;
use App\Model\SocialMedia;
use App\Model\ProductCategories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $companies        = Companies::find(1);
        $socials          = SocialMedia::all();
        $categories       = ProductCategories::all();
        $custom_pages     = CustomPage::all();
        $discountCount    = Discount::where('published', true)->count();

        view()->composer([ 'frontend.layouts.footer', 'frontend.layouts.header' ], function ($view) use($socials, $categories, $custom_pages, $discountCount) {
            $view->with([
                'socials' => $socials,
                'categories' => $categories,
                'custom_pages' => $custom_pages,
                'discountCount' => $discountCount,
            ]);
        });

        view()->composer('*', function ($view) use($companies) {
            $view->with([
                'companies' => $companies,
            ]);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
