<?php

namespace App\Console\Commands;

use App\Model\Province;
use App\FrontEnd\RajaOngkir;
use Illuminate\Console\Command;

class SynchronizeProvinceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:province';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize Province from RajaOngkir';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $address = new RajaOngkir();

        foreach ($address->syncProvince() as $result) {
            $this->info("create or update data province {$result->province}");

            Province::updateOrCreate(
                ['province_id' => $result->province_id],
                ['province' => $result->province]
            );
        }

        $this->info('Done.. !!');

        return 0;
    }
}
