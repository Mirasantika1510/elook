<?php

namespace App\Console\Commands;

use App\Model\City;
use App\FrontEnd\RajaOngkir;
use Illuminate\Console\Command;

class SynchronizeCityCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:city';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize City from RajaOngkir';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $address = new RajaOngkir();

        foreach ($address->syncCity() as $result) {
            $this->info("create or update data city {$result->city_name}");

            City::updateOrCreate(
                ['city_id' => $result->city_id],
                [
                    'province_id' => $result->province_id,
                    'province' => $result->province,
                    'type' => $result->type,
                    'city_name' => $result->city_name,
                    'postal_code' => $result->postal_code,
                ]
            );
        }

        $this->info('Done.. !!');

        return 0;
    }
}
