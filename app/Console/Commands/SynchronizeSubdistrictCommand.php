<?php

namespace App\Console\Commands;

use App\Model\City;
use App\Model\Subdistrict;
use App\FrontEnd\RajaOngkir;
use Illuminate\Console\Command;

class SynchronizeSubdistrictCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:subdistrict';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize Subdistrict from RajaOngkir';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::all();

        $address = new RajaOngkir();

        foreach ($cities as $city) {
            $this->info("Fetching subdistrict from city {$city->city_name}, id : {$city->city_id}");

            foreach ($address->syncSubdistrict($city->city_id) as $result) {
                $this->info("create or update data subdistrict {$result->subdistrict_name}");

                Subdistrict::updateOrCreate(
                    ['subdistrict_id' => $result->subdistrict_id],
                    [
                        'province_id' => $result->province_id,
                        'province' => $result->province,
                        'city_id' => $result->city_id,
                        'city' => $result->city,
                        'type' => $result->type,
                        'subdistrict_name' => $result->subdistrict_name,
                    ]
                );
            }
        }

        $this->info('Done.. !!');

        return 0;
    }
}
