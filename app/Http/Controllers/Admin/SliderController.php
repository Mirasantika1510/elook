<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Slider;
use DB, Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Datatables;

class SliderController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.sliders.index');
    }


    public function sliderDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $sliders = Slider::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'link_url', 'image', 'created_at')->get();

        $datatables = Datatables::of($sliders);

        return $datatables
                ->addColumn('actions', function ($slider) {
                     return '
                        <form id="delete-row-'.$slider->id.'" action="'.url($this->adminUrl.'/sliders', $slider->id).'" method="POST">'.csrf_field().method_field("DELETE").'</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="'.url($this->adminUrl.'/sliders', $slider->id).'/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-'.$slider->id.'\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('image', function ($slider) {
                    return '<img src="'.$slider->image.'" alt="" width="100">';
                })
                ->editColumn('link_url', function ($slider) {
                    return $slider->link_url? '<a href="'.$slider->link_url.'">'.$slider->link_url.'</a>' : '-';
                })
                ->rawColumns(['image', 'link_url', 'actions'])
                ->make(true);
    }


    public function create()
    {
        return view('admin.pages.sliders.create');
    }


    public function store()
    {
        request()->validate([
            'link_url'		    => 'nullable|url',
            'image'      	    => 'required|mimes:jpg,jpeg,png',
        ]);

        $imageOriginal  = Image::make(request('image'));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/sliders/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/sliders/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/sliders/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/sliders/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        Slider::create([

            'link_url' 		    => request('link_url')?: null,
            'image' 		    => $fileName

        ]);

        return redirect($this->adminUrl.'/sliders');
    }


    public function edit(Slider $slider)
    {
        return view('admin.pages.sliders.edit', compact('slider'));
    }


    public function update(Slider $slider)
    {
        request()->validate([
            'link_url'		    => 'nullable|url',
            'image'             => 'nullable|mimes:jpeg,jpg,png',
        ]);

        if(request()->has('image')){

        	$imageOriginal = Image::make(request('image'));

	        $fileName       = str_random(40);

	        $imageOriginal->resize(1366, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/sliders/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/sliders/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


	        $imageOriginal->resize(1024, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/sliders/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/sliders/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

	        $slider->update([

	            'link_url' 		    => request('link_url')?: null,
	            'image'			    => $fileName

	        ]);

	        return redirect($this->adminUrl.'/sliders');
        }

        $slider->update([

            'link_url' 		    => request('link_url')?: null,

        ]);

        return redirect($this->adminUrl.'/sliders');
    }


    public function destroy(Slider $slider)
    {
        $slider->delete();

        return redirect($this->adminUrl.'/sliders');
    }
}
