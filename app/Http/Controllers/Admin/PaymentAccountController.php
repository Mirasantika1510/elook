<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\PaymentAccount;
use DB, Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Datatables;

class PaymentAccountController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.payment_accounts.index');
    }


    public function accountDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $accounts   = PaymentAccount::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'account_name', 'account_bank', 'account_number', 'instructions', 'created_at')->get();

        $datatables = Datatables::of($accounts);

        return $datatables
                ->addColumn('actions', function ($account) {
                     return '
                        <form id="delete-row-'.$account->id.'" action="'.url($this->adminUrl.'/payment-accounts', $account->id).'" method="POST">'.csrf_field().method_field("DELETE").'</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="'.url($this->adminUrl.'/payment-accounts', $account->id).'/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-'.$account->id.'\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('instructions', function ($account) {
                    return \Illuminate\Support\Str::words($account->instructions, 20, '...');
                })
                ->rawColumns([ 'actions'])
                ->make(true);
    }


    public function create()
    {
        return view('admin.pages.payment_accounts.create');
    }


    public function store()
    {
        request()->validate([
            'account_bank'      => 'required',
            'account_number'	=> 'required',
            'account_name'	    => 'required',
            'instructions'	    => 'required',
        ]);

        PaymentAccount::create([

            'account_bank' 		=> request('account_bank'),
            'account_number' 	=> request('account_number'),
            'account_name' 		=> request('account_name'),
            'instructions' 		=> request('instructions'),

        ]);

        return redirect($this->adminUrl.'/payment-accounts');
    }


    public function edit(PaymentAccount $payment_account)
    {
        return view('admin.pages.payment_accounts.edit', compact('payment_account'));
    }


    public function update(PaymentAccount $payment_account)
    {
        request()->validate([
            'account_bank'      => 'required',
            'account_number'	=> 'required',
            'account_name'	    => 'required',
            'instructions'	    => 'required',
        ]);

        $payment_account->update([

            'account_bank' 		=> request('account_bank'),
            'account_number' 	=> request('account_number'),
            'account_name' 		=> request('account_name'),
            'instructions' 		=> request('instructions'),

        ]);

        return redirect($this->adminUrl.'/payment-accounts');
    }


    public function destroy(PaymentAccount $payment_account)
    {
        $payment_account->delete();

        return redirect($this->adminUrl.'/payment-accounts');
    }
}
