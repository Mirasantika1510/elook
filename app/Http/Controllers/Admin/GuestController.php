<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Model\Order;
use App\Exports\GuestExport;
use App\Model\OrderProducts;
use Illuminate\Http\Request;
use App\Model\OrderShippings;
use Yajra\DataTables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class GuestController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.guests.index');
    }

    public function guestDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));

        $guests     = OrderShippings::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'firstname', 'lastname', 'email', 'created_at')
                            ->whereHas('order', function($query){
                                $query->whereNull('customer_id');
                            })
                            ->with('order')
                            ->groupBy('email')
                            ->orderBy('created_at', 'desc')
                            ->get();

        $datatables = Datatables::of($guests);

        return $datatables
                 ->addColumn('actions', function ($custom_page) {
                                         return '
                                            <td class="text-center">
                                                <div class="list-icons">
                                                    <div class="dropdown">
                                                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                            <i class="icon-menu9"></i>
                                                        </a>

                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a href="'.url($this->adminUrl.'/guests-detail', $custom_page->id).'" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Detail</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                         ';
                                    })
                    ->addColumn('name', function ($guest) {
                        return $guest->firstname. ' ' .$guest->lastname;
                    })
                    ->addColumn('has_buy', function ($guest) {

                        $orders = Order::whereNull('customer_id')
                        ->whereHas('shipping', function($query) use($guest) {
                            $query->where('email', $guest->email);
                        })->whereHas('payment', function($query){
                            $query->where('transaction_status', 'settlement');
                        })->count();

                        return $orders > 0? '<span class="badge badge-success">Yes</span>' : '<span class="badge badge-danger">No</span>';
                    })
                    ->rawColumns(['has_buy', 'actions'])
                    ->make(true);
    }

    public function detail(OrderShippings $shipping)
    {
        $orders = Order::whereNull('customer_id')
                        ->whereHas('shipping', function($query) use($shipping) {
                            $query->where('email', $shipping->email);
                        })->whereHas('payment', function($query){
                            $query->where('transaction_status', 'settlement');
                        })->count();
        if ($orders == 0) {
            return view('admin.pages.guests.detail_not_found');
        } else {
            return view('admin.pages.guests.detail', compact('shipping'));
        }
    }

    public function detailDatatables(OrderShippings $shipping)
    {
        DB::statement(DB::raw('set @rownum=0'));

        $members    = Order::whereNull('customer_id')
                        ->whereHas('shipping', function($query) use($shipping) {
                            $query->where('email', $shipping->email);
                        })->whereHas('payment', function($query){
                            $query->where('transaction_status', 'settlement');
                        })->get();

        $id = array();
        foreach ($members as $key) {
            $id[] = $key->id;
        }

        $products = OrderProducts::whereIn('order_id', $id)->select('*',DB::raw('@rownum  := @rownum  + 1 AS rownum'))->get();

        $datatables = Datatables::of($products);

        return $datatables
                    ->editColumn('product_price', function ($member) {
                        return 'Rp. '.number_format($member->product_price);
                    })
                     ->editColumn('invoice_number', function ($member) {
                        return $member->order->invoice_number;
                    })
                     ->editColumn('created_at', function ($member) {
                        return Carbon::parse($member->created_at)->format('d M Y');
                    })
                    ->make(true);
    }

    public function export()
    {  
       return Excel::download(new GuestExport, 'guests.xlsx');
    }
}
