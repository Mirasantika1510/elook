<?php

namespace App\Http\Controllers\Admin;

use App\Model\Order;
use App\Exports\OrderExport;
use App\Model\OrderProducts;
use App\Model\TrackingOrder;
use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\FrontEnd\Report;

class OrderController extends Controller
{
    private $adminUrl;
    private $report;

    public function __construct(Report $report)
    {
        $this->adminUrl = config('elook.admin_url');
        $this->report = $report;
    }

    public function index()
    {
        return view('admin.pages.orders.index');
    }

    public function orderDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $orders = Order::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'invoice_number', 'unique_number', 'total_price', 'shipping_price', 'created_at', 'order_status')
                        ->with('payment')
                        ->with('shipping')
                        ->orderBy('created_at', 'desc')
                        ->get();
        $datatables = Datatables::of($orders);

        return $datatables
                ->addColumn('check_id', function ($order) {
                    return '<input type="checkbox" name="check_id[]" value="' . $order->id . '"">';
                })
                ->addColumn('actions', function ($order) {
                    return '
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="' . url($this->adminUrl . '/orders', $order->id) . '" class="dropdown-item">
                                            <i class="icon-eye" title="View Order"></i> View Order
                                        </a>
                                        <a href="' . url($this->adminUrl . '/orders-tracking', $order->id) . '" class="dropdown-item">
                                            <i class="icon-eye" title="View Tracking Order"></i> View Tracking Order
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    ';
                })
                ->editColumn('final_price', function ($order) {
                    return 'Rp. ' . number_format($order->total_price + $order->unique_number, 0, ',', '.');
                })
                ->editColumn('payment.payment_receipt', function ($order) {
                    if ($order->payment->payment_receipt) {
                        return '<a href="' . $order->payment->payment_receipt . '" data-toggle="lightbox">
                            <img src="' . $order->payment->payment_receipt . '" class="img-fluid" style="width: 100px;">
                        </a>';
                    } else {
                        return 'no receipt uploaded';
                    }
                })
                ->editColumn('payment.transaction_status', function ($order) {
                    if ($order->payment->transaction_status === 'pending') {
                        return '<span class="badge badge-warning">Pending</span>';
                    } else {
                        return '<span class="badge badge-success">Settlement</span>';
                    }
                })
                ->editColumn('order_status', function ($order) {
                    if ($order->order_status === 'ordered') {
                        return '<span class="badge badge-primary">Ordered</span>';
                    } elseif ($order->order_status === 'packing') {
                        return '<span class="badge badge-warning">Packing</span>';
                    } elseif ($order->order_status === 'shipping') {
                        return '<span class="badge badge-success">Shipping</span>';
                    } else {
                        return '-';
                    }
                })
                ->rawColumns(['check_id', 'payment.payment_receipt', 'payment.transaction_status', 'order_status', 'actions'])
                ->make(true);
    }

    public function show(Order $order)
    {
        return view('admin.pages.orders.show', compact('order'));
    }

    public function orderAcceptPayment(Order $order)
    {
        DB::beginTransaction();

        try {
            $order->payment()->update([
                'transaction_status' => 'settlement'
            ]);

            $order->trackings()->create([
                'title' => 'Payment Order Received',
                'description' => 'Payment order has received, order continue to next progress'
            ]);

            // foreach ($order->products as $item) {
            //     if (!$item->product->ignore_stock) {
            //         $item->product()->decrement('stock', $item->quantities);
            //         $item->product()->decrement('stock_used', $item->quantities);
            //     }
            // }
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }

        DB::commit();

        return redirect($this->adminUrl . '/orders');
    }

    public function orderAcceptPaymentAll()
    {
        $order_id = explode(',', request('order_id'));

        DB::beginTransaction();
        $orders = Order::whereIn('id', $order_id)->get();

        try {
            foreach ($orders as $order) {
                if ($order->payment->transaction_status != 'settlement') {
                    $order->payment()->update([
                        'transaction_status' => 'settlement'
                    ]);

                    $order->trackings()->create([
                        'title' => 'Payment Order Received',
                        'description' => 'Payment order has received, order continue to next progress'
                    ]);

                    foreach ($order->products as $item) {
                        if (!$item->product->ignore_stock) {
                            $item->product()->decrement('stock', $item->quantities);
                            $item->product()->decrement('stock_used', $item->quantities);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }
        DB::commit();
        return redirect($this->adminUrl . '/orders');
    }

    public function orderStatusAll()
    {
        $order_id = explode(',', request('order_id'));

        DB::beginTransaction();
        $orders = Order::whereIn('id', $order_id);

        try {
            foreach ($orders->get() as $order) {
                if (request('order_status') == 'packing' && $order->order_status == 'ordered' && $order->payment->transaction_status == 'settlement') {
                    $order->trackings()->create([
                        'title' => 'Packing',
                        'description' => 'Packing, order continue to next progress'
                    ]);

                    $order->update([
                        'order_status' => request('order_status')
                    ]);
                } elseif (request('order_status') == 'shipping' && $order->order_status == 'packing' && $order->payment->transaction_status == 'settlement') {
                    $order->trackings()->create([
                        'title' => 'Shipping',
                        'description' => 'Shipping'
                    ]);

                    $order->update([
                        'order_status' => request('order_status')
                    ]);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }
        DB::commit();
        return redirect($this->adminUrl . '/orders');
    }

    public function showTracking(Order $order)
    {
        return view('admin.pages.orders.tracking_list', compact('order'));
    }

    public function trackingDatatables(\Illuminate\Http\Request $request, Order $order)
    {
        DB::statement(DB::raw('set @rownum=0'));

        $trackings = TrackingOrder::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'order_id', 'title', 'description', 'created_at')
                        ->whereOrderId($order->id)
                        ->orderBy('created_at', 'desc')
                        ->get();

        $datatables = Datatables::of($trackings);

        return $datatables
                ->addColumn('actions', function ($tracking) {
                    return '
                        <form id="delete-row-' . $tracking->id . '" action="' . url($this->adminUrl . '/delete-tracking', $tracking->id) . '" method="POST">' . csrf_field() . method_field('DELETE') . '</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="' . url($this->adminUrl . '/edit-tracking', $tracking->id) . '" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-' . $tracking->id . '\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    ';
                })
                ->rawColumns(['actions'])
                ->make(true);
    }

    public function createTracking(Order $order)
    {
        return view('admin.pages.orders.create_tracking', compact('order'));
    }

    public function storeTracking(Order $order)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        $order->trackings()->create([
            'title' => request('title'),
            'description' => request('description'),
        ]);

        return redirect($this->adminUrl . '/orders-tracking/' . $order->id);
    }

    public function editTracking(TrackingOrder $tracking)
    {
        return view('admin.pages.orders.edit_tracking', compact('tracking'));
    }

    public function updateTracking(TrackingOrder $tracking)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        $tracking->update([
            'title' => request('title'),
            'description' => request('description'),
        ]);

        return redirect($this->adminUrl . '/orders-tracking/' . $tracking->order->id);
    }

    public function deleteTracking(TrackingOrder $tracking)
    {
        $orderId = $tracking->order->id;

        $tracking->delete();

        return redirect($this->adminUrl . '/orders-tracking/' . $orderId);
    }

    public function exportOrder()
    {
        $ordersPending = Order::select('id', 'created_at', DB::raw('COUNT(id) as waiting_list'), DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) month'))
                ->whereHas('payment', function ($query) {
                    $query->where('transaction_status', 'pending');
                })
                ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
                ->orderBy('created_at', 'asc')
                ->get();

        $ordersSettlement = Order::select('id', 'created_at', DB::raw('SUM(total_price) as tot_price'), DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) month'))
                ->whereHas('payment', function ($query) {
                    $query->where('transaction_status', 'settlement');
                })
                ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
                ->orderBy('created_at', 'asc')
                ->get();

        $bestSeller = OrderProducts::select('id', 'product_name', 'created_at', DB::raw('SUM(quantities) as tot_sold_q'), DB::raw('COUNT(product_id) as tot_sold'), DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) month'))
                ->whereHas('order', function ($queryOrder) {
                    $queryOrder->whereHas('payment', function ($queryOrder2) {
                        $queryOrder2->where('transaction_status', 'settlement');
                    });
                })
                ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
                ->orderBy('created_at', 'asc')
                // ->limit(1)
                ->get();

        $allData = $ordersPending->merge($ordersSettlement);

        // return $allData;

        $result = $allData->groupBy([
            'year',
            'month',
        ], $preserveKeys = true);

        return $bestSeller;

        return Excel::download(new OrderExport, 'report.xlsx');
    }

    public function report()
    {
        return view('admin.pages.orders.reports.' . str_slug(request('report'), '_'))->with([
            'data' => request('report')
        ]);
    }

    public function exportExcel()
    {
        switch (request('report')) {
            case 'monthly-sells-orders':
                $func = 'monthlySellDatatables';
                $header = [
                    'No',
                    'Month',
                    'Total Sales',
                    'Total Items',
                    'Total Shipping Fee',
                    'Total Discount',
                    'Total Sales Amount',
                ];
                $values = [
                    'rownum',
                    'date',
                    'total_sells',
                    'total_items',
                    'total_shipping_price',
                    'total_discount',
                    'total_price'
                ];
                break;
            case 'best-selling-products':
                $func = 'bestSellingProductsDatatables';
                $header = [
                    'No',
                    'Title',
                    'Instock Inventory',
                    'Total Quantity Sold',
                    'Total Inventory',
                    'Total Distinct Order',
                    'Total Revenue',
                ];
                $values = [
                    'rownum',
                    'title',
                    'instock_inventory',
                    'total_quantity_sold',
                    'total_inventory',
                    'total_distinct_order',
                    'total_revenue'
                ];
                break;
            case 'best-member':
                $func = 'bestMemberDatatables';
                $header = [
                    'No',
                    'Email',
                    'Total Order Amount',
                    'Total Order',
                    'Total Shipping Order',
                    'Total Discount',
                    'Last Order Date',
                ];
                $values = [
                    'rownum',
                    'email',
                    'total_order_amount',
                    'total_order',
                    'total_shipping_cost',
                    'total_discount',
                    'last_order_date'
                ];
                break;
            case 'waiting-list':
                $func = 'waitingListDatatables';
                $header = [
                    'No',
                    'Item Name',
                    'Total Waiting List',
                ];
                $values = [
                    'rownum',
                    'product_name',
                    'total_waiting_list'
                ];
                break;
            case 'abandoned-carts':
                $func = 'abandonedCartDatatables';
                $header = [
                    'No',
                    'Email',
                    'Items',
                ];
                $values = [
                    'rownum',
                    'email',
                    'item'
                ];
                break;

            case 'packing-list':
                $func = 'packingListDatatables';
                $header = [
                    ''
                ];
                $values = [
                    'item',
                ];
                break;
            case 'inventory':
                $func = 'inventoryDatatables';
                $header = [
                    'No',
                    'Title',
                    'Instock Inventory',
                    'Sold Inventory',
                    'On Hold Inventory',
                    'Total Inventory',
                ];
                $values = [
                    'rownum',
                    'title',
                    'instock_inventory',
                    'sold_inventory',
                    'on_hold_inventory',
                    'total_inventory'
                ];
                break;

            default:
                $func = 'monthlySellDatatables';
                $header = [
                    'No',
                    'Month',
                    'Total Sales',
                    'Total Items',
                    'Total Shipping Fee',
                    'Total Discount',
                    'Total Sales Amount',
                ];
                $values = [
                    'rownum',
                    'date',
                    'total_sells',
                    'total_items',
                    'total_shipping_price',
                    'total_discount',
                    'total_price'
                ];
                break;
        }

        return Excel::download(
            new OrderExport(
                $this->report->$func(request()),
                $header,
                $values
            ),
            'orders.xlsx'
        );

        return $this->report->$func(request());
    }
}
