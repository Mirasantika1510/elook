<?php

namespace App\Http\Controllers\Admin;

use DB, Storage;
use App\Model\Product;
use App\Model\Customer;
use App\Model\ProductReview;
use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Http\Controllers\Controller;

class ProductReviewController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function reviewDatatables(Product $product, \Illuminate\Http\Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $reviews    = ProductReview::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'product_id', 'customer_id', 'rate', 'comment', 'created_at')
                            ->where('product_id', $product->id)
                            ->with('customer')
                            ->get();

        $datatables = Datatables::of($reviews);

        return $datatables
                ->addColumn('actions', function ($review) {
                     return '
                        <form id="delete-row-'.$review->id.'" action="'.url($this->adminUrl.'/reviews', $review->id).'" method="POST">'.csrf_field().method_field("DELETE").'</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="'.url($this->adminUrl.'/reviews', $review->id).'/edit?product='.$review->product->id.'" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-'.$review->id.'\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->addColumn('name', function($review){
                    return $review->customer->firstname. ' ' . $review->customer->lastname;
                })
                ->rawColumns(['actions'])
                ->make(true);
    }

    public function create()
    {
        $product = Product::whereId(request('product'))->firstOrFail();
        $members = Customer::all();

        return view('admin.pages.reviews.create', compact('product', 'members'));
    }

    public function store()
    {
        request()->validate([
            'customer'		=> 'required',
            'rate'   		=> 'required',
            'comment'	    => 'required',
        ]);

        if(request('rate') > 5) abort(404, 'Rate Format not Found');

        $customer   = Customer::whereId(request('customer'))->firstOrFail();
        $product    = Product::whereId(request('product'))->firstOrFail();

        $product->reviews()->create([

            'customer_id'   => $customer->id,
            'rate' 		    => request('rate'),
            'comment' 	    => request('comment'),

        ]);

        return redirect($this->adminUrl.'/products/'.$product->id);
    }

    public function edit(ProductReview $review)
    {
        $product = Product::whereId(request('product'))->firstOrFail();

        $members = Customer::all();

        return view('admin.pages.reviews.edit', compact('product', 'review', 'members'));
    }

    public function update(ProductReview $review)
    {
        request()->validate([
            'customer'		=> 'required',
            'rate'   		=> 'required',
            'comment'	    => 'required',
        ]);

        if(request('rate') > 5) abort(404, 'Rate Format not Found');

        $customer   = Customer::whereId(request('customer'))->firstOrFail();
        $product    = Product::whereId(request('product'))->firstOrFail();

        $review->update([

            'customer_id'   => $customer->id,
            'rate' 		    => request('rate'),
            'comment' 	    => request('comment'),

        ]);

        return redirect($this->adminUrl.'/products/'.$product->id);
    }

    public function destroy(ProductReview $review)
    {
        $product    = Product::whereId(request('product'))->firstOrFail();

        $review->delete();

        return redirect($this->adminUrl.'/products/'.$product->id);
    }
}
