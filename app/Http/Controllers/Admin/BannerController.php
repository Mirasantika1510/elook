<?php

namespace App\Http\Controllers\Admin;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Banner;
use Session;
use Storage;

class BannerController extends Controller
{
	private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
    	return view('admin.pages.banner.index');
    }

    public function bannerDatatables(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $banners = Banner::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'page','title', 'description', 'image', 'created_at')->get();

        $datatables = Datatables::of($banners);

        return $datatables
                ->addColumn('actions', function ($banner) {
                     return '
                        <td class="text-center">
                            <a href="'.url($this->adminUrl.'/banners', $banner->id).'/edit" class="btn btn-outline bg-warning-400 text-warning-400 border-warning-400 border-2"><i class="icon-database-edit2" title="Edit"></i> Edit
                            </a>
                        </td>
                     ';
                })
                ->editColumn('image', function ($banner) {
                    return '<img src="'.$banner->image.'" alt="" width="100">';
                })
                ->editColumn('description', function ($account) {
                    return \Illuminate\Support\Str::words($account->description, 20, '...');
                })
                ->rawColumns(['image', 'description', 'actions'])
                ->make(true);
    }

    public function create()
    {
    	return view('admin.pages.banner.create');
    }

    public function store(Banner $banner)
    {
    	request()->validate([
            'page'		=> 'required',
            'title'		=> 'required',
            'image' 	=> 'required|mimes:jpg,jpeg,png',
        ]);

        $imageOriginal  = Image::make(request('image'));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/banner/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/banner/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/banner/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/banner/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        Banner::create([
	        'page' 		    => request('page')?: null,
	        'title' 		=> request('title')?: null,
	        'description' 	=> request('description')?: null,
	        'image'			=> $fileName
        ]);

        return redirect($this->adminUrl.'/banners');
    }

    public function edit(Banner $banner)
    {
    	return view('admin.pages.banner.edit', compact('banner'));
    }

    public function update(Banner $banner)
    {
    	request()->validate([
            'image'     => 'nullable|mimes:jpg,jpeg,png',
        ]);

        $dataRequest = [
            'page'         => request('page')?: null,
            'title'        => request('title')?: null,
            'description'  => request('description')?: null,
         ];

        if(request()->has('image')){

        	$imageOriginal = Image::make(request('image'));

	        $fileName       = str_random(40);

	        $imageOriginal->resize(1366, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/banner/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/banner/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


	        $imageOriginal->resize(1024, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/banner/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/banner/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

	        $dataRequest = collect($dataRequest)->merge([
                'image' => $fileName
            ])->all();
        }

        $banner->update($dataRequest);

		return redirect($this->adminUrl.'/banners');
    }
}
