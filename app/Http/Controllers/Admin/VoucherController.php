<?php

namespace App\Http\Controllers\Admin;

use DB, Storage;
use App\Model\Product;
use App\Model\Voucher;
use Yajra\DataTables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class VoucherController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.vouchers.index');
    }

    public function voucherDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $vouchers  = Voucher::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'voucher_code', 'voucher_type', 'nominal', 'percent', 'description', 'image', 'published', 'created_at')->get();

        $datatables = Datatables::of($vouchers);

        return $datatables
                ->addColumn('actions', function ($voucher) {
                     return '
                        <form id="delete-row-'.$voucher->id.'" action="'.url($this->adminUrl.'/vouchers', $voucher->id).'" method="POST">'.csrf_field().method_field("DELETE").'</form>
                        <form id="publish-row-'.$voucher->id.'" action="'.url($this->adminUrl.'/vouchers-publish', $voucher->id).'" method="POST">'.csrf_field().method_field("PUT").'</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="'.url($this->adminUrl.'/vouchers', $voucher->id).'/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-'.$voucher->id.'\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'publish-row-'.$voucher->id.'\').submit();" class="dropdown-item"><i class="icon-database-add" title="'.($voucher->published? "Unpublish" : "Publish").'"></i> '.($voucher->published? "Unpublish" : "Publish").'</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('description', function ($voucher) {
                    return $voucher->description? \Illuminate\Support\Str::words($voucher->description, 20, '...') : '---' ;
                })
                ->editColumn('image', function ($voucher) {
                    return '<img src="'.$voucher->image.'" alt="" width="100">';
                })
                ->editColumn('nominal', function ($voucher) {
                    if($voucher->voucher_type === 'nominal'){
                        return "Rp. " . number_format($voucher->nominal, 0, ',', '.');
                    }else{
                        return $voucher->percent . ' %';
                    }
                })
                ->addColumn('products', function($voucher){
                    if($voucher->products()->count()){
                        return join(', <br>' , $voucher->products()->pluck('title')->toArray());
                    }else {
                        return '-';
                    }
                })
                ->editColumn('published', function ($discount) {
                    if($discount->published) {
                        return '<span class="badge badge-success">Published</span>';
                    }else {
                        return '<span class="badge badge-warning">Unpublished</span>';
                    }
                })
                ->rawColumns(['image', 'description', 'products', 'published', 'actions'])
                ->make(true);
    }


    public function create()
    {
        $products       = Product::all();

        return view('admin.pages.vouchers.create', compact('products'));
    }


    public function store()
    {
        request()->validate([
            'title'	            => 'required',
            'description'	    => 'required',
            'voucher_code'	    => 'required',
            'nominal'           => 'required|numeric',
            'percent'           => 'required|numeric|max:100',
            'products'	        => 'required|array|min:1',
            'image'             => 'nullable|mimes:jpeg,jpg,png',
        ]);

        $imageOriginal = Image::make(request('image'));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/vouchers/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/vouchers/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/vouchers/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/vouchers/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        $avaliable_for = (count(request('avaliable_for')) > 1)? 'all' : request('avaliable_for')[0];
        $voucher = Voucher::create([

            'title' 	        => request('title'),
            'slug' 	            => str_slug(request('title')),
            'voucher_code'      => request('voucher_code'),
            'nominal'           => request('nominal'),
            'percent'           => request('percent'),
            'description' 	    => request('description'),
            'voucher_type' 	    => request('voucher_type'),
            'avaliable_for' 	=> $avaliable_for,
            'image' 		    => $fileName

        ]);

        $products = Product::whereIn('id', request('products'))->pluck('id')->toArray();

        $voucher->products()->attach($products);

        return redirect($this->adminUrl.'/vouchers');
    }

    public function edit(Voucher $voucher)
    {
        $products       = Product::all();
        $avaliable_for  = array();

        if($voucher->avaliable_for == 'member') {
            $avaliable_for  = array(
                'member'    => 'checked',
                'guest'     => ''
            );
        } elseif($voucher->avaliable_for == 'guest') {
            $avaliable_for  = array(
                'member'    => '',
                'guest'     => 'checked'
            );
        } else {
            $avaliable_for  = array(
                'member'    => 'checked',
                'guest'     => 'checked'
            );
        }
        return view('admin.pages.vouchers.edit', compact('voucher', 'products','avaliable_for'));
    }


    public function update(Voucher $voucher)
    {
        request()->validate([
            'title'	            => 'required',
            'description'	    => 'required',
            'voucher_code'	    => 'required',
            'nominal'           => 'required|numeric',
            'percent'           => 'required|numeric|max:100',
            'products'	        => 'required|array|min:1',
            'image'             => 'nullable|mimes:jpeg,jpg,png',
        ]);

        $avaliable_for = (count(request('avaliable_for')) > 1)? 'all' : request('avaliable_for')[0];
        if(request()->has('image')){

        	$imageOriginal = Image::make(request('image'));

	        $fileName       = str_random(40);

	        $imageOriginal->resize(1366, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/vouchers/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/vouchers/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


	        $imageOriginal->resize(1024, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/vouchers/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/vouchers/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

	        $voucher->update([
                'title' 	        => request('title'),
                'slug' 	            => str_slug(request('title')),
                'voucher_code'      => request('voucher_code'),
                'nominal'           => request('nominal'),
                'percent'           => request('percent'),
                'description' 	    => request('description'),
                'voucher_type' 	    => request('voucher_type'),
                'avaliable_for' 	=> $avaliable_for,
                'image' 		    => $fileName
	        ]);

            $voucher->products()->detach();

            $products = Product::whereIn('id', request('products'))->pluck('id')->toArray();

            $voucher->products()->attach($products);


	        return redirect($this->adminUrl.'/vouchers');
        }

        $voucher->update([
            'title' 	        => request('title'),
            'slug' 	            => str_slug(request('title')),
            'voucher_code'      => request('voucher_code'),
            'nominal'           => request('nominal'),
            'percent'           => request('percent'),
            'description' 	    => request('description'),
            'voucher_type' 	    => request('voucher_type'),
            'avaliable_for' 	=> $avaliable_for
        ]);

        $voucher->products()->detach();

        $products = Product::whereIn('id', request('products'))->pluck('id')->toArray();

        $voucher->products()->attach($products);


        return redirect($this->adminUrl.'/vouchers');
    }


    public function destroy(Voucher $voucher)
    {
        $voucher->products()->detach();

        $voucher->delete();

        return redirect($this->adminUrl.'/vouchers');
    }

    public function publishVoucher(Voucher $voucher)
    {
        $voucher->update([ 'published' => !$voucher->published ]);

        return redirect($this->adminUrl.'/vouchers');
    }
}
