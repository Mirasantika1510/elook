<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Model\Product;
use App\Model\ProductTag;
use App\Model\ProductImage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Model\ProductCategories;
use Yajra\DataTables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.products.index');
    }

    public function productDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $products = Product::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'type', 'title', 'product_code', 'price', 'featured', 'published', 'stock', 'created_at')->get();
        $datatables = Datatables::of($products);

        return $datatables
                ->addColumn('check_id', function ($product) {
                    return '<input type="checkbox" name="check_id[]" value="' . $product->id . '"">';
                })
                ->addColumn('actions', function ($product) {
                    return '
                        <form id="delete-row-' . $product->id . '" action="' . url($this->adminUrl . '/products', $product->id) . '" method="POST">' . csrf_field() . method_field('DELETE') . '</form>
                        <form id="featured-row-' . $product->id . '" action="' . url($this->adminUrl . '/products-featured', $product->id) . '" method="POST">' . csrf_field() . method_field('PUT') . '</form>
                        <form id="publish-row-' . $product->id . '" action="' . url($this->adminUrl . '/products-publish', $product->id) . '" method="POST">' . csrf_field() . method_field('PUT') . '</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="' . url($this->adminUrl . '/products', $product->id) . '/edit?type=' . $product->type . '" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="' . url($this->adminUrl . '/products', $product->id) . '" class="dropdown-item"><i class="icon-comment" title="Review"></i> Product Review</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'publish-row-' . $product->id . '\').submit();" class="dropdown-item"><i class="icon-square-' . ($product->published ? 'down' : 'up') . '" title="' . ($product->published ? 'Unpublish' : 'Publish') . '"></i> ' . ($product->published ? 'Unpublish' : 'Publish') . '</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-' . $product->id . '\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                        <a href="' . url($this->adminUrl . '/image', $product->id) .'" class="dropdown-item sorting-button"><i class="icon-database-remove" title="Delete"></i> Sorting Image</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('title', function ($product) {
                    if ($product->featured) {
                        return $product->title . ' <i class="icon-star-full2" style="font-size:11px;color:green;" title="Featured"></i>';
                    } else {
                        return $product->title;
                    }
                })
                ->addColumn('categories', function ($product) {
                    if ($product->categories()->count()) {
                        return join(', <br>', $product->categories()->pluck('title')->toArray());
                    } else {
                        return '<span class="badge badge-danger"><em>Uncategories</em></span>';
                    }
                })
                ->editColumn('type', function ($product) {
                    if ($product->type === 'normal') {
                        return '<span class="badge badge-primary">' . strtoupper($product->type) . '</span>';
                    } else {
                        return '<span class="badge badge-warning">' . strtoupper($product->type) . '</span>';
                    }
                })
                ->addColumn('images', function ($product) {
                    if ($product->images) {
                        return '<button type="button" class="btn btn-primary btn-icon images-button" data-id="' . $product->id . '"><i class="icon-images2"></i></button>';
                    } else {
                        return '<em>No Image</em>';
                    }
                })
                ->editColumn('price', function ($product) {
                    return 'Rp. ' . number_format($product->price, 0, ',', '.');
                })
                ->editColumn('published', function ($product) {
                    if ($product->published) {
                        return '<span class="badge badge-success">Published</span>';
                    } else {
                        return '<span class="badge badge-warning">Unpublished</span>';
                    }
                })
                ->rawColumns(['check_id', 'title', 'stock', 'price', 'type', 'categories', 'published', 'images', 'actions'])
                ->make(true);
    }

    public function create()
    {
        $categories = ProductCategories::all();
        $products = Product::all();
        $product_tags = ProductTag::all();

        return view('admin.pages.products.create_normal', compact('categories', 'products', 'product_tags'));

        // if(!request()->has('type') || request('type') === 'normal') {
        //     return view('admin.pages.products.create_normal', compact('categories', 'products', 'product_tags'));
        // }else {
        //     return view('admin.pages.products.create_agregator', compact('categories', 'products', 'product_tags'));
        // }
    }

    public function store()
    {
        request()->validate([
            'title' => 'required|string|unique:products,title,NULL,id,deleted_at,NULL',
            'seo_page_slug' => 'nullable|alpha_dash|unique:products,seo_page_slug,NULL,id,deleted_at,NULL',
            'specification' => 'required',
            'weight' => 'required|numeric',
            'categories' => 'required|array',
            'categories.*' => 'required',
            'price' => 'required',
            'sell_price' => 'required',
            'images' => 'required|array',
            'images.*' => 'mimes:jpg,jpeg,png',
            'type' => 'required|in:normal' // normal or agregator
        ]);

        $categories = ProductCategories::whereIn('id', request('categories'))->pluck('id')->toArray();

        $imagesUpload = [];

        DB::beginTransaction();

        $price = (int) preg_replace('/[^0-9]/m', '', request('price'));
        $sell_price = (int) preg_replace('/[^0-9]/m', '', request('sell_price'));

        $lastOrdinalNumber = Product::max('ordinal_number');

        try {
            $product = Product::create([
                'type' => 'normal',
                'title' => request('title'),
                'slug' => str_slug(request('title')),
                'specification' => request('specification'),
                'description' => request()->has('description') && request()->filled('description') ? request('description') : null,
                'additional_information' => request()->has('additional_information') && request()->filled('additional_information') ? request('additional_information') : null,
                'price' => $price,
                'sell_price' => $sell_price,
                'product_code' => request()->filled('product_code') ? request('product_code') : null,
                'brand' => request()->filled('brand') ? request('brand') : null,
                'stock' => request()->filled('stock') ? request('stock') : 0,
                'weight' => request()->filled('weight') ? request('weight') : 0,
                'length' => request()->filled('length') ? request('length') : 0,
                'width' => request()->filled('width') ? request('width') : 0,
                'height' => request()->filled('height') ? request('height') : 0,
                'height' => request()->filled('height') ? request('height') : 0,
                'related' => request('related_product'),
                'seo_page_title' => request()->filled('seo_page_title') ? request('seo_page_title') : null,
                'seo_meta_description' => request()->filled('seo_meta_description') ? request('seo_meta_description') : null,
                'seo_meta_keywords' => request()->filled('seo_meta_keywords') ? request('seo_meta_keywords') : null,
                'seo_page_slug' => request()->filled('seo_page_slug') ? request('seo_page_slug') : null,
                'featured' => request()->has('featured') ? true : false,
                'best_seller' => request()->has('best_seller') ? true : false,
                'top_rated' => request()->has('top_rated') ? true : false,
                'new' => request()->has('new') ? true : false,
                'allow_back_order' => request()->has('allow_back_order') ? true : false,
                'ignore_stock' => request()->has('ignore_stock') ? true : false,
                'published' => request()->has('published') ? true : false,
                'ordinal_number' => ($lastOrdinalNumber + 1)
            ]);

            $product->categories()->attach($categories);

            if (request()->has('tags') && count(request('tags')) > 0) {
                foreach (request('tags') as $tag) {
                    $collectionTag = ProductTag::updateOrCreate(
                        [
                            'name' => strtolower(str_slug($tag, '_')),
                        ],
                        [
                            'title' => ucwords($tag)
                        ]
                    );

                    $product->tags()->attach($collectionTag);
                }
            }

            foreach (request('images') as $image) {
                $fileName = Str::random(40);
                $defaultTitle = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);

                $this->handleUploadImage($image, $fileName, $product);

                array_push($imagesUpload, ['caption' => $defaultTitle, 'filename' => $fileName]);
            }

            $product->images()->createMany($imagesUpload);

            if (request('type') === 'agregator') {
            }
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }

        DB::commit();

        return redirect($this->adminUrl . '/products');
    }

    public function productImages($id)
    {
        $product = Product::whereId($id)->first();

        if (!$product) {
            throw ValidationException::withMessages([
                'product' => 'Product not Found',
            ]);
        }

        $productImages = $product->images->makeVisible(['downloadUrl', 'size', 'caption', 'filetype', 'url'])->toArray();

        return response()->json([
            'preview' => array_column($productImages, 'imageUrl'),
            'config' => $productImages
        ]);
    }

    public function productUploadImage($id)
    {
        request()->validate([
            'images' => 'required|array',
            'images.*' => 'mimes:jpg,jpeg,png',
        ]);

        $product = Product::whereId($id)->first();

        if (!$product) {
            throw ValidationException::withMessages([
                'product' => 'Product not Found',
            ]);
        }

        $imagesUpload = [];

        foreach (request('images') as $image) {
            $fileName = Str::random(40);
            $defaultTitle = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);

            $this->handleUploadImage($image, $fileName, $product);

            array_push($imagesUpload, ['caption' => $defaultTitle, 'filename' => $fileName]);
        }

        DB::beginTransaction();

        try {
            $product->images()->createMany($imagesUpload);
        } catch (\Exception $e) {
            DB::rollback();

            throw ValidationException::withMessages([
                'query_result' => 'Query Failed',
            ]);
        }

        DB::commit();

        return response()->json([
            'data' => $product->images()->latest()->first()->makeVisible(['downloadUrl', 'size', 'caption', 'filetype', 'url'])->toArray(),
            'message' => 'success upload image'
        ]);
    }

    protected function handleUploadImage($image, $fileName, $product)
    {
        $imageOriginal = Image::make($image);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-full.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-full', $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-large.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-large', $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginal->resize(768, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-medium.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-medium', $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginal->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-thumbnail.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put('images/product_images/' . $product->id . "/$fileName/$fileName" . '-thumbnail', $imageOriginal->encode('webp')->__toString(), 'public');
    }

    public function productDeleteImage($id)
    {
        $image = ProductImage::whereId($id)->first();

        if (!$image) {
            throw ValidationException::withMessages([
                'image' => 'Image not Found',
            ]);
        }

        if ($image->product->images()->count() === 1) {
            throw ValidationException::withMessages([
                'image' => 'Cannot delete image, at least 1 image on each product.',
            ]);
        }

        DB::beginTransaction();

        try {
            $image->delete();
        } catch (\Exception $e) {
            DB::rollback();

            throw ValidationException::withMessages([
                'query_result' => 'Query Failed',
            ]);
        }

        DB::commit();
        Storage::deleteDirectory('images/product_images/' . $image->product->id . '/' . $image->filename);

        return response()->json('success delete image');
    }

    public function productDownloadImage($id)
    {
        $image = ProductImage::whereId($id)->first();

        if (!$image) {
            throw ValidationException::withMessages([
                'image' => 'Image not Found',
            ]);
        }

        return Storage::download('images/product_images/' . $image->product->id . '/' . $image->filename . '/' . $image->filename . '-full.jpg', $image->caption);
    }

    public function show(Product $product)
    {
        return view('admin.pages.products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $categories = ProductCategories::all();
        $products = Product::where('id', '<>', $product->id)->where('published', true)->get();
        $product_tags = ProductTag::all();

        return view('admin.pages.products.edit_normal', compact('product', 'categories', 'products', 'product_tags'));

        // if(!request()->has('type') || request('type') === 'normal') {
        //     return view('admin.pages.products.edit_normal', compact('product', 'categories', 'products', 'product_tags'));
        // }else {
        //     return view('admin.pages.products.edit_agregator', compact('product', 'categories', 'products', 'product_tags'));
        // }
    }

    public function update(Product $product)
    {
        request()->validate([
            'title' => 'required|string|unique:products,title,' . $product->id . ',id,deleted_at,NULL',
            'seo_page_slug' => 'nullable|alpha_dash|unique:products,seo_page_slug,' . $product->id . ',id,deleted_at,NULL',
            'specification' => 'required',
            'weight' => 'required|numeric',
            'categories' => 'required|array',
            'categories.*' => 'required',
            'price' => 'required',
            'sell_price' => 'required',
            'type' => 'required|in:normal'// normal or agregator
        ]);

        $categories = ProductCategories::whereIn('id', request('categories'))->pluck('id')->toArray();

        $imagesUpload = [];

        DB::beginTransaction();

        $price = (int) preg_replace('/[^0-9]/m', '', request('price'));
        $sell_price = (int) preg_replace('/[^0-9]/m', '', request('sell_price'));

        try {
            $product->update([
                'title' => request('title'),
                'slug' => str_slug(request('title')),
                'specification' => request('specification'),
                'description' => request()->has('description') && request()->filled('description') ? request('description') : null,
                'additional_information' => request()->has('additional_information') && request()->filled('additional_information') ? request('additional_information') : null,
                'specification' => request('specification'),
                'price' => $price,
                'sell_price' => $sell_price,
                'product_code' => request()->filled('product_code') ? request('product_code') : null,
                'brand' => request()->filled('brand') ? request('brand') : null,
                'stock' => request()->filled('stock') ? request('stock') : 0,
                'weight' => request()->filled('weight') ? request('weight') : 0,
                'length' => request()->filled('length') ? request('length') : 0,
                'width' => request()->filled('width') ? request('width') : 0,
                'height' => request()->filled('height') ? request('height') : 0,
                'related' => request('related_product'),
                'seo_page_title' => request()->filled('seo_page_title') ? request('seo_page_title') : null,
                'seo_meta_description' => request()->filled('seo_meta_description') ? request('seo_meta_description') : null,
                'seo_meta_keywords' => request()->filled('seo_meta_keywords') ? request('seo_meta_keywords') : null,
                'seo_page_slug' => request()->filled('seo_page_slug') ? request('seo_page_slug') : null,
                'featured' => request()->has('featured') ? true : false,
                'best_seller' => request()->has('best_seller') ? true : false,
                'top_rated' => request()->has('top_rated') ? true : false,
                'new' => request()->has('new') ? true : false,
                'allow_back_order' => request()->has('allow_back_order') ? true : false,
                'ignore_stock' => request()->has('ignore_stock') ? true : false,
                'published' => request()->has('published') ? true : false,
            ]);

            $product->categories()->sync($categories);

            $tags = [];

            if (request()->has('tags') && count(request('tags')) > 0) {
                foreach (request('tags') as $tag) {
                    $collectionTag = ProductTag::updateOrCreate(
                        [
                            'name' => strtolower(str_slug($tag, '_')),
                        ],
                        [
                            'title' => ucwords($tag)
                        ]
                    );

                    array_push($tags, $collectionTag);
                }

                $product->tags()->sync(array_column($tags, 'id'));
            }
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }

        DB::commit();

        return redirect($this->adminUrl . '/products');
    }

    public function sortProduct()
    {
        $products = Product::orderBy('ordinal_number', 'asc')->get();

        return view('admin.pages.products.products_sort', compact('products'));
    }

    public function updateSortProduct()
    {
        $storeOrder = [];

        foreach (request('product_sorted') as $key => $value) {
            array_push($storeOrder, "WHEN id = $value THEN $key ");
        }

        DB::beginTransaction();

        try {
            DB::statement(DB::raw('UPDATE products SET ordinal_number = CASE ' . implode('', $storeOrder) . ' ELSE ordinal_number END'));
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json('failed', 500);
        }

        DB::commit();

        return response()->json('success');
    }

    public function destroy(Product $product)
    {
        $product->categories()->detach();
        $product->tags()->detach();
        $product->images()->delete();
        $product->delete();

        return redirect($this->adminUrl . '/products');
    }

    public function publishProduct(Product $product)
    {
        $product->update(['published' => !$product->published]);
        return redirect($this->adminUrl . '/products');
    }

    public function publishAllProduct()
    {
        $product_id = explode(',', request('product_id'));
        Product::whereIn('id', $product_id)
        ->update(['published' => request('status')]);
        return redirect($this->adminUrl . '/products');
    }

    public function getData($id)
    {
        $product = Product::find($id);
        $data = $product->images->makeVisible(['downloadUrl', 'size', 'caption', 'filetype', 'url']);
        $products = ProductImage::where('product_id',$id)->orderBy('order')->get();

        return view('admin.pages.products.sorting-image', compact('data','product','products'));
    }

    public function sortingImage($code, Request $request)
    {
        $id = $request->id;
        $order = $request->order;
        $count = count($request->id);
        
        for ($i=0; $i < $count; $i++) { 
            ProductImage::where('id', $id[$i])->update([
                'order' => $order[$i]
            ]);
        }
        return redirect()->back();
    }
}
