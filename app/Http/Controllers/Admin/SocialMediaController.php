<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\SocialMedia;
use DB;
use Yajra\DataTables\Datatables;

class SocialMediaController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.socials.index');
    }

    public function socialDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $socials = SocialMedia::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'link', 'vendor', 'created_at')->get();

        $datatables = Datatables::of($socials);

        return $datatables
                ->addColumn('actions', function ($social) {
                    return '
                        <form id="delete-row-' . $social->id . '" action="' . url($this->adminUrl . '/socials', $social->id) . '" method="POST">' . csrf_field() . method_field('DELETE') . '</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="' . url($this->adminUrl . '/socials', $social->id) . '/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-' . $social->id . '\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('link', function ($social) {
                    return '<a href="' . $social->link . '" target="_blank">' . $social->link . '</a>';
                })
                ->rawColumns(['link', 'actions'])
                ->make(true);
    }

    public function create()
    {
        return view('admin.pages.socials.create');
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'link' => 'required|url',
            'vendor' => 'required',
        ]);

        SocialMedia::create([
            'title' => request('title'),
            'link' => request('link'),
            'vendor' => request('vendor'),
        ]);

        return redirect($this->adminUrl . '/socials');
    }

    public function edit(SocialMedia $social)
    {
        return view('admin.pages.socials.edit', compact('social'));
    }

    public function update(SocialMedia $social)
    {
        request()->validate([
            'title' => 'required',
            'link' => 'required|url',
            'vendor' => 'required',
        ]);

        $social->update([
            'title' => request('title'),
            'link' => request('link'),
            'vendor' => request('vendor'),
        ]);

        return redirect($this->adminUrl . '/socials');
    }

    public function destroy(SocialMedia $social)
    {
        $social->delete();

        return redirect($this->adminUrl . '/socials');
    }
}
