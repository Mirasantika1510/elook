<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Companies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class SettingController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function viewSettings()
    {
        $companies = Companies::Find(1);

        return view('admin.pages.settings.index', compact('companies'));
    }

    public function updateCompanySettings()
    {
        $validator = Validator::make(request()->all(), [
            'title'                     => 'required',
            'email'                     => 'required',
            'phone'                     => 'required',
            'address'                   => 'required',
            'instagram_access_key'      => 'required',
            'mini_notification_text'    => 'required',
            'logo'      	            => 'nullable|mimes:jpg,jpeg,png',
            'pop_up_image'              => 'nullable|mimes:jpg,jpeg,png',
        ]);

        if ($validator->fails()) {
            session()->flash('error-companies', true);
            return redirect($this->adminUrl.'/companies-settings')
                        ->withErrors($validator)
                        ->withInput();
        }

        $companies      = Companies::Find(1);

        if(request()->file('logo')) {

            $imageOriginal  = Image::make(request('logo'));

            $fileName       = request()->file('logo')->hashName();

            Storage::put("images/companies/$fileName", $imageOriginal->encode('jpg')->__toString(), 'public');

        }

        if(request()->file('pop_up_image')) {

            $imageOriginal  = Image::make(request('pop_up_image'));

            $fileNamePopUp  = request()->file('pop_up_image')->hashName();

            Storage::put("images/companies/$fileNamePopUp", $imageOriginal->encode('jpg')->__toString(), 'public');

        }

        $companies->update([
            'title'                     => request('title'),
            'email'                     => request('email'),
            'phone'                     => request('phone'),
            'address'                   => request('address'),
            'instagram_access_key'      => request('instagram_access_key'),
            'mini_notification_text'    => request('mini_notification_text'),
            'logo'                      => request()->file('logo')? $fileName : $companies->getRawOriginal('logo'),
            'pop_up_image'              => request()->file('pop_up_image')? $fileNamePopUp : $companies->getRawOriginal('pop_up_image')
        ]);

        return redirect($this->adminUrl.'/companies-settings');

    }

    public function updateBannerSettings()
    {
        $validator = Validator::make(request()->all(), [
            'tracking_order_title_page'         => 'required',
            'tracking_order_banner_image'       => 'nullable|mimes:jpg,jpeg,png',
            'confirmation_payment_title_page'   => 'required',
            'confirmation_payment_banner_image' => 'nullable|mimes:jpg,jpeg,png',
            'contact_title_page'                => 'required',
            'contact_banner_image'              => 'nullable|mimes:jpg,jpeg,png',
        ]);

        if ($validator->fails()) {
            session()->flash('error-banner', true);
            return redirect($this->adminUrl.'/banner-settings')
                        ->withErrors($validator)
                        ->withInput();
        }

        $companies      = Companies::Find(1);

        if(request()->file('tracking_order_banner_image')) {

            $imageOriginalTracking  = Image::make(request('tracking_order_banner_image'));

            $fileNameTracking       = request()->file('tracking_order_banner_image')->hashName();

            Storage::put("images/banners/$fileNameTracking", $imageOriginalTracking->encode('jpg')->__toString(), 'public');

        }

        if(request()->file('confirmation_payment_banner_image')) {

            $imageOriginalConfirmation  = Image::make(request('confirmation_payment_banner_image'));

            $fileNameConfirmation       = request()->file('confirmation_payment_banner_image')->hashName();

            Storage::put("images/banners/$fileNameConfirmation", $imageOriginalConfirmation->encode('jpg')->__toString(), 'public');

        }

        if(request()->file('contact_banner_image')) {

            $imageOriginalContact  = Image::make(request('contact_banner_image'));

            $fileNameContact       = request()->file('contact_banner_image')->hashName();

            Storage::put("images/banners/$fileNameContact", $imageOriginalContact->encode('jpg')->__toString(), 'public');

        }


        $companies->update([
            'tracking_order_title_page'         => request('tracking_order_title_page'),
            'confirmation_payment_title_page'   => request('confirmation_payment_title_page'),
            'contact_title_page'                => request('contact_title_page'),
            'tracking_order_banner_image'       => request()->file('tracking_order_banner_image')? $fileNameTracking : $companies->getRawOriginal('tracking_order_banner_image'),
            'confirmation_payment_banner_image' => request()->file('confirmation_payment_banner_image')? $fileNameConfirmation : $companies->getRawOriginal('confirmation_payment_banner_image'),
            'contact_banner_image'              => request()->file('contact_banner_image')? $fileNameContact : $companies->getRawOriginal('contact_banner_image')
        ]);

        return redirect($this->adminUrl.'/banner-settings');

    }
}
