<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Model\Order;
use App\Model\OrderProducts;
use App\Model\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Datatables;

class ReportController extends Controller
{
    public function monthlySellDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $orders = Order::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'created_at',
                        DB::raw('count(id) as total_sells'),
                        DB::raw('SUM(shipping_price) as total_shipping_price'),
                        DB::raw('SUM(total_price+unique_number) as total_price'),
                        DB::raw('MONTH(created_at) month'),
                        DB::raw('YEAR(created_at) year')
                    ])
                    ->selectRaw('(SELECT SUM(quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND month = MONTH(orders_products.created_at) AND year = YEAR(orders_products.created_at)) as total_items')
                    ->selectRaw('(SELECT SUM(CASE WHEN orders_products.product_discount IS NULL THEN 0 ELSE ((orders_products.product_price*orders_products.product_discount/100) * orders_products.quantities) END) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND month = MONTH(orders_products.created_at) AND year = YEAR(orders_products.created_at)) AS total_discount')
                    ->whereHas('payment', function($q){
                        $q->where('transaction_status', 'settlement');
                    })
                    ->groupBy(['month', 'year'])
                    ->orderBy('created_at', 'asc')
                    ->get();

        $datatables = Datatables::of($orders);

        return $datatables
                ->addColumn('date', function($order){
                    return $order->year . ' ' . date("F", mktime(0, 0, 0, $order->month, 1));
                })
                ->make(true);
    }

    public function bestSellingProductsDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $products   = DB::table('products')->select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'title',
                        DB::raw('(stock) as total_inventory')
                    ])
                    ->selectRaw('(SELECT SUM(orders_products.quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND orders_products.product_id = products.id) as total_quantity_sold')
                    ->selectRaw('(SELECT COUNT(*) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status <> "settlement") AND orders_products.product_id = products.id) as total_distinct_order')
                    ->selectRaw('(SELECT SUM(CASE WHEN orders_products.product_discount IS NULL THEN (orders_products.product_price*orders_products.quantities) ELSE ((orders_products.product_price-(orders_products.product_price*orders_products.product_discount/100)) * orders_products.quantities) END) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND orders_products.product_id = products.id) as total_revenue')
                    ->selectRaw('(SELECT (stock - SUM(orders_products.quantities)) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status IN("settlement", "pending")) AND orders_products.product_id = products.id) as instock_inventory')
                    ->orderBy('total_quantity_sold', 'desc')
                    ->orderBy('total_inventory', 'desc')
                    ->get();

        $datatables = Datatables::of($products);

        return $datatables
                ->editColumn('instock_inventory', function($order){
                    return $order->instock_inventory?: 0;
                })
                ->editColumn('total_revenue', function($order){
                    return $order->total_revenue?: 0;
                })
                ->editColumn('total_quantity_sold', function($order){
                    return $order->total_quantity_sold?: 0;
                })
                ->make(true);
    }

    public function bestMemberDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $products   = Customer::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'email'
                    ])
                    ->selectRaw('(SELECT SUM(total_price+unique_number) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_order_amount')
                    ->selectRaw('(SELECT COUNT(*) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_order')
                    ->selectRaw('(SELECT SUM(shipping_price) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_shipping_cost')
                    ->selectRaw('(SELECT SUM((SELECT SUM(CASE WHEN orders_products.product_discount IS NULL THEN 0 ELSE ((orders_products.product_price*orders_products.product_discount/100) * orders_products.quantities) END) FROM orders_products WHERE orders_products.order_id = orders.id)) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_discount')
                    ->selectRaw('(SELECT orders.created_at FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id ORDER BY orders.created_at DESC LIMIT 1) as last_order_date')
                    ->get();

        $datatables = Datatables::of($products);

        return $datatables
                ->editColumn('total_order_amount', function($order){
                    return $order->total_order_amount?: 0;
                })
                ->editColumn('total_shipping_cost', function($order){
                    return $order->total_shipping_cost?: 0;
                })
                ->editColumn('total_discount', function($order){
                    return $order->total_discount?: 0;
                })
                ->editColumn('last_order_date', function($order){
                    return $order->last_order_date? Carbon::parse($order->last_order_date)->format('Y-m-d') : '-';
                })
                ->make(true);
    }

    public function waitingListDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $products   = OrderProducts::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'product_name'
                    ])
                    ->selectRaw('COUNT(id) as total_waiting_list')
                    ->orderBy('total_waiting_list', 'desc')
                    ->whereHas('order', function($q){
                        $q->whereHas('customer');
                        $q->whereHas('payment', function($qr){
                            $qr->where('transaction_status', 'pending');
                        });
                    })
                    ->groupBy('product_id')
                    ->orderBy('total_waiting_list', 'desc')
                    ->get();

        $datatables = Datatables::of($products);

        return $datatables->make(true);
    }

    public function abandonedCartDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $orders = Order::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                    ])
                    ->whereHas('payment', function($q){
                        $q->where('transaction_status', 'pending');
                    })
                    ->with(['products' => function($q){
                        $q->select('id', 'order_id', 'product_name', 'quantities');
                    }])
                    ->with(['shipping' => function($q){
                        $q->select('id', 'order_id', 'email');
                    }])
                    ->get();

        $datatables = Datatables::of($orders);

        return $datatables
                    ->addColumn('items', function($order){
                        $html = '<ul>';

                        foreach ($order->products as $product) {
                            $html .= '<li>'.$product->quantities.'x '. $product->product_name. '</li>';
                        }

                        $html .= '</ul>';

                        return $html;
                    })
                    ->rawColumns(['items'])
                    ->make(true);
    }

    public function inventoryDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $products   = DB::table('products')->select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'title',
                        DB::raw('(stock) as total_inventory')
                    ])
                    ->selectRaw('(SELECT SUM(orders_products.quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "pending") AND orders_products.product_id = products.id) as on_hold_inventory')
                    ->selectRaw('(SELECT SUM(orders_products.quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND orders_products.product_id = products.id) as sold_inventory')
                    ->selectRaw('(SELECT (stock - SUM(orders_products.quantities)) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status IN("settlement", "pending")) AND orders_products.product_id = products.id) as instock_inventory')
                    ->orderBy('total_inventory', 'desc')
                    ->get();

        $datatables = Datatables::of($products);

        return $datatables
                ->editColumn('instock_inventory', function($order){
                    return $order->instock_inventory?: 0;
                })
                ->editColumn('on_hold_inventory', function($order){
                    return $order->on_hold_inventory?: 0;
                })
                ->editColumn('sold_inventory', function($order){
                    return $order->sold_inventory?: 0;
                })
                ->make(true);
    }

    public function packingListDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $orders = Order::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                    ])
                    ->whereHas('payment', function($q){
                        $q->where('transaction_status', 'settlement');
                    })
                    ->with(['products' => function($q){
                        $q->select('id', 'order_id', 'product_name', 'quantities');
                    }])
                    ->with(['shipping' => function($q){
                        $q->select(
                            'id',
                            'order_id',
                            'email',
                            'firstname',
                            'lastname',
                            'subdistrict',
                            'address',
                            'province',
                            'city',
                            'postcode',
                            'phonenumber');
                    }])
                    ->where('order_status','packing')
                    ->get();

        $datatables = Datatables::of($orders);

        return $datatables
                    ->addColumn('items', function($order){
                        $html = '<ul>';

                        foreach ($order->products as $product) {
                            $html .= '<li>'.$product->quantities.'x '. $product->product_name. '</li>';
                        }

                        $html .= '</ul>';

                        return $html;
                    })
                    ->addColumn('name',function($order){
                        return $order->shipping->firstname. ' ' .$order->shipping->lastname ;
                    })
                    ->addColumn('phonenumber',function($order){
                        return $order->shipping->phonenumber;
                    })
                    ->addColumn('address',function($order){
                        return $order->shipping->address.', '.$order->shipping->subdistrict. ', ' .$order->shipping->city. ', ' .$order->shipping->province. ', ' .$order->shipping->postcode ;
                    })
                    ->rawColumns(['items','name','phonenumber','address'])
                    ->make(true);
    }

}
