<?php

namespace App\Http\Controllers\Admin;

use DB, Storage;
use App\Model\Product;
use App\Model\Lookbook;
use Yajra\DataTables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class LookbookController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.lookbooks.index');
    }

    public function lookbookDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $lookbooks  = Lookbook::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'description', 'image', 'created_at')->get();

        $datatables = Datatables::of($lookbooks);

        return $datatables
                ->addColumn('actions', function ($lookbook) {
                     return '
                        <form id="delete-row-'.$lookbook->id.'" action="'.url($this->adminUrl.'/lookbooks', $lookbook->id).'" method="POST">'.csrf_field().method_field("DELETE").'</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="'.url($this->adminUrl.'/lookbooks', $lookbook->id).'/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-'.$lookbook->id.'\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('description', function ($lookbook) {
                    return $lookbook->description? \Illuminate\Support\Str::words($lookbook->description, 20, '...') : '---' ;
                })
                ->editColumn('image', function ($lookbook) {
                    return '<img src="'.$lookbook->image.'" alt="" width="100">';
                })
                ->rawColumns(['image', 'description', 'actions'])
                ->make(true);
    }


    public function create()
    {
        $products       = Product::all();

        return view('admin.pages.lookbooks.create', compact('products'));
    }


    public function store()
    {
        request()->validate([
            'title'	            => 'required',
            'description'	    => 'required',
            'products'	        => 'required|array|min:1',
            'image'      	    => 'required|mimes:jpg,jpeg,png',
            'banner'            => 'required|mimes:jpg,jpeg,png',
        ]);

        $imageOriginal = Image::make(request('image'));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/lookbooks/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/lookbooks/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/lookbooks/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/lookbooks/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginalBanner  = Image::make(request('banner'));

        $fileNameBanner       = str_random(40);

        Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
        Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

        Lookbook::create([

            'title' 	        => request('title'),
            'slug' 	            => str_slug(request('title')),
            'description' 	    => request('description'),
            'products' 	        => request('products'),
            'image' 		    => $fileName,
            'banner'            => $fileNameBanner

        ]);

        return redirect($this->adminUrl.'/lookbooks');
    }


    public function edit(Lookbook $lookbook)
    {
        $products       = Product::all();
        
        return view('admin.pages.lookbooks.edit', compact('lookbook', 'products'));
    }


    public function update(Lookbook $lookbook)
    {
        request()->validate([
            'title'	            => 'required',
            'description'	    => 'required',
            'products'	        => 'required|array|min:1',
            'image'             => 'nullable|mimes:jpeg,jpg,png',
            'banner'            => 'nullable|mimes:jpeg,jpg,png',
        ]);

        $dataRequest = [

            'title' 	        => request('title'),
            'slug' 	            => str_slug(request('title')),
            'description' 	    => request('description'),
            'products' 	        => request('products'),

        ];

        if(request()->has('image')){

        	$imageOriginal = Image::make(request('image'));

	        $fileName       = str_random(40);

	        $imageOriginal->resize(1366, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/lookbooks/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/lookbooks/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


	        $imageOriginal->resize(1024, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/lookbooks/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/lookbooks/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');
            
            $dataRequest = collect($dataRequest)->merge(['image' => $fileName])->all();

        }

        if(request()->has('banner')) {
            $imageOriginalBanner  = Image::make(request('banner'));

            $fileNameBanner       = str_random(40);

            Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
            Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

            $dataRequest = collect($dataRequest)->merge(['banner' => $fileNameBanner])->all();
        }

        $lookbook->update($dataRequest);

        return redirect($this->adminUrl.'/lookbooks');
    }


    public function destroy(Lookbook $lookbook)
    {
        $lookbook->delete();

        return redirect($this->adminUrl.'/lookbooks');
    }
}
