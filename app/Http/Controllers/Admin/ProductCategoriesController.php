<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\ProductCategories;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Datatables;
use DB;
use Storage;

class ProductCategoriesController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.product_categories.index');
    }

    public function categoriesDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $categories = ProductCategories::select('*')->selectRaw('@rownum  := @rownum  + 1 AS rownum')->get();

        $datatables = Datatables::of($categories);

        return $datatables
                ->addColumn('actions', function ($category) {
                    $icon = $category->highlight ? 'icon-close2' : 'icon-checkmark-circle2';
                    $status = $category->highlight ? 'Ordonary' : 'Highlight';
                    return '
                        <form id="delete-row-' . $category->id . '" action="' . url($this->adminUrl . '/product-categories', $category->id) . '" method="POST">' . csrf_field() . method_field('DELETE') . '</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a  href="' . url($this->adminUrl . '/product-categories', $category->id) . '/highlight" class="dropdown-item"><i class="'.$icon.'" title="Delete"></i> '.$status.'</a>
                                        <a href="' . url($this->adminUrl . '/product-categories', $category->id) . '/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-' . $category->id . '\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('description', function ($category) {
                    return $category->description ? \Illuminate\Support\Str::words($category->description, 20, '...') : '---' ;
                })
                ->addColumn('ancestor', function ($category) {
                    if ($category->ancestors->toFlatTree()->count()) {
                        return '<ul style="padding-left:0px;">' .
                            implode($category->ancestors->toFlatTree()->map(function ($item) {
                                return '<li>' . $item->title . '</li>';
                            })->toArray())
                        . '</ul>';
                    } else {
                        return '---';
                    }
                })
                ->editColumn('image', function ($category) {
                    return '<img src="' . $category->image . '" alt="" width="100">';
                })
                ->rawColumns(['image', 'ancestor', 'actions'])
                ->make(true);
    }

    public function create()
    {
        $categories = ProductCategories::all();

        return view('admin.pages.product_categories.create', compact('categories'));
    }

    public function edit(ProductCategories $product_category)
    {
        $categories = ProductCategories::where('id', '<>', $product_category->id)->get();

        return view('admin.pages.product_categories.edit', compact('product_category', 'categories'));
    }

    public function store()
    {
        request()->validate([
            'title' => 'required|string|unique:product_categories,title,NULL,id,deleted_at,NULL',
            'image' => 'required|mimes:jpg,jpeg,png',
            'banner' => 'required|mimes:jpg,jpeg,png',
        ]);

        $imageName = str_random(40);
        $imageOriginal = Image::make(request('image'));

        Storage::put("images/product_categories/$imageName.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_categories/$imageName", $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginalBanner = Image::make(request('banner'));

        $fileNameBanner = str_random(40);

        Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
        Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

        $dataRequest = [
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'description' => request()->filled('description') ? request('description') : null,
            'image' => $imageName,
            'banner' => $fileNameBanner
        ];

        if (request()->filled('parent')) {
            $node = ProductCategories::whereId(request('parent'))->firstOrFail();

            $newNode = $node->children()->create($dataRequest);
        } else {
            // if (ProductCategories::whereNull('parent_id')->count() === 4) {
            //     throw ValidationException::withMessages([
            //         'limit_categories' => 'You have meet maximum limit categories for root parents.'
            //     ]);
            // }

            $newNode = ProductCategories::create($dataRequest);
        }

        return redirect($this->adminUrl . '/product-categories');
    }

    public function update(ProductCategories $product_category)
    {
        request()->validate([
            'title' => 'required|string|unique:product_categories,title,' . $product_category->id . ',id,deleted_at,NULL',
            'image' => 'nullable|mimes:jpg,jpeg,png',
            'banner' => 'nullable|mimes:jpeg,jpg,png',
        ]);

        $dataRequest = [
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'description' => request()->filled('description') ? request('description') : null,
        ];

        if (request()->has('image')) {
            $imageName = str_random(40);
            $imageOriginal = Image::make(request('image'));

            Storage::put("images/product_categories/$imageName.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
            Storage::put("images/product_categories/$imageName", $imageOriginal->encode('webp')->__toString(), 'public');

            $dataRequest = collect($dataRequest)->merge(['image' => $imageName])->all();
        }

        if (request()->has('banner')) {
            $imageOriginalBanner = Image::make(request('banner'));

            $fileNameBanner = str_random(40);

            Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
            Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

            $dataRequest = collect($dataRequest)->merge(['banner' => $fileNameBanner])->all();
        }

        if (request()->filled('parent')) {
            $node = ProductCategories::whereId(request('parent'))->firstOrFail();
            $dataRequest = collect($dataRequest)->merge(['parent_id' => $node->id])->all();
        } else {
            $dataRequest = collect($dataRequest)->merge(['parent_id' => null])->all();
        }

        $product_category->update($dataRequest);

        ProductCategories::fixTree();

        return redirect($this->adminUrl . '/product-categories');
    }

    public function destroy(ProductCategories $product_category)
    {
        $meAndDescendants = ProductCategories::descendantsAndSelf($product_category);

        foreach ($meAndDescendants as $descendants) {
            $descendants->products()->detach();
            $descendants->delete();
        }

        return redirect($this->adminUrl . '/product-categories');
    }
    
    public function highlight(ProductCategories $product_categories)
    {
        if ($product_categories->highlight == 1) {
         $product_categories->update([
            'highlight' => !$product_categories->highlight
        ]);
        } else {
            if (ProductCategories::whereNull('parent_id')->where('highlight', 1)->count() === 4) {
                throw ValidationException::withMessages([
                    'limit_categories' => 'You have meet maximum limit categories for highlights.'
                ]);
            } else {
                $product_categories->update([
                    'highlight' => !$product_categories->highlight
                ]);
            }
        }

        return redirect($this->adminUrl . '/product-categories');
    }
}
