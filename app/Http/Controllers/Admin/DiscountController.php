<?php

namespace App\Http\Controllers\Admin;

use DB;
use Storage;
use App\Model\Product;
use App\Model\Discount;
use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

class DiscountController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.discounts.index');
    }

    public function discountDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $discounts = Discount::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'discount_percent', 'description', 'image', 'published', 'created_at')->get();

        $datatables = Datatables::of($discounts);

        return $datatables
                ->addColumn('actions', function ($discount) {
                    return '
                        <form id="delete-row-' . $discount->id . '" action="' . url($this->adminUrl . '/discounts', $discount->id) . '" method="POST">' . csrf_field() . method_field('DELETE') . '</form>
                        <form id="publish-row-' . $discount->id . '" action="' . url($this->adminUrl . '/discounts-publish', $discount->id) . '" method="POST">' . csrf_field() . method_field('PUT') . '</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="' . url($this->adminUrl . '/discounts', $discount->id) . '/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-' . $discount->id . '\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'publish-row-' . $discount->id . '\').submit();" class="dropdown-item"><i class="icon-database-add" title="' . ($discount->published ? 'Unpublish' : 'Publish') . '"></i> ' . ($discount->published ? 'Unpublish' : 'Publish') . '</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('description', function ($discount) {
                    return $discount->description ? \Illuminate\Support\Str::words($discount->description, 20, '...') : '---' ;
                })
                ->editColumn('image', function ($discount) {
                    return '<img src="' . $discount->image . '" alt="" width="100">';
                })
                ->editColumn('discount_percent', function ($discount) {
                    return "$discount->discount_percent %";
                })
                ->editColumn('published', function ($discount) {
                    if ($discount->published) {
                        return '<span class="badge badge-success">Published</span>';
                    } else {
                        return '<span class="badge badge-warning">Unpublished</span>';
                    }
                })
                ->addColumn('products', function ($discount) {
                    if ($discount->products()->count()) {
                        return join(', <br>', $discount->products()->pluck('title')->toArray());
                    } else {
                        return '-';
                    }
                })
                ->rawColumns(['image', 'description', 'products', 'published', 'actions'])
                ->make(true);
    }

    public function create()
    {
        $products = Product::whereDoesntHave('discount')->get();

        return view('admin.pages.discounts.create', compact('products'));
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'discount_percent' => 'required|max:100|min:1',
            'products' => 'required|array|min:1',
            'image' => 'required|mimes:jpg,jpeg,png',
            'banner' => 'required|mimes:jpg,jpeg,png',
        ]);

        $imageOriginal = Image::make(request('image'));

        $fileName = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/discounts/$fileName/$fileName" . '-full.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/discounts/$fileName/$fileName" . '-full', $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/discounts/$fileName/$fileName" . '-large.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/discounts/$fileName/$fileName" . '-large', $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginalBanner = Image::make(request('banner'));

        $fileNameBanner = str_random(40);

        Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
        Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

        $discount = Discount::create([
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'discount_percent' => request('discount_percent'),
            'description' => request('description'),
            'image' => $fileName,
            'banner' => $fileNameBanner
        ]);

        Product::whereIn('id', request('products'))->update([
            'discount_id' => $discount->id
        ]);

        return redirect($this->adminUrl . '/discounts');
    }

    public function edit(Discount $discount)
    {
        $products = Product::whereDoesntHave('discount')
        ->orWhereHas('discount', function ($q) use ($discount) {
            $q->where('id', $discount->id);
        })->get();

        return view('admin.pages.discounts.edit', compact('discount', 'products'));
    }

    public function update(Request $request, Discount $discount)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'discount_percent' => 'required|max:100|min:1',
            'products' => 'required|array|min:1',
            'image' => 'nullable|mimes:jpeg,jpg,png',
            'banner' => 'nullable|mimes:jpeg,jpg,png',
        ]);

        if (request()->has('image')) {
            $imageOriginal = Image::make(request('image'));

            $fileName = str_random(40);

            $imageOriginal->resize(1366, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put("images/discounts/$fileName/$fileName" . '-full.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
            Storage::put("images/discounts/$fileName/$fileName" . '-full', $imageOriginal->encode('webp')->__toString(), 'public');

            $imageOriginal->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put("images/discounts/$fileName/$fileName" . '-large.jpg', $imageOriginal->encode('jpg')->__toString(), 'public');
            Storage::put("images/discounts/$fileName/$fileName" . '-large', $imageOriginal->encode('webp')->__toString(), 'public');
        }

        if (request()->has('banner')) {
            $imageOriginalBanner = Image::make(request('banner'));

            $fileNameBanner = str_random(40);

            Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
            Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');
        }

        $discount->update([
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'discount_percent' => request('discount_percent'),
            'description' => request('description'),
            'image' => request()->has('image') ? $fileName : $discount->getRawOriginal('image'),
            'banner' => request()->has('banner') ? $fileNameBanner : $discount->getRawOriginal('banner'),
        ]);

        Product::whereNotIn('id', request('products'))
        ->where('discount_id', $discount->id)
        ->update([
            'discount_id' => null
        ]);

        Product::whereIn('id', request('products'))
        ->whereDoesntHave('discount')
        ->update([
            'discount_id' => $discount->id
        ]);

        return redirect($this->adminUrl . '/discounts');
    }

    public function destroy(Discount $discount)
    {
        $discount->products()->update(['discount_id' => null]);

        $discount->delete();

        return redirect($this->adminUrl . '/discounts');
    }

    public function publishDiscount(Discount $discount)
    {
        $discount->update(['published' => !$discount->published]);

        return redirect($this->adminUrl . '/discounts');
    }
}
