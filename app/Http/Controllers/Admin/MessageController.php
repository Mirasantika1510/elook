<?php

namespace App\Http\Controllers\Admin;

use App\Model\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Datatables;

class MessageController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.messages.index');
    }

    public function messageDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $messages  = Messages::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name', 'email', 'subject', 'message', 'created_at')->get();

        $datatables = Datatables::of($messages);

        return $datatables->make(true);
    }
}
