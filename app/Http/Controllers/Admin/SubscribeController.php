<?php

namespace App\Http\Controllers\Admin;

use App\Model\Subscribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Datatables;

class SubscribeController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.subscribes.index');
    }

    public function subscribeDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $subscribes  = Subscribe::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'email', 'created_at')->get();

        $datatables = Datatables::of($subscribes);

        return $datatables->make(true);
    }
}
