<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Model\Customer;
use App\Model\Order;
use App\Exports\Export;
use App\Model\OrderProducts;
use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontEnd\RajaOngkir;

class MemberController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.members.index');
    }

    public function memberDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));

        $members    = Customer::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'firstname', 'lastname', 'email', 'dob', 'created_at')
                            ->with('orders')
                            ->orderBy('created_at', 'desc')
                            ->get();

        $datatables = Datatables::of($members);

        return $datatables
                    ->addColumn('actions', function ($custom_page) {
                                 return '
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="'.url($this->adminUrl.'/members-detail', $custom_page->id).'" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Detail</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                 ';
                            })
                    ->addColumn('name', function ($member) {
                        return $member->firstname. ' ' .$member->lastname;
                    })
                    ->addColumn('has_buy', function ($member) {
                        return $member->orders()->whereHas('payment', function($query){
                            $query->where('transaction_status', 'settlement');
                        })->count() > 0? '<span class="badge badge-success">Yes</span>' : '<span class="badge badge-danger">No</span>';
                    })
                    ->addColumn('total_order', function ($member) {
                        return $member->orders()->whereHas('payment', function($query){
                            $query->where('transaction_status', 'settlement');
                        })->count();
                    })
                    ->rawColumns(['has_buy', 'actions'])
                    ->make(true);
    }

    public function detail(Customer $customer)
    {
        $check = $customer->orders()->whereHas('payment', function($query){
                            $query->where('transaction_status', 'settlement');
                        })->count();
        if ($check > 0) {
            $province       = (new RajaOngkir)->getProvince($customer->address->province);
            $city           = (new RajaOngkir)->getCity($customer->address->city);
            $subdistrict    = (new RajaOngkir)->getSubdistrict($customer->address->subdistrict, $customer->address->city);
            return view('admin.pages.members.detail', compact('customer', 'province', 'city', 'subdistrict'));
        } else {
            return view('admin.pages.members.detail_not_found');
        }
    }

    public function detailDatatables($customer)
    {
        DB::statement(DB::raw('set @rownum=0'));

        $members    = Order::where('customer_id', $customer)->whereHas('payment', function($query){
                            $query->where('transaction_status', 'settlement');
                        })->get();

        $id = array();
        foreach ($members as $key) {
            $id[] = $key->id;
        }

        $products = OrderProducts::whereIn('order_id', $id)->select('*',DB::raw('@rownum  := @rownum  + 1 AS rownum'))->get();

        $datatables = Datatables::of($products);

        return $datatables
                    ->editColumn('product_price', function ($member) {
                        return 'Rp. '.number_format($member->product_price);
                    })
                     ->editColumn('invoice_number', function ($member) {
                        return $member->order->invoice_number;
                    })
                    ->editColumn('created_at', function ($member) {
                        return Carbon::parse($member->created_at)->format('d M Y');
                    })
                    ->make(true);
    }

    public function export()
    {
       return Excel::download(new Export, 'members.xlsx');
    }
}
