<?php

namespace App\Http\Controllers\Admin;

use DB, Storage;
use App\Model\CustomPage;
use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;
use Intervention\Image\ImageManagerStatic as Image;

class CustomPageController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.custom_pages.index');
    }

    public function customPageDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $custom_pages  = CustomPage::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'description', 'url_link', 'banner', 'created_at')->get();

        $datatables = Datatables::of($custom_pages);

        return $datatables
                ->addColumn('actions', function ($custom_page) {
                     return '
                        <form id="delete-row-'.$custom_page->id.'" action="'.url($this->adminUrl.'/custom-pages', $custom_page->id).'" method="POST">'.csrf_field().method_field("DELETE").'</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="'.url($this->adminUrl.'/custom-pages', $custom_page->id).'/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-'.$custom_page->id.'\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('description', function ($custom_page) {
                    return $custom_page->description? \Illuminate\Support\Str::words($custom_page->description, 20, '...') : '---' ;
                })
                ->editColumn('banner', function ($custom_page) {
                    return '<img src="'.$custom_page->banner.'" alt="" width="100">';
                })
                ->rawColumns(['banner', 'description', 'actions'])
                ->make(true);
    }


    public function create()
    {
        return view('admin.pages.custom_pages.create');
    }


    public function store()
    {
        request()->validate([
            'title'	            => 'required',
            'url_link'          => 'required|alpha_dash|unique:custom_pages',
            'description'	    => 'required',
            'content'	        => 'required',
            'banner'      	    => 'required|mimes:jpg,jpeg,png',
        ]);

        $routes = collect(\Route::getRoutes())->map(function ($route) { return $route->uri(); })->all();
        
        if(in_array(request('url_link'), $routes)) {
            throw ValidationException::withMessages([
                'url_link' => 'Cannot use this link url, input another link',
            ]);
        }

        $imageOriginal  = Image::make(request('banner'));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/custom_pages/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/custom_pages/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/custom_pages/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/custom_pages/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        CustomPage::create([

            'title' 	        => request('title'),
            'slug' 	            => str_slug(request('title')),
            'url_link' 	        => request('url_link'),
            'description' 	    => request('description'),
            'content' 	        => request('content'),
            'banner' 		    => $fileName

        ]);

        return redirect($this->adminUrl.'/custom-pages');
    }


    public function edit(CustomPage $custom_page)
    {
        return view('admin.pages.custom_pages.edit', compact('custom_page'));
    }


    public function update(CustomPage $custom_page)
    {
        request()->validate([
            'title'	            => 'required',
            'url_link'          => 'required|alpha_dash|unique:custom_pages,url_link,'. $custom_page->id,
            'description'	    => 'required',
            'content'	        => 'required',
            'banner'            => 'nullable|mimes:jpeg,jpg,png',
        ]);

        $routes = collect(\Route::getRoutes())->map(function ($route) { return $route->uri(); })->all();
        
        if(in_array(request('url_link'), $routes)) {
            throw ValidationException::withMessages([
                'url_link' => 'Cannot use this link url, input another link',
            ]);
        }

        if(request()->has('banner')){

        	$imageOriginal = Image::make(request('banner'));

	        $fileName       = str_random(40);

	        $imageOriginal->resize(1366, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/custom_pages/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/custom_pages/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


	        $imageOriginal->resize(1024, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/custom_pages/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/custom_pages/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

	        $custom_page->update([

                'title' 	        => request('title'),
                'slug' 	            => str_slug(request('title')),
                'url_link' 	        => request('url_link'),
                'description' 	    => request('description'),
                'content' 	        => request('content'),
	            'banner'    	    => $fileName

	        ]);

	        return redirect($this->adminUrl.'/custom-pages');
        }

        $custom_page->update([

            'title' 	        => request('title'),
            'slug' 	            => str_slug(request('title')),
            'url_link' 	        => request('url_link'),
            'description' 	    => request('description'),
            'content' 	        => request('content'),

        ]);

        return redirect($this->adminUrl.'/custom-pages');
    }


    public function destroy(CustomPage $custom_page)
    {
        $custom_page->delete();

        return redirect($this->adminUrl.'/custom-pages');
    }
}
