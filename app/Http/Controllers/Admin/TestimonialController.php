<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Testimonial;
use DB, Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Datatables;

class TestimonialController extends Controller
{
    private $adminUrl;

    public function __construct()
    {
        $this->adminUrl = config('elook.admin_url');
    }

    public function index()
    {
        return view('admin.pages.testimonials.index');
    }


    public function testimonialDatatables(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $testimonials = Testimonial::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'description', 'author', 'image', 'created_at')->get();

        $datatables = Datatables::of($testimonials);

        return $datatables
                ->addColumn('actions', function ($testimonial) {
                     return '
                        <form id="delete-row-'.$testimonial->id.'" action="'.url($this->adminUrl.'/testimonials', $testimonial->id).'" method="POST">'.csrf_field().method_field("DELETE").'</form>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="'.url($this->adminUrl.'/testimonials', $testimonial->id).'/edit" class="dropdown-item"><i class="icon-database-edit2" title="Edit"></i> Edit</a>
                                        <a href="#" onClick="event.preventDefault(); document.getElementById(\'delete-row-'.$testimonial->id.'\').submit();" class="dropdown-item"><i class="icon-database-remove" title="Delete"></i> Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                     ';
                })
                ->editColumn('image', function ($testimonial) {
                    return '<img src="'.$testimonial->image.'" alt="" width="100">';
                })
                ->editColumn('description', function ($testimonial) {
                    return \Illuminate\Support\Str::words($testimonial->description, 20, '...');
                })
                ->rawColumns([ 'image', 'actions'])
                ->make(true);
    }


    public function create()
    {
        return view('admin.pages.testimonials.create');
    }


    public function store()
    {
        request()->validate([
            'title'   		=> 'required',
            'description'	=> 'required',
            'author'	    => 'required',
            'image'      	=> 'required|mimes:jpg,jpeg,png',
        ]);

        $imageOriginal = Image::make(request('image'));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        Testimonial::create([

            'title' 		=> request('title'),
            'description' 	=> request('description'),
            'author' 	    => request('author'),
            'image' 		=> $fileName

        ]);

        return redirect($this->adminUrl.'/testimonials');
    }


    public function edit(Testimonial $testimonial)
    {
        return view('admin.pages.testimonials.edit', compact('testimonial'));
    }


    public function update(Testimonial $testimonial)
    {
        request()->validate([
            'title'   		=> 'required',
            'description'	=> 'required',
            'author'	    => 'required',
            'image'         => 'nullable|mimes:jpeg,jpg,png',
        ]);

        if(request()->has('image')){

        	$imageOriginal = Image::make(request('image'));

	        $fileName       = str_random(40);

	        $imageOriginal->resize(1366, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/testimonials/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/testimonials/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


	        $imageOriginal->resize(1024, null, function ($constraint) {
	            $constraint->aspectRatio();
	            $constraint->upsize();
	        });

	        Storage::put("images/testimonials/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
	        Storage::put("images/testimonials/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

	        $testimonial->update([

	            'title' 		=> request('title'),
                'description' 	=> request('description'),
                'author' 	    => request('author'),
                'image' 		=> $fileName

	        ]);

	        return redirect($this->adminUrl.'/testimonials');
        }

        $testimonial->update([

            'title' 		=> request('title'),
            'description' 	=> request('description'),
            'author' 	    => request('author'),

        ]);

        return redirect($this->adminUrl.'/testimonials');
    }


    public function destroy(Testimonial $testimonial)
    {
        $testimonial->delete();

        return redirect($this->adminUrl.'/testimonials');
    }
}
