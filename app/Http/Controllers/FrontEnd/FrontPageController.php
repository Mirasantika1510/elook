<?php

namespace App\Http\Controllers\FrontEnd;

use Carbon\Carbon;
use App\Model\Order;
use App\Model\Slider;
use App\Model\Product;
use App\Model\Discount;
use App\Model\Lookbook;
use App\Model\Companies;
use App\Model\Testimonial;
use App\Http\Controllers\FrontEnd\RajaOngkir;
use Illuminate\Http\Request;
use App\Model\PaymentAccount;
use App\Model\ProductCategories;
use App\Model\Customer;
use App\Model\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Intervention\Image\ImageManagerStatic as Image;

class FrontPageController extends Controller
{
    public function index(Request $request)
    {
        $this->updateCartTotal();

        $sliders = Slider::all();
        $testimonials = Testimonial::all();
        $categories = ProductCategories::whereIsRoot()->where('highlight', true)->get();

        $productsBestSell = Product::whereHas('categories')
        ->where('published', true)
        ->where('best_seller', true)
        ->orderBy('id', 'asc')
        ->with('publishedDiscount')
        ->with('images')
        ->get();

        $productsFeatured = Product::whereHas('categories')
        ->where('published', true)
        ->where('featured', true)
        ->orderBy('ordinal_number', 'asc')
        ->with('publishedDiscount')
        ->with('images')
        ->get();

        $productsSale = Product::whereHas('categories')->whereHas('discount', function ($query) {
            $query->where('published', true);
        })
        ->where('published', true)
        ->orderBy('ordinal_number', 'asc')
        ->with('publishedDiscount')
        ->with('images')
        ->get();

        $productsTopRated = Product::whereHas('categories')
        ->where('published', true)
        ->where('top_rated', true)
        ->orderBy('ordinal_number', 'asc')
        ->with('publishedDiscount')
        ->with('images')
        ->get();

        $latestLastMonthLookbook = Lookbook::whereMonth('created_at', Carbon::now('Asia/Jakarta')->subMonth()->month)->latest()->first();
        $lastLookbook = Lookbook::whereMonth('created_at', Carbon::now('Asia/Jakarta')->month)->latest()->first();
        $latestDiscount = Discount::where('published', true)->latest()->first();

        $instagrams = $this->instagram();


        return view('frontend.index', compact('productsBestSell', 'productsFeatured', 'productsSale', 'productsTopRated', 'testimonials', 'categories', 'sliders', 'instagrams', 'latestLastMonthLookbook', 'lastLookbook', 'latestDiscount'));
    }

    public function detailProduct($slug)
    {
        $this->updateCartTotal();
        $order = 'ASC';
        $product = Product::where('seo_page_slug', $slug)
        ->orWhere('slug', $slug)
        ->with('publishedDiscount')
        ->with('reviews')
        ->with(['images' => function ($q) use ($order) {
            $q->orderBy('order', $order);
        }])
        ->with('categories')
        ->firstOrFail();

        if ($product->related) {
            $products_related = Product::whereIn('id', $product->related)
            ->where('published', true)
            ->with('publishedDiscount')
            ->with('images')
            ->get();
        } else {
            $products_related = collect([]);
        }

        return view('frontend.product_detail', compact('product', 'products_related'));
    }

    public function shop()
    {
        $this->updateCartTotal();

        if (request()->has('categories') || request()->filled('categories')) {
            $categories = ProductCategories::where('slug', request('categories'))->firstOrFail();

            $banner = $categories;

            $products = $categories->products()
            ->where('published', true)
            ->with('publishedDiscount')
            ->with('images')
            ->orderBy('ordinal_number', 'asc')
            ->get();
        } elseif (request()->has('lookbook') || request()->filled('lookbook')) {
            $lookbook = Lookbook::whereSlug(request('lookbook'))->firstOrFail();

            $banner = $lookbook;

            $products = Product::whereIn('id', $lookbook->products)
            ->where('published', true)
            ->with('publishedDiscount')
            ->with('images')
            ->orderBy('ordinal_number', 'asc')
            ->get();
        } elseif (request()->has('discount')) {
            $discount = Discount::wherePublished(true)->latest()->firstOrFail();

            $products = Product::whereHas('discount')
            ->where('published', true)
            ->with('publishedDiscount')
            ->with('images')
            ->orderBy('ordinal_number', 'asc')
            ->get();

            $banner = $discount;
        } else {
            $products = Product::orderBy('ordinal_number', 'asc')
            ->where('published', true)
            ->with('publishedDiscount')
            ->with('images')
            ->get();

            $banner = null;
        }

        $banners    = Banner::where('page', 'shop')->first();
        return view('frontend.shop', compact('products', 'banner', 'banners'));
    }

    public function cart()
    {
        $provinces = (new RajaOngkir)->province();
        $banner    = Banner::first();
        $this->updateCartTotal();

        return view('frontend.cart', compact('provinces', 'banner'));
    }

    public function checkout()
    {
        $this->updateCartTotal();

        if (session()->has('S_sP')) {
            session()->flash('S_sP');
        }

        if (!session()->has('S_iP')) {
            session()->flash('order-alert', 'Your cart is empty, Please add product to cart');
            return redirect()->back();
        }

        $banner    = Banner::where('page', 'checkout')->first();
        $provinces = (new RajaOngkir)->province();
        $accounts = PaymentAccount::all();
        if (auth()->guard('web')->check()) {
            $customer = Customer::findOrFail(auth()->user()->id);
            return view('frontend.checkout', compact('customer', 'provinces', 'accounts', 'banner'));
        } else {
            return view('frontend.checkout', compact('provinces', 'accounts', 'banner'));
        }
    }

    public function trackingPage()
    {
        $this->updateCartTotal();
        return view('frontend.track_order');
    }

    public function trackingResult()
    {
        $this->updateCartTotal();
        return view('frontend.track_order_detail');
    }

    public function viewConfirmationPage()
    {
        $this->updateCartTotal();
        return view('frontend.confirmation_payment');
    }

    public function contactPage()
    {
        $this->updateCartTotal();
        $companies = Companies::find(1);

        return view('frontend.contact', compact('companies'));
    }

    public function sendMessage()
    {
        return 'ok';
    }

    public function termCondition()
    {
        $this->updateCartTotal();
        return view('frontend.term_conditions');
    }

    public function returnExchanges()
    {
        $this->updateCartTotal();
        return view('frontend.return_exchanges');
    }

    public function sizeGuides()
    {
        $this->updateCartTotal();
        return view('frontend.size_guides');
    }

    public function aboutUs()
    {
        $this->updateCartTotal();
        return view('frontend.about_us');
    }

    protected function instagram()
    {
        $curl = curl_init();
        $companies = Companies::find(1);

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.instagram.com/v1/users/self/media/recent/?access_token={$companies->instagram_access_key}&count=10",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ]);

        $instagrams = json_decode(curl_exec($curl));

        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $instagrams = [];
        }

        return $instagrams;
    }

    protected function updateCartTotal()
    {
        if (session()->has('S_iP')) {
            $oldCart = request()->session()->get('S_iP');

            $totalPrice = 0;
            $hasDiscount = false;

            foreach (session()->get('S_iP')->items as $item) {
                if ($item['item']->published_discount) {
                    $hasDiscount = true;

                    $priceNow = $item['item']->sell_price - ($item['item']->published_discount->discount_percent / 100 * $item['item']->sell_price);

                    $totalPrice += $item['qty'] * $priceNow;
                } else {
                    $totalPrice += $item['qty'] * $item['item']->sell_price;
                }

                $oldCart->totalPrice = $totalPrice;

                session()->put('S_iP', $oldCart);
            }
        }
    }

    public function findInformationOrder()
    {
        request()->validate([
            'order_id' => 'required'
        ]);

        $order = Order::where('invoice_number', request('order_id'))->first();

        if (!$order) {
            throw ValidationException::withMessages([
                'order_id' => 'Order id not found',
            ]);
        }

        return view('frontend.track_order_detail', compact('order'));
    }

    public function storeConfirmation()
    {
        request()->validate([
            'email' => 'required',
            'order_id' => 'required',
            'name' => 'required',
            'account_number' => 'required',
            'receipt' => 'required|image',
        ]);

        $order = Order::where('invoice_number', request('order_id'))->first();

        if (!$order) {
            throw ValidationException::withMessages([
                'order_id' => 'Order id not found',
            ]);
        } else {
            if ($order->payment->upload_receipt_time) {
                throw ValidationException::withMessages([
                    'order_id' => 'Payment Receipt already submitted',
                ]);
            }

            if ($order->payment->transaction_status !== 'settlement' && Carbon::now('Asia/Jakarta')->diffInSeconds($order->created_at) > 86400) {
                throw ValidationException::withMessages([
                    'order_id' => 'Your order has expired',
                ]);
            }

            $imageOriginal = Image::make(request('receipt'));

            $fileName = str_random(40) . '.jpg';

            Storage::put("images/receipt/$fileName", $imageOriginal->encode('jpg')->__toString(), 'public');

            $order->payment()->update([
                'confirmer_name' => request('name'),
                'confirmer_email' => request('email'),
                'confirmer_account_number' => request('account_number'),
                'payment_receipt' => $fileName,
                'upload_receipt_time' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
            ]);
        }

        session()->flash('confirmed-payment', 'Confirmation payment submited successful.');

        return redirect()->back();
    }

    public function searchProduct()
    {
        $keywords = request('keywords');

        if ((request()->has('keywords') && !request()->filled('keywords')) || !request()->has('keywords')) {
            $products = Product::where('published', true)->paginate(12);
        } else {
            $products = Product::where('published', true)->where('title', 'LIKE', "%{$keywords}%")->paginate(12);
        }

        $banner = null;
        $banners    = Banner::where('page', 'shop')->first();

        return view('frontend.shop', compact('products', 'banner', 'banners'));
    }
}
