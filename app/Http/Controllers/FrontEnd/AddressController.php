<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\FrontEnd\RajaOngkir;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
	protected $address;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RajaOngkir $address)
    {
        $this->address = $address;
    }

    public function province()
    {
        $provinces = collect($this->address->province())->map(function($item){
            return [
                'value'   => $item->province_id,
                'label'   => $item->province
            ];
        });

        return response()->json($provinces);
    }

    
    public function provinceId($id)
    {
        $provinces = collect($this->address->province())->map(function($item){
            return [
                'value'   => $item->province_id,
                'label'   => $item->province
            ];
        });

        $result = null;
        foreach($this->address->province() as $item){
            if($item->province_id == $id){
                $result = $item;
            }
        }

        return $result;
    }

    public function city($id)
    {
        $cities = collect($this->address->city($id))->map(function($item){
            return [
                'value'   => $item->city_id,
                'type'    => $item->type,
                'label'   => $item->city_name
            ];
        });

        return response()->json($cities);
    }

    public function subdistrict($id)
    {
        $subdistrict = collect($this->address->subdistrict($id))->map(function($item){
            return [
                'value'   => $item->subdistrict_id,
                'label'   => $item->subdistrict_name
            ];
        });

        return response()->json($subdistrict);
    }

}
