<?php

namespace App\Http\Controllers\FrontEnd;

use App\Model\Order;
use App\Model\Customer;
use App\Model\OrderProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Report
{
    public function monthlySellDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $orders =  Order::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'created_at',
                        DB::raw('count(id) as total_sells'),
                        DB::raw('SUM(shipping_price) as total_shipping_price'),
                        DB::raw('SUM(total_price) as total_price'),
                        DB::raw('MONTH(created_at) month'),
                        DB::raw('YEAR(created_at) year')
                    ])
                    ->selectRaw('(SELECT SUM(quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND month = MONTH(orders_products.created_at) AND year = YEAR(orders_products.created_at)) as total_items')
                    ->selectRaw('(SELECT SUM(CASE WHEN orders_products.product_discount IS NULL THEN 0 ELSE ((orders_products.product_price*orders_products.product_discount/100) * orders_products.quantities) END) FROM orders_products WHERE month = MONTH(orders_products.created_at) AND year = YEAR(orders_products.created_at)) AS total_discount')
                    ->whereHas('payment', function($q){
                        $q->where('transaction_status', 'settlement');
                    })
                    ->groupBy(['month', 'year'])
                    ->orderBy('created_at', 'asc')
                    ->get();

        foreach($orders as $order) {
            $order->total_items = $order->total_items?: '0';
            $order->total_discount = $order->total_discount?: '0';
            $order->date = $order->year . ' ' . date("F", mktime(0, 0, 0, $order->month, 1));
        }

        return $orders;
    }

    public function bestSellingProductsDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $products   =  DB::table('products')->select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'title',
                        DB::raw('(stock) as total_inventory')
                    ])
                    ->selectRaw('(SELECT SUM(orders_products.quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND orders_products.product_id = products.id) as total_quantity_sold')
                    ->selectRaw('(SELECT COUNT(*) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status <> "settlement") AND orders_products.product_id = products.id) as total_distinct_order')
                    ->selectRaw('(SELECT SUM(CASE WHEN orders_products.product_discount IS NULL THEN (orders_products.product_price*orders_products.quantities) ELSE ((orders_products.product_price-(orders_products.product_price*orders_products.product_discount/100)) * orders_products.quantities) END) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND orders_products.product_id = products.id) as total_revenue')
                    ->selectRaw('(SELECT (stock - SUM(orders_products.quantities)) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status IN("settlement", "pending")) AND orders_products.product_id = products.id) as instock_inventory')
                    ->orderBy('total_quantity_sold', 'desc')
                    ->orderBy('total_inventory', 'desc')
                    ->get();

        foreach($products as $product) {
            $product->total_quantity_sold = $product->total_quantity_sold?: '0';
            $product->total_distinct_order = $product->total_distinct_order?: '0';
            $product->total_revenue = $product->total_revenue?: '0';
            $product->instock_inventory = $product->instock_inventory?: '0';
        }

        return $products;

    }

    public function bestMemberDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $customers =  Customer::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'email'
                    ])
                    ->selectRaw('(SELECT SUM(total_price) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_order_amount')
                    ->selectRaw('(SELECT COUNT(*) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_order')
                    ->selectRaw('(SELECT SUM(shipping_price) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_shipping_cost')
                    ->selectRaw('(SELECT SUM((SELECT SUM(CASE WHEN orders_products.product_discount IS NULL THEN 0 ELSE ((orders_products.product_price*orders_products.product_discount/100) * orders_products.quantities) END) FROM orders_products WHERE orders_products.order_id = orders.id)) FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id) as total_discount')
                    ->selectRaw('(SELECT orders.created_at FROM orders WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders.id AND payment_details.transaction_status = "settlement") AND orders.customer_id = customers.id ORDER BY orders.created_at DESC LIMIT 1) as last_order_date')
                    ->get();

        foreach($customers as $customer) {
            $customer->total_order_amount = $customer->total_order_amount?: '0';
            $customer->total_order = $customer->total_order?: '0';
            $customer->total_shipping_cost = $customer->total_shipping_cost?: '0';
            $customer->total_discount = $customer->total_discount?: '0';
        }

        return $customers;
    }

    public function waitingListDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        return  OrderProducts::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'product_name'
                    ])
                    ->selectRaw('COUNT(id) as total_waiting_list')
                    ->orderBy('total_waiting_list', 'desc')
                    ->whereHas('order', function($q){
                        $q->whereHas('customer');
                        $q->whereHas('payment', function($qr){
                            $qr->where('transaction_status', 'pending');
                        });
                    })
                    ->groupBy('product_id')
                    ->orderBy('total_waiting_list', 'desc')
                    ->get();
    }

    public function abandonedCartDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $orders =  Order::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                    ])
                    ->whereHas('payment', function($q){
                        $q->where('transaction_status', 'pending');
                    })
                    ->with(['products' => function($q){
                        $q->select('id', 'order_id', 'product_name', 'quantities');
                    }])
                    ->with(['shipping' => function($q){
                        $q->select('id', 'order_id', 'email');
                    }])
                    ->get();

        foreach($orders as $order) {
            $order->email = $order->shipping->email;
            $items = [];

            foreach ($order->products as $product) {
                array_push($items, $product->quantities.'x '. $product->product_name);
            }

            $order->item = join(', ', $items);
        }

        return $orders;
    }

    public function inventoryDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $products =  DB::table('products')->select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                        'title',
                        DB::raw('(stock) as total_inventory')
                    ])
                    ->selectRaw('(SELECT SUM(orders_products.quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "pending") AND orders_products.product_id = products.id) as on_hold_inventory')
                    ->selectRaw('(SELECT SUM(orders_products.quantities) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status = "settlement") AND orders_products.product_id = products.id) as sold_inventory')
                    ->selectRaw('(SELECT (stock - SUM(orders_products.quantities)) FROM orders_products WHERE EXISTS (SELECT * FROM payment_details WHERE payment_details.order_id = orders_products.order_id AND payment_details.transaction_status IN("settlement", "pending")) AND orders_products.product_id = products.id) as instock_inventory')
                    ->orderBy('total_inventory', 'desc')
                    ->get();

        foreach($products as $product) {
            $product->on_hold_inventory = $product->on_hold_inventory?: '0';
            $product->sold_inventory = $product->sold_inventory?: '0';
            $product->instock_inventory = $product->instock_inventory?: '0';
        }
        return $products;
    }

    
    public function packingListDatatables(Request $request)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $orders = Order::select([
                        DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                        'id',
                    ])
                    ->whereHas('payment', function($q){
                        $q->where('transaction_status', 'settlement');
                    })
                    ->with(['products' => function($q){
                        $q->select('id', 'order_id', 'product_name', 'quantities');
                    }])
                    ->with(['shipping' => function($q){
                        $q->select(
                            'id', 
                            'order_id', 
                            'email', 
                            'firstname', 
                            'lastname',
                            'subdistrict',
                            'address',
                            'province',
                            'city',
                            'postcode',
                            'phonenumber');
                    }])
                    ->where('order_status','packing')
                    ->get();
                    
        foreach($orders as $order) {
            
            $items = [];

            foreach ($order->products as $product) {
                array_push($items, $product->quantities.'x '. $product->product_name);
            }

            $order->item = $order->shipping->firstname. ' ' .$order->shipping->lastname .', '.
            $order->shipping->phonenumber . '  ' .
            $order->shipping->email . ' ' .
            $order->shipping->address.', '.$order->shipping->subdistrict. ', ' .$order->shipping->city. ', ' .$order->shipping->province. ', ' .$order->shipping->postcode . ', Item :' . join(', ', $items);
        }


        return $orders;

    }
}
