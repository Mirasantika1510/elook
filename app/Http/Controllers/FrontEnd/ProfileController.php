<?php

namespace App\Http\Controllers\FrontEnd;
use Storage;
use App\FrontEnd\RajaOngkir;
use App\Model\Customer;
use App\Model\Banner;
use App\Model\CustomerAddress;
use App\Model\Province;
use App\Model\City;
use App\Model\Subdistrict;
use App\Http\Controllers\Controller;
use App\FrontEnd\AddressController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Utilities\ImageHandler;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class ProfileController extends Controller
{
    public function index()
    {
        $provinces  = (new RajaOngkir)->province();
        $customer   = Customer::findOrFail(auth()->user()->id);
        $banner     = Banner::where('page', 'profile')->first();
        return view('frontend.profile',compact('customer','provinces', 'banner'));
    }

    public function updateProfile($id){
        session()->flash('tabs', 'profile');
        request()->validate([
            'firstname'         => 'required|string|max:255',
            'lastname'          => 'required|string|max:255',
            'email'             => 'required|string|email|max:255|unique:customers,email,'.$id,
            'dob'   		    => 'required|date',
            'image'             => 'nullable|mimes:jpeg,jpg,png'
        ]);

    	$customer = Customer::findOrFail($id);

        $customer->update([
            'firstname' => request('firstname'),
            'lastname' => request('lastname'),
            'dob' => request('dob'),
            'email' => request('email')
        ]);

        return redirect()->back();
    }

    public function updateImage($id){
        session()->flash('tabs', 'profile');
        request()->validate([
            'image'             => 'nullable|mimes:jpeg,jpg,png'
        ]);

        if(request()->has('image')){
            $imageOriginal = Image::make(request('image'));

            $fileName       = str_random(40);
            Storage::put("images/customer/$id/$fileName/$fileName" . ".jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
            Storage::put("images/customer/$id/$fileName/$fileName" , $imageOriginal->encode('webp')->__toString(), 'public');

            $imageOriginal->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put("images/customer/$id/$fileName/$fileName" . "-thumbnail.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
            Storage::put("images/customer/$id/$fileName/$fileName" . "-thumbnail", $imageOriginal->encode('webp')->__toString(), 'public');

        }
    	$customer = Customer::findOrFail($id);

        $customer->update([
            'image' => $fileName
        ]);

        return redirect()->back();
    }

    public function addAddress(){
        session()->flash('tabs', 'address');
        request()->validate([
            'province'   		=> 'required',
            'city'   		    => 'required',
            'subdistrict'   	=> 'required',
            'address'   		=> 'required',
            'phonenumber'       => 'required'
        ]);

        $customer_address = CustomerAddress::create([
            'customer_id' => auth()->user()->id,
            'province' => request('province'),
            'city' => request('city'),
            'subdistrict' => request('subdistrict'),
            'phonenumber' => request('phonenumber'),
            'address' => request('address'),
            'active'  => (request('active'))?1:0
        ]);

        if(request('active') == 1){
            CustomerAddress::where('id','!=',$customer_address->id)
            ->where('customer_id',auth()->user()->id)
            ->update([
                'active'  => 0
            ]);
        }

        return redirect()->back();
    }

    public function updateAddress($id){

        session()->flash('tabs', 'address');
        request()->validate([
            'province'   		=> 'required',
            'city'   		    => 'required',
            'subdistrict'   	=> 'required',
            'address'   		=> 'required',
            'phonenumber'       => 'required'
        ]);

    	$customer_address = CustomerAddress::findOrFail($id);

        $customer_address->update([
            'customer_id' => auth()->user()->id,
            'province' => request('province'),
            'city' => request('city'),
            'subdistrict' => request('subdistrict'),
            'phonenumber' => request('phonenumber'),
            'address' => request('address'),
            'active'  => (request('active'))?1:0
        ]);


        if(request('active') == 1){
            CustomerAddress::where('id','!=',$id)
            ->where('customer_id',auth()->user()->id)
            ->update([
                'active'  => 0
            ]);
        }

        return redirect('profile');
    }

    public function deleteAddress($id)
    {

        session()->flash('tabs', 'address');
    	$customer_address = CustomerAddress::findOrFail($id);
        $customer_address->delete();

        return redirect()->back();
    }

    public function updatePassword($id){
        session()->flash('tabs', 'password');
        request()->validate([
            'password' => 'required|string|min:6|confirmed'
        ]);

    	$customer = Customer::findOrFail($id);

        $customer->update([
            'password' => Hash::make(request('password'))
        ]);

        return redirect()->back();
    }
}
