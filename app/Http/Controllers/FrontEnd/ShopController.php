<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\FrontEnd\RajaOngkir;
use App\Http\Controllers\Controller;
use App\Model\Product;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function addToCart($id)
    {
        $product = Product::select('id', 'discount_id', 'title', 'slug', 'weight', 'type', 'price', 'sell_price', 'stock', 'ignore_stock')
                    ->whereId($id)
                    ->with('publishedDiscount')
                    ->with('images')
                    ->where('published', true)
                    ->firstOrFail();

        $oldCart = request()->session()->has('S_iP') ? request()->session()->get('S_iP') : null;

        $quantity = request('quantity') ?: 1;

        $cart = new Cart($oldCart);

        if (!$product->ignore_stock) {
            if (($product->stock - $product->stock_used) <= 0) {
                session()->flash('order-alert', 'Stock ' . ucwords($product->title) . ' are empty');
                return redirect()->back();
            }
        }

        $cart->add($product, $quantity);

        request()->session()->put('S_iP', $cart);

        session()->flash('order-cart', 'Product added to cart');

        return redirect()->back();
    }

    public function deleteFromCart($id)
    {
        if (!session()->has('S_iP')) {
            session()->flash('order-alert', 'Your cart is empty, Please add product to cart');
            return redirect()->back();
        }

        $product = Product::select('id', 'title', 'slug', 'weight', 'type', 'price', 'sell_price', 'stock', 'ignore_stock')
                ->whereId($id)
                ->firstOrFail();

        $oldCart = request()->session()->has('S_iP') ? request()->session()->get('S_iP') : null;

        if (!$oldCart) {
            abort(404);
        }

        $cart = new Cart($oldCart);

        $cart->remove($product);

        if (count($cart->items) === 0) {
            session()->forget('S_iP');
            session()->forget('S_sP');
            session()->forget('S_SD');

            session()->flash('order-cart', 'Product deleted, cart now empty');

            return redirect('/');
        } else {
            request()->session()->put('S_iP', $cart);
        }

        request()->session()->regenerate();

        session()->flash('order-cart', 'Product deleted from cart');

        return redirect()->back();
    }

    public function checkCost($destination, $weight)
    {
        if (!$results = (new RajaOngkir)->cost($destination, $weight)) {
            abort(500, 'response failed');
        }

        $response = null;

        foreach ($results->rajaongkir->results as $result) {
            foreach ($result->costs as $cost) {
                if ($cost->service === 'REG') {
                    $response = $cost->cost[0];
                }
            }
        }
        return response()->json($response);
    }

    public function cost($destination, $weight)
    {
        if (!$results = (new RajaOngkir)->cost($destination, $weight)) {
            abort(500, 'response failed');
        }

        $response = null;

        foreach ($results->rajaongkir->results as $result) {
            foreach ($result->costs as $cost) {
                if ($cost->service === 'REG') {
                    session()->put('S_sP', $cost);
                    $response = $cost->cost[0];
                }
            }
        }

        if ($response) {
            session()->put('S_SD', $results->rajaongkir->destination_details);
        } else {
            if (session()->exists('S_SD') || session()->has('S_SD')) {
                session()->forget('S_SD');
            }
        }

        return response()->json($response);
    }

    public function updateCart()
    {
        if (!session()->has('S_iP')) {
            session()->flash('order-alert', 'Your cart is empty, Please add product to cart');
            return redirect()->back();
        }

        $oldCart = request()->session()->has('S_iP') ? request()->session()->get('S_iP') : null;

        if (!$oldCart) {
            abort(404);
        }

        $cart = new Cart($oldCart);

        $cart->change(request()->only(['items']));

        session()->put('S_iP', $cart);

        request()->session()->regenerate();

        session()->flash('order-cart', 'Product cart updated');

        return redirect()->back();
        dd($oldCart);

        return number_format(session()->get('S_iP')->totalPrice, 0, ',', '.');

        print_r(session()->get('S_iP'));

    }
}
