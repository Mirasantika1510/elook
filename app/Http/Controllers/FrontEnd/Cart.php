<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Support\Facades\Session;

class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $voucher = null;
    public $totalWeight = 0;
    public $finalPrice = 0;
    public $discountedPrice = 0;
    public $finalVoucherNominal = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
            $this->totalWeight = $oldCart->totalWeight;
            $this->voucher = $oldCart->voucher;
            $this->discountedPrice = $oldCart->discountedPrice;
            $this->finalVoucherNominal = $oldCart->finalVoucherNominal;
        }
    }

    public function add($item, $quantity = 1, $attributes = null)
    {
        $storedItem = [
            'qty' => $quantity,
            'weight' => $item->weight,
            'item' => $item,
        ];

        if ($this->items) {
            if (array_key_exists($item->id, $this->items)) {
                $storedItem = $this->items[$item->id];
                $storedItem['qty'] += $quantity;

                $item = $this->items[$item->id]['item'];
            }
        }

        if ($attributes) {
            $attributesInItem = array_first($item->attributes->toArray(), function ($value, $key) use ($attributes) { return $value['key'] === $attributes; });

            if (!$attributesInItem) {
                abort(404);
            }

            if (array_key_exists('attributes', $storedItem)) {
                data_set($storedItem, "attributes.{$attributes}", (data_get($storedItem, "attributes.{$attributes}") + $quantity));
            } else {
                data_fill($storedItem, "attributes.{$attributes}", $quantity);
            }

            if ($item->discount) {
                $discountedPrice = ($attributesInItem['scheme']['price'] - ($attributesInItem['scheme']['price'] * $item->discount / 100));
                $this->totalPrice += $quantity * $discountedPrice;
                $this->discountedPrice += $quantity * $discountedPrice;
            } else {
                $this->totalPrice += $quantity * $attributesInItem['scheme']['price'];
            }
        } else {
            if ($item->publishedDiscount) {
                $discountedPrice = (floor($item->publishedDiscount->discount_percent / 100 * $item->sell_price));
                $this->totalPrice += $quantity * ($item->sell_price - $discountedPrice);
                $this->discountedPrice += $quantity * $discountedPrice;
            } else {
                $this->totalPrice += $quantity * $item->sell_price;
            }
        }

        $this->items[$item->id] = $storedItem;
        $this->totalQty += $quantity;
        $this->totalWeight += $quantity * $item->weight;

        if ($this->voucher) {
            if ($this->voucher->voucher_type === 'nominal') {
                $totalCalculatedVoucher = 0;
                foreach ($this->items as $item) {
                    if (in_array($item['item']->id, $this->voucher->products->pluck('id')->toArray())) {
                        $totalCalculatedVoucher += $item['qty'] * $this->voucher->nominal;
                    }
                }
            } else {
                $totalCalculatedVoucher = 0;
                foreach ($this->items as $item) {
                    if (in_array($item['item']->id, $this->voucher->products->pluck('id')->toArray())) {
                        $totalCalculatedVoucher += ($item['qty'] * $item['item']->sell_price) * ($this->voucher->percent / 100);
                    }
                }
            }
            $this->finalVoucherNominal = $totalCalculatedVoucher;
        }

        $this->finalPrice = $this->totalPrice - $this->finalVoucherNominal;
    }

    public function change(array $data)
    {
        foreach ($data as $key => $item) {
            if ($key === 'attributes') {
                foreach ($item as $id => $attributes) {
                    foreach ($attributes as $key => $quantity) {
                        if (array_key_exists($id, $this->items)) {
                            $attributesInItem = array_first($this->items[$id]['item']['attributes'], function ($value) use ($key) { return $value['key'] == $key; });

                            $this->totalQty -= $this->items[$id]['attributes'][$key];
                            $this->totalWeight -= $this->items[$id]['attributes'][$key] * $this->items[$id]['item']->weight;

                            if ($this->items[$id]['item']->discount) {
                                $discountedPrice = ($attributesInItem['scheme']['price'] - floor($attributesInItem['scheme']['price'] * $this->items[$id]['item']->discount / 100));
                                $this->totalPrice -= $this->items[$id]['attributes'][$key] * $discountedPrice;
                                $this->discountedPrice -= $this->items[$id]['attributes'][$key] * $discountedPrice;
                            } else {
                                $this->totalPrice -= $this->items[$id]['attributes'][$key] * $attributesInItem['scheme']['price'];
                            }

                            $this->items[$id]['qty'] -= $this->items[$id]['attributes'][$key];
                            $this->items[$id]['weight'] -= $this->items[$id]['attributes'][$key] * $this->items[$id]['item']->weight;

                            $this->items[$id]['attributes'][$key] = $quantity;

                            $this->items[$id]['qty'] -= $quantity;
                            $this->items[$id]['weight'] -= $quantity * $this->items[$id]['item']->weight;

                            $this->totalQty += $quantity;
                            $this->totalWeight += $quantity * $this->items[$id]['item']->weight;

                            if ($this->items[$id]['item']->discount) {
                                $discountedPrice = ($attributesInItem['scheme']['price'] - floor($attributesInItem['scheme']['price'] * $this->items[$id]['item']->discount / 100));
                                $this->totalPrice += $quantity * $discountedPrice;
                                $this->discountedPrice += $quantity * $discountedPrice;
                            } else {
                                $this->totalPrice += $quantity * $attributesInItem['scheme']['price'];
                            }
                        }
                    }
                }
            } else {
                foreach ($item as $id => $quantity) {
                    if (array_key_exists($id, $this->items)) {
                        $this->totalQty -= $this->items[$id]['qty'];
                        $this->totalWeight -= $this->items[$id]['weight'];

                        if ($this->items[$id]['item']->publishedDiscount) {
                            $discountedPrice = (floor($this->items[$id]['item']->sell_price * $this->items[$id]['item']->publishedDiscount->discount_percent / 100));
                            $this->totalPrice -= $this->items[$id]['qty'] * ($this->items[$id]['item']->sell_price - $discountedPrice);
                            $this->discountedPrice -= $this->items[$id]['qty'] * $discountedPrice;
                        } else {
                            $this->totalPrice -= $this->items[$id]['qty'] * $this->items[$id]['item']->sell_price;
                        }

                        $this->items[$id]['qty'] = $quantity;
                        $this->items[$id]['weight'] = $quantity * $this->items[$id]['item']->weight;

                        $this->totalQty += $quantity;
                        $this->totalWeight += $quantity * $this->items[$id]['item']->weight;

                        if ($this->items[$id]['item']->publishedDiscount) {
                            $discountedPrice = (floor($this->items[$id]['item']->sell_price * $this->items[$id]['item']->publishedDiscount->discount_percent / 100));
                            $this->totalPrice += $quantity * ($this->items[$id]['item']->sell_price - $discountedPrice);
                            $this->discountedPrice += $quantity * $discountedPrice;
                        } else {
                            $this->totalPrice += $quantity * $this->items[$id]['item']->sell_price;
                        }
                    }
                }
            }
        }

        if ($this->voucher) {
            if ($this->voucher->voucher_type === 'nominal') {
                $totalCalculatedVoucher = 0;
                foreach ($this->items as $item) {
                    if (in_array($item['item']->id, $this->voucher->products->pluck('id')->toArray())) {
                        $totalCalculatedVoucher += $item['qty'] * $this->voucher->nominal;
                    }
                }
            } else {
                $totalCalculatedVoucher = 0;
                foreach ($this->items as $item) {
                    if (in_array($item['item']->id, $this->voucher->products->pluck('id')->toArray())) {
                        $totalCalculatedVoucher += ($item['qty'] * $item['item']->sell_price) * ($this->voucher->percent / 100);
                    }
                }
            }
            $this->finalVoucherNominal = $totalCalculatedVoucher;
        }

        $this->finalPrice = 0;
    }

    public function remove($item, $attributes = null)
    {
        if ($attributes) {
            $attributesInItem = array_first($item->attributes->toArray(), function ($value, $key) use ($attributes) { return $value['key'] === $attributes; });

            if (!$attributesInItem) {
                abort(404);
            }

            $this->totalQty -= $this->items[$item->id]['attributes'][$attributes];
            $this->totalWeight -= ($this->items[$item->id]['attributes'][$attributes] * $this->items[$item->id]['item']->weight);

            if ($this->items[$item->id]['item']->discount) {
                $discountedPrice = ($attributesInItem['scheme']['price'] - floor($attributesInItem['scheme']['price'] * $this->items[$item->id]['item']->discount / 100));
                $this->totalPrice -= $this->items[$item->id]['attributes'][$attributes] * $discountedPrice;
            } else {
                $this->totalPrice -= ($this->items[$item->id]['attributes'][$attributes] * $attributesInItem['scheme']['price']);
            }

            if (count($this->items[$item->id]['attributes']) === 1) {
                unset($this->items[$item->id]);
            } else {
                unset($this->items[$item->id]['attributes'][$attributes]);
            }
        } else {
            $this->totalQty -= $this->items[$item->id]['qty'];
            $this->totalWeight -= $this->items[$item->id]['weight'];

            if ($this->items[$item->id]['item']->publishedDiscount) {
                $discountedPrice = ($this->items[$item->id]['item']->sell_price - floor($this->items[$item->id]['item']->sell_price * $this->items[$item->id]['item']->publishedDiscount->discount_percent / 100));
                $this->totalPrice -= $this->items[$item->id]['qty'] * $discountedPrice;
            } else {
                $this->totalPrice -= ($this->items[$item->id]['qty'] * $this->items[$item->id]['item']->sell_price);
            }

            unset($this->items[$item->id]);
        }

        $this->voucher = null;
        $this->finalPrice = $this->totalPrice;

        if (count($this->items) === 0) {
            session()->forget('S_iP');
        }
    }
}
