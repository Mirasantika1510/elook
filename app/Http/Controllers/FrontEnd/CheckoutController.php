<?php

namespace App\Http\Controllers\FrontEnd;

use Carbon\Carbon;
use App\Model\Order;
use App\Mail\OrderInvoice;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Model\PaymentAccount;
use App\Model\CustomerAddress;
use App\Services\GoSMSGateway;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    private $goSMSGateway;

    public function __construct(GoSMSGateway $gosmsgateway)
    {
        $this->goSMSGateway = $gosmsgateway;
    }

    public function processCheckout()
    {
        if (!session()->has('S_iP')) {
            session()->flash('order-alert', 'Your cart is empty, Please add product to cart');
            return redirect()->back();
        }
        if (!session()->has('S_sP') || !session()->has('S_SD')) {
            session()->flash('order-alert', 'You not pick location shipping, Please pick location shipping');
            return redirect()->back();
        }

        if (request('customer_address_id')) {
            request()->validate([
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email',
                'customer_address_id' => 'required',
                'payment-menthod' => 'required',
            ]);
        } else {
            request()->validate([
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email',
                'province' => 'required',
                'city' => 'required',
                'subdistrict' => 'required',
                'address' => 'required',
                'postcode' => 'required',
                'phonenumber' => 'required',
                'payment-menthod' => 'required',
            ]);
        }

        $total = (int) session()->get('S_iP')->finalPrice + (int) session()->get('S_sP')->cost[0]->value;

        $INVOICE = mt_rand(1000000000, mt_getrandmax());

        $payment = PaymentAccount::where('id', request('payment-menthod'))->firstOrFail();

        DB::beginTransaction();

        try {
            $uniqueNumber = rand(10, 99);

            if (auth()->check()) {
                $order = auth()->user()->orders()->create([
                    'transaction_id' => Str::uuid(),
                    'invoice_number' => $INVOICE,
                    'shipping_courier' => 'SICEPAT',
                    'courier_service' => 'REG',
                    'note' => request()->has('note') ? request('note') : null,
                    'estimation_shipping' => session()->get('S_sP')->cost[0]->etd,
                    'shipping_price' => session()->get('S_sP')->cost[0]->value,
                    'subtotal_price' => (int) session()->get('S_iP')->finalPrice,
                    'total_price' => request('total'),
                    'unique_number' => $uniqueNumber
                ]);

                if (request('customer_address_id')) {
                    $address = CustomerAddress::findOrFail(request('customer_address_id'));
                } else {
                    $address = auth()->user()->allAddress()->create([
                        'customer_id' => auth()->user()->id,
                        'province' => request('province'),
                        'city' => request('city'),
                        'subdistrict' => request('subdistrict'),
                        'phonenumber' => request('phonenumber'),
                        'address' => request('address'),
                        'active' => 1
                    ]);
                }
            } else {
                $order = Order::create([
                    'transaction_id' => Str::uuid(),
                    'invoice_number' => $INVOICE,
                    'shipping_courier' => 'SICEPAT',
                    'courier_service' => 'REG',
                    'note' => request()->has('note') ? request('note') : null,
                    'estimation_shipping' => session()->get('S_sP')->cost[0]->etd,
                    'shipping_price' => session()->get('S_sP')->cost[0]->value,
                    'subtotal_price' => (int) session()->get('S_iP')->totalPrice,
                    'total_price' => request('total'),
                    'unique_number' => $uniqueNumber
                ]);
            }

            if (session()->get('S_iP')->voucher) {
                $order->voucher()->create([
                    'voucher_id' => session()->get('S_iP')->voucher->id,
                    'voucher_code' => session()->get('S_iP')->voucher->voucher_code,
                    'voucher_type' => session()->get('S_iP')->voucher->voucher_type,
                    'nominal' => session()->get('S_iP')->voucher->nominal,
                    'amount' => request('voucher_nominal'),
                ]);
            }

            $shipping = $order->shipping()->create([
                'firstname' => request('firstname'),
                'lastname' => request('lastname'),
                'email' => request('email'),
                'province' => auth()->check() ? $address->getprovince->province : session()->get('S_SD')->province,
                'city' => auth()->check() ? $address->getcity->city_name : session()->get('S_SD')->city,
                'subdistrict' => auth()->check() ? $address->getsubdistrict->subdistrict_name : session()->get('S_SD')->subdistrict_name,
                'address' => auth()->check() ? $address->address : request('address'),
                'postcode' => auth()->check() ? $address->getcity->postal_code : request('postcode'),
                'phonenumber' => auth()->check() ? $address->phonenumber : request('phonenumber'),
            ]);

            $order->payment()->create([
                'account_bank' => $payment->account_bank,
                'account_number' => $payment->account_number,
                'account_name' => $payment->account_name,
                'instructions' => $payment->instructions
            ]);

            foreach (session()->get('S_iP')->items as $item) {
                $orderProduct = $order->products()->create([
                    'product_id' => $item['item']->id,
                    'product_name' => $item['item']->title,
                    'product_ignore_stock' => $item['item']->ignore_stock,
                    'product_type' => $item['item']->type,
                    'product_discount' => $item['item']->publishedDiscount ? $item['item']->publishedDiscount->discount_percent : null,
                    'product_price' => $item['item']->sell_price,
                    'product_weight' => $item['item']->weight,
                    'product_image' => $item['item']->images->first()->filename,
                    'quantities' => $item['qty'],
                ]);

                if (!$item['item']->ignore_stock) {
                    $item['item']->increment('stock_used', $item['qty']);
                    $item['item']->decrement('stock', $item['qty']);
                }
            }

            $order->trackings()->create([
                'title' => 'Waiting Payment',
                'description' => 'Orders must be paid according to the payment method that has been determined.'
            ]);

            $this->goSMSGateway->sendMessage($order, $shipping->phonenumber);

            if (auth()->check()) {
                Mail::to(auth()->user())->send(new OrderInvoice($order));
            } else {
                Mail::to(request('email'))->send(new OrderInvoice($order));
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        session()->forget('S_iP');
        session()->forget('S_sP');
        session()->forget('S_SD');

        return redirect('transaction-status/' . $order->transaction_id);
    }

    public function viewTransactionStatus($transactionId)
    {
        $order = Order::where('transaction_id', $transactionId)->firstOrFail();

        $transactionTime = $order->created_at;
        $endTranscationTime = $order->created_at->addHours(24);
        $differenceTransactionTimeAndNow = $transactionTime->diffInSeconds(Carbon::now('Asia/Jakarta'));
        $now = Carbon::now('Asia/Jakarta');
        $generateTimeCount = Carbon::create($now->year, $now->month, $now->day, 1, 0, 0, 0);
        $countDown = $generateTimeCount->subSeconds($differenceTransactionTimeAndNow);

        return view('frontend.transaction', compact('order', 'transactionTime', 'endTranscationTime', 'differenceTransactionTimeAndNow', 'now', 'generateTimeCount', 'countDown'));
    }
}
