<?php

namespace App\Http\Controllers\FrontEnd;

use App\Model\Companies;
use App\Model\CustomPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomPageController extends Controller
{
    public function view()
    {
        $custom_page = CustomPage::where('url_link', request()->path())->firstOrFail();

        $instagrams  = $this->instagram();

        // return response()->json($instagrams);

        return view('frontend.custom_pages', compact('custom_page', 'instagrams'));
    }

    protected function instagram()
    {
        $curl         = curl_init();
        $companies    = Companies::find(1);

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.instagram.com/v1/users/self/media/recent/?access_token={$companies->instagram_access_key}&count=9",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
            ));
            
        $instagrams = json_decode(curl_exec($curl));
        
		$err = curl_error($curl);
        curl_close($curl);
        
		if ($err) {
            $instagrams = [];
        }

        return $instagrams;
    }
}
