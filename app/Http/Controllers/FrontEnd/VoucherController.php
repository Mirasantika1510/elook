<?php

namespace App\Http\Controllers\FrontEnd;

use App\Model\Voucher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class VoucherController extends Controller
{
    public function useVoucher()
    {
        request()->validate([
            'voucher_code' => 'required'
        ]);

        $voucher = Voucher::whereVoucherCode(request('voucher_code'))->wherePublished(true)->first();

        if(!$voucher) {

            session()->flash('error-voucher', true);

            throw ValidationException::withMessages([
                'voucher_code' => 'Voucher not found',
            ]);

        } else {

            if(!auth()->guard('web')->check() && $voucher->avaliable_for == 'member') {

                session()->flash('error-voucher', true);
    
                throw ValidationException::withMessages([
                    'voucher_code' => 'Voucher only available for members',
                ]);
    
            }else if(auth()->guard('web')->check() && $voucher->avaliable_for == 'guest') {
    
                session()->flash('error-voucher', true);
    
                throw ValidationException::withMessages([
                    'voucher_code' => 'Voucher only available for guest',
                ]);
    
            } else {
                session()->flash('voucher-added', 'Voucher succesfuly added');
            }

        }

        $oldCart    = request()->session()->has('S_iP')? request()->session()->get('S_iP') : null;

        $productsId  = collect($oldCart->items)->pluck('item.id');

        if($voucher->products()->whereIn('id', $productsId)->count() > 0) {
            $oldCart->voucher = $voucher;
        }
        
        return redirect()->back();

    } 
}
