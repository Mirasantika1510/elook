<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Model\Order;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function transactionState($invoice)
    {
        $order = Order::whereInvoiceNumber($invoice)->firstOrFail();

        $CURLOPT_URL = env('MT_API') . "/v2/$invoice/status";

        $client = new Client(['timeout'  => 30]);

        try {

            $response = $client->request('GET', $CURLOPT_URL, [ 
                    'headers' => [
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json'
                    ],
                    'auth' => ['VT-server-cruTI-xT0P1qlJh3xLjWP8PR', '']
                ]);

        } catch (RequestException $e) {

            abort(400);

        }

        $responseBody = json_decode($response->getBody()->getContents());

        $order->update([
            'payment_type'          => $responseBody->payment_type,
            'transaction_time'      => $responseBody->transaction_time,
            'transaction_status'    => $responseBody->transaction_status,
            'status_code'           => $responseBody->status_code,
        ]);

        $order->update([
            'payment_type'          => property_exists($responseBody, 'payment_type')? $responseBody->payment_type : ($order->payment_type?: null),
            'transaction_time'      => property_exists($responseBody, 'transaction_time')? $responseBody->transaction_time : ($order->transaction_time?: null),
            'masked_card'           => property_exists($responseBody, 'masked_card')? $responseBody->masked_card : ($order->masked_card?: null),
            'currency'              => property_exists($responseBody, 'currency')? $responseBody->currency : ($order->currency?: null),
            'bill_key'              => property_exists($responseBody, 'bill_key')? $responseBody->bill_key : ($order->bill_key?: null), 
            'biller_code'           => property_exists($responseBody, 'biller_code')? $responseBody->biller_code : ($order->biller_code?: null), 
            'permata_va_number'     => property_exists($responseBody, 'permata_va_number')? $responseBody->permata_va_number : ($order->permata_va_number?: null), 
            'approval_code'         => property_exists($responseBody, 'approval_code')? $responseBody->approval_code : ($order->approval_code?: null), 
            'store'                 => property_exists($responseBody, 'store')? $responseBody->store : ($order->store?: null), 
            'payment_code'          => property_exists($responseBody, 'payment_code')? $responseBody->payment_code : ($order->payment_code?: null), 
            'bank'                  => property_exists($responseBody, 'va_numbers')? $responseBody->va_numbers[0]->bank : ($order->bank?: null), 
            'va_number'             => property_exists($responseBody, 'va_numbers')? $responseBody->va_numbers[1]->va_number : ($order->va_number?: null),
            'paid_at'               => property_exists($responseBody, 'payment_amounts')? $responseBody->payment_amounts[0]->paid_at : ($order->paid_at?: null),
            'amount'                => property_exists($responseBody, 'payment_amounts')? $responseBody->payment_amounts[1]->amount : ($order->amount?: null),
            'fraud_status'          => property_exists($responseBody, 'fraud_status')? $responseBody->fraud_status : ($order->fraud_status?: null), 
            'transaction_time'      => property_exists($responseBody, 'transaction_time')? $responseBody->transaction_time : ($order->transaction_time?: null), 
            'transaction_status'    => property_exists($responseBody, 'transaction_status')? $responseBody->transaction_status : ($order->transaction_status?: null), 
            'status_code'           => property_exists($responseBody, 'status_code')? $responseBody->status_code : ($order->status_code?: null), 
        ]);

        return redirect('transaction-state-result/'.$order->invoice_number);

        // if($responseBody->transaction_status === 'settlement'){
        //     return redirect('completed-state/'.$order->invoice_number);
        // }elseif($responseBody->transaction_status === 'pending'){
        //     return redirect('pending-state/'.$order->invoice_number);
        // }else{
        //     return redirect('failed-state/'.$order->invoice_number);
        // }
    }

    public function transactionStateResult($invoice)
    {
        $order = Order::whereInvoiceNumber($invoice)->firstOrFail();

        return view('frontend.transaction_result', compact('order'));
    }

    // public function completedState($invoice)
    // {
    //     $order = Order::whereInvoiceNumber($invoice)->whereStatusCode('200')->firstOrFail();

    //     return view('frontend.complete_payment', compact('order'));
    // }

    // public function pendingState($invoice)
    // {
    //     $order = Order::whereInvoiceNumber($invoice)->whereStatusCode('201')->firstOrFail();

    //     return view('frontend.pending_payment', compact('order'));
    // }

    // public function failedState($invoice)
    // {
    //     $order = Order::whereInvoiceNumber($invoice)->whereStatusCode('202')->firstOrFail();

    //     return view('frontend.failed_payment', compact('order'));
    // }

}
