<?php

namespace App\Http\Controllers\FrontEnd;

use App\Model\Product;
use App\Model\Wishlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WishlistController extends Controller
{
    public function listWishlist()
    {
        $user = auth()->user();

        $wishlists = $user->wishlist;

        return view('frontend.wishlist', compact('wishlists'));
    }

    public function addWishlist(Product $product)
    {
        $user = auth()->user();

        $user->wishlist()->create([ 'product_id' => $product->id ]);

        return redirect()->back();
    }

    public function removeWishlist(Wishlist $wishlist)
    {
        $user = auth()->user();

        $user->wishlist()->where('id', $wishlist->id)->delete();
        
        return redirect()->back();
    }
}
