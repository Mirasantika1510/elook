<?php

namespace App\Http\Controllers\FrontEnd;

use App\Model\City;
use App\Model\Province;
use App\Model\Subdistrict;

class RajaOngkir {

	public function province()
	{
        $CURLOPT_URL = "https://pro.rajaongkir.com/api/province";

		$curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: cfff283dc03c61078c4e5f50595bec14"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            $response = Province::all();

            return $response;

        }

        return json_decode($response)->rajaongkir->results;
	}

	public function city($id = null)
	{
		if($id){
            $CURLOPT_URL = "https://pro.rajaongkir.com/api/city?province={$id}";
		}else{
            abort(404);
		}

		$curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: cfff283dc03c61078c4e5f50595bec14"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            $response = City::whereProvinceId($id)->get();

            return $response;

        }

        return json_decode($response)->rajaongkir->results;
	}

    public function subdistrict($id = null)
    {
        if($id){
            $CURLOPT_URL = "https://pro.rajaongkir.com/api/subdistrict?city={$id}";
        }else{
            abort(404);
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: cfff283dc03c61078c4e5f50595bec14"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            $response = Subdistrict::whereCityId($id)->get();

            return $response;

        }

        return json_decode($response)->rajaongkir->results;
    }

	public function cost($destination, $weight)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=23&originType=city&destination={$destination}&destinationType=subdistrict&weight={$weight}&courier=sicepat",
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: cfff283dc03c61078c4e5f50595bec14"
			),
		));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        }

        return json_decode($response);
	}

    public function syncProvince()
    {
        $CURLOPT_URL = "https://pro.rajaongkir.com/api/province";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: cfff283dc03c61078c4e5f50595bec14"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            abort(404);

        }

        return json_decode($response)->rajaongkir->results;
    }

    public function syncCity()
    {
        $CURLOPT_URL = "https://pro.rajaongkir.com/api/city";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: cfff283dc03c61078c4e5f50595bec14"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            abort(404);

        }

        return json_decode($response)->rajaongkir->results;
    }

    public function syncSubdistrict($id)
    {
        $CURLOPT_URL = "https://pro.rajaongkir.com/api/subdistrict?city={$id}";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: cfff283dc03c61078c4e5f50595bec14"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            abort(404);

        }

        return json_decode($response)->rajaongkir->results;
    }
    
    public function getProvince($id)
    {

       $curl = curl_init();

       curl_setopt_array($curl, array(
          CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=".$id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: cfff283dc03c61078c4e5f50595bec14"
        ),
      ));

       $response = curl_exec($curl);
       $err = curl_error($curl);

       curl_close($curl);

       if ($err) {

            abort(404);

        }

      return json_decode($response)->rajaongkir->results->province;
  }

  public function getCity($id)
    {

       $curl = curl_init();

       curl_setopt_array($curl, array(
          CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: cfff283dc03c61078c4e5f50595bec14"
        ),
      ));

       $response = curl_exec($curl);
       $err = curl_error($curl);

       curl_close($curl);

       if ($err) {

            abort(404);

        }

      return json_decode($response)->rajaongkir->results->city_name;
  }

  public function getSubdistrict($id, $cityId)
    {

       $curl = curl_init();

       curl_setopt_array($curl, array(
          CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=".$cityId."&id=".$id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: cfff283dc03c61078c4e5f50595bec14"
        ),
      ));

       $response = curl_exec($curl);
       $err = curl_error($curl);

       curl_close($curl);

       if ($err) {

            abort(404);

        }

      return json_decode($response)->rajaongkir->results->subdistrict_name;
  }

}