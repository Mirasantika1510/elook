<?php

namespace App\Http\Controllers\FrontEnd;

use App\Model\Messages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'name'              => 'required',
            'email'             => 'required|email',
            'subject'           => 'required',
            'message'           => 'required'
        ]);

        if ($validator->fails()) {
            session()->flash('error-message', 'true');
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        Messages::create([
            'name' => request('name'),
            'email' => request('email'),
            'subject' => request('subject'),
            'message' => request('message'),
        ]);

        return redirect()->back();
    }
}
