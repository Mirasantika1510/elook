<?php

namespace App\Http\Controllers\FrontEnd;

use App\Model\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SubscribeController extends Controller
{
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'email'             => 'required|email|unique:subscribes'
        ]);

        if ($validator->fails()) {
            session()->flash('error-subscribe', 'true');
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        Subscribe::create([
            'email' => request('email')
        ]);

        return redirect()->back();
    }
}
