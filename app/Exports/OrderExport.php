<?php

namespace App\Exports;

use App\Model\Order;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class OrderExport extends DefaultValueBinder  implements FromCollection, WithHeadings, WithMapping, WithCustomValueBinder
{
    private $data;
    private $header;
    private $value;

    public function __construct($data, $header, $value)
    {
        $this->data = $data;
        $this->header = $header;
        $this->value = $value;
    }

    /**
    * @var Invoice $invoice
    */
    public function map($order): array
    {
        $values = [];
        foreach ($this->value as $value) {
            array_push($values, $order->$value);
        }
        return $values;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return $this->header;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
