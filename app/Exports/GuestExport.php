<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Model\OrderShippings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class GuestExport implements FromView, ShouldAutoSize
{
    public function view(): View
	{
		$guests = OrderShippings::whereHas('order', function($query){
                                $query->whereNull('customer_id');
                            })
                            ->latest()
                            ->get();

		 return view('admin.pages.guests.export', compact('guests'));
	}
}
