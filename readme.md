## Installation

```sh
composer install
npm install
```

## Setting Env File

Create file .env dan masukkan copy script ini :

```env
APP_NAME=Elook
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost
ADMIN_URL=wee-management

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=elook
DB_USERNAME=
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync
FILESYSTEM_DRIVER=public

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

```

DB_DATABASE, DB_USERNAME, DB_PASSWORD silahkan ganti sesuai settingan mysql local server.

## Setting key dan storage link

```sh
php artisan key:generate
php artisan storage:link
```

## Step Migrate Dummy Data

Buka File AppServiceProvider.php di [app/Providers/AppServiceProvider.php][Asp]

disable / comment script ini : 

```php
    $companies        = Companies::find(1);
    $socials          = SocialMedia::all();
    $categories       = ProductCategories::all();
    $custom_pages     = CustomPage::all();
    $discountCount    = Discount::where('published', true)->count();
    
    view()->composer([ 'frontend.layouts.footer', 'frontend.layouts.header' ], function ($view) use($socials, $categories, $custom_pages, $discountCount) {
        $view->with([
            'socials' => $socials,
            'categories' => $categories,
            'custom_pages' => $custom_pages,
            'discountCount' => $discountCount,
        ]);
    });
    
    view()->composer('*', function ($view) use($companies) {
        $view->with([
            'companies' => $companies,
        ]);
    });
```

Buka Terminal dan run : 

```sh
php artisan migrate --seed
```

Setelah selesai, enable / uncomment script di file AppServiceProvider.php di [app/Providers/AppServiceProvider.php][Asp]

Run :

```sh
php artisan serve
```

done.

## Update Javascript dan CSS File

Untuk penambahan/perubahan css di file [resources/assets/frontend/css/custom.css][Ctm], pastikan watcher assets hidup.
Caranya dengan eksekusi command ini di terminal .

```sh
npm run watch
```

Saat update file maka watcher akan otomatis update file css/javascript di [resources/assets/frontend][Ffa] ke [public/front][Pfa] , tidak perlu execute ulang command.

## Update file assets front view

File assets Front View itu digenerate dari source [resources/assets/frontend][Ffa] .
Directory asset di copy saat menjalankan npm run watch, sesuai dengan settingan webpack.mix.js

Untuk penambahan file maka tambahkan di [resources/assets/frontend][Ffa] lalu execute ulang command :

```sh
npm run watch
```

atau

Jika ingin langsung, bisa lsg update file di public, tetapi pastikan file tersebut juga di copy di [resources/assets/frontend][Ffa] dengan penempatan posisi letak file serupa.


## File

Folder Frontend Assets file path : [resources/assets/frontend][Ffa]
Custom css file path : [resources/assets/frontend/css/custom.css][Ctm]

[Ctm]: <resources/assets/frontend/css/custom.css>
[Ffa]: <resources/assets/frontend>
[Pfa]: <public/front>
[Asp]: <app/Providers/AppServiceProvider.php>

