<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Kolom :attribute harus diterima.',
    'active_url'           => 'Kolom :attribute bukan URL yang valid.',
    'after'                => 'Kolom :attribute harus tanggal setelah :date.',
    'after_or_equal'       => 'Kolom :attribute harus tanggal setelah atau sama dengan :date.',
    'alpha'                => 'Kolom :attribute hanya boleh berisi huruf.',
    'alpha_dash'           => 'Kolom :attribute hanya boleh berisi huruf, angka, setrip dan garis bawah.',
    'alpha_num'            => 'Kolom :attribute hanya boleh berisi huruf dan angka.',
    'array'                => 'Kolom :attribute harus berupa larik.',
    'before'               => 'Kolom :attribute harus tanggal sebelum :date.',
    'before_or_equal'      => 'Kolom :attribute harus tanggal sebelum atau sama dengan :date.',
    'between'              => [
        'numeric' => 'Kolom :attribute harus di antara :min dan :max.',
        'file'    => 'Kolom :attribute harus di antara :min dan :max kilobyte.',
        'string'  => 'Kolom :attribute harus di antara :min dan :max karakter.',
        'array'   => 'Kolom :attribute harus memiliki :min dan :max item.',
    ],
    'boolean'              => 'Kolom :attribute harus memiliki nilai true atau false.',
    'confirmed'            => 'Kolom :attribute konfirmasi tidak cocok.',
    'date'                 => 'Kolom :attribute bukan tanggal yang valid.',
    'date_format'          => 'Kolom :attribute tidak sesuai dengan format :format.',
    'different'            => 'Kolom :attribute dan :other harus berbeda.',
    'digits'               => 'Kolom :attribute harus :digits digit.',
    'digits_between'       => 'Kolom :attribute harus diantara :min dan :max digit.',
    'dimensions'           => 'Kolom :attribute memiliki dimensi gambar yang tidak valid.',
    'distinct'             => 'Kolom :attribute memiliki duplikasi nilai.',
    'email'                => 'Kolom :attribute Harus alamat e-mail yang valid.',
    'exists'               => 'Kolom :attribute yang dipilih tidak valid.',
    'file'                 => 'Kolom :attribute harus berupa file.',
    'filled'               => 'Kolom :attribute harus memiliki nilai.',
    'gt'                   => [
        'numeric' => 'Kolom :attribute harus lebih besar daripada :value.',
        'file'    => 'Kolom :attribute harus lebih besar daripada :value kilobyte.',
        'string'  => 'Kolom :attribute harus lebih besar daripada :value karakter.',
        'array'   => 'Kolom :attribute harus memiliki lebih dari :value item.',
    ],
    'gte'                  => [
        'numeric' => 'Kolom :attribute harus lebih besar dari atau sama :value.',
        'file'    => 'Kolom :attribute harus lebih besar dari atau sama :value kilobyte.',
        'string'  => 'Kolom :attribute harus lebih besar dari atau sama :value karakter.',
        'array'   => 'Kolom :attribute harus memilikih :value item atau lebih.',
    ],
    'image'                => 'Kolom :attribute harus berupa gambar.',
    'in'                   => 'Kolom selected :attribute is invalid.',
    'in_array'             => 'Kolom :attribute tidak ada di :other.',
    'integer'              => 'Kolom :attribute harus berupa bilangan bulat.',
    'ip'                   => 'Kolom :attribute harus merupakan alamat IP yang valid.',
    'ipv4'                 => 'Kolom :attribute harus alamat IPv4 yang valid.',
    'ipv6'                 => 'Kolom :attribute harus alamat IPv6 yang valid.',
    'json'                 => 'Kolom :attribute harus berupa string JSON yang valid.',
    'lt'                   => [
        'numeric' => 'Kolom :attribute harus kurang dari :value.',
        'file'    => 'Kolom :attribute harus kurang dari :value kilobyte.',
        'string'  => 'Kolom :attribute harus kurang dari :value karakter.',
        'array'   => 'Kolom :attribute harus memiliki kurang dari :value item.',
    ],
    'lte'                  => [
        'numeric' => 'Kolom :attribute harus kurang dari atau sama :value.',
        'file'    => 'Kolom :attribute harus kurang dari atau sama :value kilobyte.',
        'string'  => 'Kolom :attribute harus kurang dari atau sama :value karakter.',
        'array'   => 'Kolom :attribute tidak boleh lebih dari :value item.',
    ],
    'max'                  => [
        'numeric' => 'Kolom :attribute mungkin tidak lebih besar dari :max.',
        'file'    => 'Kolom :attribute mungkin tidak lebih besar dari :max kilobyte.',
        'string'  => 'Kolom :attribute mungkin tidak lebih besar dari :max karakter.',
        'array'   => 'Kolom :attribute tidak memiliki lebih dari :max item.',
    ],
    'mimes'                => 'Kolom :attribute harus berupa file bertipe: :values.',
    'mimetypes'            => 'Kolom :attribute harus berupa file bertipe: :values.',
    'min'                  => [
        'numeric' => 'Kolom :attribute setidaknya harus :min.',
        'file'    => 'Kolom :attribute setidaknya harus :min kilobyte.',
        'string'  => 'Kolom :attribute setidaknya harus :min karakter.',
        'array'   => 'Kolom :attribute setidaknya harus ada :min item.',
    ],
    'not_in'               => 'Kolom :attribute yang dipilih tidak valid.',
    'not_regex'            => 'Kolom :attribute memiliki format yang tidak valid.',
    'numeric'              => 'Kolom :attribute harus berupa angka.',
    'present'              => 'Kolom :attribute harus ada.',
    'regex'                => 'Kolom :attribute memiliki format yang tidak valid.',
    'required'             => 'Kolom :attribute wajib diisi.',
    'required_if'          => 'Kolom :attribute wajib diisi jika :other adalah :value.',
    'required_unless'      => 'Kolom :attribute wajib diisi kecuali kalau :other adalah :values.',
    'required_with'        => 'Kolom :attribute wajib diisi jika :values ada.',
    'required_with_all'    => 'Kolom :attribute wajib diisi jika :values ada.',
    'required_without'     => 'Kolom :attribute wajib diisi jika :values tidak ada.',
    'required_without_all' => 'Kolom :attribute wajib diisi jika tidak ada :values dicantumkan.',
    'same'                 => 'Kolom :attribute dan :other harus cocok.',
    'size'                 => [
        'numeric' => 'Kolom :attribute harus :size.',
        'file'    => 'Kolom :attribute harus :size kilobyte.',
        'string'  => 'Kolom :attribute harus :size karakter.',
        'array'   => 'Kolom :attribute harus memiliki :size item.',
    ],
    'string'               => 'Kolom :attribute harus berupa string.',
    'timezone'             => 'Kolom :attribute harus merupakan zona yang valid.',
    'unique'               => 'Kolom :attribute sudah diambil.',
    'uploaded'             => 'Kolom :attribute gagal diunggah.',
    'url'                  => 'Kolom :attribute memiliki format yang tidak valid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | Kolom following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
