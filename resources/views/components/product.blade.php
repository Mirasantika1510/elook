<div class="ps-product {{ $class?? '' }}">
    <div class="ps-product__thumbnail">
        <a class="ps-product__overlay" href="{{ url('product-detail', $product->seo_page_slug?: $product->slug) }}"></a>
        @if($product->publishedDiscount)
            <span class="ps-product__badge"><i>{{ $product->publishedDiscount->discount_percent }}%</i></span>
        @endif
        @if(!$product->ignore_stock && ($product->stock <= $product->stock_used))
            <span class="ps-product__badge disabled"><i>Sold Out</i></span>
        @endif
        <a class="ps-product__img" href="{{ url('product-detail', $product->seo_page_slug?: $product->slug) }}">
            <img alt="" src="{{ $product->images->first()->imageUrl }}">
        </a>
        <a class="ps-product__img-alt" href="{{ url('product-detail', $product->seo_page_slug?: $product->slug) }}">
            <img alt="" src="{{ $product->images->first()->imageUrl }}">
        </a>

        <form id="add-wishlist-{{ $product->id }}-{{ $context }}" action="{{ url('add-wishlist', $product->id) }}" method="POST">
            @csrf() @method('PUT')
        </form>

        <a class="ps-product__favorite" href="#" onclick="event.preventDefault(); document.getElementById('add-wishlist-{{ $product->id }}-{{ $context }}').submit();">
            <i class="fa fa-heart-o"></i>
        </a>

        <ul class="ps-product__actions">
            @if($product->ignore_stock || (!$product->ignore_stock && ($product->stock > $product->stock_used)))
                <li>
                    <form style="display: inline-block;" id="form-add-{{ $product->id }}-{{ $context }}" action="{{ url('shop/add-to-cart', $product->id) }}" method="POST">{{ csrf_field() }}
                        @csrf
                    </form>

                    <img
                        src="{{ asset('front/img/add_to_cart.png') }}"
                        class="ps-btn-add-to-cart"
                        onClick="event.preventDefault(); document.getElementById('form-add-{{ $product->id }}-{{ $context }}').submit()"
                        class="ps-btn-add-to-cart"
                        {{ ($product->stock - $product->stock_used) <= 0? 'disabled' : '' }}
                    >
                </li>
            @endif
        </ul>
        {{-- <p class="ps-product__size">S, M, L</p> --}}
    </div>
    <div class="ps-product__content">
        <div class="ps-product__meta">
            <a href="shop-2-column.html"></a>
        </div>
        <a class="ps-product__title" href="{{ url('product-detail', $product->seo_page_slug?: $product->slug) }}">{{ $product->title }}</a>

        @if($product->publishedDiscount)
            <?php $priceNow = $product->sell_price - ($product->publishedDiscount->discount_percent/100 * $product->sell_price);?>
            <p class="ps-product__price sale">
                <del>Rp. {{ number_format($product->sell_price, 0, ',', '.') }}</del>
                Rp. {{ number_format($priceNow, 0, ',', '.') }}
            </p>
        @else
            <p class="ps-product__price">Rp. {{ number_format($product->sell_price, 0, ',', '.') }}</p>
        @endif

    </div>
</div>
