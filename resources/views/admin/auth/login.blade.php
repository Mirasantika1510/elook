<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ env('APP_NAME') }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

    <link href="{{asset('css/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendor.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <link href="{{ asset('favicon.ico') }}" rel="icon" type="image/x-icon" />
    <!-- /theme JS files -->
    @stack('styles')

    <style type="text/css">
        .info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.file-preview,.table.datatable-basic>thead>tr>th{border:none}.table.datatable-basic>thead>tr>th:last-child{width:10%}.table.datatable-basic>tbody>tr>td:last-child{text-align:center}.table.datatable-basic>tbody>tr>td{padding:.75rem 1.25rem}.file-preview-frame.kv-preview-thumb,.kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}
    </style>
    @stack('script')
</head>

<body>

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login form -->
                <form class="login-form" method="POST" action="{{ url(config('elook.admin_url').'/login') }}">
                    {{ csrf_field() }}
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Masuk ke akun anda</h5>
                                <span class="d-block text-muted">Masukkan email dan password anda.</span>
                            </div>

                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul style="margin-bottom: 0px;list-style: none;padding-left: 0;">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(Session::has('success-register'))

                                <div class="alert alert-success">
                                    {{ Session::get('success-register') }}
                                </div>

                            @endif

                            @if(Session::has('success-change-password'))

                                <div class="alert alert-success">
                                    {{ Session::get('success-change-password') }}
                                </div>

                            @endif

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="email" class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" placeholder="Username" name="email" value="{{ old('email') }}" autofocus>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" class="form-control {{ $errors->has('password')? 'border-danger' : '' }}" placeholder="Password" name="password">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Masuk <i class="icon-circle-right2 ml-2"></i></button>
                            </div>

                        </div>
                    </div>
                </form>
                <!-- /login form -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</body>
</html>
