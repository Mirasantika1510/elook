@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Banners",
			'link'	=> url(config('elook.admin_url').'/banners')
		],
		'breads' => [
			[ 'title' => 'Banners', 'link' => url(config('elook.admin_url').'/banners'), 'icon' => 'icon-images2'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Tambah Banner</h5>
			</div>

			<div class="card-body">
				<p class="text-grey info-form"><strong>NOTE : </strong>Semua kolom harus diisi.</p>

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/banners')}}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('page')? 'text-danger' : '' }}">Page</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('page')? 'border-danger' : '' }}" placeholder="Masukkan page banner" name="page" value="{{ old('page') }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Masukkan title banner" name="title" value="{{ old('title') }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('description')? 'text-danger' : '' }}">Description</label>
							<div class="col-lg-10">
								<textarea rows="5" cols="5" class="form-control summernote {{ $errors->has('description')? 'border-danger' : '' }}" name="description">{!! old('description') !!}</textarea>
							</div>
						</div>

						<div class="form-group row {{ $errors->has('image')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('image')? 'text-danger' : '' }}">Image</label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="image" data-fouc>
							</div>
						</div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection
@push('script')
	<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
	<script src="{{asset('js/fileinput.min.js')}}"></script>

	<script>

		var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		            '  <div class="modal-content">\n' +
		            '    <div class="modal-header align-items-center">\n' +
		            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		            '      <div class="kv-zoom-actions btn-group">{close}</div>\n' +
		            '    </div>\n' +
		            '    <div class="modal-body">\n' +
		            '      <div class="floating-buttons btn-group"></div>\n' +
		            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		            '    </div>\n' +
		            '  </div>\n' +
		            '</div>\n';

		document.addEventListener('DOMContentLoaded', function() {
			if($().summernote){
		    	$('.summernote').summernote({disableDragAndDrop: true,toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
				  ]});
		    }

		    if($().fileinput){
		        $('.image').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000
		        });
		    }

		});


	</script>
@endpush