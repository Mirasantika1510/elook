@extends('admin.layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ekko-lightbox.min.css') }}">
@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Orders",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Orders', 'link' => null, 'icon' => 'icon-cart'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
                <h5 class="card-title">Data Orders</h5>
				<form id="accept-row-all" action="{{ url(config('elook.admin_url').'/orders-accept-all') }}" method="POST">
					{!! csrf_field().method_field("PUT") !!}
					<input type="hidden" name="order_id" value="" />
				</form>

				<form id="status-row-all" action="{{ url(config('elook.admin_url').'/orders-status-all') }}" method="POST">
					{!! csrf_field().method_field("PUT") !!}
					<input type="hidden" name="order_id" value="" />
					<input type="hidden" name="order_status" value="" />
				</form>

		        <div class="">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<button type="button" data-action="accept" class="btn btn-primary ">Accept Transaction</button>
						</div>
						<div class="input-group-prepend">
							<button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span>Order Status</span>
							</button>
							<div class="dropdown-menu">
							<button type="button" data-status="packing" data-action="order_status" class="dropdown-item"><i class="fa fa-box"></i> Packing</button>
							<button type="button" data-status="shipping" data-action="order_status" class="dropdown-item"><i class="fa fa-shipping-fast"></i> Shipping</button>

							</div>
						</div>
						<div class="input-group-append">
							<button type="button" class="btn bg-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="">Report</span>
							</button>
							<div class="dropdown-menu">
								<a href="{{ url(config('elook.admin_url').'/reports') }}?report=monthly-sells-orders" class="dropdown-item"><i class="icon-list3"></i> Monthly Sales</a>
								<a href="{{ url(config('elook.admin_url').'/reports') }}?report=best-selling-products" class="dropdown-item"><i class="icon-list3"></i> Best Seller Products</a>
								<a href="{{ url(config('elook.admin_url').'/reports') }}?report=best-member" class="dropdown-item"><i class="icon-list3"></i> Sales per Member</a>
								<a href="{{ url(config('elook.admin_url').'/reports') }}?report=waiting-list" class="dropdown-item"><i class="icon-list3"></i> Waiting List</a>
								<a href="{{ url(config('elook.admin_url').'/reports') }}?report=abandoned-carts" class="dropdown-item"><i class="icon-list3"></i> Abandoned Carts</a>
								<a href="{{ url(config('elook.admin_url').'/reports') }}?report=inventory" class="dropdown-item"><i class="icon-list3"></i> Inventory</a>
								<a href="{{ url(config('elook.admin_url').'/reports') }}?report=packing-list" class="dropdown-item"><i class="icon-list3"></i> Packing List</a>
							</div>
						</div>
					</div>
                </div>
			</div>

			<table class="table datatable-basic table-striped" id="orders-tables">
		        <thead>
		            <tr>
                        <th>
                            <input type="checkbox" value="0" class="selectAll">
                        </th>
                        <th>No </th>
                        <th>Invoice Number </th>
                        <th>Total Price </th>
                        <th>Unique Number </th>
                        <th>Transaction Status </th>
                        <th>Payment Receipt </th>
                        <th>Order Status </th>
                        <th>Action </th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/uniform.min.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/ekko-lightbox.min.js') }}"></script>
    <script src="{{ asset('js/datatable-extension-select.min.js') }}"></script>
	<script src="{{ asset('js/datatable-extension-buttons.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('.selectAll').uniform();
            var orderTables = $('#orders-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/orders-tables')}}",
                rowId: 'id',
				columnDefs: [
					{
						orderable: false,
                        className: 'select-checkbox',
                        targets: 0,
                        data: null,
                        defaultContent: ""
					}
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                columns: [
                    null,
                    { data: 'rownum', name: 'rownum' },
                    { data: 'invoice_number', name: 'invoice_number' },
                    { data: 'final_price', name: 'final_price' },
                    { data: 'unique_number', name: 'unique_number' },
                    { data: 'payment.transaction_status', name: 'payment.transaction_status' },
                    { data: 'payment.payment_receipt', name: 'payment.payment_receipt' },
                    { data: 'order_status', name: 'order_status' },
                    { data: 'actions', name: 'actions', sortable: false },
                ],
                order: [[ 1, "asc" ]],
				drawCallback : function( settings, json ) {
                    $("#orders-tables .selectAll").on( "change", function(e) {
                        if(event.target.checked) {
                            orderTables.rows().select();
                        }else{
                            orderTables.rows().deselect();
                        }
					});

				}
            });

			$('[data-action="accept"]').on("click",function(e){
				var rowsSelected = orderTables.rows( { selected: true } );
                if(rowsSelected.count()){
					$('[name="order_id"]').val(rowsSelected.ids().toArray().join());
					$('#accept-row-all').submit();
                }else{
                    alert('Select data first before action.');
                }
			});


			$('[data-action="order_status"]').on("click",function(e){
				var rowsSelected = orderTables.rows( { selected: true } );
                if(rowsSelected.count()){
                    var status = $(this).data('status');
					$('[name="order_id"]').val(rowsSelected.ids().toArray().join());
					$('[name="order_status"]').val(status);
					$('#status-row-all').submit();
                }else{
                    alert('Select data first before action.');
                }
			});


            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
        });
    </script>
@endpush
