@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.breadcrum', [
        'previous' => [
            'title' => "Tracking List",
            'link'  => url(config('elook.admin_url').'/orders-tracking/'. $order->id)
        ],
        'breads' => [
            [ 'title' => 'Orders', 'link' => url(config('elook.admin_url').'/orders'), 'icon' => 'icon-cart'  ],
            [ 'title' => "Tracking List Order ( $order->invoice_number )", 'link' => url(config('elook.admin_url').'/orders-tracking/'. $order->id), 'icon' => 'icon-cart'  ],
            [ 'title' => 'create', 'link' => null, 'icon' => null ]
        ]
    ])


    <!-- Content area -->
    <div class="content">

        <!-- Form inputs -->
        <div class="card" style="padding: 20px 30px;">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Tambah Tracking Order Baru</h5>
            </div>

            <div class="card-body">

                @include('admin.layouts.validation_error', [ 'errors' => $errors ])

                <form action="{{ url(config('elook.admin_url').'/store-tracking', $order->id) }}" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <fieldset class="mb-3">

                        <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Masukkan title tracking order" name="title" value="{{ old('title') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('description')? 'text-danger' : '' }}">Description <span class="text-danger">*</span></label>

                            <div class="col-lg-10">
                                <textarea rows="5" cols="5" class="form-control {{ $errors->has('description')? 'border-danger' : '' }}" name="description" placeholder="Enter your comment here">{!! old('description') !!}</textarea>
                            </div>
                        </div>

                    </fieldset>


                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /form inputs -->

    </div>
    <!-- /content area -->

@endsection

@push('script')
    <script src="{{asset('js/fileinput.min.js')}}"></script>
    <script src="{{ asset('js/selects/select2.min.js') }}"></script>

    <script>

        document.addEventListener('DOMContentLoaded', function() {

            $().select2 &&  $('.select').select2({ minimumResultsForSearch: Infinity });

        });


    </script>
@endpush
