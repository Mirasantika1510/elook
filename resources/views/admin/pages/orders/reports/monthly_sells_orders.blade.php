@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.breadcrum', [
        'previous' => [
            'title' => "Orders",
            'link'  => null
        ],
        'breads' => [
            [ 'title' => 'Orders', 'link' => null, 'icon' => 'icon-cart'  ],
            [ 'title' => 'index', 'link' => null, 'icon' => null ]
        ]
    ])


    <!-- Content area -->
    <div class="content">

        <!-- Control position -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Report For Monthly Sales Performance</h5>
                <div>
                    <a href="{{ url(config('elook.admin_url').'/export-excel') }}?report={{ $data }}" type="button" class="btn bg-success"><i class="icon-clipboard3 mr-2"></i>Export Excel</a>
                    <div class="btn-group">
                        <button type="button" class="btn bg-success dropdown-toggle" data-toggle="dropdown">Report <i class="icon-clipboard3 ml-2"></i></button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ url(config('elook.admin_url').'/reports') }}?report=monthly-sells-orders" class="dropdown-item"><i class="icon-list3"></i> Monthly Sales</a>
                            <a href="{{ url(config('elook.admin_url').'/reports') }}?report=best-selling-products" class="dropdown-item"><i class="icon-list3"></i> Best Seller Products</a>
                            <a href="{{ url(config('elook.admin_url').'/reports') }}?report=best-member" class="dropdown-item"><i class="icon-list3"></i> Sales per Member</a>
                            <a href="{{ url(config('elook.admin_url').'/reports') }}?report=waiting-list" class="dropdown-item"><i class="icon-list3"></i> Waiting List</a>
                            <a href="{{ url(config('elook.admin_url').'/reports') }}?report=abandoned-carts" class="dropdown-item"><i class="icon-list3"></i> Abandoned Carts</a>
                            <a href="{{ url(config('elook.admin_url').'/reports') }}?report=inventory" class="dropdown-item"><i class="icon-list3"></i> Inventory</a>
                        	<a href="{{ url(config('elook.admin_url').'/reports') }}?report=packing-list" class="dropdown-item"><i class="icon-list3"></i> Packing List</a>
							</div>
                    </div>
                </div>
            </div>

            <table class="table datatable-basic table-striped" id="orders-tables">
                <thead>
                    <tr>
                      <th>No </th>
                      <th>Month </th>
                      <th>Total Sales </th>
                      <th>Total Items </th>
                      <th>Total Shipping Fee </th>
                      <th>Total Discount </th>
                      <th>Total Sales Amount </th>
                    </tr>
                </thead>
            </table>
        </div>

    </div>
    <!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script>
        $(function() {
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Masukkan pencarian...',
                    lengthMenu: '<span>Tampilkan:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                }
            });

            $('#orders-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/monthly-sells-orders')}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'date', name: 'date' },
                    { data: 'total_sells', name: 'total_sells' },
                    { data: 'total_items', name: 'total_items' },
                    { data: 'total_shipping_price', name: 'total_shipping_price' },
                    { data: 'total_discount', name: 'total_discount' },
                    { data: 'total_price', name: 'total_price' }
                ]
            });

        });
    </script>
@endpush
