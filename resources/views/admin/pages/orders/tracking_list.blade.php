@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.breadcrum', [
        'previous' => [
            'title' => "Orders",
            'link'  => url(config('elook.admin_url').'/orders')
        ],
        'breads' => [
            [ 'title' => 'Orders', 'link' => url(config('elook.admin_url').'/orders'), 'icon' => 'icon-cart'  ],
            [ 'title' => 'index', 'link' => null, 'icon' => null ]
        ]
    ])


    <!-- Content area -->
    <div class="content">

        <!-- Control position -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data Tracking List : Order  <strong>( {{ $order->invoice_number }} )</strong></h5>
                <a href="{{ url(config('elook.admin_url').'/create-tracking', $order->id) }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-database-add mr-2"></i> Tambah Tracking Order </a>
            </div>

            <table class="table datatable-basic table-striped" id="tracking-tables">
                <thead>
                    <tr>
                      <th>No </th>
                      <th>Title </th>
                      <th>Description </th>
                      <th>Action </th>
                    </tr>
                </thead>
            </table>
        </div>

    </div>
    <!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script>
        $(function() {
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Masukkan pencarian...',
                    lengthMenu: '<span>Tampilkan:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                }
            });

            $('#tracking-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/tracking-tables', $order->id)}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'actions', name: 'actions', sortable: false },
                ]
            });
        });
    </script>
@endpush
