@extends('admin.layouts.master')


@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Orders",
			'link'	=> url(config('elook.admin_url').'/orders')
		],
		'breads' => [
			[ 'title' => 'Orders', 'link' => url(config('elook.admin_url').'/orders'), 'icon' => 'icon-cart'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<div class="card" style="padding: 25px;">
            <div class="card-header bg-transparent header-elements-inline">
                <h6 class="card-title">Detail Order</h6>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 offset-sm-6">
                        <div class="mb-4">
                            <div class="text-sm-right">
                                <h4 class="text-primary mb-2 mt-md-2">Invoice #{{ $order->invoice_number }}</h4>
                                <ul class="list list-unstyled mb-0">
                                    <li>DateTime: <span class="font-weight-semibold">{{ $order->created_at->format('h : i - F d, Y') }}</span></li>
                                    <li>Due DateTime: <span class="font-weight-semibold">{{ $order->created_at->addHour()->format('h : i - F d, Y') }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-md-flex flex-md-wrap">
                    <div class="mb-4 mb-md-2">
                        <span class="text-muted">Invoice To:</span>
                        <ul class="list list-unstyled mb-0">
                            <li><h5 class="my-2">{{ $order->shipping->firstname }} {{ $order->shipping->lastname }}</h5></li>
                            <li><span class="font-weight-semibold">{{ $order->customer? 'Member' : 'Guest' }}</span></li>
                            <li>{!! $order->shipping->address !!}</li>
                            <li>{{ $order->shipping->subdistrict }}, {{ $order->shipping->city }}, {{ $order->shipping->province }} </li>
                            <li>{{ $order->shipping->postcode }}</li>
                            <li>{{ $order->shipping->phonenumber }}</li>
                            <li><a href="mailto:{{ $order->shipping->email }}">{{ $order->shipping->email }}</a></li>
                        </ul>
                    </div>

                    <div class="mb-2 ml-auto">
                        <span class="text-muted">Payment Details:</span>
                        <div class="d-flex flex-wrap wmin-md-400">
                            <ul class="list list-unstyled mb-0">
                                <li><h5 class="my-2">Total Due:</h5></li>
                                <li>Bank name:</li>
                                <li>Province:</li>
                                <li>City:</li>
                                <li>Address:</li>
                                <li>Status:</li>
                            </ul>

                            <ul class="list list-unstyled text-right mb-0 ml-auto">
                                <li>
                                    <h5 class="font-weight-semibold my-2">
                                        Rp. {{ number_format($order->total_price + $order->unique_number, 0, ',', '.') }}
                                    </h5>
                                </li>
                                <li><span class="font-weight-semibold">{{ $order->payment->account_bank }}</span></li>
                                <li>{{ $order->shipping->province }}</li>
                                <li>{{ $order->shipping->city }}</li>
                                <li>{!! $order->shipping->address?: '-' !!}</li>
                                <li>
                                    @if($order->payment->transaction_status === 'pending')
                                        <span class="badge badge-warning">Pending</span>
                                    @else
                                        <span class="badge badge-success">Settlement</span>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Image</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order->products as $product)
                            <tr>
                                <td><h6 class="mb-0">{{ $product->product_name }}</h6></td>
                                <td>{!! $product->product_discount? '<del>Rp. '. number_format($product->product_price, 0, ',', '.') .'</del>' : '' !!} Rp. {{ number_format($product->product_price - ($product->product_discount/100*$product->product_price), 0, ',', '.') }}</td>
                                <td>{{ $product->quantities }}</td>
                                <td><img src="{{ $product->product_image }}" style="width: 100px;"></td>
                                <td>
                                    <span class="font-weight-semibold">
                                        @if($product->product_discount)
                                            Rp. {{ number_format(($product->product_price - ($product->product_discount/100*$product->product_price)) * $product->quantities, 0, ',', '.') }}
                                        @else
                                            Rp. {{ number_format($product->product_price * $product->quantities, 0, ',', '.') }}
                                        @endif
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="card-body">
                <div class="d-md-flex flex-md-wrap">

                    <div class="pt-2 mb-3 wmin-md-400 ml-auto">
                        <h6 class="mb-3">Total due</h6>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    @if($order->voucher)
                                        <tr>
                                            <th>Subtotal:</th>
                                            <td class="text-right">Rp. {{ number_format($order->total_price + $order->voucher->amount, 0, ',', '.') }}</td>
                                        </tr>
                                        <tr>
                                            <th>Voucher: <span class="badge badge-success">{{ $order->voucher->voucher_code }}</span></th>
                                            <td class="text-right text-danger">- Rp. {{ number_format($order->voucher->amount, 0, ',', '.') }}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <th>Subtotal:</th>
                                            <td class="text-right">Rp. {{ number_format($order->total_price, 0, ',', '.') }}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th>Shipping:</th>
                                        <td class="text-right">Rp. {{ number_format($order->shipping_price, 0, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td class="text-right text-primary">
                                            <h5 class="font-weight-semibold text-warning">Rp. {{ number_format($order->total_price, 0, ',', '.') }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Unique Number:</th>
                                        <td class="text-right">{{ $order->unique_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total + Unique Number:</th>
                                        <td class="text-right text-primary">
                                            <h5 class="font-weight-semibold">Rp. {{ number_format($order->total_price + $order->unique_number, 0, ',', '.') }}</h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        @if($order->payment->transaction_status === 'pending')

                            <form action="{{ url(config('elook.admin_url').'/orders-accept-payment', $order->id) }}" method="POST">

                                @csrf()

                                @method('PUT')

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary"> Accept Transaction </button>
                                </div>

                            </form>

                        @endif

                    </div>
                </div>
            </div>

            <div class="card-footer">
                <span class="text-muted"></span>
            </div>
        </div>

	</div>
	<!-- /content area -->

@endsection
