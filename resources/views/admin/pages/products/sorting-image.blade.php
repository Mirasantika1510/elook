@extends('admin.layouts.master')


@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Products",
			'link'	=> url(config('elook.admin_url').'/products')
		],
		'breads' => [
			[ 'title' => 'Products', 'link' => url(config('elook.admin_url').'/products'), 'icon' => 'icon-cube4'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Product</h5>
			</div>

			<div class="card-body">
				{{-- <p class="mb-3">Add new category by input title of the category and submit it.<strong><br>Add new category as a child of the existing category can be done by first <code>click</code> one of the category that you wish to be the new categories parent, input title, and submit it.</strong><br> You can edit existing category by <code>double click</code> the category, type new title right there</p> --}}
				@php
				$data_count = $data->count();
				$product_count = $products->count();
				@endphp
				<form action="/wee-management/sorting/{{ $product->id }}" method="post">
					<div class="row">

						@foreach ($data as $item)
						<div class="{{ $data_count === 2 ? 'col-6' : ($data_count === 3 ? 'col-4' : '') }}">
							<img src="{{ $item['imageUrl'] }}" alt="{{ $item['caption'] }}" style="width: 50%">
						</div>
						@endforeach
						@foreach ($products as $item)
						<div class="{{ $product_count === 2 ? 'col-6' : ($product_count === 3 ? 'col-4' : '') }}">
							@csrf
							<span>For : <b>{{ $item->caption }}</b></span>
							<input type="hidden" name="id[]" value="{{ $item->id }}">
							<input type="number" name="order[]" class="form-control" value="{{ $item->order }}" required="">
							@error('order')
							<span class="text-danger">{{ $message }}</span>
							@enderror
							<br>
						</div>

						@endforeach
					</div>
					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')


@endpush
