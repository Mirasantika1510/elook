@extends('admin.layouts.master')

@push('styles')
	<style>
        .item {
            width: 100px;
            height: 100px;
            display: inline-block;
            background-color: #fff;
            border: solid 1px rgb(0,0,0,0.2);
            padding: 10px;
            margin: 12px;
            background-size: cover;
            background-position: center;
        }

        .items {
            flex-basis: 0;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
        }
	</style>
@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Products",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Products', 'link' => null, 'icon' => 'icon-twitter'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Sort Products</h5>
            </div>

            <div class="card-body">

                <div class="grid">
                    <div id="items" class="items">
                        @foreach($products as $product)
                            <div data-id="{{ $product->id }}" class="item" style="background-image: url({{ $product->images()->inRandomOrder()->first()->imageMediumUrl }})"></div>
                        @endforeach
                    </div>
                </div>

            </div>

		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
    <script>
        jQuery.each( [ "put", "delete" ], function( i, method ) {
            jQuery[ method ] = function( url, data, callback, type ) {
                if ( jQuery.isFunction( data ) ) {
                    type = type || callback;
                    callback = data;
                    data = undefined;
                }

                return jQuery.ajax({
                    url: url,
                    type: method,
                    dataType: type,
                    data: data,
                    success: callback
                });
            };
        });

        $(document).ready(function(){

            var el = document.getElementById('items');
            var sortable = new Sortable(el, {
                sort: true,
                onSort: function (evt) {
                    console.log(sortable.toArray())
                    $.put("{{ url('wee-management/sort-products') }}", { 
                        product_sorted: sortable.toArray(), 
                        _token : $('meta[name=csrf-token]').attr('content') 
                    }, function(result){
                        console.log('sorted');
                    })
                },
            });

        });
    </script>
@endpush
