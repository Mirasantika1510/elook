@extends('admin.layouts.master')

@push('styles')
	<style>
		div.border-danger .select2-selection--multiple:not([class*=bg-]):not([class*=border-]) {
		    border-color: #f00505;
		}

		.file-preview .close {
		    right: 5px;
		}

		.border-danger .input-group>.custom-select:not(:last-child),
		.border-danger .input-group>.form-control:not(:last-child) {
		    border: 1px solid #f44336;
		}

		.border-danger .btn-file {
		    background-color: #f44336;
		}

		input.select2-search__field {
            width: 100% !important;
            display: block !important;
        }

        .bootstrap-duallistbox-container {
            width: 100%;
        }

        span.info-container {
            display: none;
        }

        .form-group.row .form-check:not(.dropdown-item) {
            margin-top: 0px;
        }

		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview{border:none}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}.kv-file-content>img{width: auto!important; }.form-group.row.desc-error .note-editor{border-color: #f55246;}
	</style>
@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Products",
			'link'	=> url(config('elook.admin_url').'/products')
		],
		'breads' => [
			[ 'title' => 'Products', 'link' => url(config('elook.admin_url').'/products'), 'icon' => 'icon-cube4'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Product</h5>
			</div>

			<div class="card-body">
				{{-- <p class="mb-3">Add new category by input title of the category and submit it.<strong><br>Add new category as a child of the existing category can be done by first <code>click</code> one of the category that you wish to be the new categories parent, input title, and submit it.</strong><br> You can edit existing category by <code>double click</code> the category, type new title right there</p> --}}

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form id="form-submit" action="{{ url(config('elook.admin_url').'/products') }}?type=normal" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>


						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Input title here" name="title" value="{{ old('title') }}">
							</div>
						</div>

						<div class="form-group row {{ $errors->has('specification')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('specification')? 'text-danger' : '' }}">Specification <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea class="summernote" name="specification">{{ old('specification') }}</textarea>
							</div>
                        </div>

                        <div class="form-group row {{ $errors->has('description')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('description')? 'text-danger' : '' }}">Description</label>

							<div class="col-lg-10">
								<textarea class="summernote" name="description">{{ old('description') }}</textarea>
							</div>
                        </div>

                        <div class="form-group row {{ $errors->has('additional_information')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('additional_information')? 'text-danger' : '' }}">Additional Information</label>

							<div class="col-lg-10">
								<textarea class="summernote" name="additional_information">{{ old('additional_information') }}</textarea>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('images')? 'text-danger' : '' }}">Image <span class="text-danger">*</span></label>
							<div class="col-lg-10 {{ $errors->has('images')? 'border-danger' : '' }}">
								<div class="file-loading">
								    <input id="input-image" name="images[]" type="file" multiple accept="image/*">
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('product_code')? 'text-danger' : '' }}">Product Code</label>
							<div class="col-lg-3">
								<div class="input-group">
									<input type="product_code" class="form-control {{ $errors->has('product_code')? 'border-danger' : '' }}" placeholder="Input product_code here" name="product_code" value="{{ old('product_code') }}">
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('brand')? 'text-danger' : '' }}">Brand</label>
							<div class="col-lg-3">
								<div class="input-group">
									<input type="brand" class="form-control {{ $errors->has('brand')? 'border-danger' : '' }}" placeholder="Input brand here" name="brand" value="{{ old('brand') }}">
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('price')? 'text-danger' : '' }}">Base Price <span class="text-danger">*</span></label>
							<div class="col-lg-3">
								<div class="input-group">
									<span class="input-group-prepend">
										<span class="input-group-text">Rp.</span>
									</span>
									<input type="text" class="form-control input-price {{ $errors->has('price')? 'border-danger' : '' }}" placeholder="Input price here" name="price" value="{{ old('price') }}">
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('sell_price')? 'text-danger' : '' }}">Sell Price <span class="text-danger">*</span></label>
							<div class="col-lg-3">
								<div class="input-group">
									<span class="input-group-prepend">
										<span class="input-group-text">Rp.</span>
									</span>
									<input type="text" class="form-control input-price {{ $errors->has('sell_price')? 'border-danger' : '' }}" placeholder="Input sale price here" name="sell_price" value="{{ old('sell_price') }}">
								</div>
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('stock')? 'text-danger' : '' }}">Stock</label>
							<div class="col-lg-3">
								<div class="input-group">
									<input type="number" class="form-control {{ $errors->has('stock')? 'border-danger' : '' }}" placeholder="Input stock here" name="stock" value="{{ old('stock') }}">
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('weight')? 'text-danger' : '' }}">Weight <span class="text-danger">*</span></label>
							<div class="col-lg-3">
								<div class="input-group">
									<input type="number" class="form-control {{ $errors->has('weight')? 'border-danger' : '' }}" placeholder="Input weight here" name="weight" value="{{ old('weight') }}">
									<span class="input-group-prepend">
										<span class="input-group-text">gr</span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('length')? 'text-danger' : '' }}">Length</label>
							<div class="col-lg-3">
								<div class="input-group">
									<input type="number" class="form-control {{ $errors->has('length')? 'border-danger' : '' }}" placeholder="Input length here" name="length" value="{{ old('length') }}">
									<span class="input-group-prepend">
										<span class="input-group-text">cm</span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('width')? 'text-danger' : '' }}">Width</label>
							<div class="col-lg-3">
								<div class="input-group">
									<input type="number" class="form-control {{ $errors->has('width')? 'border-danger' : '' }}" placeholder="Input width here" name="width" value="{{ old('width') }}">
									<span class="input-group-prepend">
										<span class="input-group-text">cm</span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('height')? 'text-danger' : '' }}">Height</label>
							<div class="col-lg-3">
								<div class="input-group">
									<input type="number" class="form-control {{ $errors->has('height')? 'border-danger' : '' }}" placeholder="Input height here" name="height" value="{{ old('height') }}">
									<span class="input-group-prepend">
										<span class="input-group-text">cm</span>
									</span>
								</div>
							</div>
                        </div>

                        {{-- <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('start_date')? 'text-danger' : '' }}">Start Date</label>
							<div class="col-lg-3">
								<div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-calendar5"></i></span>
                                    </span>
                                    <input type="text" class="form-control pickadate" placeholder="Pick Date&hellip;" name="start_date" data-value="{{ old('start_date_submit') }}">
                                </div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('end_date')? 'text-danger' : '' }}">End Date</label>
							<div class="col-lg-3">
								<div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-calendar5"></i></span>
                                    </span>
                                    <input type="text" class="form-control pickadate" placeholder="Pick Date&hellip;" name="end_date" data-value="{{ old('end_date_submit') }}">
                                </div>
							</div>
						</div> --}}

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('related_product')? 'text-danger' : '' }}">Related Product</label>
							<div class="col-lg-10">
								<div class="input-group">
									<select multiple="multiple" class="form-control listbox-filter-disabled" data-fouc name="related_product[]">
                                        @foreach($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->title }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('categories')? 'text-danger' : '' }}">Categories <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<div class="input-group">
									<select multiple="multiple" class="form-control listbox-filter-disabled" data-fouc name="categories[]">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ old('categories') && in_array($category->id, old('categories'))? 'selected' : ''}}>{{ $category->title }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('tags')? 'text-danger' : '' }}">Tags</label>
							<div class="col-lg-10">
								<div class="input-group">
									<select class="form-control select-multiple-tags" multiple="multiple" data-fouc name="tags[]">
										@foreach($product_tags as $tag)
                                            <option value="{{ $tag->name }}" {{ old('tags') && in_array($tag->id, old('tags'))? 'selected' : ''}}>{{ $tag->title }}</option>
                                        @endforeach
									</select>
								</div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('seo_page_title')? 'text-danger' : '' }}">SEO Page Title</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('seo_page_title')? 'border-danger' : '' }}" placeholder="Input seo page title here" name="seo_page_title" value="{{ old('seo_page_title') }}">
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('seo_meta_description')? 'text-danger' : '' }}">SEO Meta Description</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('seo_meta_description')? 'border-danger' : '' }}" placeholder="Input seo meta description here" name="seo_meta_description" value="{{ old('seo_meta_description') }}">
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('seo_meta_keywords')? 'text-danger' : '' }}">SEO Meta Keyword</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('seo_meta_keywords')? 'border-danger' : '' }}" placeholder="Input seo meta keywords here" name="seo_meta_keywords" value="{{ old('seo_meta_keywords') }}">
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('seo_page_slug')? 'text-danger' : '' }}">SEO Page Slug</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('seo_page_slug')? 'border-danger' : '' }}" placeholder="Input seo page slug here" name="seo_page_slug" value="{{ old('seo_page_slug') }}">
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('featured')? 'text-danger' : '' }}">Featured</label>
							<div class="col-lg-10">
								<div class="form-check form-check-switch form-check-switch-left">
                                    <input type="checkbox" data-size="mini" data-off-color="danger" data-on-text="Yes" data-off-text="No" class="form-check-input-switch" name="featured" {{ old('featured')? 'checked' : '' }}>
                                </div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('best_seller')? 'text-danger' : '' }}">Best Seller</label>
							<div class="col-lg-10">
								<div class="form-check form-check-switch form-check-switch-left">
                                    <input type="checkbox" data-size="mini" data-off-color="danger" data-on-text="Yes" data-off-text="No" class="form-check-input-switch" name="best_seller" {{ old('best_seller')? 'checked' : '' }}>
                                </div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('top_rated')? 'text-danger' : '' }}">Top Rated</label>
							<div class="col-lg-10">
								<div class="form-check form-check-switch form-check-switch-left">
                                    <input type="checkbox" data-size="mini" data-off-color="danger" data-on-text="Yes" data-off-text="No" class="form-check-input-switch" name="top_rated" {{ old('top_rated')? 'checked' : '' }}>
                                </div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('new')? 'text-danger' : '' }}">New</label>
							<div class="col-lg-10">
								<div class="form-check form-check-switch form-check-switch-left">
                                    <input type="checkbox" data-size="mini" data-off-color="danger" data-on-text="Yes" data-off-text="No" class="form-check-input-switch" name="new" {{ old('new')? 'checked' : '' }}>
                                </div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('allow_back_order')? 'text-danger' : '' }}">Allow Backorder</label>
							<div class="col-lg-10">
								<div class="form-check form-check-switch form-check-switch-left">
                                    <input type="checkbox" data-size="mini" data-off-color="danger" data-on-text="Yes" data-off-text="No" class="form-check-input-switch" name="allow_back_order" {{ old('allow_back_order')? 'checked' : '' }}>
                                </div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('ignore_stock')? 'text-danger' : '' }}">Ignore Stock</label>
							<div class="col-lg-10">
								<div class="form-check form-check-switch form-check-switch-left">
                                    <input type="checkbox" data-size="mini" data-off-color="danger" data-on-text="Yes" data-off-text="No" class="form-check-input-switch" name="ignore_stock" {{ old('ignore_stock')? 'checked' : '' }}>
                                </div>
							</div>
                        </div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('published')? 'text-danger' : '' }}">Publish</label>
							<div class="col-lg-10">
								<div class="form-check form-check-switch form-check-switch-left">
                                    <input type="checkbox" data-size="mini" data-off-color="danger" data-on-text="Yes" data-off-text="No" class="form-check-input-switch" checked name="published" {{ old('published')? 'checked' : '' }}>
                                </div>
							</div>
                        </div>


					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>

			</div>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('js/jquery-mask.min.js') }}"></script>
	<script src="{{ asset('js/selects/select2.min.js') }}"></script>
	<script src="{{ asset('js/fileinput.min.js') }}"></script>
	<script src="{{ asset('js/blockui.min.js') }}"></script>
	<script src="{{ asset('js/notifications/noty.min.js') }}"></script>
	<script src="{{ asset('js/duallistbox.min.js') }}"></script>
	<script src="{{ asset('js/uniform.min.js') }}"></script>
	<script src="{{ asset('js/switchery.min.js') }}"></script>
    <script src="{{ asset('js/switch.min.js') }}"></script>
    <script src="{{ asset('js/pickers/pickadate/picker.js') }}"></script>
	<script src="{{ asset('js/pickers/pickadate/picker.date.js') }}"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {

		    if($().summernote){
		    	$('.summernote').summernote({disableDragAndDrop: true,toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
				  ]});
		    }

		    $().mask && $('.input-price').mask('000.000.000.000.000.000.000.000.000', {reverse: true});

		    if($().select2){
                $('.select').select2({ minimumResultsForSearch: Infinity });
                $('.select-multiple-tags').select2({
                    tags: true
                });
            }


		    $(document).on('select2:unselect', '.select-multiple-tags', function (e) {
		    	var $this = $(this);
				$this.next().find('input').css({ 'display': 'block', 'width': 'auto !important' })
			});

			$(document).on('select2:select', '.select-multiple-tags', function (e) {
		    	var $this = $(this);
				$this.next().find('input').css({ 'display': 'none' })
			});

		    Noty.overrideDefaults({ theme: 'limitless' });

		    $("#input-image").fileinput({
			    overwriteInitial: false,
			    showUpload: false,
			    fileActionSettings: {
			    	showUpload: false,
			    	showDrag: true,
			    	uploadIcon: '<i class="icon-upload"></i>',
			    	removeIcon: '<i class="icon-trash"></i>',
			    	zoomIcon: '<i class="icon-zoomin3"></i>',
			    	dragIcon: '<i class="icon-move"></i>',
			    	indicatorNew: '<i class="icon-plus-circle2 text-warning"></i>',
				    indicatorSuccess: '<i class="icon-checkmark4 text-success"></i>',
				    indicatorError: '<i class="icon-warning22 text-danger"></i>',
				    indicatorLoading: '<i class="icon-hour-glass text-muted"></i>',
			    },
			    allowedFileExtensions: ['jpg', 'jpeg', 'png'],
			    previewZoomButtonIcons: {
					prev: '<i class="icon-arrow-left15"></i>',
				    next: '<i class="icon-arrow-right15"></i>',
				    toggleheader: '<i class="icon-move-vertical"></i>',
				    fullscreen: '<i class="icon-screen-full"></i>',
				    borderless: '<i class="icon-screen-normal"></i>',
				    close: '<i class="icon-x"></i>'
			    },
			    initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
			    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
			    purifyHtml: true, // this by default purifies HTML data for preview
    			maxFileSize: 1000
            });

            if ($().bootstrapDualListbox) {
                $('.listbox-filter-disabled').bootstrapDualListbox({
                    showFilterInputs: false
                });
            }

            $().bootstrapSwitch && $('.form-check-input-switch').bootstrapSwitch();

            $().pickadate && $('.pickadate').pickadate({ formatSubmit: 'yyyy-mm-d' });

		});

	</script>
@endpush
