@extends('admin.layouts.master')

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/jquery-confirm.min.css') }}">
	<style>
		.jconfirm.jconfirm-white .jconfirm-box .jconfirm-buttons button, .jconfirm.jconfirm-light .jconfirm-box .jconfirm-buttons button {
		    text-transform: capitalize;
		    font-size: 12px;
			font-weight: normal;
		    text-shadow: none;
		}

		.file-preview .close {
		    right: 5px;
		}

		.noty_layout {
			z-index: 99999999 !important;
		}

		#kvFileinputModal.modal,
		#kvFileinputModal .btn-navigate {
			z-index: 99999999 !important;
		}

		#kvFileinputModal .btn-navigate {
			z-index: 99999999 !important;
			color: #1c504b;
		}
		.file-footer-buttons button.btn-danger {
			background-color: #726d6d42;
		}

		.file-footer-buttons button.btn-danger:hover {
			background-color: #f44336;
		}

		div.crop {
		    position: absolute;
		    background-color: #00000096;
		    top: 0;
		    bottom: 0;
		    width: 100%;
		    height: auto;
		    opacity: 0;
		    transition: 0.3s all;
		}

		div.crop:hover {
		    opacity: 1;
		}

		div.crop span {
		    left: 50%;
		    top: 50%;
		    transform: translate(-50%,-50%);
		    position: absolute;
		}
	</style>
@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Products",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Products', 'link' => null, 'icon' => 'icon-twitter'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
                <h5 class="card-title">Data Products</h5>
                <div>
					<form id="publish-row-all" action="{{ url(config('elook.admin_url').'/products-publish-all') }}" method="POST">
						{!! csrf_field().method_field("PUT") !!}
						<input type="hidden" name="product_id" value="" />
						<input type="hidden" name="status" value="" />
					</form>
                    <a href="{{ url(config('elook.admin_url').'/sort-products') }}" class="btn bg-teal-400 btn-labeled">Sort Products</a>
                    <div class="btn-group">
                        <button type="button" class="btn bg-teal-400 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Status</button>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(142px, 36px, 0px);">
                            <button type="button" data-status="1" data-action="publish" class="dropdown-item"><i class="icon-square-up"></i> Publish</button>
                            <button type="button" data-status="0" data-action="publish" class="dropdown-item"><i class="icon-square-up"></i> Unpublish</button>
                        </div>
                    </div>
                    <a href="{{ url(config('elook.admin_url').'/products/create?type=normal') }}" class="btn bg-teal-400 btn-labeled">
                        <i class="icon-database-add mr-2"></i>
                        Create New
                    </a>
                </div>
			</div>

			<table class="table datatable-basic table-striped" id="product-tables">
		        <thead>
		            <tr>
                        <th>
                            <input type="checkbox" value="0" class="selectAll">
                        </th>
                        <th>No </th>
                        <th>Categories </th>
                        <th>Title </th>
                        <th>Price </th>
                        <th>Type </th>
                        <th>Images </th>
                        <th>Stock </th>
                        <th>Status </th>
                        <th><span class="nobr">Action</span></th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/uniform.min.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script src="{{ asset('js/jquery-confirm.min.js') }}"></script>
	<script src="{{ asset('js/fileinput.min.js') }}"></script>
	<script src="{{ asset('js/blockui.min.js') }}"></script>
	<script src="{{ asset('js/notifications/noty.min.js') }}"></script>
	<script src="{{ asset('js/datatable-extension-select.min.js') }}"></script>
	<script src="{{ asset('js/datatable-extension-buttons.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('.selectAll').uniform();

            var productTables = $('#product-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/product-tables')}}",
                rowId: 'id',
				columnDefs: [
					{
						orderable: false,
                        className: 'select-checkbox',
                        targets: 0,
                        data: null,
                        defaultContent: ""
					}
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                columns: [
                    null,
                    { data: 'rownum', name: 'rownum' },
                    { data: 'categories', name: 'categories' },
                    { data: 'title', name: 'title' },
                    { data: 'price', name: 'price' },
                    { data: 'type', name: 'type' },
                    { data: 'images', name: 'images', sortable: false },
                    { data: 'stock', name: 'stock'},
                    { data: 'published', name: 'published' },
                    { data: 'actions', name: 'actions', sortable: false }
                ],
                order: [[ 1, "asc" ]],
				drawCallback : function( settings, json ) {
					$("#product-tables .selectAll").on( "change", function(e) {
                        if(event.target.checked) {
                            productTables.rows().select();
                        }else{
                            productTables.rows().deselect();
                        }
					});
				}
            });

			$('[data-action="publish"]').on("click",function(e){
                var rowsSelected = productTables.rows( { selected: true } );
                if(rowsSelected.count()){
                    var status = $(this).data('status');
					$('[name="product_id"]').val(rowsSelected.ids().toArray().join());
					$('[name="status"]').val(status);
					$('#publish-row-all').submit();
                }else{
                    alert('Select data first before action.');
                }
			})

		    Noty.overrideDefaults({ theme: 'limitless' });

		    function read(callback, file) {
				var reader = new FileReader();

				reader.onload = function() {
					callback(reader.result);
				}

				reader.readAsDataURL(file);
			}

            $(document).on('click', '.images-button', function(){

            	var $this = $(this);

            	var getData = $.get( "{{ url(config('elook.admin_url').'/product-images') }}" + "/" + $this.attr('data-id') );

            	$.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

				getData.done(function(response) {

					$.unblockUI();

	            	$.confirm({
						title: "Images",
						escape: true,
						columnClass: 'col-md-12',
						content: '<input id="input-image" name="images[]" type="file" accept="image/*">',
					    onContentReady: function(){
					        var self = this;

				            $("#input-image").fileinput({
				            	uploadUrl: "{{ url(config('elook.admin_url').'/product-upload-image') }}" + "/" + $this.attr('data-id'),
				            	ajaxDeleteSettings: {
				            		type: 'DELETE'
				            	},
							    overwriteInitial: false,
							    showUpload: false,
							    showRemove: false,
							    fileActionSettings: {
							    	showDrag: false,
							    	downloadIcon: '<i class="icon-download7"></i>',
							    	uploadIcon: '<i class="icon-upload"></i>',
							    	uploadRetryIcon: '<i class="icon-loop3"></i>',
							    	removeIcon: '<i class="icon-trash"></i>',
							    	zoomIcon: '<i class="icon-zoomin3"></i>',
							    	dragIcon: '<i class="icon-move"></i>',
							    	indicatorNew: '<i class="icon-arrow-up16 text-warning"></i>',
								    indicatorSuccess: '<i class="icon-checkmark4 text-success"></i>',
								    indicatorError: '<i class="icon-warning22 text-danger"></i>',
								    indicatorLoading: '<i class="icon-hour-glass text-muted"></i>',
							    },
							    allowedFileExtensions: ['jpg', 'jpeg', 'png'],
							    previewZoomButtonIcons: {
									prev: '<i class="icon-arrow-left15"></i>',
								    next: '<i class="icon-arrow-right15"></i>',
								    toggleheader: '<i class="icon-move-vertical"></i>',
								    fullscreen: '<i class="icon-screen-full"></i>',
								    borderless: '<i class="icon-screen-normal"></i>',
								    close: '<i class="icon-x"></i>'
							    },
							    uploadExtraData: function(previewId, index) {
						            return { _token: $('meta[name="csrf-token"]').attr('content') };
						        },
						        deleteExtraData: function(previewId, index) {
						            return { _token: $('meta[name="csrf-token"]').attr('content') };
						        },
						        showAjaxErrorDetails: false,
							    initialPreview: response.preview,
							    initialPreviewConfig: response.config,
							    initialPreviewDownloadUrl: "{{ url('/') }}" + '/{filename}',
							    initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
							    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
							    purifyHtml: true, // this by default purifies HTML data for preview,
							});

							// $('#input-image').on('fileimagesloaded', function(event) {
							// 	// get all image that selected via browse button
							// 	var stacks = $('#input-image').fileinput('getFileStack');

							// 	var newThumbnails = $('.file-preview-thumbnails').children('.file-preview-frame.kv-preview-thumb').eq(parseInt(response.config.length - 1)).nextAll('div.file-preview-frame.kv-preview-thumb');

							// 	$.each(newThumbnails, function(item){
							// 		newThumbnails.find('.kv-file-content').prepend(`<div class="crop">
							// 							<span>
							// 								<button type="button" class="btn btn-outline-primary">Crop Image</button>
							// 							</span>
							// 						</div>`);
							// 	});
							// });

							$('#input-image').on('fileuploaded', function(event, data, previewId, index) {
							    new Noty({ text: data.response.message, layout: 'topRight', timeout: 3000, type: 'success' }).show();

							    response.config.push(data.response.data);
								response.preview.push(data.response.data.imageUrl);

								$('#input-image').fileinput('destroy').fileinput({
					            	uploadUrl: "{{ url(config('elook.admin_url').'/product-upload-image') }}" + "/" + $this.attr('data-id'),
					            	ajaxDeleteSettings: {
					            		type: 'DELETE'
					            	},
								    overwriteInitial: false,
								    showUpload: false,
								    showRemove: false,
								    fileActionSettings: {
								    	showDrag: false,
								    	downloadIcon: '<i class="icon-download7"></i>',
								    	uploadIcon: '<i class="icon-upload"></i>',
								    	uploadRetryIcon: '<i class="icon-loop3"></i>',
								    	removeIcon: '<i class="icon-trash"></i>',
								    	zoomIcon: '<i class="icon-zoomin3"></i>',
								    	dragIcon: '<i class="icon-move"></i>',
								    	indicatorNew: '<i class="icon-plus-circle2 text-warning"></i>',
									    indicatorSuccess: '<i class="icon-checkmark4 text-success"></i>',
									    indicatorError: '<i class="icon-warning22 text-danger"></i>',
									    indicatorLoading: '<i class="icon-hour-glass text-muted"></i>',
								    },
								    allowedFileExtensions: ['jpg', 'jpeg', 'png'],
								    previewZoomButtonIcons: {
										prev: '<i class="icon-arrow-left15"></i>',
									    next: '<i class="icon-arrow-right15"></i>',
									    toggleheader: '<i class="icon-move-vertical"></i>',
									    fullscreen: '<i class="icon-screen-full"></i>',
									    borderless: '<i class="icon-screen-normal"></i>',
									    close: '<i class="icon-x"></i>'
								    },
								    uploadExtraData: function(previewId, index) {
							            return { _token: $('meta[name="csrf-token"]').attr('content') };
							        },
							        deleteExtraData: function(previewId, index) {
							            return { _token: $('meta[name="csrf-token"]').attr('content') };
							        },
							        showAjaxErrorDetails: false,
								    initialPreview: response.preview,
								    initialPreviewConfig: response.config,
								    initialPreviewDownloadUrl: "{{ url('/') }}" + '/{filename}',
								    initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
								    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
								    purifyHtml: true, // this by default purifies HTML data for preview,
								}).fileinput('enable');


							});

							$('#input-image').on('fileuploaderror', function(event, data, msg) {
							    if(data.jqXHR.status === 422){
									var messages = [];
									$.each(data.jqXHR.responseJSON.errors, function(key, item){
										messages.push(...item);
									})
									new Noty({ text: messages.join('<br>'), layout: 'topRight', timeout: 3000, type: 'error' }).show();

									return false;
								}
								new Noty({ text: msg, layout: 'topRight', timeout: 3000, type: 'error' }).show();
							});

							$('#input-image').on('filedeleted', function(event, key, jqXHR, data) {
								var index = response.config.findIndex(function(config) {
								  	return config.key === key;
								});
								response.config.splice(index, 1);
								response.preview.splice(index, 1);
							    new Noty({ text: jqXHR.responseText, layout: 'topRight', timeout: 3000, type: 'success' }).show();
							});

							$('#input-image').on('filebeforedelete', function(event, key, jqXHR, data) {
							    return new Promise(function(resolve, reject) {
						            $.confirm({
						                title: 'Confirmation!',
						                content: 'Are you sure you want to delete this file?',
						                type: 'red',
						                buttons: {
						                    ok: {
						                        btnClass: 'btn-primary text-white',
						                        keys: ['enter'],
						                        action: function(){
						                            resolve();
						                        }
						                    },
						                    cancel: function(){}
						                }
						            });
						        });
							});

							$('#input-image').on('filedeleteerror', function(event, data, msg) {
								if(data.jqXHR.status === 422){
									var messages = [];
									$.each(data.jqXHR.responseJSON.errors, function(key, item){
										messages.push(...item);
									})

									new Noty({ text: messages.join('<br>'), layout: 'topRight', timeout: 3000, type: 'error' }).show();

									return false;
								}
								new Noty({ text: msg, layout: 'topRight', timeout: 3000, type: 'error' }).show();
							});

							$('.kv-cust-btn').on('click', function() {
							    console.log('button crop')
							});


					    },
					    buttons: {
					        close: function () {},
					    }
					});

				})
				.fail(function(response) {

					$.unblockUI();

					if(response.status === 422){
						var messages = [];
						$.each(response.responseJSON.errors, function(key, item){
							messages.push(...item);
						})
						new Noty({ text: messages.join('<br>'), layout: 'topRight', timeout: 3000, type: 'error' }).show();

						return false;
					}
					new Noty({ text: response.responseText, layout: 'topRight', timeout: 3000, type: 'error' }).show();
				});


            });
        });
    </script>
@endpush
