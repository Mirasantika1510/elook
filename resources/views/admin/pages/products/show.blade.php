@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.breadcrum', [
        'previous' => [
            'title' => "Products",
            'link'	=> url(config('elook.admin_url').'/products')
        ],
        'breads' => [
            [ 'title' => 'Products', 'link' => url(config('elook.admin_url').'/products'), 'icon' => 'icon-cube4'  ],
            [ 'title' => 'index', 'link' => null, 'icon' => null ]
        ]
    ])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
                <h5 class="card-title">Data Product Review : {{ $product->title }}</h5>
                <a href="{{ url(config('elook.admin_url').'/reviews/create') }}?product={{ $product->id }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-database-add mr-2"></i> Create New </a>
			</div>

			@include('admin.layouts.validation_error', [ 'errors' => $errors ])

			<table class="table datatable-basic table-striped" id="reviews-tables">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>User </th>
		              <th>Rate</th>
		              <th>Comment</th>
		              <th>Action</th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#reviews-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/reviews-tables')}}/{{ $product->id }}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'name', name: 'name' },
                    { data: 'rate', name: 'rate' },
                    { data: 'comment', name: 'comment' },
                    { data: 'actions', name: 'actions' },
                ]
            });
        });
    </script>
@endpush
