@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Data Order",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Customers', 'link' => url(config('elook.admin_url').'/members'), 'icon' => 'icon-users'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])

	<style type="text/css">
		.no-available-attendance {
			font-size: 30px;
			color: #7777778c;
			font-weight: 600;
		}
	</style>
	<!-- Content area -->
	<div class="content">

		<div class="text-center no-available-attendance">
                NO DETAIL AVAILABLE
            </div>

	</div>
	<!-- /content area -->

@endsection
