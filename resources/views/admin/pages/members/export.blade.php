<table >
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">No</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Name</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Email</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Date Of Birth</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Phone Number</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Address</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Order Date</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Invoice Number</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Product Name</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Product Price</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Quantity</th>
        </tr>
    </thead>
    <tbody>
        @php $no = 1  @endphp

        @forelse($members as $member)
        @php $check = \App\Model\Order::where('customer_id', $member->id)->whereHas('payment', function($query){
                $query->where('transaction_status', 'settlement');
            })->orderBy('created_at', 'desc');
        @endphp
        @if ($check->count() == 0) 
        <tr>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $no++ }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->firstname." ".$member->lastname }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->email }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->dob }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">-</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">-</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">-</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">-</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">-</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">-</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">-</td>
        </tr>
        @else
        @foreach ($check->get() as $item)
        @php $product = \App\Model\OrderProducts::where('order_id', $item->id)->first() @endphp
        <tr>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $no++ }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->firstname." ".$member->lastname }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->email }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->dob }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->address->phonenumber }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->address->address.", ". $member->address->getsubdistrict->subdistrict_name.", ". $member->address->getcity->city_name.", ". $member->address->getprovince->province }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ \Carbon\Carbon::parse($item->created_at)->format('d M Y') }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $item->invoice_number }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $product->product_name }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Rp. {{ number_format($product->product_price) }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $product->quantities }}</td>
        </tr>
        @endforeach
        @endif
        @empty
        <tr style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">
            <td colspan="25" align="center">
                No Data Available
            </td>
        </tr>
        @endforelse
    </tbody>
</table>