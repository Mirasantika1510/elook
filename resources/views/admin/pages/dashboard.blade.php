@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Dashboard",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Dashboard', 'link' => null  ],
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">WELCOME TO ELOOK OFFICAL ADMIN PANEL.</h5>
			</div>
		</div>

	</div>
	<!-- /content area -->

@endsection
