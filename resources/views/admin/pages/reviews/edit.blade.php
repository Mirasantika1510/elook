@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Review",
			'link'	=> url(config('elook.admin_url').'/products/'. $product->id)
		],
		'breads' => [
			[ 'title' => 'Review', 'link' => url(config('elook.admin_url').'/products/'. $product->id), 'icon' => 'icon-comment'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Edit Review</h5>
			</div>

			<div class="card-body">
				<p class="text-grey info-form"><strong>NOTE : </strong>Semua kolom harus diisi.</p>

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/reviews', $review->id) }}?product={{ $product->id }}" method="POST" enctype="multipart/form-data">

					{{ method_field('PUT') }}
					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('customer')? 'text-danger' : '' }}">Customer</label>
							<div class="col-lg-3 {{ $errors->has('customer')? 'border-danger' : '' }}">
								<select class="form-control select customer" name="customer" data-fouc>
									<option value="">Choose Customer</option>
                                    @foreach($members as $member)
                                        <option value="{{ $member->id }}" {{ old('customer')? (old('customer') == $member->id? 'selected' : '') : ( $review->customer_id === $member->id? 'selected' : '' ) }}>{{ $member->firstname. ' ' .$member->lastname  }}</option>
                                    @endforeach
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('rate')? 'text-danger' : '' }}">Rate</label>
							<div class="col-lg-3 {{ $errors->has('rate')? 'border-danger' : '' }}">
								<select class="form-control select rate" name="rate" data-fouc>
									<option value="">Choose Rate</option>
									<option value="1" {{ old('rate')? (old('rate') == 1? 'selected' : '') : ($review->rate == 1? 'selected' : '') }}>1</option>
									<option value="2" {{ old('rate')? (old('rate') == 2? 'selected' : '') : ($review->rate == 2? 'selected' : '') }}>2</option>
									<option value="3" {{ old('rate')? (old('rate') == 3? 'selected' : '') : ($review->rate == 3? 'selected' : '') }}>3</option>
									<option value="4" {{ old('rate')? (old('rate') == 4? 'selected' : '') : ($review->rate == 4? 'selected' : '') }}>4</option>
									<option value="5" {{ old('rate')? (old('rate') == 5? 'selected' : '') : ($review->rate == 5? 'selected' : '') }}>5</option>
								</select>
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('comment')? 'text-danger' : '' }}">Comment <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea rows="5" cols="5" class="form-control {{ $errors->has('comment')? 'border-danger' : '' }}" name="comment" placeholder="Enter your comment here">{!! old('comment')?: $review->comment !!}</textarea>
							</div>
                        </div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Update <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{asset('js/fileinput.min.js')}}"></script>
	<script src="{{ asset('js/selects/select2.min.js') }}"></script>

	<script>

		document.addEventListener('DOMContentLoaded', function() {

			$().select2 && 	$('.select').select2({ minimumResultsForSearch: Infinity });

		});


	</script>
@endpush
