@extends('admin.layouts.master')

	<!-- <style type="text/css">
		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview,.table.datatable-basic>thead>tr>th{border:none}.table.datatable-basic>thead>tr>th:last-child,.table.datatable-basic>thead>tr>th:first-child{width:10%;text-align:center;}.table.datatable-basic>tbody>tr>td:last-child,.table.datatable-basic>tbody>tr>td:first-child{text-align:center;padding-right: 2.25rem;}.table.datatable-basic>tbody>tr>td{padding:.75rem 1.25rem}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}
	</style> -->

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Review",
			'link'	=> url(config('elook.admin_url').'/products/'. $product->id)
		],
		'breads' => [
			[ 'title' => 'Review', 'link' => url(config('elook.admin_url').'/products/'. $product->id), 'icon' => 'icon-comment'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Tambah Review Baru</h5>
			</div>

			<div class="card-body">

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/reviews') }}?product={{ $product->id }}" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('customer')? 'text-danger' : '' }}">Customer</label>
							<div class="col-lg-3 {{ $errors->has('customer')? 'border-danger' : '' }}">
								<select class="form-control select customer" name="customer" data-fouc>
									<option value="">Choose Customer</option>
                                    @foreach($members as $member)
                                        <option value="{{ $member->id }}" {{ old('customer') == $member->id? 'selected' : '' }}>{{ $member->firstname. ' ' .$member->lastname  }}</option>
                                    @endforeach
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('rate')? 'text-danger' : '' }}">Rate</label>
							<div class="col-lg-3 {{ $errors->has('rate')? 'border-danger' : '' }}">
								<select class="form-control select rate" name="rate" data-fouc>
									<option value="">Choose Rate</option>
									<option value="1" {{ old('rate') == 1? 'selected' : '' }}>1</option>
									<option value="2" {{ old('rate') == 2? 'selected' : '' }}>2</option>
									<option value="3" {{ old('rate') == 3? 'selected' : '' }}>3</option>
									<option value="4" {{ old('rate') == 4? 'selected' : '' }}>4</option>
									<option value="5" {{ old('rate') == 5? 'selected' : '' }}>5</option>
								</select>
							</div>
						</div>

                        <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('comment')? 'text-danger' : '' }}">Comment <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea rows="5" cols="5" class="form-control {{ $errors->has('comment')? 'border-danger' : '' }}" name="comment" placeholder="Enter your comment here">{!! old('comment') !!}</textarea>
							</div>
                        </div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{asset('js/fileinput.min.js')}}"></script>
	<script src="{{ asset('js/selects/select2.min.js') }}"></script>

	<script>

		document.addEventListener('DOMContentLoaded', function() {

			$().select2 && 	$('.select').select2({ minimumResultsForSearch: Infinity });

		});


	</script>
@endpush
