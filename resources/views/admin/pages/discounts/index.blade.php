@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Discounts",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Discounts', 'link' => null, 'icon' => 'icon-percent'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Discounts</h5>
				<a href="{{ url(config('elook.admin_url').'/discounts/create') }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-database-add mr-2"></i> Tambah Discounts </a>
			</div>

			<table class="table datatable-basic table-striped" id="discounts-tables">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>Title </th>
		              <th>Discount Percent </th>
		              <th>Products </th>
		              <th>Images </th>
		              <th>Status </th>
		              <th><span class="nobr">Action</span></th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script src="{{ asset('js/jquery-confirm.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#discounts-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/discounts-tables')}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'title', name: 'title' },
                    { data: 'discount_percent', name: 'discount_percent' },
                    { data: 'products', name: 'products' },
                    { data: 'image', name: 'image', sortable: false },
                    { data: 'published', name: 'published' },
                    { data: 'actions', name: 'actions', sortable: false }
                ]
            });
        });
    </script>
@endpush
