@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Testimonials",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Testimonials', 'link' => null, 'icon' => 'icon-user-tie'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Testimonials</h5>
				<a href="{{ url(config('elook.admin_url').'/testimonials/create') }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-database-add mr-2"></i> Tambah Testimonials </a>
			</div>

			@include('admin.layouts.validation_error', [ 'errors' => $errors ])

			<table class="table datatable-basic table-striped" id="testimonials-tables">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>Title </th>
		              <th>Description</th>
		              <th>Author</th>
		              <th>Image</th>
		              <th><span class="nobr">Action</span></th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#testimonials-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/testimonials-tables')}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'author', name: 'author' },
                    { data: 'image', name: 'image' },
                    { data: 'actions', name: 'actions', orderable: false }
                ]
            });
        });
    </script>
@endpush
