@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Guest",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Guest', 'link' => null, 'icon' => 'icon-users'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Guest</h5>
				<a href=" {{ url(config('elook.admin_url').'/guests-export') }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><span class="icon-file-excel"></span> Export Excel</a>
			</div>

			@include('admin.layouts.validation_error', [ 'errors' => $errors ])

			<table class="table datatable-basic table-striped" id="guests-tables">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>Name</th>
		              <th>Email</th>
		              <th style="width: 16%;">Has Ever Buy</th>
		              <th>Action</th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#guests-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/guests-tables')}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'has_buy', name: 'has_buy' },
                    { data: 'actions', name: 'actions' },
                ]
            });
        });
    </script>
@endpush
