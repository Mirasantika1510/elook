<table >
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">No</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Name</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Email</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Phone Number</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Address</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Order Date</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Invoice Number</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Product Name</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Product Price</th>
            <th style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Quantity</th>
        </tr>
    </thead>
    <tbody>
        @php $no = 1  @endphp

        @forelse($guests as $member)
        @php $check = \App\Model\Order::where('id', $member->order_id)->whereHas('payment', function($query){
                $query->where('transaction_status', 'settlement');
            })->orderBy('created_at', 'desc')->first();
        @endphp
        @if (!$check) 
        @else
        @php $product = \App\Model\OrderProducts::where('order_id', $check->id)->get() @endphp
        @foreach ($product as $item)
        <tr>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $no++ }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->firstname." ".$member->lastname }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->email }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->phonenumber }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->address.", ". $member->subdistrict.", ". $member->city.", ". $member->province.", ". $member->postcode }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ \Carbon\Carbon::parse($member->order->created_at)->format('d M Y') }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $member->order->invoice_number }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $item->product_name }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">Rp. {{ number_format($item->product_price) }}</td>
            <td style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">{{ $item->quantities }}</td>
        </tr>
        @endforeach
        @endif
        @empty
        <tr style="text-align: center;border: 1px solid #49494a;padding: 0.5rem;">
            <td colspan="25" align="center">
                No Data Available
            </td>
        </tr>
        @endforelse
    </tbody>
</table>