@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Data Order",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Guest', 'link' => url(config('elook.admin_url').'/guests'), 'icon' => 'icon-users'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Order</h5>
				<a href=" {{ url(config('elook.admin_url').'/guests') }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-circle-left2"></i> Back</a>
			</div>
			<div class="card-body">
				<table class="table table-bordered">
					<tr>
						<td>Name</td>
						<td>{{ $shipping->firstname." ".$shipping->lastname }}</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{ $shipping->email }}</td>
					</tr>
					<tr>
						<td>Address</td>
						<td>{{ $shipping->address.", ".$shipping->subdistrict.", ".$shipping->city.", ".$shipping->province }}</td>
					</tr>
					<tr>
						<td>Phone Number</td>
						<td>{{ $shipping->phonenumber }}</td>
					</tr>
				</table>
			</div>

			@include('admin.layouts.validation_error', [ 'errors' => $errors ])

			<table class="table datatable-basic table-striped" id="members-tables">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>Order Date</th>
		              <th>Invoice Number</th>
		              <th>Product Name</th>
		              <th>Product Price</th>
                      <th>Qty</th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#members-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/guests-detail-tables/'.$shipping->id)}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'invoice_number', name: 'invoice_number' },
                    { data: 'product_name', name: 'product_name' },
                    { data: 'product_price', name: 'product_price' },
                    { data: 'quantities', name: 'quantities' },
                ]
            });
        });
    </script>
@endpush
