@extends('admin.layouts.master')

@push('styles')
	<style>
		div.border-danger .select2-selection--multiple:not([class*=bg-]):not([class*=border-]) {
		    border-color: #f00505;
		}

		.file-preview .close {
		    right: 5px;
		}

		.border-danger .input-group>.custom-select:not(:last-child),
		.border-danger .input-group>.form-control:not(:last-child) {
		    border: 1px solid #f44336;
		}

		.border-danger .btn-file {
		    background-color: #f44336;
		}

		input.select2-search__field {
            width: 100% !important;
            display: block !important;
        }

        .bootstrap-duallistbox-container {
            width: 100%;
        }

        span.info-container {
            display: none;
        }

		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview{border:none}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}.kv-file-content>img{width: auto!important; }.form-group.row.desc-error .note-editor{border-color: #f55246;}
	</style>
@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Vouchers",
			'link'	=> url(config('elook.admin_url').'/vouchers')
		],
		'breads' => [
			[ 'title' => 'Vouchers', 'link' => url(config('elook.admin_url').'/vouchers'), 'icon' => 'icon-ticket'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Voucher</h5>
			</div>

			<div class="card-body">
				{{-- <p class="mb-3">Add new category by input title of the category and submit it.<strong><br>Add new category as a child of the existing category can be done by first <code>click</code> one of the category that you wish to be the new categories parent, input title, and submit it.</strong><br> You can edit existing category by <code>double click</code> the category, type new title right there</p> --}}

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form id="form-submit" action="{{ url(config('elook.admin_url').'/vouchers', $voucher->id) }}?type=normal" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Input title here" name="title" value="{{ old('title')?: $voucher->title }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('voucher_code')? 'text-danger' : '' }}">Voucher Code <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('voucher_code')? 'border-danger' : '' }}" placeholder="Input voucher_code here" name="voucher_code" value="{{ old('voucher_code')?: $voucher->voucher_code }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('voucher_type')? 'text-danger' : '' }}">Voucher Type <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<div class="input-group">
									<div class="btn-group custom-radio">
										<a class="btn btn-primary btn-sm @if($voucher->voucher_type == 'nominal') active @else notActive @endif" data-toggle="voucher_type" data-title="nominal">Nominal</a>
										<a class="btn btn-primary btn-sm @if($voucher->voucher_type == 'percent') active @else notActive @endif" data-toggle="voucher_type" data-title="percent">Percent</a>
									</div>
									<input type="hidden" name="voucher_type" id="voucher_type" value="{{ old('voucher_type')?: $voucher->voucher_type }}">
								</div>
							</div>
						</div>
						
						<div class="form-group row" data-type="voucher" data-val="nominal">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('nominal')? 'text-danger' : '' }}">Voucher Nominal <span class="text-danger">*</span></label>
							<div class="col-lg-3">
								<div class="input-group">
									<span class="input-group-prepend">
										<span class="input-group-text">Rp. </span>
									</span>
									<input type="number" class="form-control {{ $errors->has('nominal')? 'border-danger' : '' }}" placeholder="Input voucher nominal here" name="nominal" value="{{ old('nominal')?: $voucher->nominal }}">
								</div>
							</div>
						</div>

						<div class="form-group row" data-type="voucher" data-val="percent">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('percent')? 'text-danger' : '' }}">Voucher Percent % <span class="text-danger">*</span></label>
							<div class="col-lg-3">
								<div class="input-group">
									<span class="input-group-prepend">
										<span class="input-group-text">%</span>
									</span>
									<input type="number" class="form-control {{ $errors->has('percent')? 'border-danger' : '' }}" placeholder="Input voucher percent here" name="percent" value="{{ old('percent')?: $voucher->percent }}">
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('description')? 'text-danger' : '' }}">Description <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<textarea class="form-control" rows="5" {{ $errors->has('description')? 'border-danger' : '' }}" placeholder="Masukkan description slider" name="description">{{ old('description')?: $voucher->description }}</textarea>
							</div>
						</div>

						<div class="form-group row {{ $errors->has('image')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('image')? 'text-danger' : '' }}">Image</label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="image" data-fouc>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('products')? 'text-danger' : '' }}">Products <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<div class="input-group">
									<select multiple="multiple" class="form-control listbox-filter-disabled" data-fouc name="products[]">
                                        @foreach($products as $product)
                                            <option value="{{ $product->id }}" {{ old('products') && in_array($product->id, old('products'))? 'selected' : ( $voucher->products()->count() > 0 && in_array($product->id, $voucher->products()->pluck('id')->toArray())? 'selected' : '')}}>{{ $product->title }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
						</div>
						
						
						
						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('products')? 'text-danger' : '' }}">Avaliable For</label>
							<div class="col-lg-10">
								<div class="input-group">
									<div class="custom-control custom-checkbox" style="margin-right:10px">
									
										<input type="checkbox" name="avaliable_for[]" value="member"  class="custom-control-input" id="member" {{$avaliable_for['member']}}>
										<label class="custom-control-label" for="member">Member</label>
									</div>
									
									<div class="custom-control custom-checkbox">
										<input type="checkbox" name="avaliable_for[]" value="guest"  class="custom-control-input" id="guest" {{$avaliable_for['guest']}}>
										<label class="custom-control-label" for="guest">Guest</label>
									</div>
								</div>
							</div>
                        </div>


					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>

			</div>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('js/selects/select2.min.js') }}"></script>
	<script src="{{ asset('js/blockui.min.js') }}"></script>
	<script src="{{ asset('js/notifications/noty.min.js') }}"></script>
	<script src="{{ asset('js/duallistbox.min.js') }}"></script>
	<script src="{{ asset('js/fileinput.min.js') }}"></script>

	<script>

		var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		            '  <div class="modal-content">\n' +
		            '    <div class="modal-header align-items-center">\n' +
		            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		            '      <div class="kv-zoom-actions btn-group">{close}</div>\n' +
		            '    </div>\n' +
		            '    <div class="modal-body">\n' +
		            '      <div class="floating-buttons btn-group"></div>\n' +
		            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		            '    </div>\n' +
		            '  </div>\n' +
		            '</div>\n';

		document.addEventListener('DOMContentLoaded', function() {

		    if($().summernote){
		    	$('.summernote').summernote({disableDragAndDrop: true,toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
				  ]});
		    }

            if ($().bootstrapDualListbox) {
                $('.listbox-filter-disabled').bootstrapDualListbox({
                    showFilterInputs: false
                });
            }

			if($().fileinput){
		        $('.image').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
		            initialPreview: [ "{{ url($voucher->image) }}" ],
				    initialPreviewConfig: [{
				    	previewAsData: true,
				    	filetype: 'image',
				    	caption: '{{ str_slug($voucher->title) }}',
				    	size: "{{ $voucher->size }}",
				    	key: "{{$voucher->key}}"
				    }],
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000,
		        });
		    }

			
			$('.custom-radio').each(function(i,v){
				$(this).find('a').on('click', function(){
					var sel = $(this).data('title');
					var tog = $(this).data('toggle');
					$('#'+tog).prop('value', sel);
					
					$('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
					$('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
					
					$('#'+tog).trigger('change');
				});
			});
			

			$('#voucher_type').on('change',function(){
				var type = $(this).val();
				$('[data-type="voucher"]').hide();
				$('[data-type="voucher"][data-val="'+type+'"]').show();
			});
			$('#voucher_type').trigger('change');


		});

	</script>
@endpush
