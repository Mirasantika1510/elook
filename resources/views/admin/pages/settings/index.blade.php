@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Profile",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Profile', 'link' => null, 'icon' => null ]
		]
	])


	<div class="content">

		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Settings</h5>
			</div>

			<div class="card-body">

				<ul class="nav nav-tabs">
					<li class="nav-item"><a href="#companies" data-link="companies-settings" class="nav-link {{ request()->segment(2) === 'companies-settings'? 'show active' : '' }}" data-toggle="tab">Companies</a></li>
					<li class="nav-item"><a href="#banners" data-link="banner-settings" class="nav-link {{ request()->segment(2) === 'banner-settings'? 'show active' : '' }}" data-toggle="tab">Banners</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade {{ request()->segment(2) === 'companies-settings'? 'show active' : '' }}" id="companies">
						<div class="card">
							<div class="card-header header-elements-inline"><h5 class="card-title">Company Informations  </h5></div>
							<div class="card-body">

								@if(session()->has('error-companies'))
									@include('admin.layouts.validation_error', [ 'errors' => $errors ])
								@endif

								<form action="{{ url(config('elook.admin_url').'/companies-settings') }}" method="POST" enctype="multipart/form-data">
									@csrf()
									@method('PUT')
									<fieldset class="mb-3">

										<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Title <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" name="title" value="{{ $companies->title }}">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Email <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" name="email" value="{{ $companies->email }}">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Phone <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('phone')? 'border-danger' : '' }}" name="phone" value="{{ $companies->phone }}">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Address <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('address')? 'border-danger' : '' }}" name="address" value="{{ $companies->address }}">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Instagram Access Key <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('instagram_access_key')? 'border-danger' : '' }}" name="instagram_access_key" value="{{ $companies->instagram_access_key }}">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Mini Notification Text <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('mini_notification_text')? 'border-danger' : '' }}" name="mini_notification_text" value="{{ $companies->mini_notification_text }}">
											</div>
										</div>

										<div class="form-group row {{ $errors->has('image')? 'img-error' : '' }}">
											<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('image')? 'text-danger' : '' }}">Image <span class="text-danger">*</span></label>
											<div class="col-lg-10 single-image">
												<input id="logo-image" type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="logo" data-fouc>
											</div>
										</div>

                                        <div class="form-group row {{ $errors->has('pop_up_image')? 'img-error' : '' }}">
                                            <label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('pop_up_image')? 'text-danger' : '' }}">Pop Up Image <span class="text-danger">*</span></label>
                                            <div class="col-lg-10 single-image">
                                                <input id="pop-up-image" type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="pop_up_image" data-fouc>
                                            </div>
                                        </div>

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Update <i class="icon-paperplane ml-2"></i></button>
										</div>

									</fieldset>
								</form>
							</div>
						</div>
					</div>

					<div class="tab-pane fade {{ request()->segment(2) === 'banner-settings'? 'show active' : '' }}" id="banners">
						<div class="card">
							<div class="card-header header-elements-inline"><h5 class="card-title">Ganti Password</h5></div>
							<div class="card-body">

								@if(session()->has('error-banner'))
									@include('admin.layouts.validation_error', [ 'errors' => $errors ])
								@endif

								<form action="{{ url(config('elook.admin_url').'/banner-settings') }}" method="POST" enctype="multipart/form-data">
									@csrf()
									@method('PUT')
									<fieldset class="mb-3">

										<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Tracking Order Title Page <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('tracking_order_title_page')? 'border-danger' : '' }}" name="tracking_order_title_page" value="{{ $companies->tracking_order_title_page }}">
											</div>
										</div>

										<div class="form-group row {{ $errors->has('tracking_order_banner_image')? 'img-error' : '' }}">
											<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('tracking_order_banner_image')? 'text-danger' : '' }}">Tracking Order Banner Image <span class="text-danger">*</span></label>
											<div class="col-lg-10 single-image">
												<input id="tracking-banner" type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="tracking_order_banner_image" data-fouc>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Confirmation Payment Title Page <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('confirmation_payment_title_page')? 'border-danger' : '' }}" name="confirmation_payment_title_page" value="{{ $companies->confirmation_payment_title_page }}">
											</div>
										</div>

										<div class="form-group row {{ $errors->has('confirmation_payment_banner_image')? 'img-error' : '' }}">
											<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('confirmation_payment_banner_image')? 'text-danger' : '' }}">Confirmation Banner Image <span class="text-danger">*</span></label>
											<div class="col-lg-10 single-image">
												<input id="confirmation-banner" type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="confirmation_payment_banner_image" data-fouc>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-2 font-weight-semibold">Contact Title Page <span class="text-danger">*</span></label>
											<div class="col-lg-10">
												<input type="text" class="form-control {{ $errors->has('contact_title_page')? 'border-danger' : '' }}" name="contact_title_page" value="{{ $companies->contact_title_page }}">
											</div>
										</div>

										<div class="form-group row {{ $errors->has('contact_banner_image')? 'img-error' : '' }}">
											<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('contact_banner_image')? 'text-danger' : '' }}">Contact Banner Image <span class="text-danger">*</span></label>
											<div class="col-lg-10 single-image">
												<input id="contact-banner" type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="contact_banner_image" data-fouc>
											</div>
										</div>

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Update <i class="icon-paperplane ml-2"></i></button>
										</div>

									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

@endsection

@push('script')
	<script src="{{asset('js/fileinput.min.js')}}"></script>

	<script>

		var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		            '  <div class="modal-content">\n' +
		            '    <div class="modal-header align-items-center">\n' +
		            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		            '      <div class="kv-zoom-actions btn-group">{close}</div>\n' +
		            '    </div>\n' +
		            '    <div class="modal-body">\n' +
		            '      <div class="floating-buttons btn-group"></div>\n' +
		            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		            '    </div>\n' +
		            '  </div>\n' +
		            '</div>\n';

		document.addEventListener('DOMContentLoaded', function() {

			$('.nav-link').on('click', function(e){
				window.history.replaceState(null, null, $(this).data('link'));
			});

			if($().fileinput){
		        $('#logo-image').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
		            initialPreview: [ "{{ url($companies->logo) }}" ],
				    initialPreviewConfig: [{
				    	previewAsData: true,
				    	filetype: 'image',
				    	caption: '{{ str_slug($companies->title) }}',
				    	key: 41231
				    }],
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000,
		        });

                $('#pop-up-image').fileinput({
                    autoReplace: true,
                    browseLabel: 'Browse',
                    browseIcon: '<i class="icon-folder-search mr-2"></i>',
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        modal: modalTemplate,
                        actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
                    },
                    initialPreview: [ "{{ url($companies->pop_up_image) }}" ],
                    initialPreviewConfig: [{
                        previewAsData: true,
                        filetype: 'image',
                        caption: '{{ str_slug($companies->title) }}',
                        key: 973649
                    }],
                    purifyHtml: true,
                    previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
                    previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
                    fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    showClose: false,
                    maxFileSize: 1000,
                });

		        $('#tracking-banner').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
		            initialPreview: [ "{{ url($companies->tracking_order_banner_image) }}" ],
				    initialPreviewConfig: [{
				    	previewAsData: true,
				    	filetype: 'image',
				    	caption: '{{ str_slug($companies->tracking_order_title_page) }}',
				    	key: 41231
				    }],
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000,
		        });

		        $('#confirmation-banner').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
		            initialPreview: [ "{{ url($companies->confirmation_payment_banner_image) }}" ],
				    initialPreviewConfig: [{
				    	previewAsData: true,
				    	filetype: 'image',
				    	caption: '{{ str_slug($companies->confirmation_payment_title_page) }}',
				    	key: 412131
				    }],
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000,
		        });

		        $('#contact-banner').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
		            initialPreview: [ "{{ url($companies->contact_banner_image) }}" ],
				    initialPreviewConfig: [{
				    	previewAsData: true,
				    	filetype: 'image',
				    	caption: '{{ str_slug($companies->contact_title_page) }}',
				    	key: 512314
				    }],
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000,
		        });
		    }

		});
	</script>
@endpush
