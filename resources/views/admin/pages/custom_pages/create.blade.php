@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Custom Pages",
			'link'	=> url(config('elook.admin_url').'/custom-pages')
		],
		'breads' => [
			[ 'title' => 'Custom Pages', 'link' => url(config('elook.admin_url').'/custom-pages'), 'icon' => 'icon-browser'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Page</h5>
			</div>

			<div class="card-body">
				{{-- <p class="mb-3">Add new category by input title of the category and submit it.<strong><br>Add new category as a child of the existing category can be done by first <code>click</code> one of the category that you wish to be the new categories parent, input title, and submit it.</strong><br> You can edit existing category by <code>double click</code> the category, type new title right there</p> --}}

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form id="form-submit" action="{{ url(config('elook.admin_url').'/custom-pages') }}" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>


						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Input title here" name="title" value="{{ old('title') }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('url_link')? 'text-danger' : '' }}">Url Link <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('url_link')? 'border-danger' : '' }}" placeholder="Input url link here" name="url_link" value="{{ old('url_link') }}">
							</div>
						</div>

                        <div class="form-group row {{ $errors->has('description')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('description')? 'text-danger' : '' }}">Description <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea class="summernote-description" name="description">{{ old('description') }}</textarea>
							</div>
						</div>

                        <div class="form-group row {{ $errors->has('content')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('content')? 'text-danger' : '' }}">Content <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea class="summernote-content" name="content">{{ old('content') }}</textarea>
							</div>
						</div>

						<div class="form-group row {{ $errors->has('banner')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('banner')? 'text-danger' : '' }}">Banner <span class="text-danger">*</span></label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="banner" data-fouc>
							</div>
						</div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>

			</div>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('js/fileinput.min.js') }}"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {

			var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		            '  <div class="modal-content">\n' +
		            '    <div class="modal-header align-items-center">\n' +
		            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		            '      <div class="kv-zoom-actions btn-group">{close}</div>\n' +
		            '    </div>\n' +
		            '    <div class="modal-body">\n' +
		            '      <div class="floating-buttons btn-group"></div>\n' +
		            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		            '    </div>\n' +
		            '  </div>\n' +
		            '</div>\n';

		    if($().summernote){
		    	$('.summernote-description').summernote({disableDragAndDrop: true,toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
				]});
				$('.summernote-content').summernote();
		    }

		    $(".image").fileinput({
			    overwriteInitial: false,
			    showUpload: false,
			    fileActionSettings: {
			    	showUpload: false,
			    	showDrag: true,
			    	uploadIcon: '<i class="icon-upload"></i>',
			    	removeIcon: '<i class="icon-trash"></i>',
			    	zoomIcon: '<i class="icon-zoomin3"></i>',
			    	dragIcon: '<i class="icon-move"></i>',
			    	indicatorNew: '<i class="icon-plus-circle2 text-warning"></i>',
				    indicatorSuccess: '<i class="icon-checkmark4 text-success"></i>',
				    indicatorError: '<i class="icon-warning22 text-danger"></i>',
				    indicatorLoading: '<i class="icon-hour-glass text-muted"></i>',
			    },
			    allowedFileExtensions: ['jpg', 'jpeg', 'png'],
			    previewZoomButtonIcons: {
					prev: '<i class="icon-arrow-left15"></i>',
				    next: '<i class="icon-arrow-right15"></i>',
				    toggleheader: '<i class="icon-move-vertical"></i>',
				    fullscreen: '<i class="icon-screen-full"></i>',
				    borderless: '<i class="icon-screen-normal"></i>',
				    close: '<i class="icon-x"></i>'
			    },
			    initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
			    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
			    purifyHtml: true, // this by default purifies HTML data for preview
    			maxFileSize: 1000
            });

		});

	</script>
@endpush
