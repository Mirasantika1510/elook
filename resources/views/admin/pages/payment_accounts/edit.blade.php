@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Payment Account",
			'link'	=> url(config('elook.admin_url').'/payment-accounts')
		],
		'breads' => [
			[ 'title' => 'Payment Account', 'link' => url(config('elook.admin_url').'/payment-accounts'), 'icon' => 'icon-coins'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Edit Payment Account</h5>
			</div>

			<div class="card-body">
				<p class="text-grey info-form"><strong>NOTE : </strong>Semua kolom harus diisi.</p>

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/payment-accounts', $payment_account->id) }}" method="POST" enctype="multipart/form-data">

					{{ method_field('PUT') }}
					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('account_name')? 'text-danger' : '' }}">Account Name <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('account_name')? 'border-danger' : '' }}" placeholder="Masukkan account_name slider" name="account_name" value="{{ old('account_name')?: $payment_account->account_name }}">
							</div>
						</div>
						
						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('account_bank')? 'text-danger' : '' }}">Account Bank <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('account_bank')? 'border-danger' : '' }}" placeholder="Masukkan account_bank slider" name="account_bank" value="{{ old('account_bank')?: $payment_account->account_bank }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('account_number')? 'text-danger' : '' }}">Account Number <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('account_number')? 'border-danger' : '' }}" placeholder="Masukkan account_number slider" name="account_number" value="{{ old('account_number')?: $payment_account->account_number }}">
							</div>
						</div>

						<div class="form-group row {{ $errors->has('instructions')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('instructions')? 'text-danger' : '' }}">Instructions <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea class="summernote" name="instructions">{{ old('instructions')?: $payment_account->instructions }}</textarea>
							</div>
						</div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Update <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {

			if($().summernote){
				$('.summernote').summernote();
			}

		});
	</script>
@endpush