@extends('admin.layouts.master')

	<!-- <style type="text/css">
		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview,.table.datatable-basic>thead>tr>th{border:none}.table.datatable-basic>thead>tr>th:last-child,.table.datatable-basic>thead>tr>th:first-child{width:10%;text-align:center;}.table.datatable-basic>tbody>tr>td:last-child,.table.datatable-basic>tbody>tr>td:first-child{text-align:center;padding-right: 2.25rem;}.table.datatable-basic>tbody>tr>td{padding:.75rem 1.25rem}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}
	</style> -->

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Payment Account",
			'link'	=> url(config('elook.admin_url').'/payment-accounts')
		],
		'breads' => [
			[ 'title' => 'Payment Account', 'link' => url(config('elook.admin_url').'/payment-accounts'), 'icon' => 'icon-coins'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Tambah Payment Account Baru</h5>
			</div>

			<div class="card-body">

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/payment-accounts') }}" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>


						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('account_name')? 'text-danger' : '' }}">Account Name <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('account_name')? 'border-danger' : '' }}" placeholder="Masukkan account_name slider" name="account_name" value="{{ old('account_name') }}">
							</div>
                        </div>
						
						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('account_bank')? 'text-danger' : '' }}">Account Bank <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('account_bank')? 'border-danger' : '' }}" placeholder="Masukkan account_bank slider" name="account_bank" value="{{ old('account_bank') }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('account_number')? 'text-danger' : '' }}">Account Number <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('account_number')? 'border-danger' : '' }}" placeholder="Masukkan account_number slider" name="account_number" value="{{ old('account_number') }}">
							</div>
						</div>

                        <div class="form-group row {{ $errors->has('instructions')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('instructions')? 'text-danger' : '' }}">Instructions <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea class="summernote" name="instructions">{{ old('instructions') }}</textarea>
							</div>
						</div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {

			if($().summernote){
				$('.summernote').summernote();
			}

		});
	</script>
@endpush