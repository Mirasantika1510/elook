@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Lookbook",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Lookbook', 'link' => null, 'icon' => 'icon-book3'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Lookbook</h5>
				<a href="{{ url(config('elook.admin_url').'/lookbooks/create') }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-database-add mr-2"></i> Tambah Lookbook </a>
			</div>

			<table class="table datatable-basic table-striped" id="lookbooks-tables">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>Title </th>
		              <th>Description </th>
		              <th>Images </th>
		              <th><span class="nobr">Action</span></th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script src="{{ asset('js/jquery-confirm.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#lookbooks-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/lookbooks-tables')}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'image', name: 'image', sortable: false },
                    { data: 'actions', name: 'actions', sortable: false }
                ]
            });
        });
    </script>
@endpush
