@extends('admin.layouts.master')

@push('styles')
	<style>
		div.border-danger .select2-selection--multiple:not([class*=bg-]):not([class*=border-]) {
		    border-color: #f00505;
		}

		.file-preview .close {
		    right: 5px;
		}

		.border-danger .input-group>.custom-select:not(:last-child),
		.border-danger .input-group>.form-control:not(:last-child) {
		    border: 1px solid #f44336;
		}

		.border-danger .btn-file {
		    background-color: #f44336;
		}

		input.select2-search__field {
            width: 100% !important;
            display: block !important;
        }

        .bootstrap-duallistbox-container {
            width: 100%;
        }

        span.info-container {
            display: none;
        }

		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview{border:none}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}.kv-file-content>img{width: auto!important; }.form-group.row.desc-error .note-editor{border-color: #f55246;}
	</style>
@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Lookbooks",
			'link'	=> url(config('elook.admin_url').'/lookbooks')
		],
		'breads' => [
			[ 'title' => 'Lookbooks', 'link' => url(config('elook.admin_url').'/lookbooks'), 'icon' => 'icon-book3'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Lookbook</h5>
			</div>

			<div class="card-body">
				{{-- <p class="mb-3">Add new category by input title of the category and submit it.<strong><br>Add new category as a child of the existing category can be done by first <code>click</code> one of the category that you wish to be the new categories parent, input title, and submit it.</strong><br> You can edit existing category by <code>double click</code> the category, type new title right there</p> --}}

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form id="form-submit" action="{{ url(config('elook.admin_url').'/lookbooks', $lookbook->id) }}?type=normal" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>


						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Input title here" name="title" value="{{ old('title')?: $lookbook->title }}">
							</div>
						</div>

                        <div class="form-group row {{ $errors->has('description')? 'desc-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('description')? 'text-danger' : '' }}">Description <span class="text-danger">*</span></label>

							<div class="col-lg-10">
								<textarea class="summernote" name="description">{{ old('description')?: $lookbook->description }}</textarea>
							</div>
						</div>
						
						<div class="form-group row {{ $errors->has('image')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('image')? 'text-danger' : '' }}">Image <span class="text-danger">*</span></label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="image" data-fouc>
							</div>
						</div>

						<div class="form-group row {{ $errors->has('banner')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('banner')? 'text-danger' : '' }}">Banner <span class="text-danger">*</span></label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input banner" data-show-caption="false" data-show-upload="false" accept="image/*" name="banner" data-fouc>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('products')? 'text-danger' : '' }}">Products <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<div class="input-group">
									<select multiple="multiple" class="form-control listbox-filter-disabled" data-fouc name="products[]">
                                        @foreach($products as $product)
                                            <option value="{{ $product->id }}" {{ old('products') && in_array($product->id, old('products'))? 'selected' : ( count($lookbook->products) > 0 && in_array($product->id, $lookbook->products)? 'selected' : '')}}>{{ $product->title }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
						</div>
						
					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>

			</div>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('js/selects/select2.min.js') }}"></script>
	<script src="{{ asset('js/blockui.min.js') }}"></script>
	<script src="{{ asset('js/notifications/noty.min.js') }}"></script>
	<script src="{{ asset('js/duallistbox.min.js') }}"></script>
	<script src="{{asset('js/fileinput.min.js')}}"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {

		    if($().summernote){
		    	$('.summernote').summernote({disableDragAndDrop: true,toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
				  ]});
		    }

            if ($().bootstrapDualListbox) {
                $('.listbox-filter-disabled').bootstrapDualListbox({
                    showFilterInputs: false
                });
            }

			var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		            '  <div class="modal-content">\n' +
		            '    <div class="modal-header align-items-center">\n' +
		            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		            '      <div class="kv-zoom-actions btn-group">{close}</div>\n' +
		            '    </div>\n' +
		            '    <div class="modal-body">\n' +
		            '      <div class="floating-buttons btn-group"></div>\n' +
		            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		            '    </div>\n' +
		            '  </div>\n' +
		            '</div>\n';

			if($().fileinput){
				$('.image').fileinput({
					autoReplace: true,
					browseLabel: 'Browse',
					browseIcon: '<i class="icon-folder-search mr-2"></i>',
					layoutTemplates: {
						icon: '<i class="icon-file-check"></i>',
						modal: modalTemplate,
						actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
					},
					initialPreview: [ "{{ url($lookbook->image) }}" ],
					initialPreviewConfig: [{
						previewAsData: true,
						filetype: 'image',
						caption: '{{ str_slug($lookbook->title) }}',
						size: "{{ $lookbook->size }}",
						key: "{{$lookbook->key}}"
					}],
					purifyHtml: true,
					previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
					previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
					fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
					allowedFileExtensions: ["jpg", "png", "jpeg"],
					showClose: false,
					maxFileSize: 1000,
				});

				$('.banner').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
		            initialPreview: [ "{{ url($lookbook->banner) }}" ],
				    initialPreviewConfig: [{
				    	previewAsData: true,
				    	filetype: 'image',
				    	caption: 'banner',
				    	key: 957
				    }],
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000,
		        });
			}

		});

	</script>
@endpush
