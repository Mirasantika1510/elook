@extends('admin.layouts.master')

@push('styles')
	<style type="text/css">
		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview{border:none}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}.kv-file-content>img{width: auto!important; }.form-group.row.desc-error .note-editor{border-color: #f55246;}
	</style>
@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Product Categories",
			'link'	=> url(config('elook.admin_url').'/product-categories')
		],
		'breads' => [
			[ 'title' => 'Product Categories', 'link' => url(config('elook.admin_url').'/product-categories'), 'icon' => 'icon-stars'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Add New Product Categories</h5>
			</div>

			<div class="card-body">
				<p class="text-grey info-form"><strong>NOTE : </strong>All input must be filled.</p>

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/product-categories') }}" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title <span class="text-danger">*</span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Enter your title here" name="title" value="{{ old('title') }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('description')? 'text-danger' : '' }}">Description</label>

							<div class="col-lg-10">
								<textarea rows="5" cols="5" class="form-control {{ $errors->has('description')? 'border-danger' : '' }}" name="description" placeholder="Enter your description here">{{ old('description') }}</textarea>
							</div>
						</div>

						<div class="form-group row {{ $errors->has('image')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('image')? 'text-danger' : '' }}">Image <span class="text-danger">*</span></label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="image" data-fouc>
							</div>
						</div>

						<div class="form-group row {{ $errors->has('banner')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('banner')? 'text-danger' : '' }}">Banner <span class="text-danger">*</span></label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="banner" data-fouc>
							</div>
						</div>

                        {{-- <div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('parent')? 'text-danger' : '' }}">Parent</label>
							<div class="col-lg-3 {{ $errors->has('parent')? 'border-danger' : '' }}">
								<select class="form-control select parent" name="parent" data-fouc>
									<option value="">Choose Parent</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ old('parent') == $category->id? 'selected' : '' }}>{{ $category->title }}</option>
                                    @endforeach
								</select>
							</div>
						</div> --}}

					</fieldset>

					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{asset('js/fileinput.min.js')}}"></script>
    <script src="{{ asset('js/selects/select2.min.js') }}"></script>
	<script>

		var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		            '  <div class="modal-content">\n' +
		            '    <div class="modal-header align-items-center">\n' +
		            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		            '      <div class="kv-zoom-actions btn-group">{close}</div>\n' +
		            '    </div>\n' +
		            '    <div class="modal-body">\n' +
		            '      <div class="floating-buttons btn-group"></div>\n' +
		            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		            '    </div>\n' +
		            '  </div>\n' +
		            '</div>\n';

		document.addEventListener('DOMContentLoaded', function() {

		    if($().fileinput){
		        $('.image').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000
		        });
		    }

            $().select2 && 	$('.select').select2({ minimumResultsForSearch: Infinity });

		});
	</script>
@endpush
