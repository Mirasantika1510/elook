@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Product Categories",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Product Categories', 'link' => null, 'icon' => 'icon-stars'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Product Categories</h5>
				<a href="{{ url(config('elook.admin_url').'/product-categories/create') }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-database-add mr-2"></i> Create New </a>
			</div>
			
			@include('admin.layouts.validation_error', [ 'errors' => $errors ])


			<table class="table datatable-basic table-striped" id="product-categories">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>Title </th>
		              <th>Description </th>
		              <th>Image </th>
		              <th><span class="nobr">Action</span></th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script>
        $(function() {

        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Search:</span> _INPUT_',
	                searchPlaceholder: 'Input your keywords...',
	                lengthMenu: '<span>Show:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#product-categories').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/product-categories-tables')}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'image', name: 'image' },
                    { data: 'actions', name: 'actions' }
                ]
            });
        });
    </script>
@endpush
