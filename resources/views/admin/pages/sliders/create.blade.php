@extends('admin.layouts.master')

	<!-- <style type="text/css">
		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview,.table.datatable-basic>thead>tr>th{border:none}.table.datatable-basic>thead>tr>th:last-child,.table.datatable-basic>thead>tr>th:first-child{width:10%;text-align:center;}.table.datatable-basic>tbody>tr>td:last-child,.table.datatable-basic>tbody>tr>td:first-child{text-align:center;padding-right: 2.25rem;}.table.datatable-basic>tbody>tr>td{padding:.75rem 1.25rem}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}
	</style> -->

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Slider",
			'link'	=> url(config('elook.admin_url').'/sliders')
		],
		'breads' => [
			[ 'title' => 'Slider', 'link' => url(config('elook.admin_url').'/sliders'), 'icon' => 'icon-images2'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Tambah Slider Baru</h5>
			</div>

			<div class="card-body">

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/sliders') }}" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row {{ $errors->has('image')? 'img-error' : '' }}">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('image')? 'text-danger' : '' }}">Image</label>
							<div class="col-lg-10 single-image">
								<input type="file" class="file-input image" data-show-caption="false" data-show-upload="false" accept="image/*" name="image" data-fouc>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('link_url')? 'text-danger' : '' }}">Link Url</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('link_url')? 'border-danger' : '' }}" placeholder="Masukkan link url slider, e.g https://google.com" name="link_url" value="{{ old('link_url') }}">
							</div>
						</div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection

@push('script')
	<script src="{{asset('js/fileinput.min.js')}}"></script>

	<script>

		var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		            '  <div class="modal-content">\n' +
		            '    <div class="modal-header align-items-center">\n' +
		            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		            '      <div class="kv-zoom-actions btn-group">{close}</div>\n' +
		            '    </div>\n' +
		            '    <div class="modal-body">\n' +
		            '      <div class="floating-buttons btn-group"></div>\n' +
		            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		            '    </div>\n' +
		            '  </div>\n' +
		            '</div>\n';

		document.addEventListener('DOMContentLoaded', function() {

		    if($().fileinput){
		        $('.image').fileinput({
		    		autoReplace: true,
		            browseLabel: 'Browse',
		            browseIcon: '<i class="icon-folder-search mr-2"></i>',
		            layoutTemplates: {
		                icon: '<i class="icon-file-check"></i>',
		                modal: modalTemplate,
		                actions: '<div class="file-actions"><div class="file-footer-buttons">{zoom}</div></div>'
		            },
				    purifyHtml: true,
		            previewZoomButtonClasses: {close: 'btn btn-light btn-icon btn-sm'},
		            previewZoomButtonIcons: { close: '<i class="icon-cross2 font-size-base"></i>' },
		            fileActionSettings: { zoomIcon: '<i class="icon-zoomin3"></i>' },
		            allowedFileExtensions: ["jpg", "png", "jpeg"],
    				showClose: false,
    				maxFileSize: 1000
		        });
		    }

		});


	</script>
@endpush
