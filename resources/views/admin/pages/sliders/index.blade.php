@extends('admin.layouts.master')

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Sliders",
			'link'	=> null
		],
		'breads' => [
			[ 'title' => 'Sliders', 'link' => null, 'icon' => 'icon-images2'  ],
			[ 'title' => 'index', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Control position -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Data Sliders</h5>
				<a href="{{ url(config('elook.admin_url').'/sliders/create') }}" class="btn btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2"><i class="icon-database-add mr-2"></i> Tambah Sliders </a>
			</div>

			@include('admin.layouts.validation_error', [ 'errors' => $errors ])

			<table class="table datatable-basic table-striped" id="sliders-tables">
		        <thead>
		            <tr>
		              <th>No </th>
		              <th>Link Url</th>
		              <th>Image</th>
		              <th><span class="nobr">Action</span></th>
		            </tr>
	          	</thead>
	        </table>
		</div>

	</div>
	<!-- /content area -->

@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
	<script>
        $(function() {
        	$.extend( $.fn.dataTable.defaults, {
	            autoWidth: false,
	            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	            language: {
	                search: '<span>Pencarian:</span> _INPUT_',
	                searchPlaceholder: 'Masukkan pencarian...',
	                lengthMenu: '<span>Tampilkan:</span> _MENU_',
	                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
	            }
	        });

            $('#sliders-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{url(config('elook.admin_url').'/sliders-tables')}}",
                columns: [
                    { data: 'rownum', name: 'rownum' },
                    { data: 'link_url', name: 'link_url' },
                    { data: 'image', name: 'image' },
                    { data: 'actions', name: 'actions', orderable: false }
                ]
            });
        });
    </script>
@endpush
