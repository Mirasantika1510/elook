@extends('admin.layouts.master')

@push('styles')

	<style>
		.sos-selector input{
		    margin:0;padding:0;
		    -webkit-appearance:none;
		       -moz-appearance:none;
		            appearance:none;
		}

		.sos-selector input:active +.drinkcard-sos{opacity: .9;}
		.sos-selector input:checked +.drinkcard-sos{
		    -webkit-filter: none;
		       -moz-filter: none;
		            filter: none;
		}

		.drinkcard-sos{
		    cursor:pointer;
		    -webkit-transition: all 100ms ease-in;
		       -moz-transition: all 100ms ease-in;
		            transition: all 100ms ease-in;
		    -webkit-filter: brightness(1.8) grayscale(1) opacity(.7);
		       -moz-filter: brightness(1.8) grayscale(1) opacity(.7);
		            filter: brightness(1.8) grayscale(1) opacity(.7);
		}
		.drinkcard-sos:hover{
		    -webkit-filter: brightness(1.2) grayscale(.5) opacity(.9);
		       -moz-filter: brightness(1.2) grayscale(.5) opacity(.9);
		            filter: brightness(1.2) grayscale(.5) opacity(.9);
		}
	</style>

@endpush

@section('content')

	@include('admin.layouts.breadcrum', [
		'previous' => [
			'title' => "Social Media",
			'link'	=> url(config('elook.admin_url').'/socials')
		],
		'breads' => [
			[ 'title' => 'Social Media', 'link' => url(config('elook.admin_url').'/socials'), 'icon' => 'icon-users'  ],
			[ 'title' => 'create', 'link' => null, 'icon' => null ]
		]
	])


	<!-- Content area -->
	<div class="content">

		<!-- Form inputs -->
		<div class="card" style="padding: 20px 30px;">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Edit Social Media</h5>
			</div>

			<div class="card-body">
				<p class="text-grey info-form"><strong>NOTE : </strong>All field must be filled.</p>

				@include('admin.layouts.validation_error', [ 'errors' => $errors ])

				<form action="{{ url(config('elook.admin_url').'/socials', $social->id) }}" method="POST" enctype="multipart/form-data">

					{{ method_field('PUT') }}
					{{ csrf_field() }}

					<fieldset class="mb-3">

						<legend class="text-uppercase font-size-sm font-weight-bold"></legend>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('title')? 'text-danger' : '' }}">Title</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('title')? 'border-danger' : '' }}" placeholder="Input your title here" name="title" value="{{ old('title')?: $social->title }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('link')? 'text-danger' : '' }}">Link</label>
							<div class="col-lg-10">
								<input type="text" class="form-control {{ $errors->has('link')? 'border-danger' : '' }}" placeholder="Input your link here e.g https://www.facebook.com" name="link" value="{{ old('link')?: $social->link }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-form-label col-lg-2 font-weight-semibold {{ $errors->has('vendor')? 'text-danger' : '' }}">Social Media</label>
							<div class="col-lg-10">
								<div class="sos-selector">
									<?php $checked = old('vendor')?: $social->vendor; ?>
							        <input {{ $checked === 'twitter'? 'checked' : '' }} id="twitter" type="radio" name="vendor" value="twitter" />
							        <label class="drinkcard-sos" for="twitter"><i class="icon-twitter2 icon-2x"></i></label>

							        <input {{ $checked === 'facebook'? 'checked' : '' }} id="facebook" type="radio" name="vendor" value="facebook" />
							        <label class="drinkcard-sos"for="facebook"><i class="icon-facebook2 icon-2x"></i></label>

							        <input {{ $checked === 'youtube'? 'checked' : '' }} id="youtube" type="radio" name="vendor" value="youtube" />
							        <label class="drinkcard-sos"for="youtube"><i class="icon-youtube3 icon-2x"></i></label>

							        <input {{ $checked === 'linkedin'? 'checked' : '' }} id="linkedin" type="radio" name="vendor" value="linkedin" />
							        <label class="drinkcard-sos"for="linkedin"><i class="icon-linkedin icon-2x"></i></label>

							        <input {{ $checked === 'instagram'? 'checked' : '' }} id="instagram" type="radio" name="vendor" value="instagram" />
							        <label class="drinkcard-sos"for="instagram"><i class="icon-instagram icon-2x"></i></label>

							        <input {{ $checked === 'tumblr'? 'checked' : '' }} id="tumblr" type="radio" name="vendor" value="tumblr" />
							        <label class="drinkcard-sos"for="tumblr"><i class="icon-tumblr2 icon-2x"></i></label>
							    </div>
							</div>

						</div>

					</fieldset>


					<div class="text-right">
						<button type="submit" class="btn btn-primary">Update <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /form inputs -->

	</div>
	<!-- /content area -->

@endsection
