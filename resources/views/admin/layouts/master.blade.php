<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>{{ env('APP_NAME') }}</title>

	<!-- Global stylesheets -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->

	<link href="{{asset('css/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/vendor.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<meta name="csrf-token" content="{{ csrf_token() }}">

	<script src="{{asset('js/vendor.js')}}"></script>
	<script src="{{asset('js/admin_init.js')}}"></script>

	<link href="{{ asset('favicon.ico') }}" rel="icon" type="image/x-icon" />
	<!-- /theme JS files -->
	@stack('styles')

	<style type="text/css">
		.info-form{padding:10px;color:#2094e4;border:1px dashed #00ac9a;background-color:#00ac9a33}.single-image .file-preview,.table.datatable-basic>thead>tr>th{border:none}.table.datatable-basic>thead>tr>th:last-child,.table.datatable-basic>thead>tr>th:first-child{width:6%;text-align:center;}.table.datatable-basic>tbody>tr>td:last-child,.table.datatable-basic>tbody>tr>td:first-child{text-align:center;padding-right: 2.25rem;}.table.datatable-basic>tbody>tr>td{padding:.75rem 1.25rem}.single-image .file-preview-frame.kv-preview-thumb,.single-image .kv-fileinput-error.file-error-message{margin:0}.kv-zoom-body>img{width:auto!important}.dataTables_filter>label>input{margin-left:5px}.form-group.row.desc-error .note-editor{border-color: #f55246;}.form-group .uniform-checker.check-error span{border-color: #f55246;}.form-group.img-error div.btn.btn-file{background-color: #f55246;}.form-group.option-error div.uniform-select{border-color:#f44336;}
	</style>
	@stack('script')
</head>

<body>

	@include('admin.layouts.navbar')


	<!-- Page content -->
	<div class="page-content">

		@include('admin.layouts.sidebar')

		<!-- Main content -->
		<div class="content-wrapper">

			@yield('content')


			@include('admin.layouts.footer')

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>
