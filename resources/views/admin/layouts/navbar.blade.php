<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="index.html" class="d-inline-block">
            <img style="height:2rem;" src="" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <span class="navbar-text ml-md-3 mr-md-auto">
            <span id="indicator-connection" class="badge bg-success">Online</span>
        </span>

        <script type="text/javascript">
            let $indicator = $('#indicator-connection');
            $(function(){
                if(navigator.onLine){
                    $indicator.text('Online').removeClass('bg-warning').addClass('bg-success');
                }else{
                    $indicator.text('Offline').removeClass('bg-success').addClass('bg-warning');
                }
            });

            // console.log(navigator.onLine);
            window.addEventListener('offline', function(e) {
                $indicator.text('Offline').removeClass('bg-success').addClass('bg-warning');
            });

            window.addEventListener('online', function(e) {
                $indicator.text('Online').removeClass('bg-warning').addClass('bg-success');
            });

        </script>

        <ul class="navbar-nav">

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('images/users.png') }}" class="rounded-circle" alt="">
                    <span>{{ auth()->user()->name }}</span>
                </a>

                <form id="logout" action="{{ url(config('elook.admin_url').'/logout') }}" method="POST">{{ csrf_field() }}</form>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{ url(config('elook.admin_url').'/companies-settings') }}" class="dropdown-item"><i class="icon-cog5"></i> Settings</a>
                    <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout').submit();"><i class="icon-switch2"></i> Keluar</a>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->
