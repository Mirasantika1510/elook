<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

	<!-- Sidebar mobile toggler -->
	<div class="sidebar-mobile-toggler text-center">
		<a href="#" class="sidebar-mobile-main-toggle">
			<i class="icon-arrow-left8"></i>
		</a>
		Navigation
		<a href="#" class="sidebar-mobile-expand">
			<i class="icon-screen-full"></i>
			<i class="icon-screen-normal"></i>
		</a>
	</div>
	<!-- /sidebar mobile toggler -->


	<!-- Sidebar content -->
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="card-body">
				<div class="media">
					<div class="mr-3">
						<a href="#"><img src="{{ asset('images/users.png') }}" width="38" height="38" class="rounded-circle" alt=""></a>
					</div>

					<div class="media-body">
						<div class="media-title font-weight-semibold">{{ auth()->user()->name }}</div>
						<div class="font-size-xs opacity-50">
							<i class="icon-pin font-size-sm"></i> &nbsp;Bandung
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="card card-sidebar-mobile">
			<ul class="nav nav-sidebar" data-nav-type="accordion">

				<!-- Main -->
				<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>

				<li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/dashboard')}}" class="nav-link {{ request()->segment(2) === 'dashboard'? 'active' : '' }}">
						<i class="icon-sphere3"></i>
						<span> Dashboard </span>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/orders')}}" class="nav-link {{ request()->segment(2) === 'orders' || request()->segment(2) === 'reports'? 'active' : '' }}">
						<i class="icon-cart"></i>
						<span> Orders </span>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/members')}}" class="nav-link {{ request()->segment(2) === 'members' || request()->segment(2) === 'members-detail'? 'active' : '' }}">
						<i class="icon-users"></i>
						<span> Member </span>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/guests')}}" class="nav-link {{ request()->segment(2) === 'guests' || request()->segment(2) === 'guests-detail'? 'active' : '' }}">
						<i class="icon-users"></i>
						<span> Guest </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/sliders')}}" class="nav-link {{ request()->segment(2) === 'sliders'? 'active' : '' }}">
						<i class="icon-images3"></i>
						<span> Sliders </span>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/banners')}}" class="nav-link {{ request()->segment(2) === 'banners'? 'active' : '' }}">
						<i class="icon-images3"></i>
						<span> Banners </span>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/product-categories')}}" class="nav-link {{ request()->segment(2) === 'product-categories'? 'active' : '' }}">
						<i class="icon-cube4"></i>
						<span> Products Categories </span>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/products')}}" class="nav-link {{ request()->segment(2) === 'products'? 'active' : '' }}">
						<i class="icon-bag"></i>
						<span> Products </span>
					</a>
                </li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/socials')}}" class="nav-link {{ request()->segment(2) === 'socials'? 'active' : '' }}">
						<i class="icon-twitter"></i>
						<span> Social Media </span>
					</a>
                </li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/testimonials')}}" class="nav-link {{ request()->segment(2) === 'testimonials'? 'active' : '' }}">
						<i class="icon-comments"></i>
						<span> Testimonials </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/payment-accounts')}}" class="nav-link {{ request()->segment(2) === 'payment-accounts'? 'active' : '' }}">
						<i class="icon-coins"></i>
						<span> Payment Accounts </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/lookbooks')}}" class="nav-link {{ request()->segment(2) === 'lookbooks'? 'active' : '' }}">
						<i class="icon-book3"></i>
						<span> Lookbooks </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/subscribes')}}" class="nav-link {{ request()->segment(2) === 'subscribes'? 'active' : '' }}">
						<i class="icon-bell2"></i>
						<span> Subscribes </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/messages')}}" class="nav-link {{ request()->segment(2) === 'messages'? 'active' : '' }}">
						<i class="icon-envelope"></i>
						<span> Messages </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/discounts')}}" class="nav-link {{ request()->segment(2) === 'discounts'? 'active' : '' }}">
						<i class="icon-percent"></i>
						<span> Discounts </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/vouchers')}}" class="nav-link {{ request()->segment(2) === 'vouchers'? 'active' : '' }}">
						<i class="icon-ticket"></i>
						<span> Vouchers </span>
					</a>
				</li>

                <li class="nav-item">
					<a href="{{url(config('elook.admin_url').'/custom-pages')}}" class="nav-link {{ request()->segment(2) === 'custom-pages'? 'active' : '' }}">
						<i class="icon-browser"></i>
						<span> Custom Pages </span>
					</a>
				</li>

			</ul>
		</div>
		<!-- /main navigation -->

	</div>
	<!-- /sidebar content -->

</div>
<!-- /main sidebar -->
