<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				@if($previous['link'])
					<a href="{{ $previous['link'] }}"><i class="icon-arrow-left52 mr-2"></i></a>
				@endif
					<span class="font-weight-semibold">{{ $previous['title'] }}</span>
			</h4>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
		<div class="d-flex">
			<div class="breadcrumb">

				@foreach($breads as $bread)
					@if(!$bread['link'])
						<span class="breadcrumb-item active">{{ $bread['title'] }}</span>
					@else
						<a href="{{ $bread['link'] }}" class="breadcrumb-item">
							@if($bread['icon'])
								<i class="{{ $bread['icon'] }} mr-1"></i>
							@endif
							{{ $bread['title'] }}
						</a>
					@endif
				@endforeach

			</div>

			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>

	@if(request()->segment(1) === 'profile')
		<div class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="text-center d-lg-none w-100">
				<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-second">
					<i class="icon-menu7 mr-2"></i>
					Navigasi Profil
				</button>
			</div>

			<div class="navbar-collapse collapse" id="navbar-second">
				<ul class="nav navbar-nav">
					<li class="nav-item">
						<a href="#information" class="navbar-nav-link {{ request()->segment(2) === 'information'? 'active' : '' }}" data-toggle="tab">
							<i class="icon-user-tie mr-2"></i>
							Informasi
						</a>
					</li>
					<li class="nav-item">
						<a href="#settings" class="navbar-nav-link {{ request()->segment(2) === 'settings'? 'active' : '' }}" data-toggle="tab">
							<i class="icon-cog3 mr-2"></i>
							Setting Keamanan
						</a>
					</li>
				</ul>
			</div>
		</div>
	@endif

</div>
<!-- /page header -->