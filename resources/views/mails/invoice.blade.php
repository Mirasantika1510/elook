@component('mail::message')
# Order Notification

You have order product from our website. Here is your list of order:

<div class="table" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
    <table style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 30px auto; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
        <thead style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
            <tr>
                <th style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-bottom: 1px solid #EDEFF2; padding-bottom: 8px; margin: 0;">Product</th>
                <th style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-bottom: 1px solid #EDEFF2; padding-bottom: 8px; margin: 0; text-align: center;">Quantity</th>
                <th style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-bottom: 1px solid #EDEFF2; padding-bottom: 8px; margin: 0; text-align: right;">Price (@)</th>
                <th style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-bottom: 1px solid #EDEFF2; padding-bottom: 8px; margin: 0; text-align: right;">Total Price</th>
            </tr>
        </thead>
        <tbody style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
            @foreach($order->products as $product)
                <tr>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;">{{ $product->product_name }}</td>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0; text-align: center;">{{ $product->quantities }}</td>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0; text-align: right;">
                        @if($product->product_discount)
                            Rp. {{ number_format($product->product_price - ($product->product_discount/100 * $product->product_price),0, ',','.') }}
                        @else
                        Rp. {{ number_format($product->product_price,0, ',','.') }}
                        @endif
                    </td>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0; text-align: right;">
                        @if($product->product_discount)
                            Rp. {{ number_format(($product->product_price - ($product->product_discount/100 * $product->product_price)) * $product->quantities,0, ',','.') }}
                        @else
                        Rp. {{ number_format($product->product_price * $product->quantities,0, ',','.') }}
                        @endif
                    </td>
                </tr>
            @endforeach
            @if($order->voucher)
            <tr>
                <td colspan="3" style="border-top: 1px solid #EDEFFF;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Subtotal Price</strong></td>
                <td style="border-top: 1px solid #EDEFFF;text-align: right;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Rp. {{ number_format($order->subtotal_price + $order->voucher->amount,0, ',','.') }}</strong></td>
            </tr>
            @else
            <tr>
                <td colspan="3" style="border-top: 1px solid #EDEFFF;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Subtotal Price</strong></td>
                <td style="border-top: 1px solid #EDEFFF;text-align: right;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Rp. {{ number_format($order->subtotal_price,0, ',','.') }}</strong></td>
            </tr>
            @endif
            <tr>
                <td colspan="3" style="border-top: 1px solid #EDEFFF;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Shipping Price</strong></td>
                <td style="border-top: 1px solid #EDEFFF;text-align: right;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Rp. {{ number_format($order->shipping_price,0, ',','.') }}</strong></td>
            </tr>
            @if($order->voucher)
                <tr>
                    <td colspan="3" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Voucher</strong></td>
                    <td style="text-align: right;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong style="color:red;">
                        - Rp. {{ number_format($order->voucher->amount,0, ',','.') }}</strong>
                    </td>
                </tr>
            @endif
            <tr>
                <td colspan="3" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Total Price + Kode Unik</strong></td>
                <td style="text-align: right;font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0;"><strong>Rp. {{ number_format(($order->total_price + $order->unique_number),0, ',','.') }}</strong></td>
            </tr>
        </tbody>
    </table>
</div>

<p>Order Id : <strong> {{ $order->invoice_number }} </strong></p>
<p>Metode Pembayaran : <strong> 'Bank Transfer' </strong></p>
<p>Bank : <strong> {{ strtoupper($order->payment->account_bank) }} </strong></p>
<p>Nomor Rekening: <strong> {{ $order->payment->account_number }} </strong></p>
<p>
    Jumlah yang harus dibayar : <strong> Rp. {{ number_format(($order->total_price + $order->unique_number), 0, ',', '.') }}
</p>
<p>Pembayaran harus dilakukan sebelum <strong>{{ $order->created_at->addHours(24)->format('Y-m-d') }} Pukul {{ $order->created_at->addHours(24)->format('H:i:s') }}</strong></p>
<p>Instructions :</p>
<div class="instructions-content">
    {!! $order->payment->instructions !!}
</div>

Thank you for order in our website,<br>
{{ config('app.name') }}
@endcomponent
