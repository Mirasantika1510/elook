@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ asset('front/img/hero/shop.jpg') }}">
        <div class="container">
            <h1>WISHLIST</h1>
        </div>
    </div>
    <div class="ps-page ps-page--blog">
        <div class="ps-blog">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="ps-block--document-content no-uppercase">
                            <div class="table-responsive mb-50">
                                <table class="table ps-table">
                                    <tbody>
                                        <tr>
                                            <td><strong>No</strong></td>
                                            <td><strong>Product Name</strong></td>
                                            <td><strong>Unit Price</strong></td>
                                            <td><strong>Stock Status</strong></td>
                                            <td><strong>Action</strong></td>
                                        </tr>
                                        <?php $i = 0; ?>
                                        @forelse(auth()->user()->wishlist as $wishlist)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $wishlist->product->title }}</td>
                                                <td>
                                                    @if($wishlist->product->publishedDiscount)
                                                        <?php $priceNow = $wishlist->product->sell_price - ($wishlist->product->publishedDiscount->discount_percent/100 * $wishlist->product->sell_price);?>
                                                        <del>Rp. {{ number_format($wishlist->product->sell_price, 2, ',', '.') }}</del>
                                                        <h4 class="ps-product__price sale" style="display: inline-block;color:red;">Rp. {{ number_format($priceNow, 2, ',', '.') }}</h4>
                                                    @else
                                                        Rp. {{ number_format($wishlist->product->sell_price, 2, ',', '.') }}
                                                    @endif
                                                </td>
                                                <td>{{ $wishlist->product->ignore_stock? 'Ready Stock' : ($wishlist->product->stock_used < $wishlist->product->stock? 'Ready Stock' : 'No Stock') }}</td>
                                                <td>
                                                    <form id="remove-wishlist-{{ $wishlist->id }}" action="{{ url('remove-wishlist', $wishlist->id) }}" method="POST" style="display: inline-block;">
                                                        @csrf() @method('DELETE')
                                                    </form>
                                                        <a href="#" onclick="event.preventDefault(); document.getElementById('remove-wishlist-{{ $wishlist->id }}').submit();">
                                                        <i class="fa fa-remove"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" style="text-align:center;">
                                                    No products were added to the wishlist
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
