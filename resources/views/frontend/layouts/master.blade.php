<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from warethemes.com/html/claue/homepage-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 03:42:22 GMT -->
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="telephone=no" name="format-detection">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<link href="{{ asset('front/apple-touch-icon.png') }}" rel="apple-touch-icon">
	<link href="{{ asset('front/favicon.png') }}" rel="icon">
	<meta content="" name="author">
	<meta content="" name="keywords">
	<meta content="Elook Official" name="description">
	<title>Elook Official</title>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,500i,600,600i,700%7CLibre+Baskerville:400,400i,700&amp;subset=latin-ext" rel="stylesheet">
	<link href="{{ asset('front/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">


	<link href="{{ asset('front/plugins/bootstrap4/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('front/plugins/slick/slick/slick.css') }}" rel="stylesheet">
	<link href="{{ asset('front/plugins/lightGallery-master/dist/css/lightgallery.min.css') }}" rel="stylesheet">
	<link href="{{ asset('front/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}" rel="stylesheet">
	<link href="{{ asset('front/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
	<link href="{{ asset('front/plugins/pe7/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" rel="stylesheet">
	<link href="{{ asset('front/plugins/YTPlayer/dist/css/jquery.mb.YTPlayer.min.css') }}" rel="stylesheet">
	<link href="{{ asset('front/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('front/css/custom.css') }}" rel="stylesheet">

	<script src="{{ asset('front/plugins/jquery-1.12.4.min.js') }}"></script>
    <style>
        #cart-sticky-wa {
            position: fixed;
            bottom: 20px;
            right: 20px;
        }

        .top-messages {
            text-align: center;
            position: fixed;
            width: 100%;
            z-index: 10;
            top: 0;
        }

        @media (max-width: 425px) {
            .ps-our-products .ps-product .ps-product__thumbnail img {
                height: 200px;
            }
        }

    </style>
    @stack('styles')

    @if(env('ENABLED_FACEBOOK_PIXEL', true))

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1108575139352020');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1108575139352020&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel Code -->

    @endif
</head>
<body class="bg--parallax">

    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')

    <div id="cart-sticky-wa" style="z-index:10">
        <div class="form-group--number"></div>
        <a href="https://wa.me/{{ $companies->phone }}" target="_blank">
            <img src="{{ asset('front/img/icon-WA-transparan-pic2.png') }}" style="width: 85px;">
        </a>
    </div>

	<div id="back2top">
		<i class="pe-7s-angle-up"></i>
	</div>
	<div class="ps-site-overlay"></div>
	<div id="loader-wrapper">
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
    </div>

	<div class="ps-search" id="site-search">
		<a class="ps-btn--close" href="#"></a>
		<div class="ps-search__content">
			<form action="{{ url('search') }}" class="ps-form--primary-search" method="GET">
                <input class="form-control" placeholder="Search for..." type="text" name="keywords">
                <button><i class="aroma-magnifying-glass"></i></button>
			</form>
		</div>
    </div>

	<div class="ps-cart--sidebar">
		<div class="ps-cart__header">
            <h3>Mini Cart</h3>
            <a class="ps-btn--close ps-btn--no-boder" href="#"></a>
		</div>
		<div class="ps-cart__content">
            @if(session()->has('S_iP'))
                @foreach(session()->get('S_iP')->items as $sessionCart)
                    <div class="ps-product--cart">
                        <div class="ps-product__thumbnail">
                            <a href="{{ url('product-detail', $sessionCart['item']->seo_page_slug?: $sessionCart['item']->slug) }}">
                                <img alt="" src="{{ $sessionCart['item']->images->first()->imageUrl }}">
                            </a>
                                <span onclick="event.preventDefault(); window.location.href='{{ url('shop/delete-from-cart', $sessionCart['item']->id) }}';" class="ps-btn--close ps-btn--no-boder" style="cursor:pointer;"></span>

                        </div>
                        <div class="ps-product__content">
                            <a href="#">{{ $sessionCart['item']->title }}</a>
                            <span>
								{{ $sessionCart['qty'] }}x
								@if($sessionCart['item']->publishedDiscount)
									<?php $priceNow = $sessionCart['item']->sell_price - ($sessionCart['item']->publishedDiscount->discount_percent/100 * $sessionCart['item']->sell_price);?>
									<del>Rp. {{ number_format($sessionCart['item']->sell_price, 0, ',', '.') }}</del>
									<span style="display: inline-block;color:red;">Rp. {{ number_format($priceNow, 0, ',', '.') }}</span>
								@else
									Rp. {{ number_format($sessionCart['item']->sell_price, 0, ',', '.') }}
								@endif
							</span>
                        </div>
                    </div>
                @endforeach
            @endif
		</div>
		<div class="ps-cart__footer">
            <h4>SubTotal: <span>Rp. {{ session()->has('S_iP') ? number_format(session()->get('S_iP')->totalPrice,0, ',','.') : 0 }}</span></h4>
            <a href="{{ url('cart') }}" style="width:100%">
			<img style="cursor: pointer"  src="{{ asset('front/img/view_cart.png') }}" style="width:100%"  />
			</a>

		</div>
    </div>

	<script src="{{ asset('front/plugins/bootstrap4/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('front/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('front/plugins/popper.min.js') }}"></script>
	<script src="{{ asset('front/plugins/imagesloaded.pkgd.js') }}"></script>
	<script src="{{ asset('front/plugins/masonry.pkgd.min.js') }}"></script>
	<script src="{{ asset('front/plugins/isotope.pkgd.min.js') }}"></script>
	<script src="{{ asset('front/plugins/jquery.matchHeight-min.js') }}"></script>
	<script src="{{ asset('front/plugins/slick/slick/slick.min.js') }}"></script>
	<script src="{{ asset('front/plugins/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
	<script src="{{ asset('front/plugins/slick-animation.min.js') }}"></script>
	<script src="{{ asset('front/plugins/lightGallery-master/dist/js/lightgallery-all.min.js') }}"></script>
	<script src="{{ asset('front/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('front/plugins/sticky-sidebar/dist/sticky-sidebar.min.js') }}"></script>
	{{-- <script src="{{ asset('front/plugins/YTPlayer/dist/jquery.mb.YTPlayer.min.js') }}"></script> --}}
	{{-- <script src="{{ asset('front/plugins/gmap3.min.js') }}"></script> --}}

	<script src="{{ asset('front/js/main.js') }}"></script>
	<script src="{{ asset('front/js/custom.js') }}"></script>
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxflHHc5FlDVI-J71pO7hM1QJNW1dRp4U&amp;region=GB"></script><!-- Mirrored from warethemes.com/html/claue/homepage-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 03:42:27 GMT --> --}}

    @stack('scripts')

    <script>
		$(function(){
			setTimeout(function() {
			    $('#order-cart-message').fadeOut('slow');
			    $('#order-cart-alert').fadeOut('slow');
			}, 3000);
		});
	</script>
</body>
</html>
