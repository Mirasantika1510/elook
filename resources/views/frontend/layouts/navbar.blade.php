<header class="header">
	<!-- START NAVIGATION -->
	<nav class="navbar navbar-default navbar-fat navbar-shadow navbar-border navbar-sticky bootsnav">
	    <!-- Start Top Search -->
	    <div class="top-search top-search-fixed">
	        <div class="cover-wrapper">
	        	<div class="cover-inner-center">
	        		<div class="container">
	        			<form method="get">
	        				<div class="input-group">
			                    <input type="text" class="form-control" placeholder="Type & Press Enter" autofocus="" >
			                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
			                    <span class="input-group-addon close-search full-screen-butto"></span>
			                </div>
	        			</form>
	        		</div>
	        	</div>
	        </div>
	    </div>
	    <!-- End Top Search -->

	    <div class="container-fluid">

	        <!-- Start Atribute Navigation -->
	        <div class="attr-nav">
	            <ul>
	                <li class="dropdown">
	                    <a href="{{ url('checkout') }}" class="dropdown-toggle" data-toggle="dropdown" >
	                        <i class="fa fa-shopping-bag"></i>
	                        <span class="badge">{{ session()->has('S_iP')? session()->get('S_iP')->totalQty : 0 }}</span>
	                    </a>
	                    <ul class="dropdown-menu cart-list">
	                    	@if(session()->has('S_iP'))
		                    	@foreach(session()->get('S_iP')->items as $sessionCart)
			                        <li class="list-cart-on-badge">
			                            <a href="#" class="photo"><img src="{{ $sessionCart['item']->images()->inRandomOrder()->first()->imageUrl }}" class="cart-thumb" alt="" /></a>
			                        	<div>
				                            <h6><a href="#">{{ $sessionCart['item']->title }}</a></h6>
				                            @if($sessionCart['item']->avaibility === 'stock' && $sessionCart['item']->stock_status === 'attributes')
				                            	@foreach($sessionCart['attributes'] as $key => $qtyAttributes)
													<?php $attributesInItem = array_first($sessionCart['item']->attributes->toArray(), function ($value) use($key) { return $value['key'] === $key; }); ?>
						                            <p>{{ $attributesInItem['title'] }} - {{ $qtyAttributes }}x -
						                            <span class="price">
						                            	@if($sessionCart['item']->discount)
							                            	Rp. {{ number_format(($attributesInItem['scheme']['price'] - ($sessionCart['item']->discount / 100 * $attributesInItem['scheme']['price'])),0, ',','.') }}
							                            @else
															Rp. {{ number_format($attributesInItem['scheme']['price'],0, ',','.') }}
							                            @endif
							                        </span>
													<a href="{{ url('shop/delete-from-cart', $sessionCart['item']->key) }}?attributes={{ $key }}"><span class="pull-right delete-cart"><i class="fa fa-close"></i></span></a>
						                            </p>
					                            @endforeach
				                            @else
					                            <p>
					                            	{{ $sessionCart['qty'] }}x -
					                            	<span class="price">
					                            		@if($sessionCart['item']->discount)
					                            			Rp. {{ number_format(($sessionCart['item']->price - ($sessionCart['item']->discount / 100 * $sessionCart['item']->price)),0, ',','.') }}
					                            		@else
					                            			Rp. {{ number_format($sessionCart['item']->price,0, ',','.') }}
					                            		@endif
					                            	</span>
													<a href="{{ url('shop/delete-from-cart', $sessionCart['item']->key) }}"><span class="pull-right delete-cart"><i class="fa fa-close"></i></span></a>
					                            </p>
				                            @endif
			                        	</div>
			                        </li>
		                        @endforeach
		                        <li class="total">
		                            <span class="pull-right"><strong>Total</strong>: Rp. {{ number_format(session()->get('S_iP')->totalPrice,0, ',','.') }}</span>
		                            <a href="{{ url('cart') }}" class="btn btn-default btn-cart">Cart</a>
		                        </li>
	                        @endif
	                    </ul>
	                </li>
	                <!-- <li class="search"><a href="#"><i class="fa fa-search"></i></a></li> -->
	                <!-- <li class="side-menu"><a href="#"><i class="fa fa-bars"></i></a></li> -->
	            </ul>
	        </div>
	        <!-- End Atribute Navigation -->

	        <!-- Start Header Navigation -->
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
	                <i class="fa fa-bars"></i>
	            </button>
	            <a class="navbar-brand" href="{{ url('/') }}">
	                <img src="{{ asset('front/img/winstuff-logo.PNG') }}" class="logo" alt="Brand">
	            </a>
	        </div>
	        <!-- End Header Navigation -->

	        <?php

				function render($node){

					if($node->depth === 0){

						if(!$node->isLeaf()){

							return '<li class="dropdown '.($node->_lft === 1? "active": "").' megamenu-fw">
										<a href="'.url("shop").'?categories='.$node->slug.'" class="dropdown-toggle" data-toggle="dropdown">'.$node->title.'</a>
											<ul class="dropdown-menu megamenu-content" role="menu">
												<li>
													<div class="row">
														<div class="col-menu col-md-12">
							';

						}else{

							return '<li class="'.($node->_lft === 1? "active": "").'"><a href="'.url("shop").'?categories='.$node->slug.'">'.$node->title.'</a></li>';

						}


					}elseif($node->depth === 1){

						if(!$node->isLeaf()){

							return '<div class="col-menu col-md-3">
										<a href="'.url("shop").'?categories='.$node->slug.'"><h5 class="title">'.$node->title.'</h5></a>
										<div class="content">
											<ul class="menu-col">';

						}else{

							return '<a href="'.url("shop").'?categories='.$node->slug.'"><h5 class="title">'.$node->title.'</h5></a>';

						}

					}else{

						if(!$node->isLeaf()){

							return '<li class="dropdown">
										<a href="'.url("shop").'?categories='.$node->slug.'" class="dropdown-toggle" data-toggle="dropdown">'.$node->title.'</a>
										<ul class="dropdown-menu">';

						}else{

							return '<li>
										<a href="'.url("shop").'?categories='.$node->slug.'">'.$node->title.'</a>
									</li>';

						}

					}

				}

				function closeRender($node){

					if($node->depth === 0){

						if(!$node->isLeaf()){

							return '</div></div></li></ul></a></li>';

						}else{

							return '';

						}


					}elseif($node->depth === 1){

						if(!$node->isLeaf()){

							return '</ul></div></div>';

						}else{

							return '';

						}

					}else{

						if(!$node->isLeaf()){

							return '</ul></li>';

						}else{

							return '';

						}

					}

				}

				$nodes = \App\Model\ProductCategories::withDepth()->get()->toTree();
				$traverse = function ($categories, $prefix = '-') use (&$traverse) {
				    foreach ($categories as $category) {
				        // echo PHP_EOL. $prefix .' '. $category->title;
				        echo PHP_EOL. render($category);

				        $traverse($category->children, $prefix.'--');
				        echo PHP_EOL. closeRender($category);
				    }
				};

				// $traverse($nodes);
			?>

	        <!-- Navbar Collapse  -->
	        <div class="collapse navbar-collapse" id="navbar-menu">
	            <ul class="nav navbar-nav navbar-right">

	            	<?php $traverse($nodes);?>

	            </ul>
	        </div><!-- navbar collapse end -->
	    </div>

	    <!-- Start Side Menu -->
	    <div class="side">
	        <a href="#" class="close-side"><i class="fa fa-times"></i></a>
	        <a href="#" class="nav-side-close close-side"></a>
	        <div class="side-wrap">
	        	<div class="widget">
	                <h6 class="title">Custom Pages</h6>
	                <ul class="link">
	                    <li><a href="#">About</a></li>
	                    <li><a href="#">Services</a></li>
	                    <li><a href="#">Blog</a></li>
	                    <li><a href="#">Portfolio</a></li>
	                    <li><a href="#">Contact</a></li>
	                </ul>
	            </div>
	            <div class="widget">
	                <h6 class="title">Additional Links</h6>
	                <ul class="link">
	                    <li><a href="#">Retina Homepage</a></li>
	                    <li><a href="#">New Page Examples</a></li>
	                    <li><a href="#">Parallax Sections</a></li>
	                    <li><a href="#">Shortcode Central</a></li>
	                    <li><a href="#">Ultimate Font Collection</a></li>
	                </ul>
	            </div>
	        </div>
	    </div>
	    <!-- End Side Menu -->
	</nav>
	<!-- END NAVIGATION -->
</header>
