<style>
    .wishlist-number {
        position: absolute;
        top: 40%;
        left: 50%;
        width: 20px;
        height: 20px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -ms-border-radius: 50%;
        border-radius: 50%;
        -webkit-transform: translate(25%, -100%);
        -moz-transform: translate(25%, -100%);
        -ms-transform: translate(25%, -100%);
        -o-transform: translate(25%, -100%);
        transform: translate(25%, -100%);
        background-color: #000;
    }

    #all-products h4{
        transition: all 0.5s ease;
    }

    #all-products:hover,
    #all-products h4:hover {
        color: #56cfe1 !important;
    }

    input[name="keywords"]:focus {
        color: #495057;
    }

    .ps-form--primary-search input {
        font-size: 15px;
    }

    .btn-close-search {
        font-size: 1.5rem;
    }

    .ps-form--primary-search input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        font-size: 15px;
    }
    .ps-form--primary-search input::-moz-placeholder { /* Firefox 19+ */
        font-size: 15px;
    }
    .ps-form--primary-search input:-ms-input-placeholder { /* IE 10+ */
        font-size: 15px;
    }
    .ps-form--primary-search input:-moz-placeholder { /* Firefox 18- */
        font-size: 15px;
    }

</style>

<header class="header header--3" data-sticky="true">
    <div class="header__top">
        <div class="header__left">
            <p>
                <span>
                    <!-- <i class="pe-7s-call"></i>  -->
                    <img alt="" src="{{ asset('front/img/phone.png') }}" style="height: 16px;">
                    <a href="https://wa.me/{{ $companies->phone }}" target="_blank">{{ $companies->phone }}</a>
                </span>
                <span>
                    <!-- <i class="pe-7s-mail"></i> -->
                    <img alt="" src="{{ asset('front/img/email.png') }}" style="height: 16px;">
                    <a href="mailto:{{ $companies->email }}" target="_top">{{ $companies->email }}</a>
                </span>
            </p>
        </div>
        <div class="header__center">
            <p style="color: #de4d4d;">{{ $companies->mini_notification_text }}</p>
        </div>
        <div class="header__right">
        </div>
    </div>
    <div class="navigation">
        <div class="navigation__left">
            <p class="header__socials">
                @foreach($socials as $social)
                    <a href="{{ $social->link }}" target="_blank">
                        <i class="fa fa-{{ $social->vendor }}"></i>
                    </a>
                @endforeach
            </p>
            <ul class="menu menu--left">
                <li class="menu-item">
                    <a href="{{ url('tracking-order') }}">Tracking Order</a>
                </li>
                <li class="menu-item">
                    <a href="{{ url('confirmation-payment') }}">Confirmation Payment</a>
                </li>
            </ul>
        </div>
        <div class="navigation__center">
            <a class="ps-logo" href="{{ url('/') }}"><img alt="" src="{{ asset('front/img/logo.png')}} "></a>
        </div>
        <div class="navigation__right">
            <ul class="menu menu--right">
                <li class="menu-item-has-children has-mega-menu">
                    <a class="{{ $discountCount > 0? 'has-badge sale': '' }}" href="#">Product</a><span class="sub-toggle"></span>
                    <div class="mega-menu">
                        <div class="mega-menu__column">
                            <a id="all-products" href="{{ url('shop') }}">
                                <h4>All Products</h4>
                            </a>
                            <hr>
                            <h4>
                                <strong>Categories<span class="sub-toggle"></span></strong>
                            </h4>
                            <ul class="mega-menu__list">
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{ url('shop') }}?categories={{$category->slug}}">{{ $category->title }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="menu-item">
                    <a href="{{ url('contact') }}">Contact</a>
                </li>
            </ul>

            <form action="{{ url('search') }}" id="search-form" class="ps-form--primary-search hidden" method="GET">
                <input class="form-control" placeholder="Search for..." type="text" name="keywords">
                <button></button>
                <a href="#" class="btn-close-search" id="search-close">X</a>
			</form>
            <div class="header__actions">
                <a class="search-btn" id="search-open" href="#">
                    <img alt="" src="{{ asset('front/img/search.png') }}" style="height: 25px;">
                </a>

                @if(auth()->check())

                <form id="logout" action="{{ url('logout') }}" method="POST" style="display: inline-block;">
                    @csrf()
                </form>
                    <div class="ps-dropdown">
                        <a href="#">Hi, {{ auth()->user()->firstname }}</a>
                        <ul class="ps-dropdown-menu">
                            <li>
                                <a href="{{url('profile')}}">Profile</a>
                            </li>
                            <li>
                                <a href="#" onclick="event.preventDefault(); document.getElementById('logout').submit();">Logout</a>
                            </li>
                        </ul>
                    </div>

                @else

                    <div class="ps-dropdown">
                        <a href="#"><img alt="" src="{{ asset('front/img/user.png') }}" style="height: 25px;"></i></a>
                        <ul class="ps-dropdown-menu">
                            <li>
                                <a href="{{ url('login') }}">Login</a>
                            </li>
                            <li>
                                <a href="{{ url('register') }}">Register</a>
                            </li>
                        </ul>
                    </div>

                @endif

                <a class="ps-wishlist-toggle" href="{{ url('wishlist') }}">
                    <!-- <i class="pe-7s-like"></i> -->
                    <img alt="" src="{{ asset('front/img/love.png') }}" style="height: 25px;">
                    @if(auth()->check())
                        <span><i>{{ auth()->user()->wishlist()->count() > 0? auth()->user()->wishlist()->count() : 0 }}</i></span>
                    @endif
                </a>

                <a class="ps-cart-toggle" href="#">
                    <!-- <i class="pe-7s-shopbag"></i> -->
                    <img alt="" src="{{ asset('front/img/whislist.png') }}" style="height: 25px;">
                    <span><i>{{ session()->has('S_iP')? session()->get('S_iP')->totalQty : 0 }}</i></span>
                </a>

            </div>
        </div>
    </div>
</header>

<header class="header header--mobile" data-sticky="true">
    <div class="header__top">
        <div class="header__left">
            <p>
                <span>
                    <!-- <i class="pe-7s-call"></i>  -->
                    <img alt="" src="{{ asset('front/img/phone.png') }}" style="height: 16px;">
                    {{ $companies->phone }}
                </span>
                <span>
                    <!-- <i class="pe-7s-mail"></i> -->
                    <img alt="" src="{{ asset('front/img/email.png') }}" style="height: 16px;">
                    <a href="mailto:{{ $companies->email }}" target="_top">{{ $companies->email }}</a>
                </span>
            </p>
        </div>
        <div class="header__center">
            <p style="color: #de4d4d;">{{ $companies->mini_notification_text }}</p>
        </div>
        <div class="header__right">
        </div>
    </div>
    <div class="navigation--mobile">
        <div class="navigation__left">
            <div class="menu-toggle">
                <span></span>
            </div>
        </div>
        <div class="navigation__center">
            <a class="ps-logo" href="{{ url('/') }}"><img alt="" src="{{ asset('front/img/logo.png') }}"></a>
        </div>
        <div class="navigation__right">
            <div class="header__actions">
                <a class="search-btn" href="#"><img alt="" src="{{ asset('front/img/search.png') }}" style="height: 25px;"></i></a>
                <div class="ps-dropdown">
                    <a href="#"><img alt="" src="{{ asset('front/img/user.png') }}" style="height: 25px;"></i></a>
                    <ul class="ps-dropdown-menu">
                        <li>
                            <a href="{{ url('login') }}">Login</a>
                        </li>
                        <li>
                            <a href="{{ url('register') }}">Register</a>
                        </li>
                    </ul>
                </div>

                <a class="ps-wishlist-toggle" href="{{ url('wishlist') }}">
                    <!-- <i class="pe-7s-like"></i> -->
                    <img alt="" src="{{ asset('front/img/love.png') }}" style="height: 25px;">
                    @if(auth()->check())
                        <span><i>{{ auth()->user()->wishlist()->count() > 0? auth()->user()->wishlist()->count() : 0 }}</i></span>
                    @endif
                </a>

                <a class="ps-cart-toggle" href="#">
                    <!-- <i class="pe-7s-shopbag"></i> -->
                    <img alt="" src="{{ asset('front/img/whislist.png') }}" style="height: 25px;">
                    <span><i>{{ session()->has('S_iP')? session()->get('S_iP')->totalQty : 0 }}</i></span>
                </a>
            </div>
        </div>
    </div>
</header>


<div class="navigation--sidebar">
      <div class="navigation__header">
        <h3>Menu</h3><a class="ps-btn--close ps-btn--no-boder" href="#"></a>
      </div>
      <div class="navigation__content">
        <div class="navigation__actions">
          <div class="header__actions"><a class="ps-search-btn" href="#"><img alt="" src="{{ asset('front/img/search.png') }}" style="height: 25px;"></a>


			@if(auth()->check())
				<div class="ps-dropdown">
					<a href="#">Hi, {{ auth()->user()->firstname }}</a>
					<ul class="ps-dropdown-menu">
						<li>
							<form id="logout" action="{{ url('logout') }}" method="POST" style="display: inline-block;">
								@csrf()
							</form>
							<a href="#" onclick="event.preventDefault(); document.getElementById('logout').submit();">Logout</a>
						</li>
					</ul>
				</div>

			@else

				<div class="ps-dropdown">
					<a href="#"><img alt="" src="{{ asset('front/img/user.png') }}" style="height: 25px;"></i></a>
					<ul class="ps-dropdown-menu">
						<li>
							<a href="{{ url('login') }}">Login</a>
						</li>
						<li>
							<a href="{{ url('register') }}">Register</a>
						</li>
					</ul>
				</div>

			@endif

			<a class="ps-wishlist-toggle" href="{{ url('wishlist') }}">
				<!-- <i class="pe-7s-like"></i> -->
				<img alt="" src="{{ asset('front/img/love.png') }}" style="height: 25px;">
				@if(auth()->check())
					<span><i>{{ auth()->user()->wishlist()->count() > 0? auth()->user()->wishlist()->count() : 0 }}</i></span>
				@endif
			</a>
		  </div>


        </div>
        <ul class="menu--mobile">
          <li>
				<a href="{{ url('tracking-order') }}">Tracking Order</a>
          </li>
          <li>
				<a href="{{ url('confirmation-payment') }}">Confirmation Payment</a>
		  </li>
		  <li class="menu-item-has-children">

			  <a  href="#">Product</a><span class="sub-toggle"></span>
				<ul class="sub-menu">
				<li><a id="all-products" href="{{ url('shop') }}">All Products</a></li>

				@foreach($categories as $category)
					<li>
						<a href="{{ url('shop') }}?categories={{$category->slug}}">{{ $category->title }}</a>
					</li>
				@endforeach
				</ul>
		  </li>

          <li>
				<a href="{{ url('contact') }}">Contact</a>
		  </li>
        </ul>
      </div>
	</div>
@if(session()->has('order-cart'))
<div id="order-cart-message" class="alert alert-success top-messages" style="text-align: center">{{ session()->get('order-cart') }}</div>
@endif

@if(session()->has('order-alert'))
<div id="order-cart-alert" class="alert alert-danger top-messages" style="text-align: center">{{ session()->get('order-alert') }}</div>
@endif
