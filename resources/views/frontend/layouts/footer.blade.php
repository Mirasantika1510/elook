<style>
    .icon-information {
        left: 0;
        position: absolute;
    }
</style>

<footer class="ps-footer">
    <div class="ps-footer__content">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12">
                    <div class="ps-site-info">
                        <a class="ps-logo" href="#"><img alt="" src="{{ asset('front/img/logo.png') }}"></a>
                        <figure>
                            <p>
                                <!-- <i class="pe-7s-map-marker"></i>  -->
                                <img class="icon-information" alt="" src="{{ asset('front/img/pin.png') }}" style="height: 25px;">
                                {!! $companies->address !!}
                            </p>
                            <p>
                                <!-- <i class="pe-7s-mail"></i> -->
                                <img class="icon-information" alt="" src="{{ asset('front/img/email.png') }}" style="height: 25px;">
                                <a href="mailto:{{ $companies->email }}">{{ $companies->email }}</a>
                            </p>
                            <p>
                                <!-- <i class="pe-7s-call"></i> -->
                                <img class="icon-information" alt="" src="{{ asset('front/img/phone.png') }}" style="height: 25px;">
                                {{ $companies->phone }}
                            </p>
                        </figure>
                        <ul class="ps-list--social">
                            @foreach($socials as $social)
                                <li>
                                    <a href="{{ $social->link_url }}"><i class="fa fa-{{ $social->vendor }}"></i></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="widget widget_footer">
                                <h3 class="widget-title">Infomation</h3>
                                <ul>
                                    @foreach($custom_pages as $custom_page)
                                        <li>
                                            <a href="{{ url($custom_page->url_link) }}">{{ $custom_page->title }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="widget widget_footer">
                                <h3 class="widget-title">Quick Links</h3>
                                <ul>
                                    <li>
                                        <a href="#">My Account</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('tracking-order') }}">Orders Tracking</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('contact') }}">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12">

                    <!--@if(session()->has('error-subscribe') && $errors->any())-->

                    <!--    <div class="alert alert-danger">-->
                    <!--        <ul style="margin-bottom: 0px;list-style: none; padding-left: 0px;">-->
                    <!--            @foreach ($errors->all() as $error)-->
                    <!--                <li>{{ $error }}</li>-->
                    <!--            @endforeach-->
                    <!--        </ul>-->
                    <!--    </div>-->

                    <!--@endif-->

                    <!--<form id="subscribe" action="{{ url('subscribe') }}" class="ps-form--subscribe-footer" method="post">-->
                    <!--    @csrf()-->
                    <!--    <p>Subscribe to our newsletter and get 10% off your first purchase</p>-->
                    <!--    <div class="form-group">-->
                    <!--        <input class="form-control" placeholder="Your EmaiL Address" name="email" type="email" value="email">-->

                    <!--        <img onClick="event.preventDefault(); document.getElementById('subscribe').submit();"  src="{{ asset('front/img/subscribe.png') }}" style="width:150px;cursor:pointer;"  />-->
                    <!--    </div>-->
                    <!--    <img alt="" src="{{ asset('front/img/payment2.png') }}">-->
                    <!--</form>-->
                </div>
            </div>
        </div>
    </div>
    <div class="ps-footer__copyright">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <p>Copyright © 2019 <a href="#">Elook</a> all rights reserved. Powered by <a href="https://sobat-teknologi.com">Sobat Teknologi</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
