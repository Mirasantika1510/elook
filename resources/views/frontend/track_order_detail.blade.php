@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ $companies->tracking_order_banner_image }}">
        <div class="container">
            <h1>{{ $companies->tracking_order_title_page }}</h1>
        </div>
    </div>
    <div class="ps-page ps-page--blog">
        <div class="ps-blog">
            <div class="container">
                <div class="row">
                    <div class="container mt-5 mb-5">
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <h4>Tracking Order ( id : {{ $order->invoice_number }} )</h4>
                                <ul class="timeline">
                                    @foreach($order->trackings as $tracking)
                                        <li>
                                            @if($tracking->title == 'Waiting Payment' && $order->payment->transaction_status === 'pending')
                                                <a href="{{ url('transaction-status', $order->transaction_id) }}"><font>{{ $tracking->title }}</font></a>
                                            @else
                                                <font>{{ $tracking->title }}</font>
                                            @endif
                                            <font class="float-right">{{ $tracking->created_at->format('d F, Y') }}</font>
                                            <p>{{ $tracking->description }}</p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
