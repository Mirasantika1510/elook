@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{!! asset($banner->image) !!}">
		<div class="container">
			<h1>{!! $banner->title !!}</h1>
		</div>
	</div>
	<div class="ps-page">
		<div class="ps-page__content">
			<div class="container">
				<div class="ps-shopping-cart">
                    @if(session()->has('S_iP'))

                        <div class="table-responsive">
                            <table class="table ps-tablet ps-table--shopping-cart">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form id="form-checkout" action="" method="POST">
                                        @csrf
                                        @php $total = 0 @endphp
                                        @foreach(session()->get('S_iP')->items as $sessionCart)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('product-detail', $sessionCart['item']->seo_page_slug?: $sessionCart['item']->slug) }}">
                                                        <img alt="" src="{{ $sessionCart['item']->images->first()->imageUrl }}">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ url('product-detail', $sessionCart['item']->seo_page_slug?: $sessionCart['item']->slug) }}">{{ $sessionCart['item']->title }}</a>
                                                </td>
                                                <td>
                                                    @if($sessionCart['item']->publishedDiscount)
                                                        <?php $priceNow = $sessionCart['item']->sell_price - ($sessionCart['item']->publishedDiscount->discount_percent/100 * $sessionCart['item']->sell_price);?>
                                                        <del>Rp. {{ number_format($sessionCart['item']->sell_price, 0, ',', '.') }}</del>
                                                        <span style="display: inline-block;color:red;">Rp. {{ number_format($priceNow, 0, ',', '.') }}</span>
                                                    @else
                                                        Rp. {{ number_format($sessionCart['item']->sell_price, 0, ',', '.') }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="form-group--number">
                                                        <button class="up" data-id="{{ $sessionCart['item']->id }}">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                        <button class="down" data-id="{{ $sessionCart['item']->id }}">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <input class="form-control quantity-box quantity-{{ $sessionCart['item']->id }}" placeholder="1" type="text" value="{{ $sessionCart['qty'] }}" name="items[{{$sessionCart['item']->id}}]">
                                                    </div>
                                                </td>
                                                <td>
                                                    <p>
                                                        @if($sessionCart['item']->publishedDiscount)
                                                            <?php $priceNow = $sessionCart['item']->sell_price - ($sessionCart['item']->publishedDiscount->discount_percent/100 * $sessionCart['item']->sell_price);?>
                                                            <del>Rp. {{ number_format($sessionCart['qty'] * $sessionCart['item']->sell_price, 0, ',', '.') }}</del>
                                                            <span style="display: inline-block;color:red;">Rp. {{ number_format($sessionCart['qty'] * $priceNow, 0, ',', '.') }}</span>
                                                        @php $total += $sessionCart['qty'] * $priceNow @endphp
                                                        @else
                                                        Rp. {{ number_format($sessionCart['qty'] * $sessionCart['item']->sell_price,0, ',','.') }}
                                                        @php $total += $sessionCart['qty'] * $sessionCart['item']->sell_price @endphp
                                                        @endif
                                                    </p>
                                                    <a class="ps-btn--close ps-btn--no-boder" href="{{ url('shop/delete-from-cart', $sessionCart['item']->id) }}"></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    <tr class="coupon">
                                        <td colspan="3">
                                                @csrf()

                                                @if($errors->any())

                                                    <div class="alert" style="color: #f30017;">
                                                        <ul style="margin-bottom: 0px;list-style:none;padding-left:0px;">
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>

                                                @endif

                                                @if(session()->has('voucher-added'))

                                                    <div class="alert" style="color: #24cc19;">
                                                        {{ session()->get('voucher-added') }}
                                                    </div>
                                                @endif

                                                <div class="form-group--inline">
                                                    <label>Coupon</label>
                                                    <div class="form-group__content" style="display: flex;align-items: center;">
                                                        <input class="form-control {{ $errors->has('voucher_code')? 'border-danger' : '' }}" placeholder="Coupon Code" type="text" name="voucher_code" value="{{ old('voucher_code') }}">
                                                        <button type="button" data-submit="coupon" data-action="submit" style="background: transparent; border:none;">
                                                            <img src="{{ asset('front/img/apply_coupon.png') }}" alt="" style="max-width: 200px; width: 100%;">
                                                        </button>

                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                        <td colspan="2">
                                            <button type="button" data-submit="cart" data-action="submit" style="background: transparent; border:none;">
                                                <img src="{{ asset('front/img/update_cart.png') }}" alt="">
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <figure class="ps-shopping-cart__total">
                            <figcaption>
                                CART TOTALS
                            </figcaption>
                            <div class="table-responsive">
                                <table class="table ps-table">
                                    <tbody>
                                        <tr>
                                            <td><strong>Subtotal</strong></td>
                                            <!-- <td>Rp. {{ session()->get('S_iP')->discountedPrice }}</td> -->
                                            <!-- <td>Rp. {{ number_format(session()->get('S_iP')->totalPrice, 0, ',', '.') }}</td> -->
                                            <td>Rp. {{ number_format($total, 0, ',', '.') }}</td>
                                        </tr>
                                        @if(session()->get('S_iP')->voucher)
                                            <tr>
                                                <td><strong>Voucher</strong></td>
                                                <td style="color: red;">
                                                    @if(session()->get('S_iP')->voucher->voucher_type == 'percent')
                                                    @php $totalVoucher = (session()->get('S_iP')->voucher->percent/100)*$total @endphp
                                                     - Rp. {{ number_format($totalVoucher, 0, ',', '.') }}
                                                    <br>
                                                    ({{session()->get('S_iP')->voucher->percent}} %)
                                                    @else
                                                     - Rp. {{ number_format(session()->get('S_iP')->finalVoucherNominal, 0, ',', '.') }}
                                                    @endif
                                                    <br>
                                                    Voucher Code : {{session()->get('S_iP')->voucher->voucher_code}}
                                                </td>
                                            </tr>
                                        @endif
                                            <tr>
                                                <td><strong>Total</strong></td>
                                                <!-- <td>Rp. {{ number_format(session()->get('S_iP')->finalPrice, 0, ',', '.') }}</td> -->
                                                @if(session()->get('S_iP')->voucher)
                                                @if(session()->get('S_iP')->voucher->voucher_type == 'percent')
                                                @php $totalVoucher = (session()->get('S_iP')->voucher->percent/100)*$total @endphp
                                                <td>Rp. {{ number_format($total - $totalVoucher, 0, ',', '.') }}</td>
                                                @else
                                                <td>Rp. {{ number_format($total - (session()->get('S_iP')->finalVoucherNominal), 0, ',', '.') }}</td>
                                                @endif
                                                @else
                                                <td>Rp. {{ number_format($total, 0, ',', '.') }}</td>
                                                @endif
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="footer" style="text-align: right;">
                                <a href="{{ url('checkout') }}" style="background: transparent; border:none;">
                                    <img src="{{ asset('front/img/process_checkout.png') }}" alt="">
                                </a>
                            </div>
                        </figure>
                    @else

                        <div style="text-align:center;">

                            <p> NO CART AVAILABLE </p>

                            <a class="ps-btn ps-btn--outline ps-btn--black" href="{{ url('shop') }}">Go To Shop</a>

                        </div>


                    @endif
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
    <script type="text/javascript">

        $(function(){
            $('[data-action="submit"]').on("click",function(e){
             var form = $('#form-checkout');
             var type = $(this).data('submit');
             var action = (type == 'cart') ? "{{ url('update-cart') }}":"{{ url('add-voucher') }}";
             form.attr('action','');
             if(action){
                form.attr('action',action);
                form.submit();
             } else {
                 alert('Error: contact admin');
             }
            });

            @if(session()->has('voucher-added'))
            $('[data-submit="cart"][data-action="submit"]').click();
            @endif

			$('.quantity-box').on('keypress keyup blur', function(event){

				var oldValue = $(this).val();

				$(this).val($(this).val().replace(/[^\d].+/, ""));

            	if((event.which < 48 || event.which > 57)){

            		if (event.which === 8) {

		            	if(event.target.value.length === 1){

			                event.preventDefault();

		            	}else{

		            		var splitValue = event.target.value.split('');

		            		if(splitValue.length >= 1 && parseInt(splitValue[0]) === 0) $(this).val(1)

		            	}


		            }

	                event.preventDefault();

            	}else{

            		var splitValue = event.target.value.split('');

            		if(splitValue.length >= 1 && parseInt(splitValue[0]) === 0) $(this).val(1)

            	}

			});

			$('.quantity-box').on('paste', function(event){

				event.preventDefault();

				var pasteData = event.originalEvent.clipboardData.getData('text');

				if(isNaN(pasteData)) {

					$(this).val(1);

				}else{

					if(pasteData.split('').length && parseInt(pasteData.split('')[0]) === 0) {
						$(this).val(1);
					}

				}

			});

			$('.quantity-box').on('keydown', function(event){

	            if (event.which === 8) {

	            	if(event.target.value.length === 1){

		                event.preventDefault();

	            	}else{

	            		var splitValue = event.target.value.split('');

	            		if(splitValue.length >= 1 && parseInt(splitValue[0]) === 0){
	            			splitValue.splice(0, 1);
	            			$(this).val(splitValue.join(''))
	            		}

	            	}


	            }else{

	            	if(event.which === 46){
	            		event.preventDefault();
	            	}

	            }

            });

            $('.up').on('click', function(event){
                event.preventDefault();

                var id = $(this).attr('data-id');

                var quantityBox = $('.quantity-'+id);

                quantityBox.val(parseInt(quantityBox.val()) + 1);

            });

            $('.down').on('click', function(event){
                event.preventDefault();

                var id = $(this).attr('data-id');

                var quantityBox = $('.quantity-'+id);

                if(quantityBox.val() > 1){

                    quantityBox.val(parseInt(quantityBox.val()) - 1);

                }else {
                    return false;
                }


            });
		})

	</script>
@endpush
