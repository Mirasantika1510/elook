@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ asset('front/img/hero/shop.jpg') }}">
        <div class="container">
            <h1>SIZES GUIDE</h1>
        </div>
    </div>
    <div class="ps-page ps-page--blog">
        <div class="ps-blog">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="ps-block--document-content no-uppercase">
                            <h3>SIZES GUIDE</h3>
                            <div class="table-responsive mb-50">
                                <table class="table ps-table">
                                    <tbody>
                                        <tr>
                                            <td><strong>US</strong></td>
                                            <td><strong>Bust</strong></td>
                                            <td><strong>Waist</strong></td>
                                        </tr>
                                        <tr>
                                            <td>XXXS-0</td>
                                            <td>31%</td>
                                            <td>23%</td>
                                        </tr>
                                        <tr>
                                            <td>XXS-2</td>
                                            <td>32%</td>
                                            <td>24%</td>
                                        </tr>
                                        <tr>
                                            <td>XS-4</td>
                                            <td>34%</td>
                                            <td>26%</td>
                                        </tr>
                                        <tr>
                                            <td>S-6</td>
                                            <td>36%</td>
                                            <td>28%</td>
                                        </tr>
                                        <tr>
                                            <td>M-8</td>
                                            <td>38%</td>
                                            <td>30%</td>
                                        </tr>
                                        <tr>
                                            <td>XL-12</td>
                                            <td>42%</td>
                                            <td>35%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table ps-table">
                                    <tbody>
                                        <tr>
                                            <td><strong>US</strong></td>
                                            <td><strong>Bust</strong></td>
                                            <td><strong>Waist</strong></td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>31%</td>
                                            <td>23%</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>32%</td>
                                            <td>24%</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>34%</td>
                                            <td>26%</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>36%</td>
                                            <td>28%</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>38%</td>
                                            <td>30%</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>42%</td>
                                            <td>35%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <aside class="widget widget_shop widget_instagram">
                            <h3 class="widget-title">Instagram Feed</h3>
                            <ul>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/1.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/2.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/3.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/4.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/5.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/6.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/7.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/8.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/9.jpg') }}"></a>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection