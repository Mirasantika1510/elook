@extends('frontend.layouts.master')

@section('content')
    <div class="ps-page">
		<div class="ps-hero bg--cover" data-background="">
			<div class="container">
				<h1></h1>
				<p></p>
			</div>
		</div>
		<div class="ps-shopping ps-shopping--fullwidth">
			<div class="container-fluid">
				<div class="ps-shopping__content">
					@php $no = $products->count() @endphp
					@for ($i=0; $i < $no; $i+=10)
					<div class="row" id="products-view{{ $i }}">

						@forelse($products->slice($i, 10) as $product)
						<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12">
							<x-product :product="$product" context="shop"></x-product>
						</div>
						@empty
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12" style="text-align: center;">
							NO PRODUCT AVAILABLE
						</div>
						@endforelse

					</div>
					@endfor
					@if($no == 0)
                    <div class="row">
                         <div class="col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12" style="text-align: center;">
                            NO PRODUCT AVAILABLE
                        </div>  
                    </div>
                    @endif
                </div>

                    <div class="ps-shopping__footer">
                        <img src="{{ asset('Group 30.png') }}" class="loadMore btn mx-auto d-block" style="width: 15%;" id="loadMore" onclick="loadMore()">
                    </div>
			</div>
		</div>
	</div>

	<input type="hidden" value="0" id="count"> 
	
	<script>
        $(function () {
            $('#loadMore').removeClass('d-block');
            $('#loadMore').addClass('d-none');
        });

    </script>

	@php $no = $products->count() @endphp
    @for ($i=0; $i < $no; $i+=10)
    @php $button = $i+10 @endphp
    <script>
        $(function () {
            var value = '{{ $button }}';
            $('#products-view{{ $i }}').hide();
            $('#products-view0').show();
            var total = '{{ $products->count() }}';
            if (10 <  parseInt(total)) {
                $('#loadMore').removeClass('d-none');
                $('#loadMore').addClass('d-block');
            }

        });

        function loadMore() {
            var no = $('#count').val();
            var count = parseInt(no) + parseInt(10);
           $('#products-view'+count).show();
           $('#count').val(count);
           var total = '{{ $products->count() }}';
           if (count+10 > total) {
                $('#loadMore').removeClass('d-block');
                $('#loadMore').addClass('d-none');
           }
        }

    </script>
    @endfor

@endsection
