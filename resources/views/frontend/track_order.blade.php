@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ $companies->tracking_order_banner_image }}">
        <div class="container">
            <h1>{{ $companies->tracking_order_title_page }}</h1>
        </div>
    </div>
    <div class="ps-page ps-page--blog">
        <div class="ps-blog">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="ps-document">
                            <p>To track your order please enter your Order ID in the box below and press the "Track" button. This was given to you on your receipt and in the confirmation email you should have received</p>
                            <form action="{{ url('order-information') }}" class="ps-form--orders-tracking" method="post">
                                @csrf()

                                @if($errors->any())

                                    <div class="alert alert-danger">
                                        <ul style="margin-bottom: 0px;">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                
                                @endif
                                
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label>Order ID</label> 
                                            <input class="form-control {{ $errors->has('order_id')? 'border-danger' : '' }}" placeholder="Found in your order information." type="text" name="order_id" value="{{ old('order_id') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group submit">
                                    <input type="submit" class="ps-btn ps-btn--black ps-btn--outline" value="Track">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection