@extends('frontend.layouts.master')

@push('styles')
<style>
        .checkoutpage .card h3 {
            font-size: 14px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
        }

        .checkoutpage .card h2 {
            font-size: 20px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
        }

        .checkoutpage .card .grand-total {
            font-size: 20px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
        }

        .checkoutpage .card .list-group.incoterm .list-group-item.active {
            z-index: 2;
            color: #2196f3;
            background-color: transparent;
            border-color: #2196f3;
        }

        .checkoutpage .card .list-group.incoterm .list-group-item {
            background-color: #f6f6f6;
        }

        .checkoutpage .card .alert-confirm-seller {
            background-color: #ffffff;
            color: #292a33;
            border-radius: 3px;
            border: 1px solid #cccccc;
        }

        .box-countdown {
            border-radius: 3px;
            background-color: #f6f6f6;
            padding: 40px;
        }

        .box-countdown b {
            font-size: 14px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
        }

        .box-countdown p {
            font-size: 13px;
            font-weight: normal;
            font-style: italic;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
        }

        .cdtrans {
            font-size: 48px;
            font-weight: 300;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            display: flex;
            margin: 30px 0;
            color: #292a33;
        }

        .cdtrans span.seperator {
            margin: 0 20px;
        }

        .cdtrans span.day:after {
            content: 'Hari';
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #292a33;
            display: grid;
        }

        .cdtrans span.hour:after {
            content: 'Jam';
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #292a33;
            display: grid;
        }

        .cdtrans span.minute:after {
            content: 'Menit';
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #292a33;
            display: grid;
        }

        .cdtrans span.second:after {
            content: 'Detik';
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #292a33;
            display: grid;
        }

        .instructions-content {
            padding: 30px;
            text-align: justify;
        }

    </style>
@endpush

@section('content')
    <section class='checkoutpage martop'>
        <div class='container'>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='card' style="padding: 3rem!important;">
                        @if((!$order->payment->transaction_status || $order->payment->transaction_status === 'pending') && $now->diffInSeconds($order->created_at) <= 3600)
                            <h2>Menunggu Pembayaran</h2>
                            <p>Silahkan melakukan pembayaran sebelum waktu habis</p>
                            <div class='box-countdown text-center mt-2 bg-time-transaction'>
                                <b>Sisa waktu pembayaran</b>

                                @if($transactionTime->diffInHours($now) >= 1)
                                    <div class="wideCountdownEvergreen  clearfix cdLabelBold wideCountdownSize2 cdYellow cdStyleCircleFill wideCountdown-demo is-countdown styled">
                                        <span class='cdtrans d-flex justify-content-center'>
                                            <span class='hour'>00</span>
                                            <span class='seperator'>:</span>
                                            <span class='minute'>00</span>
                                            <span class='seperator'>:</span>
                                            <span class='second'>00</span>
                                        </span>
                                    </div>
                                @else
                                <div class="wideCountdownEvergreen  clearfix cdLabelBold wideCountdownSize2 cdYellow cdStyleCircleFill wideCountdown-demo is-countdown countdown styled">
                                        <span class='cdtrans d-flex justify-content-center'>
                                            <span class='hour'>{{ $countDown->hour }}</span>
                                            <span class='seperator'>:</span>
                                            <span class='minute'>{{ $countDown->minute }}</span>
                                            <span class='seperator'>:</span>
                                            <span class='second'>{{ $countDown->second }}</span>
                                        </span>
                                    </div>
                                @endif
                                <p class='mt-3'>( Sebelum {{ $endTranscationTime->format('Y-m-d') }} Pukul {{ $endTranscationTime->format('H:i:s') }} )</p>
                            </div>
                            <div class='alert alert-yellow mt-2 text-center' role='alert'>
                                <p>Pastikan transaksi Anda telah terverifikasi sebelum melakukan transaksi kembali dengan metode
                                    yang sama.</p>
                            </div>
                            <p>Order Id : <strong> {{ $order->invoice_number }} </strong></p>
                            <p>Metode Pembayaran : <strong> 'Bank Transfer' </strong></p>
                            <p>Bank : <strong> {{ strtoupper($order->payment->account_bank) }} </strong></p>
                            <p>Nomor Rekening: <strong> {{ $order->payment->account_number }} </strong></p>
                            <p>Jumlah yang harus dibayar : <strong>Rp. {{ number_format($order->total_price + $order->unique_number, 0, ',', '.') }}</p>
                            <p>Instructions :</p>
                            <div class="instructions-content">
                                {!! $order->payment->instructions !!}
                            </div>
                        @else

                            <div class='box-countdown text-center mt-2'>

                                <h2>Order anda dengan id <strong style="color: blue;">{{ $order->invoice_number }}</strong> sudah kadaluarsa</h2>
                                <p>Silahkan order ulang kembali.</p>

                            </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="{{ asset('front/plugins/jquery-countdown.js') }}" type="text/javascript"></script>

    <script>

        $(document).ready(function () {

            /* -------------------------------------------------------

            COUNTDOWN INIT

            https://github.com/rendro/countdown

            ---------------------------------------------------------- */
            var timerSeconds = 86400;
            var generateDate = new Date("{{ $endTranscationTime->year }}", "{{ $endTranscationTime->month }}", "{{ $endTranscationTime->day }}", "{{ $endTranscationTime->hour }}", "{{ $endTranscationTime->minute }}", "{{ $endTranscationTime->second }}", "{{ $endTranscationTime->micro }}")
            var endTimeDiff = generateDate.getTime() + timerSeconds * 1000;
            console.log(generateDate)
            $('.countdown.styled').countdown({
                date: generateDate,
                render: function(data) {
                    $('.hour').html( this.leadingZeros(data.hours, 2));
                    $('.second').html( this.leadingZeros(data.sec, 2));
                    $('.minute').html( this.leadingZeros(data.min, 2));
                },
                onEnd: function () {
                    window.location.reload();
                }
            });
        });

    </script>
@endpush
