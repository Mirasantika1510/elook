@extends('frontend.layouts.master')

@push('styles')
    <link href="{{ asset('front/plugins/owl-carousel/assets/owl.carousel.css') }}" rel="stylesheet">
    <style>
        .tiles {
	       position: absolute;
	       top: 0;
	       left: 0;
	       width: 100%;
	       height: 100%;
	    }

	    .tile {
	       position: relative;
	       float: left;
	       width: 33.333%;
	       height: 100%;
	       overflow: hidden;
	    }

	    .photo {
	       position: absolute;
	       top: 0;
	       left: 0;
	       width: 100%;
	       height: 100%;
	       background-repeat: no-repeat;
	       background-position: center;
	       background-size: cover;
	       transition: transform .5s ease-out;
	    }

        .section-add-to-cart {
            display: inline-block;
        }

        @media only screen and (max-width: 768px) {
            .section-add-to-cart {
                display: none;
            }
        }


    </style>
@endpush

@push('open_graph')
	<meta property="og:url"           content="{{ url()->current() }}" />
	<meta property="og:type"          content="article" />
	<meta property="og:title"         content="{{ $product->seo_page_title }}" />
	<meta property="og:description"   content="{{ $product->seo_meta_description }}" />
	<meta property="og:image"         content="{{ $product->images->first()->imageUrl }}" />

	<meta name="twitter:card"           content="summary" />
	<meta name="twitter:site"           content="@elook" />
	<meta name="twitter:title"          content="{{ $product->seo_page_title }}" />
	<meta name="twitter:description"    content="{{ $product->seo_meta_description }}" />
	<meta name="twitter:image"          content="{{ $product->images->first()->imageUrl }}" />

	<meta name="description" content="{{ $product->seo_meta_description }}">
	<meta name="keywords"   content="{{ $product->seo_meta_keywords }}">
@endpush

@section('content')
    <div class="ps-breadcrumb">
		<div class="container-fluid">
			{{-- <ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="shop-4-column.html">Women</a>
				</li>
				<li>Cluse La Bohème Rose Gold</li>
			</ul> --}}
		</div>
	</div>

	<div class="container">

		<div class="ps-product--detail">
			<div class="ps-product__header">
				<div class="ps-product__thumbnail" data-vertical="true">
					<figure>
						<div class="ps-wrapper">
							@if($product->publishedDiscount)
								<span class="ps-product__badge"><i>{{ $product->publishedDiscount->discount_percent }}%</i></span>
							@endif
							<div class="ps-product__gallery tiles" data-arrow="true">
                                @foreach($product->images as $image)
                                    <div class="item tile" data-image="{{ $image->imageLargeUrl }}" data-scale="2.4">
                                        <a href="{{ $image->imageLargeUrl }}"><img alt="" src="{{ $image->imageLargeUrl }}"></a>
                                    </div>
                                @endforeach
							</div>
						</div>
					</figure>
					<div class="ps-product__variants" data-arrow="false" data-item="{{$product->images->count()}}" data-md="3" data-sm="3">
                        @foreach($product->images as $image)
                            <div class="item"><img alt="" src="{{ $image->imageUrl }}"></div>
                        @endforeach
					</div>
				</div>

				<div class="ps-product__info">
					<h1>{{ $product->title }}</h1>

					<div class="br-wrapper br-theme-fontawesome-stars">
						<div class="br-widget">
							@foreach(range(1,5) as $star)
                                <a href="#"
                                    data-rating-value="1"
                                    data-rating-text="{{ $star }}"
                                    class="{{ $star <= floor($product->reviews->avg('rate'))? 'br-selected br-current' : '' }}"
                                ></a>
							@endforeach
						</div>
					</div>

					@if($product->publishedDiscount)
						<?php $priceNow = $product->sell_price - ($product->publishedDiscount->discount_percent/100 * $product->sell_price);?>
						<h4 class="ps-product__price" style="display: inline-block;">
							<del>Rp. {{ number_format($product->sell_price, 2, ',', '.') }}</del>
						</h4>
						<h4 class="ps-product__price sale" style="display: inline-block;">Rp. {{ number_format($priceNow, 2, ',', '.') }}</h4>
					@else
						<h4 class="ps-product__price">Rp. {{ number_format($product->sell_price, 2, ',', '.') }}</h4>
					@endif

					@if(!$product->ignore_stock && ($product->stock <= $product->stock_used))
						<span class="ps-product__badge disabled" style="position: relative;top: auto;right: auto;"><i>Sold Out</i></span>
					@endif
					<div class="ps-product__desc">
						<p>{!! $product->specification !!}</p>
					</div>
					<div class="ps-product__variations">
						{{-- <figure>
							<figcaption>
								Color
							</figcaption>
							<div class="ps-variant ps-variant--image active"><span class="ps-variant__tooltip">Blue</span><img alt="" src="img/product-detail/bundle/1.jpg"></div>
							<div class="ps-variant ps-variant--image"><span class="ps-variant__tooltip">Cyan</span><img alt="" src="img/product-detail/bundle/2.jpg"></div>
						</figure> --}}
					</div>
					<div class="ps-product__shopping" style="display: flex;align-items: center;">

                        <form style="display: inline-block;" id="form-add" action="{{ url('shop/add-to-cart', $product->id) }}" method="POST">{{ csrf_field() }}
                            <div class="form-group--number">
                                <button class="up">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="down">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <input class="form-control quantity-box" value="1" type="text" name="quantity">
                            </div>
                        </form>

                        <div class="section-add-to-cart">

                            @if($product->ignore_stock || (!$product->ignore_stock && ($product->stock > $product->stock_used)))

                                <a href="javacsript:;"
                                    onClick="event.preventDefault(); document.getElementById('form-add').submit()"
                                    {{ ($product->stock - $product->stock_used) <= 0? 'disabled' : '' }}
                                >
                                    <img src="{{ asset('front/img/add_to_cart.png') }}" class="ps-btn-add-to-cart"  style="width:245px;">
                                </a>


                            @else

                                <a href="javacsript:;"
                                    {{ ($product->stock - $product->stock_used) <= 0? 'disabled' : '' }}
                                    style="cursor: not-allowed;"
                                >
                                    <img src="{{ asset('front/img/add_to_cart.png') }}" class="ps-btn-add-to-cart"  style="width:245px;">
                                </a>

                            @endif

                        </div>

                        <a class="ps-product__favorite" href="{{ url('add-wishlist', $product->id) }}">
                            <i class="fa fa-heart-o"></i>
                        </a>
					</div>
					<div class="ps-product__specification">
                        @php
                            $categories = $product->categories->map(function($category){
                                return '<a href="javascript:;">'.$category->title.'</a>';
                            });
                            $tags = $product->tags->map(function($tag){
                                return '<a href="javascript:;">'.$tag->title.'</a>';
                            });
                        @endphp

						<p>
                            <strong>Categories:</strong>
                            @if($categories->count() > 0)
                                {!! $categories->join(', ') !!}
                            @else
                                ---
                            @endif
                        </p>
						<p>
                            <strong>Tags:</strong>

                            @if($tags->count() > 0)
                                {!! $tags->join(', ') !!}
                            @else
                                ---
                            @endif
                        </p>
					</div>
					<div class="ps-product__accordions">
						<div class="ps-accordion">
							<div class="ps-accordion__header">
								<p>Description</p><span><i class="fa fa-plus"></i></span>
							</div>
							<div class="ps-accordion__content">
								<p>{!! $product->description?: '---' !!}</p>
							</div>
						</div>
						<div class="ps-accordion">
							<div class="ps-accordion__header">
								<p>Addition Information</p><span><i class="fa fa-plus"></i></span>
							</div>
							<div class="ps-accordion__content">
								{!! $product->additional_information?: '---' !!}
							</div>
						</div>
						<div class="ps-accordion">
							<div class="ps-accordion__header">
								<p>Reviews</p><span><i class="fa fa-plus"></i></span>
							</div>
							<div class="ps-accordion__content">
								@forelse($product->reviews as $review)
								<div class="ps-block--comment ps-block--comment-review">
									<div class="ps-block__thumbnail">
                                        <img alt="" src="{{ asset('front/img/user/1.jpg') }}">
                                    </div>
									<div class="ps-block__content">
										<h5>{{ $review->customer->firstname. ' ' .$review->customer->lastname }}</h5>
										<div class="br-wrapper br-theme-fontawesome-stars">
											<div class="br-widget">
												@foreach(range(1,5) as $star)
													<a href="#" data-rating-value="1" data-rating-text="{{ $star }}" class="{{ $star <= $review->rate? 'br-selected br-current' : '' }}"></a>
												@endforeach
											</div>
										</div>
										<p>{!! $review->comment !!}</p>
									</div>
								</div>
								@empty
									---
								@endforelse
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="add-cart-fixed d-md-block d-lg-none" style="
        position: fixed;
        bottom: 0%;
        width:100%;
        padding:20px 10px 20px 10px;
        background:#fff;
        z-index:1;
	">
		<div style="width:80%">
            @if($product->ignore_stock || (!$product->ignore_stock && ($product->stock > $product->stock_used)))

                <a href="javacsript:;"
                    class="ps-btn ps-btn--black  btn-block text-center"
                    style="
                    background: #b0ddd1;
                    color: #e9929a;"
                    onClick="event.preventDefault(); document.getElementById('form-add').submit()"
                    {{ ($product->stock - $product->stock_used) <= 0? 'disabled' : '' }}
                >
                    <img alt="" src="{{ asset('front/img/whislist.png') }}" style="height: 25px;">
                    Add To Cart
                </a>

            @else

                <a href="javacsript:;"
                    class="ps-btn ps-btn--black ps-btn--outline"
                    {{ ($product->stock - $product->stock_used) <= 0? 'disabled' : '' }}
                    style="cursor: not-allowed;"
                >
                    <img alt="" src="{{ asset('front/img/whislist.png') }}" style="height: 25px;">
                    Add To Cart
                </a>

            @endif

		</div>
	</div>
	<div class="ps-section ps-related-products">
		<div class="container">
			<div class="ps-section__header">
				<img
					src="{{ asset('front/img/related-products.png') }}"
					style="height: 75px;"
				>
			</div>
			<div class="ps-section__content">
                @if($products_related->count() === 1)
                    <div class="row">
                        @foreach($products_related as $related)
                            <x-product :product="$related" context="related" class="col-md-3 col-lg-3 col-xs-1" />
                        @endforeach
                    </div>
                @else
                    <div class="ps-carousel--nav owl-slider"
                        data-owl-auto="true"
                        data-owl-dots="true"
                        data-owl-duration="1000"
                        data-owl-gap="30"
                        data-owl-item="4"
                        data-owl-item-lg="4"
                        data-owl-item-md="3"
                        data-owl-item-sm="2"
                        data-owl-item-xs="1"
                        data-owl-loop="true"
                        data-owl-mousedrag="on"
                        data-owl-nav="true"
                        data-owl-speed="5000"
                    >
                        @foreach($products_related as $related)
                            <x-product :product="$related" context="related"/>
                        @endforeach
                    </div>
                @endif
			</div>
		</div>
	</div>
@endsection

@push('scripts')
    <script type="text/javascript">
	    $('.tile')
        // tile mouse actions
	    .on('mouseover', function(){
	       $(this).children('.photo').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
	    })
	    .on('mouseout', function(){
	       $(this).children('.photo').css({'transform': 'scale(1)'});
	    })
	    .on('mousemove', function(e){
	       $(this).children('.photo').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
	    })
	    // tiles set up
	    .each(function(){
	       $(this)
	        // add a photo container
	        .append('<div class="photo"><\/div>')
	        // some text just to show zoom level on current item in this example
	        // .append('<div class="txt"><div class="x">'+ $(this).attr('data-scale') +'x<\/div>ZOOM ON<br>HOVER<\/div>')
	        // set up a background image for each tile based on data-image attribute
	        .children('.photo').css({'background-image': 'url('+ $(this).attr('data-image') +')'});
        })

        $(function(){
			$('.quantity-box').on('keypress keyup blur', function(event){

				var oldValue = $(this).val();

				$(this).val($(this).val().replace(/[^\d].+/, ""));

            	if((event.which < 48 || event.which > 57)){

            		if (event.which === 8) {

		            	if(event.target.value.length === 1){

			                event.preventDefault();

		            	}else{

		            		var splitValue = event.target.value.split('');

		            		if(splitValue.length >= 1 && parseInt(splitValue[0]) === 0) $(this).val(1)

		            	}


		            }

	                event.preventDefault();

            	}else{

            		var splitValue = event.target.value.split('');

            		if(splitValue.length >= 1 && parseInt(splitValue[0]) === 0) $(this).val(1)

            	}

			});

			$('.quantity-box').on('paste', function(event){

				event.preventDefault();

				var pasteData = event.originalEvent.clipboardData.getData('text');

				if(isNaN(pasteData)) {

					$(this).val(1);

				}else{

					if(pasteData.split('').length && parseInt(pasteData.split('')[0]) === 0) {
						$(this).val(1);
					}

				}

			});

			$('.quantity-box').on('keydown', function(event){

	            if (event.which === 8) {

	            	if(event.target.value.length === 1){

		                event.preventDefault();

	            	}else{

	            		var splitValue = event.target.value.split('');

	            		if(splitValue.length >= 1 && parseInt(splitValue[0]) === 0){
	            			splitValue.splice(0, 1);
	            			$(this).val(splitValue.join(''))
	            		}

	            	}


	            }else{

	            	if(event.which === 46){
	            		event.preventDefault();
	            	}

	            }

            });

            $('.up').on('click', function(event){
                event.preventDefault();

                var quantityBox = $('.quantity-box');

                quantityBox.val(parseInt(quantityBox.val()) + 1);

            });

            $('.down').on('click', function(event){
                event.preventDefault();

                var quantityBox = $('.quantity-box');

                if(quantityBox.val() > 1){

                    quantityBox.val(parseInt(quantityBox.val()) - 1);

                }else {
                    return false;
                }


            });
		})

	</script>
@endpush
