@extends('frontend.layouts.master')

@push('style')
	<style>
		.border-danger {
			border: 1px solid red !important;
		}
	</style>
@endpush

@section('content')

	<div class="ps-hero bg--cover" data-background="{!! asset($banner->image) !!}">
		<div class="container">
			<h1>{{ $banner->title }}</h1>
		</div>
	</div>
	<div class="ps-page ps-page--blog">
		<div class="ps-my-account">
			<div class="container">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                        <form action="{{ url('register') }}" class="ps-form--account" method="post">
                            @csrf()
                            <h3>Register</h3>

                            @if($errors->any())

                                <div class="alert alert-danger">
                                    <ul style="margin-bottom: 0px;">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>

                            @endif

							<div class="form-group">
								<label>First Name <sup>*</sup></label>
								<input class="form-control {{ $errors->has('firstname')? 'border-danger' : '' }}" placeholder="Input your first name here" name="firstname" type="text" value="{{ old('firstname') }}">
                            </div>
                            <div class="form-group">
								<label>Last Name <sup>*</sup></label>
								<input class="form-control {{ $errors->has('lastname')? 'border-danger' : '' }}" placeholder="Input your last name here" name="lastname" type="text" value="{{ old('lastname') }}">
							</div>
							<div class="form-group">
								<label>Email address <sup>*</sup></label>
								<input class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" placeholder="Input your email here" name="email" type="email" value="{{ old('email') }}">
							</div>
                            <div class="form-group">
                                <label>Date Of Birth <sup>*</sup></label>
                                <input class="form-control {{ $errors->has('dob')? 'border-danger' : '' }}" placeholder="Input your date of birth here" name="dob" type="date" value="{{ old('dob') }}">
                            </div>
							<div class="form-group">
								<label>Password <sup>*</sup></label>
								<input class="form-control {{ $errors->has('password')? 'border-danger' : '' }}" placeholder="Input your password here" name="password" type="password">
							</div>

							<div class="form-group">
								<label>Re-type Password <sup>*</sup></label>
								<input class="form-control {{ $errors->has('password_confirmation')? 'border-danger' : '' }}" placeholder="Input your password confirmation here" name="password_confirmation" type="password">
							</div>
							<div class="form-group submit">
								<div class="row">
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
										<button class="ps-btn ps-btn--black ps-btn--outline">Submit</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
