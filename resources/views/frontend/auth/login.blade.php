@extends('frontend.layouts.master')

@push('style')
	<style>
		.border-danger {
			border: 1px solid red !important;
		}
	</style>
@endpush

@section('content')

	<div class="ps-hero bg--cover" data-background="{!! asset($banner->image) !!}">
		<div class="container">
			<h1>{{ $banner->title }}</h1>
		</div>
	</div>
	<div class="ps-page ps-page--blog">
		<div class="ps-my-account">
			<div class="container">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
						<form action="{{ url('login') }}" class="ps-form--account" method="post">
							@csrf()
							<h3>Login</h3>

							@if($errors->any())

                                <div class="alert alert-danger">
                                    <ul style="margin-bottom: 0px;">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>

							@endif

							<div class="form-group">
								<label>Email address <sup>*</sup></label>
								<input class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" placeholder="" name="email" type="email">
							</div>
							<div class="form-group">
								<label>Password <sup>*</sup></label>
								<input class="form-control {{ $errors->has('password')? 'border-danger' : '' }}" placeholder="" name="password" type="password">
							</div>
							<div class="form-group submit">
								<div class="row">
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="d-flex flex-column align-items-start">
                                            <div class="ps-checkbox">
                                                <input class="form-control" id="remember" name="remember" type="checkbox"> <label for="remember">Remember me</label>
                                            </div>
                                            <div class="mt-2">
                                                No Account ?
                                                <a href="{{ url('register') }}"
                                                    style="color: #70cab2;text-decoration: underline;"
                                                >
                                                    click here
                                                </a>
                                                to register
                                            </div>
                                        </div>
									</div>
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
										<button class="ps-btn-login">
										<img
											src="{{ asset('front/img/btn_login.png') }}"
											style="height:75px"
										>
										</button>
									</div>
								</div>
							</div>
							{{-- <div class="form-group footer">
								<a href="#">Lost your password?</a>
							</div> --}}
						</form>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="justify-content: center; align-items: center; display: flex;">
						<a href="{{ url('guest-checkout') }}">
							<img
								src="{{ asset('front/img/btn_as_guest.png') }}"
								style="height:75px"
							>
                        </a>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')
	<script>
		$(function(){
			var fragmentUrl = window.location.hash;
			console.log(fragmentUrl)
			if(fragmentUrl){

				if(fragmentUrl === '#login'){

					$('#list-register').removeClass('active');
					$('#register').removeClass('in active');

					$('#list-login').addClass('active');
					$('#login').addClass('in active');

				}else if(fragmentUrl === '#register'){

					$('#list-login').removeClass('active');
					$('#login').removeClass('in active');

					$('#list-register').addClass('active');
					$('#register').addClass('in active');

				}

			}
		});
	</script>
@endpush
