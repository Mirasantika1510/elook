@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ asset('front/img/hero/shop.jpg') }}">
        <div class="container">
            <h1>ABOUT US</h1>
            <p>Follow your passion, and sucess will follow you</p>
        </div>
    </div>
    <div class="ps-page ps-page--blog">
        <div class="ps-blog">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="ps-about-us ps-document">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="ps-block--document-content">
                                        <h3>OUR MISSION</h3>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, <i>totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae</i> vitae dicta sunt explicabo nemo enim ipsam.</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="ps-block--document-content">
                                        <h3>Our Stories</h3>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="ps-block--document-content">
                                        <h3>OUR APPROACH</h3>
                                        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="ps-block--document-content">
                                        <h3>OUR PHILOSOPHY</h3>
                                        <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Quis nostrum exercitationem ullam.</p>
                                    </div>
                                </div>
                            </div>
                            <blockquote>
                                <p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Quis nostrum exercitationem ullam.</p>
                            </blockquote>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <div class="ps-block--member">
                                        <div class="ps-block__container">
                                            <ul class="ps-list--social">
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="ps-block__thumbnail"><img alt="" src="{{ asset('front/img/member/1.jpg') }}"></div>
                                        <div class="ps-block__content">
                                            <h4>Lisa John</h4>
                                            <p>Fashion Design</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <div class="ps-block--member">
                                        <div class="ps-block__container">
                                            <ul class="ps-list--social">
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="ps-block__thumbnail"><img alt="" src="{{ asset('front/img/member/2.jpg') }}"></div>
                                        <div class="ps-block__content">
                                            <h4>Jane Doe</h4>
                                            <p>Director</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <div class="ps-block--member">
                                        <div class="ps-block__container">
                                            <ul class="ps-list--social">
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="ps-block__thumbnail"><img alt="" src="{{ asset('front/img/member/3.jpg') }}"></div>
                                        <div class="ps-block__content">
                                            <h4>Cartherin Forres</h4>
                                            <p>Makerting Director</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <aside class="widget widget_shop widget_instagram">
                            <h3 class="widget-title">Instagram Feed</h3>
                            <ul>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/1.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/2.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/3.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/4.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/5.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/6.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/7.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/8.jpg') }}"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" src="{{ asset('front/img/blog/instagram/9.jpg') }}"></a>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection