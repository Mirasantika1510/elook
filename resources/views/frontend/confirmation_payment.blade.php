@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ $companies->confirmation_payment_banner_image }}">
        <div class="container">
            <h1>{{ $companies->confirmation_payment_title_page }}</h1>
        </div>
    </div>
    <div class="ps-page ps-page--blog">
        <div class="ps-blog">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="ps-document">
                            <p>To track your order please enter your Order ID in the box below and press the "Track" button. This was given to you on your receipt and in the confirmation email you should have received</p>

                            @if(session()->has('confirmed-payment'))

                                <div class="alert alert-success">
                                    {{ session()->get('confirmed-payment') }}
                                </div>

                            @endif

                            @if($errors->any())

                                <div class="alert alert-danger">
                                    <ul style="margin-bottom: 0px;">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>

                            @endif

                            <form action="{{ url('confirmation-payment') }}" id="orders_tracking" class="ps-form--orders-tracking" method="post" enctype="multipart/form-data">
                                @csrf()
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" placeholder="Found in your order information email." type="email" name="email" value="{{ old('email') }}"><br>
                                        </div>

                                        <div class="form-group">
                                            <label>ID pembelian</label>
                                            <input class="form-control {{ $errors->has('order_id')? 'border-danger' : '' }}" placeholder="Found in your order information ID." type="text" name="order_id" value="{{ old('order_id') }}"><br>
                                        </div>

                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input class="form-control {{ $errors->has('name')? 'border-danger' : '' }}" placeholder="Found in your order information Nama." type="text" name="name" value="{{ old('name') }}"><br>
                                        </div>

                                        <div class="form-group">
                                            <label>No rekening</label>
                                            <input class="form-control {{ $errors->has('account_number')? 'border-danger' : '' }}" placeholder="Found in your order information No rekening." type="text" name="account_number" value="{{ old('account_number') }}"><br>
                                        </div>

                                        <div class="form-group">
                                            <label for="file">Upload file:</label>
                                            <div class="input-group input-file" name="receipt">
                                                <span class="input-group-btn">
                                                    <button class="btn text-light bg-dark btn-choose h-100 rounded-0" type="button">Browse</button>
                                                </span>
                                                <input type="text" class="form-control {{ $errors->has('receipt')? 'border-danger' : '' }}" placeholder='Choose a file...' />
                                            </div>
                                        </div>

                                        <div class="form-group mt-5 submit">
                                            <button type="submit" class="ps-btn ps-btn--outline ps-btn--black">Submit</button>
                                        </div>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function bs_input_file() {
            $(".input-file").before(
                function() {
                    if ( ! $(this).prev().hasClass('input-ghost') ) {
                        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                        element.attr("name",$(this).attr("name"));
                        element.change(function(){
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function(){
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function(){
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor","pointer");
                        $(this).find('input').mousedown(function() {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
        $(function() {
            bs_input_file();
        });
    </script>
@endpush
