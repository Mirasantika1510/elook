@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{!! asset($banner->image) !!}">
        <div class="container">
            <h1>{{ $banner->title }}</h1>
        </div>
    </div>
    <div class="ps-page">
        <div class="ps-page__content">
            <div class="container">
                <div class="ps-checkout">
                    <div class="row">

                        @if(auth()->guard('web')->check())
                        <form id="form_change_address" action="" method="post">
                            @csrf()
                        </form>
                        @endif
                        <form id="process-checkout" action="{{ url('process-checkout') }}" class="ps-form--checkout" method="post" style="width: 100%;">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    @csrf()
                                    <h3 class="ps-checkout__heading">BILLING DETAILS</h3>

                                    @if($errors->any())

                                        <div class="alert alert-danger">
                                            <ul style="margin-bottom: 0px;">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>

                                    @endif

                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <label>First Name <sup>*</sup></label>
                                                <input class="form-control {{ $errors->has('firstname')? 'border-danger' : '' }}" placeholder="" name="firstname" value="{{ old('firstname')?: (auth()->check()? auth()->user()->firstname : '')  }}" type="text">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <label>Last Name <sup>*</sup></label>
                                                <input class="form-control {{ $errors->has('lastname')? 'border-danger' : '' }}" placeholder="" name="lastname" value="{{ old('lastname')?: (auth()->check()? auth()->user()->lastname : '')  }}" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Email address <sup>*</sup></label>
                                        <input class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" placeholder="" name="email" value="{{ old('email')?: (auth()->check()? auth()->user()->email : '')  }}" type="email">
                                    </div>

                                    @if(auth()->guard('web')->check() && $customer->address)
                                    <div class="form-group">
                                        <label>Address <sup>*</sup></label>
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title">Address</h5>
                                                <h6 class="card-subtitle mb-2 text-muted">Phone: <span data-address="phonenumber">{{$customer->address->phonenumber}}</span> </h6>
                                                <p class="card-text">

                                                    <p> <span data-address="address">{{$customer->address->address}}</span></p>
                                                    <p><b>
                                                    <span data-address="province">{{ $customer->address->getprovince->province }}</span><br>
                                                    <span data-address="city">{{$customer->address->getcity->type}} {{$customer->address->getcity->city_name}}</span><br>
                                                    <span data-address="subdistrict">{{$customer->address->getsubdistrict->subdistrict_name}}</span><br>
                                                    <span data-address="postal_code">{{$customer->address->getcity->postal_code}}</span>
                                                    </b></p>
                                                </p>

                                                <button type="button" class="card-link update-address btn btn-dark change-address">Change Address</button>

                                                <div class="form-group hidden select-address">
                                                    <select class="form-control" name="customer_address_id">
                                                        @foreach($customer->allAddress as $address)
                                                            <option data-subdistrict-id="{{ $address->subdistrict}}" value="{{ $address->id }}" @if($address->active == 1) selected @endif >{{ $address->phonenumber }} / {{ $address->address }} / {{ $address->getprovince->province }} / {{ $address->getcity->type }} {{ $address->getcity->city_name }} / {{ $address->getsubdistrict->subdistrict_name}} / {{ $address->getcity->postal_code }} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group hidden select-address">
                                                    <button type="button" data-address='' class="card-link update-address btn btn-lg btn-dark save-address">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="form-group">
                                        <label>Phone <sup>*</sup></label>
                                        <input class="form-control {{ $errors->has('phonenumber')? 'border-danger' : '' }}" placeholder="" name="phonenumber" value="{{ old('phonenumber')?: (auth()->check()? auth()->user()->phone : '')  }}" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label>Province <sup>*</sup></label>
                                        <select class="form-control {{ $errors->has('province')? 'border-danger' : '' }}" name="province" id="province">
                                            @foreach($provinces as $province)
                                                <option value="{{ $province->province_id }}">{{ $province->province }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>City <sup>*</sup></label>
                                        <select class="form-control {{ $errors->has('city')? 'border-danger' : '' }}" name="city" id="city"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Subdistrict <sup>*</sup></label>
                                        <select class="form-control {{ $errors->has('subdistrict')? 'border-danger' : '' }}" name="subdistrict" id="subdistrict"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Street Address <sup>*</sup></label>
                                        <input class="form-control {{ $errors->has('address')? 'border-danger' : '' }}" placeholder="" type="text" name="address" value="{{ old('address')?: (auth()->check()? auth()->user()->address : '') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Post Code <sup>*</sup></label>
                                        <input class="form-control {{ $errors->has('postcode')? 'border-danger' : '' }}" placeholder="" name="postcode" value="{{ old('postcode')?: (auth()->check()? auth()->user()->phone : '')  }}" type="text">
                                    </div>
                                    @endif


                                    <div class="form-group">
                                        <label>Order notes (optional)</label>
                                        <textarea class="form-control {{ $errors->has('note')? 'border-danger' : '' }}" placeholder="" rows="5" name="note">{{ old('note') }}</textarea>
                                    </div>

                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <h3 class="ps-checkout__heading">YOUR ORDER</h3>
                                    <div class="table-responsive">
                                        <table class="table ps-table ps-table--checkout">
                                            <tbody>
                                                <tr>
                                                    <td><strong>Product</strong></td>
                                                    <td><strong>Total</strong></td>
                                                </tr>
                                                @if(session()->has('S_iP'))
                                                @php $total = 0 @endphp
                                                    @foreach(session()->get('S_iP')->items as $sessionCart)
                                                        <tr>
                                                            <td>
                                                                <a href="#">
                                                                    <img src="{{ $sessionCart['item']->images->first()->imageUrl }}" class="cart-thumb" alt="" />
                                                                </a>
                                                                <p><a href="#">{{ $sessionCart['item']->title }}</a> × {{$sessionCart['qty']}}</p>
                                                            </td>
                                                            <td>
                                                               @if($sessionCart['item']->publishedDiscount)
                                                               <?php $priceNow = $sessionCart['item']->sell_price - ($sessionCart['item']->publishedDiscount->discount_percent/100 * $sessionCart['item']->sell_price);?>
                                                               <del>Rp. {{ number_format($sessionCart['qty'] * $sessionCart['item']->sell_price, 0, ',', '.') }}</del>
                                                               <span style="display: inline-block;color:red;">Rp. {{ number_format($sessionCart['qty'] * $priceNow, 0, ',', '.') }}</span>
                                                               @php $total += $sessionCart['qty'] * $priceNow @endphp
                                                               @else
                                                               Rp. {{ number_format($sessionCart['qty'] * $sessionCart['item']->sell_price,0, ',','.') }}
                                                               @php $total += $sessionCart['qty'] * $sessionCart['item']->sell_price @endphp
                                                               @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    <tr class="total">
                                                        <td><strong>Subtotal</strong></td>
                                                        <!-- <td>Rp. {{ number_format(session()->get('S_iP')->totalPrice,0, ',','.') }}</td> -->
                                                        <td>Rp. {{ number_format($total, 0, ',', '.') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Shipping</strong></td>
                                                        <td id="shipping-total">Rp. 0</td>
                                                    </tr>


                                                    @if(session()->get('S_iP')->voucher)
                                                        <tr>
                                                            <td><strong>Voucher</strong></td>
                                                            <td style="color: red;">
                                                                @if(session()->get('S_iP')->voucher->voucher_type == 'percent')
                                                                @php $totalVoucher = (session()->get('S_iP')->voucher->percent/100)*$total @endphp
                                                                - Rp. {{ number_format($totalVoucher, 0, ',', '.') }}
                                                                <br>
                                                                ({{session()->get('S_iP')->voucher->percent}} %)
                                                                @else
                                                                - Rp. {{ number_format(session()->get('S_iP')->finalVoucherNominal, 0, ',', '.') }}
                                                                @endif
                                                                <br>
                                                                Voucher Code : {{session()->get('S_iP')->voucher->voucher_code}}
                                                            </td>
                                                        </tr>
                                                    @endif

                                                    <tr class="total">
                                                        <td><strong>Total</strong></td>
                                                        <!-- <td id="cart-total">Rp. {{ number_format(session()->get('S_iP')->finalPrice, 0, ',', '.') }}</td> -->
                                                        @if(session()->get('S_iP')->voucher)
                                                        @if(session()->get('S_iP')->voucher->voucher_type == 'percent')
                                                        @php $totalVoucher = (session()->get('S_iP')->voucher->percent/100)*$total @endphp
                                                        <td>Rp. {{ number_format($total - $totalVoucher, 0, ',', '.') }}</td>
                                                        @else
                                                        <td>Rp. {{ number_format($total - (session()->get('S_iP')->finalVoucherNominal), 0, ',', '.') }}</td>
                                                        @endif
                                                        @else
                                                        <td>Rp. {{ number_format($total, 0, ',', '.') }}</td>
                                                        @endif
                                                    </tr>


                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    @if(session()->get('S_iP')->voucher)
                                    @if(session()->get('S_iP')->voucher->voucher_type == 'percent')
                                    @php $totalVoucher = (session()->get('S_iP')->voucher->percent/100)*$total @endphp
                                    <input type="hidden" name="total" value="{{ $total - $totalVoucher }}">
                                    <input type="hidden" name="voucher_nominal" value="{{ $totalVoucher }}">
                                    @else
                                    <input type="hidden" name="total" value="{{ $total - (session()->get('S_iP')->finalVoucherNominal) }}">
                                    <input type="hidden" name="voucher_nominal" value="{{ session()->get('S_iP')->finalVoucherNominal }}">
                                    @endif
                                    @else
                                    <input type="hidden" name="total" value="{{ $total }}">
                                    @endif
                                    <div class="ps-checkout__payment-menthod payment-option">
                                        @foreach($accounts as $account)
                                            <div class="ps-radio">
                                                <input class="form-control {{ $errors->has('payment-method')? 'border-danger' : '' }}" id="payment-method-{{ $account->id }}" name="payment-menthod" type="radio" value="{{ $account->id }}">
                                                <label for="payment-method-{{ $account->id }}">{{ $account->account_bank}}</label>
                                            </div>
                                        @endforeach
                                         <img onClick="event.preventDefault(); document.getElementById('process-checkout').submit();" style="cursor: pointer"  src="{{ asset('front/img/place_order.png') }}" style="width:150px"  />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
	<script src="{{ asset('js/blockui.min.js') }}"></script>
	<script>
		$(function(){

            var numberFormat = (x) => { return 'Rp. ' + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }

			var selectProvince = $('#province');
			var selectCity = $('#city');
			var selectSubdistrict = $('#subdistrict');
			var shippingTotal = $('#shipping-total');
			var cartTotal = $('#cart-total');

			selectProvince.on('change', function(e){

				$.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

				var value = $(this).val();

				$.get("{{ url('city') }}" + "/" + value).done(function(response){

					selectCity.empty();
					selectSubdistrict.empty();

					selectCity.append($("<option></option>").attr("value", "").text("Choose City"));

					$.each(response, function(key, item) {
					     selectCity
					         .append($("<option></option>")
					         .attr("value", item.value)
					         .text(item.type + ' ' + item.label));
					});

					$.unblockUI();
				})

			});

			selectCity.on('change', function(e){

				$.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

				var value = $(this).val();

				$.get("{{ url('subdistrict') }}" + "/" + value).done(function(response){

					selectSubdistrict.empty();

					selectSubdistrict.append($("<option></option>").attr("value", "").text("Choose Subdistrict"));

					$.each(response, function(key, item) {
					     selectSubdistrict
					         .append($("<option></option>")
					         .attr("value", item.value)
					         .text(item.label));
					});

					$.unblockUI();

				})

			});

			selectSubdistrict.on('change', function(e){

				$.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

				var value = $(this).val();
                cost(value);
			});

            function cost(value){

				$.get("{{ url('cost') }}" + "/" + value + "/{{ session()->has('S_iP')? session()->get('S_iP')->totalWeight : 0 }}").done(function(response){

                    if(Object.keys(response).length) {

                        shippingTotal.text(numberFormat(response.value) + " ( est. "+ response.etd +" days )");
                        cartTotal.text(
                            numberFormat(
                                response.value + parseInt({{session()->get('S_iP')->finalPrice}})
                            )
                        );

                    }else {

                        shippingTotal.text(numberFormat(0));

                        shippingTotal.text(numberFormat(response.value) + " ( est. "+ response.etd +" days )");
                        cartTotal.text(
                            numberFormat(
                                response.value + parseInt({{session()->get('S_iP')->finalPrice}})
                            )
                        );

                    }

                    $.unblockUI();
                    })

            }

            $('.change-address').on('click',function(e){
                $('.select-address').show();
                $(this).hide();
            });


            $('.save-address').on('click',function(e){
                var select = $('[name="customer_address_id"] option:selected').html();
                var value = $('[name="customer_address_id"] option:selected').data('subdistrict-id');
                cost(value);

                var data = select.split('/');

                $('[data-address="phonenumber"]').html(data[0]);
                $('[data-address="address"]').html(data[1]);
                $('[data-address="province"]').html(data[2]);
                $('[data-address="city"]').html(data[3]);
                $('[data-address="subdistrict"]').html(data[4]);
                $('[data-address="subpostal_codedistrict"]').html(data[5]);

                $('.select-address').hide();
                $('.change-address').show();
            });

            var value = $('[name="customer_address_id"] option:selected').data('subdistrict-id');
            cost(value);
		});
	</script>
@endpush
