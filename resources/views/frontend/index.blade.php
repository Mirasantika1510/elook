@extends('frontend.layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('front/css/owl.carousel.css') }}">
    <style>

        .container img.img-wrap {
            width: 100%;
            object-fit: contain;
            object-position:center;
        }

        @media (min-width: 992px) {

            .container img.img-wrap {
                width:100%;
            }
        }

        .ps-our-products .ps-section__content .ps-tab-list li.active a,
        .ps-our-products .ps-section__content .ps-tab-list li a:hover,
        .ps-our-products .ps-section__content .ps-tab-list li a {
            border:none !important;
        }


    </style>
@endpush

@section('content')
    <div id="homepage-2">
		<div class="ps-home-banner">
			<div class="ps-carousel--animate ps-carousel--1st">
                @forelse($sliders as $slider)
                    <div class="item">
                        <div class="ps-banner">
                            <div class="container" style="max-width: 100%;padding: 0px;">
                                @if($slider->link_url)
                                    <a href="{{ $slider->link_url }}" target="_blank">
                                        <img alt="" src="{{ $slider->image }}" class="img-wrap">
                                    </a>
                                @else
                                    <img alt="" src="{{ $slider->image }}" class="img-wrap">
                                @endif
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="item">
                        <div class="ps-banner center-1">
                            <div class="container">
                                <img alt="" src="{{ asset('front/img/home-banner/home-1/3.png') }}">
                                <div class="ps-banner__content">
                                    <h4>ECOMMERCE</h4>
                                    <h1>ELOOK OFFICIAL</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforelse
			</div>
		</div>
		<div class="ps-lookbook">
			<div class="container">
				<div class="masonry-wrapper" data-col-lg="4" data-col-md="4" data-col-sm="2" data-col-xs="2" data-gap="30" data-radio="100%">
					<div class="ps-masonry">
                        <div class="grid-sizer"></div>
                        @foreach($categories as $category)
                            <div class="grid-item">
                                <div class="grid-item__content-wrapper">
                                    <div class="ps-block--lookbook">
                                        <a href="{{ url('shop') }}?categories={{$category->slug}}">
                                            <img alt="" src="{{ $category->image }}">
                                        </a>
                                        <a style="display:none;" href="{{ url('shop') }}?categories={{$category->slug}}">{{ $category->title }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
					</div>
				</div>
			</div>
		</div>
		<div class="ps-home-collection">
			<div class="container">
				<div class="row justify-content-md-center">
                    @if($latestLastMonthLookbook)
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="ps-block--collection ps-block--bottom">
                                <a href="{{ url('shop') }}?lookbook={{$latestLastMonthLookbook->slug}}"></a>
                                <img alt="" src="{{ $latestLastMonthLookbook->image }}">
                                <div class="ps-block__content">
                                    <p>View Collections</p>
                                    <h4>{{ $latestLastMonthLookbook->title }}</h4>
                                    <small>{!! $latestLastMonthLookbook->description !!}</small>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($lastLookbook)
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="ps-block--collection ps-block--bottom">
                                <a href="{{ url('shop') }}?lookbook={{$lastLookbook->slug}}"></a>
                                <img alt="" src="{{ $lastLookbook->image }}">
                                <div class="ps-block__content">
                                    <p>View Collections</p>
                                    <h4>{{ $lastLookbook->title }}</h4>
                                    <small>{!! $lastLookbook->description !!}</small>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($latestDiscount)
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="ps-block--collection ps-block--reverse">
                                <a href="{{ url('shop') }}?discount={{ $latestDiscount->slug }}"></a>
                                <img alt="" src="{{ $latestDiscount->image }}">
                                <div class="ps-block__content">
                                    <h4>{{ $latestDiscount->title }}</h4>
                                    <h2>{!! $latestDiscount->description !!}</h2>
                                    <a class="ps-btn ps-btn--outline ps-btn--black" href="{{ url('shop') }}?discount={{ $latestDiscount->slug }}">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    @endif
				</div>
			</div>
		</div>
		<div class="ps-section ps-our-products">
			<div class="container">
				<div class="ps-section__header">
                    <img alt="" src="{{ asset('front/img/our-products.png') }}" style="height: 75px;">
					<p>Top sale in this week</p>
				</div>
				<div class="ps-section__content ps-tab-root">
					<ul class="ps-tab-list">
						<li class="active">
							<a href="#tab-1" class="no-border">
                                <img  src="{{ asset('front/img/new.png') }}" style="width:150px"  >
                            </a>
						</li>
						<li>
							<a href="#tab-2">
                                <img  src="{{ asset('front/img/best_seller.png') }}" style="width:150px"  >
                            </a>
						</li>
						<li>
							<a href="#tab-3">
                                <img  src="{{ asset('front/img/sale.png') }}" style="width:150px"  >
                            </a>
						</li>
						<li>
							<a href="#tab-4">
                                <img  src="{{ asset('front/img/top_rate.png') }}" style="width:150px"  >
                            </a>
						</li>
                    </ul>
                    {{-- Best Seller --}}
					<div class="ps-tabs">
						<div class="ps-tab active" id="tab-1">
                            <div>
                                @php $no = $productsFeatured->count() @endphp
                                @for ($i=0; $i < $no; $i+=12)
                                <div class="row load" id="loadbest{{ $i }}">
                                    @foreach($productsFeatured->slice($i, 12) as $product)
                                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                                        <x-product :product="$product" context="featured" />
                                    </div>
                                    @php $lastId = $product->id @endphp
                                    @endforeach
                                </div>
                                @endFor
                            </div>
                            <div class="ps-shopping__footer">
                                <img src="{{ asset('Group 30.png') }}" class="loadMore btn mx-auto d-block" style="width: 15%;" id="loadMore" onclick="loadMore()">
                            </div>
                        </div>
                        {{-- Featured --}}
						<div class="ps-tab" id="tab-2">
                            <div>
                                @php $noBestSell = $productsBestSell->count() @endphp
                                @for ($i=0; $i < $noBestSell; $i+=12)
                                <div class="row load" id="loadBestSell{{ $i }}">
                                    @foreach($productsBestSell->slice($i, 12) as $product)
                                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                                        <x-product :product="$product" context="best-seller" />
                                    </div>
                                    @endforeach
                                </div>
                                @endfor
                            </div>
                           <div class="ps-shopping__footer">
                                <img src="{{ asset('Group 30.png') }}" class="loadMore btn mx-auto d-block" style="width: 15%;" id="loadMoreBestSell" onclick="loadMoreBestSell()" style="width: 15%;">
                            </div>
                        </div>
                        {{-- Sale --}}
                        <div class="ps-tab" id="tab-3">
                            <div>
                                @php $Sale = $productsSale->count() @endphp
                                @for ($i=0; $i < $Sale; $i+=12)
                                <div class="row load" id="loadSale{{ $i }}">
                                    @foreach($productsSale as $product)
                                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                                        <x-product :product="$product" context="sale" />
                                    </div>
                                    @endforeach
                                </div>
                                @endfor
                            </div>
                            <div class="ps-shopping__footer">
                                <img src="{{ asset('Group 30.png') }}" class="loadMore btn mx-auto d-block" style="width: 15%;" id="loadMoreSale" onclick="loadSale()">
                            </div>
                        </div>
                        {{-- Top Rate --}}
                        <div class="ps-tab" id="tab-4">
                            <div>
                                @php $noTopRated = $productsTopRated->count() @endphp
                                @for ($i=0; $i < $noTopRated; $i+=12)
                                <div class="row load" id="loadMoreTopRated{{ $i }}">
                                    @foreach($productsTopRated->slice($i, 12) as $product)
                                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                                        <x-product :product="$product" context="top-rated" />
                                    </div>
                                    @endforeach
                                </div>
                                @endfor
                            </div>
                             <div class="ps-shopping__footer">
                                <img src="{{ asset('Group 30.png') }}" class="loadMore btn mx-auto d-block" style="width: 15%;" id="loadTopRated" onclick="loadMoreTopRated()">
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="ps-section ps-our-blogs">
			<div class="container">
				<div class="ps-section__header">
                    <img alt="" src="{{ asset('front/img/testimoni.png') }}" style="height: 75px;">
					<p>The freshest and most exciting</p>
				</div>
				<div class="ps-section__content">
					<div class="row">
                        @forelse($testimonials as $testimoni)
                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                                <div class="ps-post">
                                    <div class="ps-post__thumbnail">
                                        <a class="ps-post__overlay" href="#"></a>
                                        <img alt="" src="{{ $testimoni->image }}" style="height: 200px;object-fit: cover;object-position: center;">
                                    </div>
                                    <div class="ps-post__content">
                                        <a class="ps-post__title" href="#">{{ $testimoni->title }}</a>
                                        <div class="ps-post__meta">
                                            <span>
                                                By <a href="#">{{ $testimoni->author }}</a>
                                                on <strong>{{ $testimoni->created_at->format('F d, Y') }}</strong>
                                            </span>
                                        </div>
                                        <p>{!! $testimoni->description !!}</p>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="text-align:center;">
                                NO DATA AVAILABLE
                            </div>
                        @endforelse
					</div>
				</div>
			</div>
		</div>

		<div class="ps-section ps-instagram" style="margin-bottom:250px;">
			<div class="container">
				<div class="ps-section__header">
					<a href="https://www.instagram.com/e.look/?hl=id">
                        <img alt="" src="{{ asset('front/img/whats-on-our-instagram.png') }}" style="height: 75px;">
                    </a><!-- <h3>@ FOLLOW US ON INSTAGRAM</h3> -->
				</div>
			</div>
            <div class="ps-carousel--nav inside owl-slider"
                data-owl-auto="true"
                data-owl-dots="true"
                data-owl-duration="1000"
                data-owl-gap="0"
                data-owl-item="6"
                data-owl-item-lg="8"
                data-owl-item-md="4"
                data-owl-item-sm="3"
                data-owl-item-xs="2"
                data-owl-loop="true"
                data-owl-mousedrag="on"
                data-owl-nav="true"
                data-owl-speed="100000"
            >
                @if(isset($instagrams->data))
                    @foreach($instagrams->data as $instagram)
                        <div class="ps-block--instagram">
                            <a href="{{ $instagram->link }}" target="_blank">
                                <img alt="" src="{{ $instagram->images->low_resolution->url }}">
                            </a>
                            <div class="ps-block__action">
                                <a href="#">
                                    <i class="fa fa-heart-o"></i>{{ $instagram->likes->count }}
                                </a>
                                <a href="#">
                                    <i class="fa fa-comments"></i>{{ $instagram->comments->count }}
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
			</div>
		</div>

		<div class="ps-site-features" style="">
			<div class="container">
				<div class="ps-block--features">
					<div class="row ps-col-tiny">
						<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
							<div class="ps-block--feature">
								<div class="ps-block__left">
									<img src="{{asset('front/img/free_shipping.png')}}" style="width:40px;">
								</div>
								<div class="ps-block__right">
									<p>loFast shipping</p><small>We choose the best courrier to deliver your products ♡</small>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
							<div class="ps-block--feature">
								<div class="ps-block__left">
									<img src="{{asset('front/img/support.png')}}" style="width:40px;">
								</div>
								<div class="ps-block__right">
									<p>Customer service support</p><small>klick the whatsapp button then we're ready to assist you! :)</small>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
							<div class="ps-block--feature">
								<div class="ps-block__left">
									<img src="{{asset('front/img/day_return.png')}}" style="width:40px;">
								</div>
								<div class="ps-block__right">
									<p>Shop 24/7</p><small>Easy shopping 24 hours in 7 days a week </small>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
							<div class="ps-block--feature">
								<div class="ps-block__left">
									<img src="{{asset('front/img/payment.png')}}" style="width:40px;">
								</div>
								<div class="ps-block__right">
									<p>Affordable price and product warranty</p><small>You'll love our products! ♡
                                    7 days warranty for any product defect</small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


        <div class="ps-popup" id="pop-up-index" data-time="1000">
            <div class="ps-popup__content bg--cover">

                <button type="button" class="ps-popup__close" ></button>
                <div class="ps-form__content">
                    <img src="{{ $companies->pop_up_image }}">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="0" id="count"> 
    <input type="hidden" value="0" id="countBestSell"> 
    <input type="hidden" value="0" id="countSale"> 
    <input type="hidden" value="0" id="countTop"> 
    
    <script>
        $(function () {
            $('#loadTopRated').removeClass('d-block');
            $('#loadTopRated').addClass('d-none');
            $('#loadMore').removeClass('d-block');
            $('#loadMore').addClass('d-none');
            $('#loadMoreBestSell').removeClass('d-block');
            $('#loadMoreBestSell').addClass('d-none');
            $('#loadMoreSale').removeClass('d-block');
            $('#loadMoreSale').addClass('d-none');
        });

    </script>

    @php $no = $productsFeatured->count() @endphp
    @for ($i=0; $i < $no; $i+=12)
    @php $button = $i+12 @endphp
    <script>
        $(function () {
            var value = '{{ $button }}';
            $('.load-more').hide();
            $('#loadbest{{ $i }}').hide();
            $('#loadbest0').show();
            var total = '{{ $productsFeatured->count() }}';
            if (12 <  parseInt(total)) {
                $('#loadMore').removeClass('d-none');
                $('#loadMore').addClass('d-block');
            }

        });

        function loadMore() {
            var no = $('#count').val();
            var count = parseInt(no) + parseInt(12);
           $('#loadbest'+count).show();
           $('#count').val(count);
           var total = '{{ $productsFeatured->count() }}';
           if (count+12 > total) {
                $('#loadMore').removeClass('d-block');
                $('#loadMore').addClass('d-none');
           }
        }

    </script>
    @endfor

    @php $noTop = $productsTopRated->count() @endphp
    @for ($i=0; $i < $noTop; $i+=12)
    @php $button = $i+12 @endphp
    <script>
        $(function () {
            var value = '{{ $button }}';
            $('#loadMoreTopRated{{ $i }}').hide();
            $('#loadMoreTopRated0').show();
            var total = '{{ $productsTopRated->count() }}';
            if (12 <  parseInt(total)) {
                $('#loadTopRated').removeClass('d-none');
                $('#loadTopRated').addClass('d-block');
            }

        });

        function loadMoreTopRated() {
            var no = $('#countTop').val();
            var count = parseInt(no) + parseInt(12);
           $('#loadMoreTopRated'+count).show();
           $('#countTop').val(count);
           var total = '{{ $productsTopRated->count() }}';
           if (count+12 > total) {
                $('#loadTopRated').removeClass('d-block');
                $('#loadTopRated').addClass('d-none');
           }
        }

    </script>
    @endfor

    @php $noBestSell = $productsBestSell->count() @endphp
    @for ($i=0; $i < $noBestSell; $i+=12)
    @php $buttonBestSell = $i+12 @endphp
    <script>
        $(function () {
            var value = '{{ $buttonBestSell }}';
            $('#loadBestSell{{ $i }}').hide();
            $('#loadBestSell0').show();
            var total = '{{ $noBestSell }}';
            if (12 <  parseInt(total)) {
                $('#loadMoreBestSell').removeClass('d-none');
                $('#loadMoreBestSell').addClass('d-block');
            }

        });

        function loadMoreBestSell() {
            var no = $('#countBestSell').val();
            var count = parseInt(no) + parseInt(12);
            $('#loadBestSell'+count).show();
            $('#countBestSell').val(count);
            var total = '{{ $noBestSell }}';
            if (count+12 > total) {
                $('#loadMoreBestSell').removeClass('d-block');
                $('#loadMoreBestSell').addClass('d-none');
            }
        }

    </script>
    @endfor

    @php $Sale = $productsSale->count() @endphp
    @for ($i=0; $i < $Sale; $i+=12)
    @php $buttonSale = $i+12 @endphp
    <script>
        $(function () {
            var value = '{{ $buttonSale }}';
            $('#loadSale{{ $i }}').hide();
            $('#loadSale0').show();
            var total = '{{ $productsSale->count() }}';
            if (12 < parseInt(total)) {
                $('#loadMoreSale').removeClass('d-none');
                $('#loadMoreSale').addClass('d-block');
            }

        });

        function loadSale() {
            var no = $('#countSale').val();
            var count = parseInt(no) + parseInt(12);
           $('#loadSale'+count).show();
           $('#countSale').val(count);
           var total = '{{ $productsSale->count() }}';
           if (count+12 > total) {
                $('#loadMoreSale').removeClass('d-block');
                $('#loadMoreSale').addClass('d-none');
           }
        }

    </script>
    @endfor

@endsection
