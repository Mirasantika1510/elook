@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ $companies->contact_banner_image }}">
        <div class="container">
            <h1>{{ $companies->contact_title_page }}</h1>
        </div>
    </div>
    <div class="ps-page">
        <div class="ps-contact">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                        <form action="{{ url('messages') }}" id="contact" class="ps-form--contact" method="post">
                            @csrf()
                            <h3>Drop us a line</h3>

                            @if(session()->has('error-message') && $errors->any())

                                <div class="alert alert-danger">
                                    <ul style="margin-bottom: 0px;">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            
                            @endif

                            <div class="form-group">
                                <label>Your name (required)</label> 
                                <input class="form-control {{ $errors->has('name')? 'border-danger' : '' }}" placeholder="" type="text" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label>Your Email (required)</label> 
                                <input class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" placeholder="" type="email" name="email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <label>Subject</label> 
                                <input class="form-control {{ $errors->has('subject')? 'border-danger' : '' }}" placeholder="" type="text" name="subject" value="{{ old('subject') }}">
                            </div>
                            <div class="form-group">
                                <label>Your Message</label> 
                                <textarea class="form-control {{ $errors->has('message')? 'border-danger' : '' }}" rows="6" name="message">{{ old('message') }}</textarea>
                            </div>
                            <div class="form-group submit">
                                <img onClick="event.preventDefault(); document.getElementById('contact').submit();" style="cursor: pointer"  src="{{ asset('front/img/send.png') }}"  />
                            </div>
                        </form>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <h3>Contact Information</h3>
                        <p>We love to hear from you on our customer service, merchandise, website or any topics you want to share with us. Your comments and suggestions will be appreciated. Please complete the form below.</p>
                        <p><i class="fa fa-home"></i> {!! $companies->address !!}</p>
                        <p><i class="fa fa-phone"></i> {{ $companies->phone }}</p>
                        <p><i class="fa fa-envelope"></i> {{ $companies->email}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection