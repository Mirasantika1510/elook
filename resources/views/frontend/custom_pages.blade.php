@extends('frontend.layouts.master')

@section('content')
    <div class="ps-hero bg--cover" data-background="{{ $custom_page->banner }}">
        <div class="container">
            <h1>{{ $custom_page->title }}</h1>
            <p>{!! $custom_page->description !!}</p>
        </div>
    </div>
    <div class="ps-page ps-page--blog">
        <div class="ps-blog">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        {!! $custom_page->content !!}
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <aside class="widget widget_shop widget_instagram">
                            <h3 class="widget-title">Instagram Feed</h3>
                            @if(isset($instagrams->data))
                                <ul>
                                    @foreach($instagrams->data as $instagram)
                                        <li>
                                            <a href="{{ $instagram->link }}" target="_blank">
                                                <img alt="" src="{{ $instagram->images->low_resolution->url }}" style="width: 100%;height: 80px;object-fit: cover;object-position: center;">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection