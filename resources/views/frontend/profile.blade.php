@extends('frontend.layouts.master')

@push('style')
	<style>
		.border-danger {
			border: 1px solid red !important;
		}

	</style>
@endpush

@section('content')

	<div class="ps-hero bg--cover" data-background="{!! asset($banner->image) !!}">
		<div class="container">
			<h1>{{ $banner->title }}</h1>
		</div>
	</div>
	<div class="ps-page ps-page--blog">
		<div class="ps-my-account">
			
			
<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-12"><h3>Hi, {{ $customer->firstname}} {{$customer->lastname }}</h3></div>
    </div>
		<div class="row">
			<div class="col-sm-3">
				<div class="text-center">
				<form class="form" action="{{ url('update-image',$customer->id) }}" method="post" id="form_image" enctype="multipart/form-data">	
					{!! csrf_field().method_field("POST") !!}
					
					<div class="form-group">
						<div class="col-md-12">
						
							<img src="{{$customer->image}}" id="image" class="avatar img-circle img-thumbnail" alt="avatar">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<input type="file" name="image" class="file-upload">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-12">
							<button class="ps-btn ps-btn--outline ps-btn--black" type="submit">Save Image</button>
						</div>
					</div>
				</form>
				</div>

				<br>
			
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link @if ($message = Session::get('tabs')) @if($message == 'profile') active @endif @else active @endif" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
					<a class="nav-link @if ($message = Session::get('tabs')) @if($message == 'address') active @endif @endif" id="v-pills-address-tab" data-toggle="pill" href="#v-pills-address" role="tab" aria-controls="v-pills-address" aria-selected="false">Address</a>
					<a class="nav-link @if ($message = Session::get('tabs')) @if($message == 'password') active @endif @endif" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false">Reset Password</a>
				</div>
			
			</div>
			<div class="col-sm-9">

				@if($errors->any())

				<div class="alert alert-danger">
					<ul style="margin-bottom: 0px;">
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>

				@endif
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade @if ($message = Session::get('tabs')) @if($message == 'profile') show active @endif @else show active @endif" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
						
						<form class="form" action="{{ url('update-profile',$customer->id) }}" method="post" id="form_profile" >	
							{!! csrf_field().method_field("POST") !!}
							<div class="form-group">
								
								<div class="col-sm-12 col-lg-6">
									<label for="firstname">First Name <sup>*</sup></label>
									<input class="form-control {{ $errors->has('firstname')? 'border-danger' : '' }}" placeholder="Input your first name here" name="firstname" type="text" value="{{ old('firstname')?:$customer->firstname }}">
                            	</div>
							</div>
							<div class="form-group">
								
								<div class="col-sm-12 col-lg-6">
								<label for="lastname">Last Name <sup>*</sup></label>
									<input class="form-control {{ $errors->has('lastname')? 'border-danger' : '' }}" placeholder="Input your last name here" name="lastname" type="text" value="{{ old('lastname')?: $customer->lastname }}">
                            	</div>
							</div>
							<div class="form-group">
								
								<div class="col-sm-12 col-lg-6">
									<label for="dob">Date of birth <sup>*</sup></label>
									<input type="date" class="form-control {{ $errors->has('dob')? 'border-danger' : '' }}" name="dob" id="dob" placeholder="enter date of birth" title="enter your phone number if any." value="{{ old('dob')?: $customer->dob }}">
								</div>
							</div>
							<div class="form-group">
								
								<div class="col-sm-12 col-lg-6">
									<label for="email">Email <sup>*</sup></label>
									<input type="email" class="form-control {{ $errors->has('email')? 'border-danger' : '' }}" name="email" id="email" placeholder="you@email.com" title="enter your email." value="{{ old('email')?: $customer->email }}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<button class="ps-btn ps-btn--outline ps-btn--black" type="submit">Save Profile</button>
								</div>
							</div>
						</form>

					</div>
					
					<div class="tab-pane fade @if ($message = Session::get('tabs')) @if($message == 'address') show active @endif @endif" id="v-pills-address" role="tabpanel" aria-labelledby="v-pills-address-tab">
						<div class="row">
							<div class="col-md-12">
								<button type="button" class="ps-btn ps-btn--outline ps-btn--black add-new-address">
									+ Add New Address
								</button>
							</div>
						</div>
						<br>

						<?php $limit = 3; $counter = 1; $number = 1;?>
					
						<div class="row">
						@foreach($customer->allAddress as $address)
							@if($counter == 1)
							@endif
							<div class="col-md-4">
								<form action="{{url('remove-address/'.$address->id)}}" method="post">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title">Address {{$number}}</h5>
											<h6 class="card-subtitle mb-2 text-muted">Phone: {{$address->phonenumber}}</h6>
											<p class="card-text">
												<p>{{$address->address}}</p>
												<p><b>{{ $address->getprovince->province }}<br>
												{{$address->getcity->type}} {{$address->getcity->city_name}}<br>
												{{$address->getsubdistrict->subdistrict_name}}<br>
												{{$address->getcity->postal_code}}</b></p>
											</p>
											<a href="#" data-address='{{$address}}' class="card-link update-address btn btn-dark">Update</a>
											{!! csrf_field().method_field("DELETE") !!}
											<button type="submit" class="card-link btn btn-danger">Delete</button>
										</div>
									</div>
								</form>
							</div>
							@if($counter === $limit)
								<?php $counter = 1; ?>
								</div>
								<div class="row">
							@else
								<?php $counter++; ?>
							@endif
								<?php $number++; ?>
						@endforeach
						<?php $counter++; ?>
						</div>

					</div>

					<div class="tab-pane fade @if ($message = Session::get('tabs')) @if($message == 'password') show active @endif @endif" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
					
						<form class="form" action="{{ url('update-password',$customer->id) }}" method="post" id="form_password">	
							{!! csrf_field().method_field("POST") !!}
							<div class="form-group">
								<div class="col-sm-12 col-lg-6">
									<label>Password <sup>*</sup></label>
									<input class="form-control {{ $errors->has('password')? 'border-danger' : '' }}" placeholder="Input your password here" name="password" type="password">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12 col-lg-6">
									<label>Re-type Password <sup>*</sup></label>
									<input class="form-control {{ $errors->has('password_confirmation')? 'border-danger' : '' }}" placeholder="Input your password confirmation here" name="password_confirmation" type="password">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-12">
									<button class="ps-btn ps-btn--outline ps-btn--black" type="submit">Save Password</button>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div><!--/col-9-->
		</div><!--/row-->
				                                  

		</div>
	</div>
	
	<!-- Add Address Modal -->
	<div class="modal fade" id="addAddressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable" role="document" style="max-width:70%">
			<div class="modal-content">
			
			<form class="form" action="{{ url('add-address') }}" method="post" id="form_add_address">	
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Address</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
						{!! csrf_field().method_field("POST") !!}
						<input type="hidden" class="form-control" name="id" value="">
							
						<div class="form-group row">
							<label for="address" class="col-md-2 col-form-label">Phone Number</label>
							<div class="col-md-10">
								<input type="text" class="form-control {{ $errors->has('phonenumber')? 'border-danger' : '' }}" name="phonenumber" id="phonenumber" placeholder="" title="enter your phonenumber if any." value="{{ old('phonenumber') }}">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="province" class="col-md-2 col-form-label">Province</label>
							<div class="col-md-10">
								<select  data-default=""  class="form-control {{ $errors->has('province')? 'border-danger' : '' }}" name="province" id="province">
									<option value="">Choose Province</option>
									@foreach($provinces as $province)
										<option value="{{ $province->province_id }}">{{ $province->province }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label for="city" class="col-md-2 col-form-label">City</label>
							<div class="col-md-10">
								<select data-default="" class="form-control {{ $errors->has('city')? 'border-danger' : '' }}" name="city" id="city"></select>
							</div>
						</div>

						<div class="form-group row">
							<label for="subdistrict" class="col-md-2 col-form-label">Subdistrict</label>
							<div class="col-md-10">
								<select data-default="" class="form-control {{ $errors->has('subdistrict')? 'border-danger' : '' }}" name="subdistrict" id="subdistrict"></select>
							</div>
						</div>

						<div class="form-group row">
							<label for="address" class="col-md-2 col-form-label">Address</label>
							<div class="col-md-10">
								<textarea class="form-control {{ $errors->has('address')? 'border-danger' : '' }}" name="address">{{ old('address')}}</textarea>
							</div>
						</div>

						<div class="form-group row">
							<label for="active" class="col-md-2 col-form-label">&nbsp;</label>
							<div class="col-md-10">
								<input type="checkbox" name="active" value="1"  class="" id="active">
								<label class="" for="active"> Set as the main address </label>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="ps-btn ps-btn--black" data-dismiss="modal">Close</button>
					<button type="submit" class="ps-btn ps-btn--outline ps-btn--black">Save</button>
				</div>
			</form>
			</div>
		</div>
	</div>
	
@endsection

@push('scripts')
	<script src="{{ asset('js/blockui.min.js') }}"></script>
	<script>
		$(function(){

			var selectProvince = $('#province');
			var selectCity = $('#city');
			var selectSubdistrict = $('#subdistrict');

			selectProvince.on('change', function(e){

				$.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

				var value = $(this).val();

				$.get("{{ url('city') }}" + "/" + value).done(function(response){

					selectCity.empty();
					selectSubdistrict.empty();

					selectCity.append($("<option></option>").attr("value", "").text("Choose City"));

					$.each(response, function(key, item) {
					     selectCity
					         .append($("<option></option>")
					         .attr("value", item.value)
					         .text(item.type + ' ' + item.label));
					});
					var z = selectCity.data('default');
					
					if(z){
						selectCity.val(z).trigger('change');
					}

					$.unblockUI();
				})

			});

			selectCity.on('change', function(e){

				$.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

				var value = $(this).val();

				$.get("{{ url('subdistrict') }}" + "/" + value).done(function(response){

					selectSubdistrict.empty();

					selectSubdistrict.append($("<option></option>").attr("value", "").text("Choose Subdistrict"));

					$.each(response, function(key, item) {
					     selectSubdistrict
					         .append($("<option></option>")
					         .attr("value", item.value)
					         .text(item.label));
					});

					var z = selectSubdistrict.data('default');
					
					if(z){
						selectSubdistrict.val(z).trigger('change');
					}

					$.unblockUI();

				})

			});

			$('.update-address').on("click",function(e){
				var address = $(this).data('address');
				$('#form_add_address').attr('action','update-address/'+address.id);
				$('#form_add_address input[name="phonenumber"]').val(address.phonenumber);
				$('#form_add_address select[name="province"]').val(address.province);
				$('#form_add_address select[name="city"]').data('default',address.city);
				$('#form_add_address select[name="subdistrict"]').data('default',address.subdistrict);
				$('#form_add_address [name="address"]').val(address.address);
				if(address.active != 0){
					$('#form_add_address [name="active"]').prop('checked',true);
				}
				$('#addAddressModal').modal();
				$('#form_add_address select[name="province"]').trigger('change');
			});

			$('.add-new-address').on("click",function(e){
				$('#form_add_address').trigger("reset");
				$('#form_add_address').attr('action','add-address');
				$('#addAddressModal').modal();
			});

			$('.delete-address').on('click',function(e){
				
				var id = $(this).data('id');
				$.get("{{ url('subdistrict') }}" + "/" + id).done(function(response){
					selectSubdistrict.empty();

					selectSubdistrict.append($("<option></option>").attr("value", "").text("Choose Subdistrict"));

					$.each(response, function(key, item) {
						selectSubdistrict
							.append($("<option></option>")
							.attr("value", item.value)
							.text(item.label));
					});

					var z = selectSubdistrict.data('default');

					if(z){
						selectSubdistrict.val(z).trigger('change');
					}

					$.unblockUI();
				})
				
			});
		});
	</script>
@endpush
