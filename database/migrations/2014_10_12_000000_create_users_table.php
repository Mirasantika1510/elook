<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('username');
            $table->string('email')->nullable();
            $table->string('password');
            $table->boolean('suspended')->default(false);

            $table->rememberToken();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->date('dob');
            $table->string('password');
            $table->string('image');
            $table->boolean('suspended')->default(false);
            $table->boolean('activated')->default(false);

            $table->rememberToken();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('customers_address', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('customer_id');
            $table->string('province');
            $table->string('city');
            $table->string('subdistrict');
            $table->string('phonenumber');
            $table->text('address');
            $table->text('optional_address')->nullable();
            $table->boolean('active')->default(false);

            $table->rememberToken();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_address');
        Schema::dropIfExists('customers');
        Schema::dropIfExists('admins');
    }
}
