<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('slug');
            $table->string('voucher_code');
            $table->text('description')->nullable();
            $table->text('image')->nullable();
            $table->double('nominal')->default(0);
            $table->double('percent')->default(0);
            $table->string('voucher_type')->default('nominal');
            $table->string('avaliable_for')->default('all');
            $table->boolean('published')->default(false);
            $table->timestamp('time_start')->nullable();
            $table->timestamp('time_end')->nullable();
            $table->timestamps();
        });

        Schema::create('voucher_products', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('voucher_id');

            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->foreign('product_id')->references('id')->on('products');

            $table->primary(['voucher_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_products');
        Schema::dropIfExists('vouchers');
    }
}
