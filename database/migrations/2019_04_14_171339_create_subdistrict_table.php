<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubdistrictTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdistrict', function (Blueprint $table) {
            $table->unsignedInteger('subdistrict_id');

            $table->unsignedInteger('province_id');
            $table->string('province');
            $table->unsignedInteger('city_id');
            $table->string('city');
            $table->string('type');
            $table->string('subdistrict_name');
            $table->timestamps();

            $table->primary('subdistrict_id');
            $table->foreign('province_id')->references('province_id')->on('province');
            $table->foreign('city_id')->references('city_id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subdistrict');
    }
}
