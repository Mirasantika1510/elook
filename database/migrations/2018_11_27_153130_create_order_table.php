<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('customer_id')->nullable();
            $table->string("transaction_id", 100)->nullable();
            $table->string('invoice_number');
            $table->string('shipping_courier');
            $table->string('courier_service');
            $table->string('estimation_shipping');
            $table->double('shipping_price');
            $table->double('subtotal_price');
            $table->text('note')->nullable();
            $table->double('total_price');
            $table->double('unique_number')->default(0);
            $table->string('order_status')->default('ordered');
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers');
        });

        Schema::create('shipping_details', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('order_id');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('subdistrict')->nullable();
            $table->string('address')->nullable();
            $table->string('postcode')->nullable();
            $table->string('phonenumber')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
        });

        Schema::create('payment_details', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('order_id');
            $table->string('account_bank');
            $table->string('account_number');
            $table->string('account_name');
            $table->text('instructions');
            $table->char('transaction_status', 50)->default('pending');
            $table->string('confirmer_name')->nullable();
            $table->string('confirmer_email')->nullable();
            $table->string('confirmer_account_number')->nullable();
            $table->text('payment_receipt')->nullable();
            $table->timestamp('upload_receipt_time')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
        });

        Schema::create('orders_products', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('order_id');
            $table->unsignedInteger('product_id');
            $table->string('product_name');
            $table->boolean('product_ignore_stock');
            $table->enum('product_type', ['normal', 'agregator']);
            $table->double('product_discount')->nullable();
            $table->double('product_price')->default(0);
            $table->double('product_weight')->default(0);
            $table->text('product_image')->nullable();
            $table->double('quantities')->default(0);
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('orders_products_variants', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('orders_products_id');
            $table->integer('variant_id');
            $table->string('variant_name');
            $table->string('variant_price');
            $table->double('quantities')->default(0);
            $table->timestamps();

            $table->foreign('orders_products_id')->references('id')->on('orders_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_products_variants');
        Schema::dropIfExists('orders_products');
        Schema::dropIfExists('payment_details');
        Schema::dropIfExists('shipping_details');
        Schema::dropIfExists('orders');
    }
}
