<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->text('instagram_access_key')->nullable();
            $table->string('mini_notification_text')->nullable();
            $table->text('logo')->nullable();
            $table->text('pop_up_image')->nullable();
            $table->string('tracking_order_title_page');
            $table->text('tracking_order_banner_image');
            $table->string('confirmation_payment_title_page');
            $table->text('confirmation_payment_banner_image');
            $table->string('contact_title_page');
            $table->text('contact_banner_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
