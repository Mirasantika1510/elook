<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('discount_id')->nullable();
            $table->string('type');
            $table->string('title');
            $table->string('slug');
            $table->string('product_code')->nullable();
            $table->string('brand')->nullable();
            $table->text('specification');
            $table->text('description')->nullable();
            $table->text('additional_information')->nullable();
            $table->longText('long_description')->nullable();
            $table->double('price')->default(0);
            $table->double('sell_price')->default(0);
            $table->integer('stock')->default(0);
            $table->double('weight')->default(0)->comment('in grams');
            $table->integer('stock_used')->default(0);
            $table->double('length')->default(0)->comment('in cm');
            $table->double('width')->default(0)->comment('in cm');
            $table->double('height')->default(0)->comment('in cm');
            $table->json('related')->nullable();
            $table->datetime('active_start_date')->nullable();
            $table->datetime('active_end_date')->nullable();
            $table->string('seo_page_title')->nullable();
            $table->string('seo_meta_description')->nullable();
            $table->string('seo_meta_keywords')->nullable();
            $table->string('seo_page_slug')->nullable();
            $table->boolean('featured')->default(false);
            $table->boolean('best_seller')->default(false);
            $table->boolean('top_rated')->default(false);
            $table->boolean('new')->default(false);
            $table->boolean('allow_back_order')->default(false);
            $table->boolean('ignore_stock')->default(false);
            $table->boolean('published')->default(true);
            $table->integer('ordinal_number')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('discount_id')->references('id')->on('discounts');
        });

        Schema::create('products_join_categories', function (Blueprint $table) {
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('product_id');

            $table->foreign('category_id')->references('id')->on('product_categories');
            $table->foreign('product_id')->references('id')->on('products');

            $table->primary(['category_id', 'product_id']);
        });

        Schema::create('products_variants', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('product_id');

            $table->double('price')->default(0);
            $table->double('stock')->default(0);
            $table->double('stock_used')->default(0);

            $table->foreign('product_id')->references('id')->on('products');

            $table->boolean('published')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('products_variants');
        Schema::dropIfExists('products_join_categories');
        Schema::dropIfExists('products');

        Schema::enableForeignKeyConstraints();
    }
}
