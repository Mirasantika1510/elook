<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SocialMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();

        factory(App\Model\SocialMedia::class, 1)->create([
            'title'         => 'Facebook',
            'link'          => 'https://www.facebook.com/ELookOutfit/',
            'vendor'        => 'facebook',
        ]);

        factory(App\Model\SocialMedia::class, 1)->create([
            'title'         => 'Instagram',
            'link'          => 'https://www.instagram.com/e.look',
            'vendor'        => 'instagram',
        ]);
    }

    protected function truncateTable()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('social_media')->truncate();

        Schema::enableForeignKeyConstraints();
    }
}
