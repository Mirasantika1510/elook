<?php

use Illuminate\Http\File;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();

        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/testimoni1.jpg')));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        factory(App\Model\Testimonial::class, 1)->create([
            'title'         => 'savitrimustikadewi',
            'description'   => 'Bahannya bagus, modelnya lucu. Sukaaaa❤️',
            'author'        => 'savitrimustikadewi',
            'image'         => $fileName,
        ]);

        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/testimoni2.jpg')));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        factory(App\Model\Testimonial::class, 1)->create([
            'title'         => 'trijayantiputri',
            'description'   => 'Cantik banget. Bahannya bagus. Packagingnya juga cantik',
            'author'        => 'trijayantiputri',
            'image'         => $fileName,
        ]);

        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/testimoni3.jpg')));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/testimonials/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/testimonials/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        factory(App\Model\Testimonial::class, 1)->create([
            'title'         => 'ulfaaw',
            'description'   => 'Bagussss cepepttggg pengirimannya tapi teryata bahannya katun :)))))',
            'author'        => 'ulfaaw',
            'image'         => $fileName,
        ]);
    }

    protected function truncateTable()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('testimonials')->truncate();

        Schema::enableForeignKeyConstraints();
    }
}
