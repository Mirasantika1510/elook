<?php

use Illuminate\Http\File;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();

        $imageOriginalBanner  = Image::make(new File(resource_path('assets/frontend/img/hero/shop.jpg')));

        $fileNameBanner       = str_random(40);

        Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
        Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

        $imageName      = str_random(40);
        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/outer.jpg')));

        Storage::put("images/product_categories/$imageName.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_categories/$imageName", $imageOriginal->encode('webp')->__toString(), 'public');

        factory(App\Model\ProductCategories::class, 1)->create([
            'title'         => 'Outer',
            'slug'          => str_slug('Outer'),
            'description'   => 'Oceana Leaf Kimono',
            'image'         => $imageName,
            'banner'        => $fileNameBanner
        ]);
        
        $imageName      = str_random(40);
        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/inner.jpg')));

        Storage::put("images/product_categories/$imageName.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_categories/$imageName", $imageOriginal->encode('webp')->__toString(), 'public');

        factory(App\Model\ProductCategories::class, 1)->create([
            'title'         => 'Inner',
            'slug'          => str_slug('Inner'),
            'description'   => 'A Shape Inner',
            'image'         => $imageName,
            'banner'        => $fileNameBanner
        ]);

        $imageName      = str_random(40);
        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/dress.jpg')));

        Storage::put("images/product_categories/$imageName.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_categories/$imageName", $imageOriginal->encode('webp')->__toString(), 'public');

        factory(App\Model\ProductCategories::class, 1)->create([
            'title'         => 'Dress',
            'slug'          => str_slug('Dress'),
            'description'   => 'Kuna Kaftan Turquoise',
            'image'         => $imageName,
            'banner'        => $fileNameBanner
        ]);

        $imageName      = str_random(40);
        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/tank_top.jpg')));

        Storage::put("images/product_categories/$imageName.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_categories/$imageName", $imageOriginal->encode('webp')->__toString(), 'public');

        factory(App\Model\ProductCategories::class, 1)->create([
            'title'         => 'Tank Top',
            'slug'          => str_slug('Tank Top'),
            'description'   => 'Kayya Tank top',
            'image'         => $imageName,
            'banner'        => $fileNameBanner
        ]);
    }

    protected function truncateTable()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('product_categories')->truncate();

        Schema::enableForeignKeyConstraints();
    }
}
