<?php

use Illuminate\Http\File;
use Illuminate\Database\Seeder;
use App\Model\ProductCategories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ProductSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();

        // DRESS

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Kuna Kaftan Turquoise',
            'slug'          => str_slug('Kuna Kaftan Turquoise'),
            'specification' => 'All Size
                                P: 135cm
                                Bahan: Lady Zara ♡
                                (Bahannya jatuh, adem, ga nerawang. Nyaman bangett!)',
            'product_code'  => 'kkt135',
            'brand'         => 'E.look',
            'price'         => 100000,
            'sell_price'    => 189000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 135,
            'best_seller'   => true,
            'featured'      => true,
            'top_rated'     => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('dress')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/kuna-kaftan-turquoise.jpg')));

            $v->images()->create([
                'caption'  => 'kuna-kaftan-turquoise',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Kuna Kaftan Purple',
            'slug'          => str_slug('Kuna Kaftan Purple'),
            'specification' => 'All Size
                                P: 135cm
                                Bahan: Lady Zara ♡
                                (Bahannya jatuh, adem, ga nerawang. Nyaman bangett!)',
            'product_code'  => 'pp135',
            'brand'         => 'E.look',
            'price'         => 100000,
            'sell_price'    => 189000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 135,
            'best_seller'   => true,
            'featured'      => true,
            'top_rated'     => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('dress')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/kuna-kaftan-purple.jpg')));

            $v->images()->create([
                'caption'  => 'kuna-kaftan-purple',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Kuna Kaftan Ivory',
            'slug'          => str_slug('Kuna Kaftan Ivory'),
            'specification' => 'All Size
                                P: 135cm
                                Bahan: Lady Zara ♡
                                (Bahannya jatuh, adem, ga nerawang. Nyaman bangett!)',
            'product_code'  => 'qq135',
            'brand'         => 'E.look',
            'price'         => 100000,
            'sell_price'    => 189000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 135,
            'best_seller'   => true,
            'featured'      => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('dress')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/kuna-kaftan-ivory.jpg')));

            $v->images()->create([
                'caption'  => 'kuna-kaftan-ivory',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        // INNER

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Short Sleeve Inner Mauve',
            'slug'          => str_slug('Short Sleeve Inner Mauve'),
            'specification' => 'Rp 95.000
                                P: 60cm
                                Bahan: Crepe',
            'product_code'  => 'cc135',
            'brand'         => 'E.look',
            'price'         => 50000,
            'sell_price'    => 95000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 60,
            'featured'      => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('inner')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/short-sleeve-inner-mauve.jpg')));

            $v->images()->create([
                'caption'  => 'short-sleeve-inner-mauve',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Kayya Patern Shirt',
            'slug'          => str_slug('Kayya Patern Shirt'),
            'specification' => 'Rp. 199.000
                                Ld: 106 cm
                                P : 65 cm
                                Bahan: Cotton Rayon Eco Twill',
            'product_code'  => 'kp135',
            'brand'         => 'E.look',
            'price'         => 150000,
            'sell_price'    => 199000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 65,
            'featured'      => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('inner')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/kayya-patern-shirt.jpg')));

            $v->images()->create([
                'caption'  => 'kayya-patern-shirt',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Puca Pocket Berry',
            'slug'          => str_slug('Puca Pocket Berry'),
            'specification' => 'Rp. 199.000
                                Ld: 106 cm
                                P : 65 cm
                                Bahan: Cotton Rayon Eco Twill',
            'product_code'  => 'ppb135',
            'brand'         => 'E.look',
            'price'         => 150000,
            'sell_price'    => 199000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 65,
            'featured'      => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('inner')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/puca-pocket-berry.jpg')));

            $v->images()->create([
                'caption'  => 'puca-pocket-berry',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        // OUTER

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Oceana Leaf Kimono',
            'slug'          => str_slug('Oceana Leaf Kimono'),
            'specification' => 'Rp. 138.000
                                Ld: Free
                                P: 65cm
                                Bahan: Cotton Rayon
                                Short Sleeve Inner Mint
                                Rp 95.000
                                P: 60cm
                                Bahan: Crepe',
            'product_code'  => 'kki135',
            'brand'         => 'E.look',
            'price'         => 100000,
            'sell_price'    => 138000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 65,
            'featured'      => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('outer')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/oceana-leaf-kimono.jpg')));

            $v->images()->create([
                'caption'  => 'oceana-leaf-kimono',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Tropical in Blue Kimono',
            'slug'          => str_slug('Tropical in Blue Kimono'),
            'specification' => 'Rp. 138.000
                                Ld: Free
                                P: 65cm
                                Bahan: Cotton Rayon
                                Short Sleeve Inner Mint
                                Rp 95.000
                                P: 60cm
                                Bahan: Crepe',
            'price'         => 100000,
            'sell_price'    => 138000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 65,
            'best_seller'   => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('outer')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/tropical-in-blue-kimono.jpg')));

            $v->images()->create([
                'caption'  => 'tropical-in-blue-kimono',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Carla Mint Kimono 🌿',
            'slug'          => str_slug('Carla Mint Kimono 🌿'),
            'specification' => 'Rp. 138.000
                                Ld: Free
                                P: 65cm
                                Bahan: Cotton Rayon
                                Short Sleeve Inner Mint
                                Rp 95.000
                                P: 60cm
                                Bahan: Crepe',
            'price'         => 100000,
            'sell_price'    => 138000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 65,
            'featured'      => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('outer')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/carla-mint-kimono.jpg')));

            $v->images()->create([
                'caption'  => 'carla-mint-kimono',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });

        // TANK TOP

        factory(App\Model\Product::class, 1)->create([
            'type'          => 'normal',
            'title'         => 'Kayya Tank top',
            'slug'          => str_slug('Kayya Tank top'),
            'specification' => 'Rp. 125.000
                                Ld: 92 cm
                                P kanan: 48cm, P kiri 65cm',
            'price'         => 100000,
            'sell_price'    => 125000,
            'weight'        => 100,
            'stock'         => 10,
            'length'        => 65,
            'top_rated'     => true
        ])->each(function($v) {
            $categories = ProductCategories::whereSlug('outer')->firstOrFail();
            $v->categories()->attach($categories);

            $imageName      = str_random(40);
            $image          = Image::make(new File(resource_path('assets/frontend/img/elooks-images/products/kayya-tank-top.jpg')));

            $v->images()->create([
                'caption'  => 'kayya-tank-top',
                'filename' => $imageName
            ]);

            $this->handleUploadImage($image, $imageName, $v->id);
        });
    }

    protected function truncateTable()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('products_join_categories')->truncate();
        DB::table('product_images')->truncate();
        DB::table('products')->truncate();

        Schema::enableForeignKeyConstraints();
    }

    protected function handleUploadImage($image, $fileName, $id)
    {
        $imageOriginal = Image::make($image);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginal->resize(768, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-medium.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-medium", $imageOriginal->encode('webp')->__toString(), 'public');

        $imageOriginal->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-thumbnail.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/product_images/". $id ."/$fileName/$fileName" . "-thumbnail", $imageOriginal->encode('webp')->__toString(), 'public');
    }
}
