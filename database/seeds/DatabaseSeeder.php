<?php

use Illuminate\Http\File;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();
        factory(App\Model\Admin::class, 1)->create();

        Storage::putFileAs('images/companies', new File(resource_path('assets/frontend/img/logo.png')), 'logo.png', 'public');
        Storage::putFileAs('images/companies', new File(resource_path('assets/frontend/img/pop_up_image.jpg')), 'pop_up_image.jpg', 'public');
        Storage::putFileAs('images/banner', new File(resource_path('assets/frontend/img/hero/shop.jpg')), 'shop.jpg', 'public');

        factory(App\Model\Companies::class, 1)->create();

        $this->call(ProductCategoriesSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(LookbookSeeder::class);
        $this->call(SocialMediaSeeder::class);
        $this->call(TestimonialSeeder::class);
        $this->call(BannerSeeder::class);
    }

    protected function truncateTable()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('admins')->truncate();
        DB::table('companies')->truncate();

        Schema::enableForeignKeyConstraints();
    }
}
