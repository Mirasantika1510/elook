<?php

use Illuminate\Database\Seeder;
use App\Model\Banner;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\File;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imageOriginalBanner  = Image::make(new File(resource_path('assets/frontend/img/hero/shop.jpg')));

        $fileNameBanner       = str_random(40);

        Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
        Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/lookbook.jpg')));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/banner/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/banner/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/banner/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/banner/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');
         $data = [
            ['page'       => 'cart', 
            'title'       => 'CART', 
            'description' => '',
            'image'       => $fileName 
            ],

            ['page'       => 'shop', 
            'title'       => 'SHOP', 
            'description' => 'List all available products',
            'image'       => $fileName 
            ],

            ['page'       => 'profile', 
            'title'       => 'PROFILE', 
            'description' => '',
            'image'       => $fileName 
            ],

            ['page'       => 'my_account', 
            'title'       => 'MY ACCOUNT', 
            'description' => '',
            'image'       => $fileName 
            ],

            ['page'       => 'checkout', 
            'title'       => 'CHECKOUT', 
            'description' => '',
            'image'       => $fileName 
            ],
        ];

        foreach($data as $list){
            Banner::create($list);
        }
    }
}
