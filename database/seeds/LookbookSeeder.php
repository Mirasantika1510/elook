<?php

use App\Model\Product;
use Illuminate\Http\File;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class LookbookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();

        $imageOriginalBanner  = Image::make(new File(resource_path('assets/frontend/img/hero/shop.jpg')));

        $fileNameBanner       = str_random(40);

        Storage::put("images/banners/$fileNameBanner.jpg", $imageOriginalBanner->encode('jpg')->__toString(), 'public');
        Storage::put("images/banners/$fileNameBanner", $imageOriginalBanner->encode('webp')->__toString(), 'public');

        $imageOriginal  = Image::make(new File(resource_path('assets/frontend/img/elooks-images/lookbook.jpg')));

        $fileName       = str_random(40);

        $imageOriginal->resize(1366, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/lookbooks/$fileName/$fileName" . "-full.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/lookbooks/$fileName/$fileName" . "-full", $imageOriginal->encode('webp')->__toString(), 'public');


        $imageOriginal->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put("images/lookbooks/$fileName/$fileName" . "-large.jpg", $imageOriginal->encode('jpg')->__toString(), 'public');
        Storage::put("images/lookbooks/$fileName/$fileName" . "-large", $imageOriginal->encode('webp')->__toString(), 'public');

        $products       = Product::inRandomOrder()->take(5)->pluck('id')->toArray();

        factory(App\Model\Lookbook::class, 1)->create([
            'title'         => 'Lookbook',
            'slug'          => str_slug('Lookbook'),
            'description'   => 'My Lookbook',
            'image'         => $fileName,
            'products'      => $products,
            'banner'        => $fileNameBanner
        ]);
    }

    protected function truncateTable()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('lookbooks')->truncate();

        Schema::enableForeignKeyConstraints();
    }
}
