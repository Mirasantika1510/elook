<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Model\Admin::class, function (Faker $faker) {
    return [
        'name' => 'Elook Administrator',
        'username' => 'elook_administrator',
        'email' => 'owner@elook.com',
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Model\Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->username,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Model\Companies::class, function (Faker $faker) {
    return [
        'title' => 'ecommerce',
        'email' => 'elook@ecommerce.com',
        'phone' => '082220000559',
        'address' => '184 Main Rd E, St Albans VIC 3021, Australia',
        'instagram_access_key' => '2276050847.785bcfa.bea95ef9e0064c2c80e2de2891ae8bea',
        'logo'  => 'logo.png',
        'pop_up_image'  => 'pop_up_image.jpg',
        'mini_notification_text' => 'Summer sale discount off 50%! Shop Now',
        'tracking_order_title_page'  => 'Tracking Order',
        'tracking_order_banner_image'  => 'shop.jpg',
        'confirmation_payment_title_page'  => 'Confirmation Payment',
        'confirmation_payment_banner_image'  => 'shop.jpg',
        'contact_title_page'  => 'Contact',
        'contact_banner_image'  => 'shop.jpg',
    ];
});

$factory->define(App\Model\ProductCategories::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Model\Lookbook::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Model\Product::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Model\SocialMedia::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Model\Testimonial::class, function (Faker $faker) {
    return [];
});
