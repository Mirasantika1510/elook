<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEnd\FrontPageController@index');
Route::get('shop', 'FrontEnd\FrontPageController@shop');
Route::get('product-detail/{slug}', 'FrontEnd\FrontPageController@detailProduct');
Route::post('shop/add-to-cart/{key}', 'FrontEnd\ShopController@addToCart');
Route::get('shop/add-to-cart/{key}', 'FrontEnd\ShopController@addToCart');
Route::get('shop/delete-from-cart/{key}', 'FrontEnd\ShopController@deleteFromCart');
Route::get('cart', 'FrontEnd\FrontPageController@cart');
Route::post('update-cart', 'FrontEnd\ShopController@updateCart');

Route::get('province', 'FrontEnd\AddressController@province');
Route::get('city/{id}', 'FrontEnd\AddressController@city');
Route::get('subdistrict/{id}', 'FrontEnd\AddressController@subdistrict');

Route::get('province-id/{id}', 'FrontEnd\AddressController@provinceId');
Route::get('city-id/{provid}/{id}', 'FrontEnd\AddressController@cityId');
Route::get('subdistrict-id/{cityid}/{id}', 'FrontEnd\AddressController@subdistrictId');

Route::get('check-cost/{dest}/{weight}', 'FrontEnd\ShopController@checkCost');
Route::get('cost/{dest}/{weight}', 'FrontEnd\ShopController@cost');

Route::get('tracking-order', 'FrontEnd\FrontPageController@trackingPage');
Route::get('tracking-order-detail/{id}', 'FrontEnd\FrontPageController@trackingResult');
Route::get('confirmation-payment', 'FrontEnd\FrontPageController@viewConfirmationPage');
Route::post('confirmation-payment', 'FrontEnd\FrontPageController@storeConfirmation');
Route::get('contact', 'FrontEnd\FrontPageController@contactPage');
Route::post('contact', 'FrontEnd\FrontPageController@sendMessage');

Route::get('about-us', 'FrontEnd\FrontPageController@aboutUs');
Route::get('term-conditions', 'FrontEnd\FrontPageController@termCondition');
Route::get('return-exchanges', 'FrontEnd\FrontPageController@returnExchanges');
Route::get('size-guides', 'FrontEnd\FrontPageController@sizeGuides');
Route::post('subscribe', 'FrontEnd\SubscribeController@store');
Route::post('messages', 'FrontEnd\MessageController@store');

Route::get('search', 'FrontEnd\FrontPageController@searchProduct');

Route::middleware('auth')->group(function () {
    Route::get('profile', 'FrontEnd\ProfileController@index');
    Route::post('update-profile/{id}', 'FrontEnd\ProfileController@updateProfile');
    Route::post('add-address', 'FrontEnd\ProfileController@addAddress');
    Route::post('update-address/{id}', 'FrontEnd\ProfileController@updateAddress');
    Route::post('update-password/{id}', 'FrontEnd\ProfileController@updatePassword');
    Route::post('update-image/{id}', 'FrontEnd\ProfileController@updateImage');
    Route::delete('remove-address/{id}', 'FrontEnd\ProfileController@deleteAddress');

    Route::get('checkout', 'FrontEnd\FrontPageController@checkout');
    Route::get('payment-checkout/{inv}', 'FrontEnd\CheckoutController@paymentCheckout');
    Route::post('transaction-state/{inv}', 'FrontEnd\TransactionController@transactionState');
    Route::get('transaction-state-result/{inv}', 'FrontEnd\TransactionController@transactionStateResult');

    Route::get('wishlist', 'FrontEnd\WishlistController@listWishlist');
    Route::get('add-wishlist/{product}', 'FrontEnd\WishlistController@addWishlist');
    Route::delete('remove-wishlist/{wishlist}', 'FrontEnd\WishlistController@removeWishlist');
});

Route::post('add-voucher', 'FrontEnd\VoucherController@useVoucher');
Route::post('process-checkout', 'FrontEnd\CheckoutController@processCheckout');

Route::get('guest-checkout', 'FrontEnd\FrontPageController@checkout');
Route::get('transaction-status/{transaction_id}', 'FrontEnd\CheckoutController@viewTransactionStatus');
Route::post('order-information', 'FrontEnd\FrontPageController@findInformationOrder');

Auth::routes();

// Route::get('completed-state/{inv}', 'FrontEnd\TransactionController@completedState');
// Route::get('pending-state/{inv}', 'FrontEnd\TransactionController@pendingState');
// Route::get('failed-state/{inv}', 'FrontEnd\TransactionController@failedState');

Route::post('payment-notification', 'FrontEnd\MidtransResponseController@paymentNotification');
Route::post('payment-finish', 'FrontEnd\MidtransResponseController@paymentFinish');
Route::post('payment-unfinish', 'FrontEnd\MidtransResponseController@paymentUnfinish');
Route::post('payment-error', 'FrontEnd\MidtransResponseController@paymentError');

Route::middleware('guest:admin')->prefix(config('elook.admin_url'))->group(function () {
    Route::get('/', function () { return redirect(config('elook.admin_url') . '/login'); });

    Route::get('login', 'Admin\Auth\LoginController@showLoginForm');
    Route::post('login', 'Admin\Auth\LoginController@login');

    // Password Reset Routes...
    Route::get('password/reset', 'Admin\Auth\ForgotPasswordAdminController@showLinkRequestForm')->name('admin.password.request');
    Route::post('password/email', 'Admin\Auth\ForgotPasswordAdminController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordAdminController@showResetForm')->name('admin.password.reset');
    Route::post('password/reset', 'Admin\Auth\ResetPasswordAdminController@reset');
});

Route::middleware('auth:admin')->prefix(config('elook.admin_url'))->group(function () {
    Route::post('logout', 'Admin\Auth\LoginController@logout');

    Route::get('dashboard', 'Admin\DashboardController@index');

    Route::get('product-categories-tables', 'Admin\ProductCategoriesController@categoriesDatatables');
    Route::resource('product-categories', 'Admin\ProductCategoriesController');
    Route::get('product-categories/{product_categories}/highlight', 'Admin\ProductCategoriesController@highlight');

    Route::get('sliders-tables', 'Admin\SliderController@sliderDatatables');
    Route::resource('sliders', 'Admin\SliderController');

    Route::get('social-tables', 'Admin\SocialMediaController@socialDatatables');
    Route::resource('socials', 'Admin\SocialMediaController');

    Route::get('testimonials-tables', 'Admin\TestimonialController@testimonialDatatables');
    Route::resource('testimonials', 'Admin\TestimonialController');

    Route::get('lookbooks-tables', 'Admin\LookbookController@lookbookDatatables');
    Route::resource('lookbooks', 'Admin\LookbookController');

    Route::get('subscribes-tables', 'Admin\SubscribeController@subscribeDatatables');
    Route::get('subscribes', 'Admin\SubscribeController@index');

    Route::get('messages-tables', 'Admin\MessageController@messageDatatables');
    Route::get('messages', 'Admin\MessageController@index');

    Route::get('members-tables', 'Admin\MemberController@memberDatatables');
    Route::get('members', 'Admin\MemberController@index');
    Route::get('members-detail/{customer}', 'Admin\MemberController@detail');
    Route::get('members-detail-tables/{customer}', 'Admin\MemberController@detailDatatables');
    Route::get('members-export', 'Admin\MemberController@export');

    Route::get('guests-tables', 'Admin\GuestController@guestDatatables');
    Route::get('guests', 'Admin\GuestController@index');
    Route::get('guests-detail/{shipping}', 'Admin\GuestController@detail');
    Route::get('guests-detail-tables/{shipping}', 'Admin\GuestController@detailDatatables');
    Route::get('guests-export', 'Admin\GuestController@export');

    Route::get('custom-pages-tables', 'Admin\CustomPageController@customPageDatatables');
    Route::resource('custom-pages', 'Admin\CustomPageController');

    Route::get('discounts-tables', 'Admin\DiscountController@discountDatatables');
    Route::put('discounts-publish/{discount}', 'Admin\DiscountController@publishDiscount');
    Route::resource('discounts', 'Admin\DiscountController');

    Route::get('vouchers-tables', 'Admin\VoucherController@voucherDatatables');
    Route::put('vouchers-publish/{voucher}', 'Admin\VoucherController@publishVoucher');
    Route::resource('vouchers', 'Admin\VoucherController');

    Route::get('payment-accounts-tables', 'Admin\PaymentAccountController@accountDatatables');
    Route::resource('payment-accounts', 'Admin\PaymentAccountController');

    Route::get('orders-tables', 'Admin\OrderController@orderDatatables');
    Route::put('orders-accept-payment/{order}', 'Admin\OrderController@orderAcceptPayment');
    Route::put('orders-accept-all', 'Admin\OrderController@orderAcceptPaymentAll');
    Route::put('orders-status-all', 'Admin\OrderController@orderStatusAll');

    Route::get('orders', 'Admin\OrderController@index');
    Route::get('orders/{order}', 'Admin\OrderController@show');
    Route::get('orders-tracking/{order}', 'Admin\OrderController@showTracking');
    Route::get('tracking-tables/{order}', 'Admin\OrderController@trackingDatatables');
    Route::get('create-tracking/{order}', 'Admin\OrderController@createTracking');
    Route::post('store-tracking/{order}', 'Admin\OrderController@storeTracking');
    Route::get('edit-tracking/{tracking}', 'Admin\OrderController@editTracking');
    Route::put('update-tracking/{tracking}', 'Admin\OrderController@updateTracking');
    Route::delete('delete-tracking/{tracking}', 'Admin\OrderController@deleteTracking');

    Route::get('reports', 'Admin\OrderController@report');
    Route::get('export-excel', 'Admin\OrderController@exportExcel');

    Route::get('monthly-sells-orders', 'Admin\ReportController@monthlySellDatatables');
    Route::get('best-selling-products', 'Admin\ReportController@bestSellingProductsDatatables');
    Route::get('best-member', 'Admin\ReportController@bestMemberDatatables');
    Route::get('waiting-list', 'Admin\ReportController@waitingListDatatables');
    Route::get('abandoned-carts', 'Admin\ReportController@abandonedCartDatatables');
    Route::get('packing-list', 'Admin\ReportController@packingListDatatables');
    Route::get('inventory', 'Admin\ReportController@inventoryDatatables');

    Route::get('export-orders', 'Admin\OrderController@exportOrder');

    Route::get('companies-settings', 'Admin\SettingController@viewSettings');
    Route::put('companies-settings', 'Admin\SettingController@updateCompanySettings');
    Route::get('banner-settings', 'Admin\SettingController@viewSettings');
    Route::put('banner-settings', 'Admin\SettingController@updateBannerSettings');

    Route::get('product-tables', 'Admin\ProductController@productDatatables');
    Route::get('product-images/{key}', 'Admin\ProductController@productImages');
    Route::get('product-download-image/{id}', 'Admin\ProductController@productDownloadImage');
    Route::post('product-upload-image/{id}', 'Admin\ProductController@productUploadImage');
    Route::delete('product-delete-image/{id}', 'Admin\ProductController@productDeleteImage');
    Route::resource('products', 'Admin\ProductController');
    Route::get('sort-products', 'Admin\ProductController@sortProduct');
    Route::put('sort-products', 'Admin\ProductController@updateSortProduct');
    Route::put('products-publish/{product}', 'Admin\ProductController@publishProduct');
    Route::put('products-publish-all', 'Admin\ProductController@publishAllProduct');
    
    Route::get('image/{id}', 'Admin\ProductController@getData');
    Route::post('sorting/{id}', 'Admin\ProductController@sortingImage');
    Route::get('reviews-tables/{product}', 'Admin\ProductReviewController@reviewDatatables');
    Route::resource('reviews', 'Admin\ProductReviewController');

    Route::get('banners-tables', 'Admin\BannerController@bannerDatatables');
    Route::resource('banners', 'Admin\BannerController');

    Route::get('{anything}', function () { return redirect(config('elook.admin_url') . '/dashboard'); });
});

Route::get('{anything}', 'FrontEnd\CustomPageController@view');

